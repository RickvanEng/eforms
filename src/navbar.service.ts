import {Injectable} from '@angular/core';

@Injectable()
export class NavbarService {
  visible: boolean;
  submitSuccess: boolean;

  constructor() {
    this.visible = true;
    this.submitSuccess = false;
  }

  hide() {
    this.visible = false;
  }

  show() {
    this.visible = true;
  }

  navbarHide() {
    this.submitSuccess = true;
  }

}
