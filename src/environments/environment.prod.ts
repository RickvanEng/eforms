export const environment = {
  production: true,
  baseUrl: '//formulieren.haarlemmermeer.nl/',
  originUrl: '//formulieren.haarlemmermeer.nl/formulieren',
  name: 'prod'
};
