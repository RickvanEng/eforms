export const environment = {
    production: true,
    baseUrl: '//dev-formulieren.haarlemmermeer.nl/',
    originUrl: '//dev-formulieren.haarlemmermeer.nl/formulieren',
    name: 'dev'
};
