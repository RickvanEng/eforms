import {AbstractControl, AsyncValidatorFn, ValidationErrors} from '@angular/forms';
import {Observable} from 'rxjs';
import {TestAsyncService} from './testAsync.service';
import {map} from 'rxjs/operators';

export class TestAsyncValidator {
	static createValidator(zipcodeService: TestAsyncService): AsyncValidatorFn {
		return (control: AbstractControl): Observable<ValidationErrors> => {
			return zipcodeService.fakeHttp(control.value).pipe(
				map((result: boolean) => result ? null : {invalidAsync: true})
			);
		};
	}
}
