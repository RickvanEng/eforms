import {Component, Injector, OnInit} from '@angular/core';
import {registerLocaleData} from '@angular/common';
import localeNl from '@angular/common/locales/nl';
import {ScreenMessageService} from "../../projects/forms/services/screenMessage.service";
import {Digid2Service} from "../../projects/form-components/2.0/_presets/digid/digid2.service";

registerLocaleData(localeNl, 'nl');

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {


    constructor(private injector: Injector,
                private digidS: Digid2Service) {
    }

    ngOnInit(): void {
        // if (localStorage.getItem('timedOut')) {
        //     console.warn('User has timed out');
        //     localStorage.removeItem('timedOut');
        //     this.injector.get(ScreenMessageService).setError({
        //         type: 'danger',
        //         message: 'Uw sessie is verlopen. Laad de pagina opnieuw om door te gaan.'
        //     });
        // }
    }

}
