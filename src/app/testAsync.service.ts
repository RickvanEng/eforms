import {Injectable} from '@angular/core';
import {delay} from 'rxjs/operators';
import {Observable, of} from 'rxjs';
import {HttpClient} from '@angular/common/http';

@Injectable({
	providedIn: 'root'
})
export class TestAsyncService {
	private validZipcodes = ['00001', '00002', '00003', '00004'];
	
	constructor(private http: HttpClient) {
	
	}
	
	fakeHttp(value: string): Observable<any> {
		this.http.get('test').subscribe(() => {
			console.log('succes');
		}, () => {
			console.log('error')
			}
		);
		return of(this.validZipcodes.includes(value)).pipe(delay(1000));
	}
}
