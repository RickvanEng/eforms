import {Injectable, Injector} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';

@Injectable()
export class SessionInit {
	init(inj: Injector): Promise<any> {
		return inj.get(HttpClient).get(environment.baseUrl + 'eforms-api/controller/login', {observe: 'response'}).toPromise();
	}
}
