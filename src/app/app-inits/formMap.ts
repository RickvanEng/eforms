import {AgdRouting2} from "../../../projects/forms/lib2/aanbieden-goederen-diensten/agd.routing";
import {IRoutingConfigurator} from "../../../projects/forms/lib2/IRoutingConfigurator";

export const formMaps: Record<string, IRoutingConfigurator> = {
    'agd': new AgdRouting2(),
};