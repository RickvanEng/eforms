import {Injectable, Injector} from '@angular/core';
import {AgdRouting2} from '../../../projects/forms/lib2/aanbieden-goederen-diensten/agd.routing';
import {FormRouting2} from '../../../projects/form-components/2.0/0-routing/form-routing/formRouting';
import {VariableFormService} from '../../../projects/forms/services/genericFormService/variableFormService';
import {FormFactory} from '../../../projects/forms/services/genericFormService/formDataPair';

@Injectable()
export class FormInit {

    private formsToBuild: Record<string, any> = {
        'agd': AgdRouting2,
    };

    init(inj: Injector): Promise<void> {
        return new Promise<void>(resolve => {
            let forms: FormRouting2[] = [];

            this.formsToBuild.forEach(routing => {
                let newRouting: { formRouting: FormRouting2 } = new FormFactory().create(routing, inj) as { formRouting: FormRouting2 };
                forms.push(newRouting.formRouting);
            });

            VariableFormService.getInstance().routers = forms;

            resolve();
        })
    }
}
