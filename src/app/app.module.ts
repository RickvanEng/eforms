import {AppComponent} from './app.component';
import {APP_INITIALIZER, Injector, LOCALE_ID, NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {MAT_MOMENT_DATE_FORMATS, MomentDateAdapter} from '@angular/material-moment-adapter';
import {DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE} from '@angular/material/core';
import {TitleService} from 'src/title.service';
import {NavbarService} from 'src/navbar.service';
import {ComponentsModule} from '../../projects/form-components/1.0/src/components.module';
import {HTTP_INTERCEPTORS, HttpClientModule, HttpClientXsrfModule} from '@angular/common/http';
import {ErrorInterceptor} from 'projects/forms/services/interceptors/error.interceptor';
import {
    faAddressBook,
    faAlignLeft,
    faCheck,
    faClock,
    faEnvelope,
    faEuroSign,
    faExclamationTriangle,
    faExternalLinkAlt,
    faEye,
    faEyeSlash,
    faInfo,
    faLock,
    faMapMarkerAlt,
    faMinus,
    faPercent,
    faPhone,
    faPlus,
    faSearch,
    faTag,
    faTimes,
    faUser,
    faWifi,
} from '@fortawesome/free-solid-svg-icons';
import {FontAwesomeModule} from "@fortawesome/angular-fontawesome";
import {library} from '@fortawesome/fontawesome-svg-core';
import {LoaderService} from "../../projects/forms/services/interceptors/loader.service";
import {LoaderInterceptor} from "../../projects/forms/services/interceptors/loader.interceptor";
import {RedirectGuard} from "./RedirectGuard";
import {NgIdleKeepaliveModule} from "@ng-idle/keepalive";
import {ScreenMessageService} from "../../projects/forms/services/screenMessage.service";
import {ModalService} from "../../projects/forms/services/modalService";
import {ErrorService} from "../../projects/forms/services/error.service";
import {ZaaknummerService} from "../../projects/forms/services/zaaknummer.service";
import {ScreenMessageModule} from "../../projects/form-components/1.0/lib/screen-message/screen-message.module";
import {LoggingInterceptor} from '../../projects/forms/services/interceptors/logging.interceptor';
import {FormComponent} from '../../projects/form-components/2.0/1-form/form/form.component';
import {TabComponent} from '../../projects/form-components/2.0/2-tab/tab.component';
import {LoginPortalModule} from '../../projects/form-components/2.0/0-routing/form-routing/login-portal/login-portal.module';
import {LoginPortalComponent} from '../../projects/form-components/2.0/0-routing/form-routing/login-portal/login-portal.component';
import {HlmFormsModule} from "../../projects/forms/hlm-forms.module";
import {AuthGuardMaintenance} from 'projects/form-components/2.0/services/auth-guard.service';
import {ComponentExampleModule} from '../../projects/component-dash/component-example/component-example.module';
import {ComponentDashboardModule} from '../../projects/component-dash/component-dashboard/component-dashboard.module';
import {MaterialModule} from './material.module';
import {FormInit} from './app-inits/form.init';
import {FormRoutingComponent} from '../../projects/form-components/2.0/0-routing/form-routing/form-routing.component';
import {EnvironmentService} from '../../projects/form-components/2.0/services/environment.service';
import {OfflinePageComponent} from '../../projects/form-components/2.0/0-routing/form-routing/offline-page/offline-page.component';
import {DankScherm2Component} from '../../projects/form-components/2.0/1-form/dank-scherm/dank-scherm2.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {DankschermAuthguard} from '../../projects/form-components/2.0/1-form/dank-scherm/dankscherm.authguard';
import {FormFieldModule} from 'projects/form-components/2.0/5-fields/form-field/form-field.module';
import {LandingComponent} from "../../projects/form-components/2.0/landing/landing.component";
import {LandingModule} from "../../projects/form-components/2.0/landing/landing.module";

// export function initializeApp1(initService: FormInit, inj: Injector) {
//     return (): Promise<any> => {
//         return initService.init(inj);
//     };
// }

@NgModule({
    declarations: [
        AppComponent,
    ],
    imports: [
        MaterialModule,
        ComponentsModule,

        FormFieldModule,
        LandingModule,

        HlmFormsModule,
        ComponentDashboardModule,
        ComponentExampleModule,
        LoginPortalModule,
        ScreenMessageModule,
        HttpClientModule,
        HttpClientXsrfModule.withOptions({
            cookieName: 'XSRF-TOKEN',
            headerName: 'X-XSRF-Token',
        }),
        FontAwesomeModule,
        RouterModule.forRoot([
            {
                path: ':routing', component: LandingComponent, children: [
                    // {
                    //     path: 'login-portal',
                    //     component: LoginPortalComponent,
                    //     children: []
                    // },
                    // {
                    //     path: 'offline',
                    //     component: OfflinePageComponent,
                    //     children: []
                    // },
                    // {
                    //     path: 'dank-scherm',
                    //     component: DankScherm2Component,
                    //     canActivate: [DankschermAuthguard]
                    // },
                    // {
                    //     path: ':form',
                    //     component: FormComponent,
                    //     children: []
                    // },
                    // {
                    //     path: ':form/:tab',
                    //     component: FormComponent,
                    //     children: []
                    // },
                ]
            },
        ]),
        NgIdleKeepaliveModule.forRoot(),
        BrowserAnimationsModule,
    ],
    entryComponents: [
        TabComponent,
        FormComponent,
        LoginPortalComponent,

        FormRoutingComponent,
    ],
    providers: [
        TitleService,
        ScreenMessageService,
        NavbarService,
        LoaderService,
        RedirectGuard,
        ModalService,
        ErrorService,
        ZaaknummerService,
        AuthGuardMaintenance,
        FormInit,
        EnvironmentService,
        DankschermAuthguard,

        // {provide: APP_INITIALIZER, useFactory: initializeApp1, deps: [FormInit, Injector], multi: true},

        {provide: MAT_DATE_LOCALE, useValue: 'nl-NL'},
        {provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE]},
        {provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS},
        {provide: LOCALE_ID, useValue: 'nl-NL'},
        {provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true},
        {provide: HTTP_INTERCEPTORS, useClass: LoaderInterceptor, multi: true},
        {provide: HTTP_INTERCEPTORS, useClass: LoggingInterceptor, multi: true},
    ],
    exports: [],
    bootstrap: [AppComponent]

})

export class AppModule {
    constructor() {
        library.add(faEnvelope,
            faPhone,
            faClock,
            faTag,
            faMapMarkerAlt,
            faAlignLeft,
            faAddressBook,
            faEye,
            faEyeSlash,
            faLock,
            faUser,
            faSearch,
            faEuroSign,
            faPercent,
            faPlus,
            faMinus,
            faCheck,
            faExclamationTriangle,
            faWifi,
            faTimes,
            faExternalLinkAlt,
            faInfo,
        )
    }
}
