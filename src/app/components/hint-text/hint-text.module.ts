import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HintTextComponent } from './hint-text.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [HintTextComponent],
  exports: [
    HintTextComponent
  ]
})
export class HintTextModule { }
