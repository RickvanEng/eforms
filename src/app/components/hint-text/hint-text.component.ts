import {Component, Input, OnInit} from '@angular/core';

@Component({
    selector: 'app-hint-text',
    templateUrl: './hint-text.component.html',
    styleUrls: ['./hint-text.component.scss']
})
export class HintTextComponent implements OnInit {

    @Input() textID: string;
    @Input() text: string;

    constructor() {
    }

    ngOnInit() {
    }

}
