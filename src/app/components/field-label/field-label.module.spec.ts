import { FieldLabelModule } from './field-label.module';

describe('FieldLabelModule', () => {
  let fieldLabelModule: FieldLabelModule;

  beforeEach(() => {
    fieldLabelModule = new FieldLabelModule();
  });

  it('should create an instance', () => {
    expect(fieldLabelModule).toBeTruthy();
  });
});
