import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FieldLabelComponent} from './field-label.component';

@NgModule({
    imports: [
        CommonModule
    ],
    declarations: [FieldLabelComponent],
    exports: [
        FieldLabelComponent
    ]
})
export class FieldLabelModule {}
