import {ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot} from "@angular/router";

export class RedirectGuard implements CanActivate {

    constructor() {
    }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {

        window.location.href = route.data['externalUrl'];
        return true

    }
}
