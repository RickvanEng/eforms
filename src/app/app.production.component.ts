import {registerLocaleData} from '@angular/common';
import localeNl from '@angular/common/locales/nl';

// TODO deze kan weg??

registerLocaleData(localeNl, 'nl');

// @Component({
//     selector: 'app-root',
//     templateUrl: './app.component.html',
//     styleUrls: ['./app.component.scss'],
// })
// export class AppComponent implements OnInit {
//
//     constructor(private injector: Injector,
//                 private sessionService: SessionService,
//                 private sendFormService: SendFormService) {
//     }
//
//     ngOnInit(): void {
//         this.sessionService.setJSession(this.injector).subscribe(res => {
//             this.sendFormService.correlationID = res.headers.get('x-correlation-id');
//         });
//         this.injector.get(StateService).checkTokenTTL();
//     }
//
// }
