import { NgModule } from '@angular/core';
import {
    MatCheckboxModule,
    MatChipsModule,
    MatDatepickerModule,
    MatDialogModule,
    MatExpansionModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    MatSliderModule,
    MatSlideToggleModule,
    MatSortModule,
    MatTableModule,
} from '@angular/material';

const MaterialModules = [
    MatCheckboxModule,
    MatChipsModule,
    MatDatepickerModule,
    MatDialogModule,
    MatExpansionModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    MatTableModule,
    MatSortModule,
    MatSliderModule,
    MatSlideToggleModule,
];

@NgModule({
    imports: [MaterialModules],
    exports: [MaterialModules],
})

export class MaterialModule {}