# HLM-forms


## .prettierrc settings
Prettier will be enabled AFTER the big change of Rick has taken place.
First time users should do the following:
- run `npm i` this will install the prettier package that has been added in the package.json
- install the `prettier` extension for your IDE

VScode users should add some config to enable `formatOnSave`. 
Press `CMD+SHIFT+P` and type `settings.json` 
Add the following in json object:

```js
// settings.json 
{
  ...
  "editor.defaultFormatter": "esbenp.prettier-vscode",
  "editor.formatOnSave": false,
  "[javascript]": {
    "editor.defaultFormatter": "esbenp.prettier-vscode",
    "editor.formatOnSave": true,
  },
  "[typescript]": {
    "editor.defaultFormatter": "esbenp.prettier-vscode",
    "editor.formatOnSave": true,
  },
  "prettier.requireConfig": true
}
```
```js
// .prettierrc config
{
    "printWidth": 120,
    "singleQuote": true,
    "useTabs": false,
    "tabWidth": 4,
    "semi": true,
    "bracketSpacing": true,
    "trailingComma": "es5",
    "arrowParens": "avoid"
}
```

## tslint options
Moved the options from tslint.json to here since comments are not valid in json files

```js
"arrow-return-shorthand": true,
"callable-types": true,
"class-name": true,
"comment-format": [
  true,
  "check-space"
],
"curly": true,
"deprecation": {
  "severity": "warn"
},
"eofline": true,
"forin": true,
"import-blacklist": [
  true,
  "rxjs/Rx"
],
"import-spacing": true,
"indent": [
  true,
  "spaces"
],
"interface-over-type-literal": true,
"label-position": true,
"max-line-length": [
  true,
  140
],
"member-access": false,
"member-ordering": [
  true,
  {
    "order": [
      "static-field",
      "instance-field",
      "static-method",
      "instance-method"
    ]
  }
],
"no-arg": true,
"no-bitwise": true,
"no-console": [
  true,
  "debug",
  "info",
  "time",
  "timeEnd",
  "trace"
],
"no-construct": true,
"no-debugger": true,
"no-duplicate-super": true,
"no-empty": false,
"no-empty-interface": true,
"no-eval": true,
"no-inferrable-types": [
  true,
  "ignore-params"
],
"no-misused-new": true,
"no-non-null-assertion": true,
"no-redundant-jsdoc": true,
"no-shadowed-variable": true,
"no-string-literal": false,
"no-string-throw": true,
"no-switch-case-fall-through": true,
"no-trailing-whitespace": true,
"no-unnecessary-initializer": true,
"no-unused-expression": true,
"no-use-before-declare": true,
"no-var-keyword": true,
"object-literal-sort-keys": false,
"one-line": [
  true,
  "check-open-brace",
  "check-catch",
  "check-else",
  "check-whitespace"
],
"prefer-const": true,
"quotemark": [
  true,
  "single"
],
"radix": true,
"semicolon": [
  true,
  "always"
],
"triple-equals": [
  true,
  "allow-null-check"
],
"typedef-whitespace": [
  true,
  {
    "call-signature": "nospace",
    "index-signature": "nospace",
    "parameter": "nospace",
    "property-declaration": "nospace",
    "variable-declaration": "nospace"
  }
],
"unified-signatures": true,
"variable-name": false,
"whitespace": [
  true,
  "check-branch",
  "check-decl",
  "check-operator",
  "check-separator",
  "check-type"
],
"no-output-on-prefix": true,
"use-input-property-decorator": true,
"use-output-property-decorator": true,
"use-host-property-decorator": true,
"no-input-rename": true,
"no-output-rename": true,
"use-life-cycle-interface": true,
"use-pipe-transform-interface": true,
"component-class-suffix": true,
"directive-class-suffix": true
```