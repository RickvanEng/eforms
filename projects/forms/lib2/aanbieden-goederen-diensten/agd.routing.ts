import {FormRoutingBuilder} from '../../../form-components/2.0/0-routing/form-routing/formRouting';
import {IFormRouting} from "../../../form-components/2.0/0-routing/form-routing/IFormRouting";
import {IRoutingConfigurator} from "../IRoutingConfigurator";
import {AgdLogin} from './agd.login';

export class AgdRouting2 implements IRoutingConfigurator {
    public getConfig(): IFormRouting {
        return FormRoutingBuilder.newBuilder()
            .setFormName('Aanbieden goederen en diensten')
            .setFormLoginConfigs([
                new AgdLogin().getConfig()
            ])
            .setId('agdRouting')
            .build() as IFormRouting;
    }
}
