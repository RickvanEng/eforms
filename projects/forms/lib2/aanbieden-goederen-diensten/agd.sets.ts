import {FormSetBuilder} from '../../../form-components/2.0/4-set/FormSet';
import {ICmsControlset} from '../../../form-components/2.0/4-set/ICmsControlset';

export class AgdControlSets {

    public getConfig(): ICmsControlset[] {
        return [
            FormSetBuilder.newBuilder()
                .setChildren([
                    'field1',
                ])
                .setId('ControlSet1')
                .setClassName('FormSet2')
                .build() as ICmsControlset,
        ];
    }

    // private static instance: AgdSets;
    // private agdFields: AgdFields = AgdFields.getInstance(this.injector);
    //
    // public static getInstance(injector: Injector): AgdSets {
    //     if (!AgdSets.instance) {
    //         AgdSets.instance = new AgdSets(injector);
    //     }
    //     return AgdSets.instance;
    // }
    //
    // private constructor(private injector: Injector) {
    //
    // }
    //
    // public naamOrganisatie: FormSetBuilder = FormSetBuilder.newBuilder(this.injector)
    //     .setComponent(SetComponent)
    //     .setChildren([
    //         this.agdFields.naamOrganisatie
    //     ])
    //     .setID('naamOrganisatieSet') as FormSetBuilder;
    //
    // public gegevensGoederen: FormSetBuilder = FormSetBuilder.newBuilder(this.injector)
    //     .setComponent(SetComponent)
    //     .setChildren([
    //         this.agdFields.legUit,
    //         this.agdFields.welkeHulpmiddelen,
    //         this.agdFields.hoeveelRuimte,
    //         this.agdFields.hoeveelPersonenPromoteam,
    //         this.agdFields.wanneerSampelenDienstenAanbieden,
    //         this.agdFields.tweedeDagSampelenDiensten,
    //         this.agdFields.derdeDagSampelenDiensten,
    //         this.agdFields.van,
    //         this.agdFields.tot,
    //         this.agdFields.waarSampelenDiensten,
    //     ])
    //     .setID('gegevensGoederenSet') as FormSetBuilder;
    //
    // public contactPersoon: FormSetBuilder = FormSetBuilder.newBuilder(this.injector)
    //     .setComponent(SetComponent)
    //     .setChildren([
    //         this.agdFields.bentUContactPersoon,
    //         this.agdFields.voorlettersContact,
    //         this.agdFields.voorvoegselGeslachtsnaamContact,
    //         this.agdFields.geslachtsnaamContact,
    //     ])
    //     .setID('contactPersoonSet') as FormSetBuilder;
}
