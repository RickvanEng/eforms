import {IFormFieldset} from '../../../form-components/2.0/3-tab-field-set/IFormFieldset';
import {TabFieldSetBuilder} from '../../../form-components/2.0/3-tab-field-set/tab-field-set';

export class AgdFieldSets {
    public getConfig(): IFormFieldset[] {
        return [
            TabFieldSetBuilder.newBuilder()
                .setChildren([
                    'ControlSet1'
                ])
                .setComponent('TabFieldSetComponent')
                .setLabel('Uw gegevens')
                .setId('uwGegevensFieldset1')
                .setClassName('TabFieldSet')
                .build() as IFormFieldset,
        ]
    }
}