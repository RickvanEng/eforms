import {FormFieldBuilder, IFormField} from '../../../form-components/2.0/5-fields/form-field/FormField';

export class AgdFields {
    public getConfig(): IFormField[] {
        return [
            FormFieldBuilder.newBuilder()
                .setId('field1')
                .setClassName('FormField')
                .build() as IFormField,
        ];
    }
}
