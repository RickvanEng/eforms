import {FormTabBuilder, IFormTab} from '../../../form-components/2.0/2-tab/FormTab2';

export class AgdTabs {

    public getConfig(): IFormTab[] {
        return [
            FormTabBuilder.newBuilder()
                .setUrl('uw-gegevens')
                .setChildren([
                    'uwGegevensFieldset1'
                ])
                .setLabel('Uw gegevens')
                .setId('uwGegevensTab')
                .setClassName('FormTab2')
                .build() as IFormTab
        ];
    }

    // constructor(private _injector: Injector) {
    // }

    // private genericFieldSets: GenericTabFieldSets = new GenericTabFieldSets(this._injector);
    // private digidImplementation: DigidImplementation = DigidImplementation.getInstance(this._injector);
    // private agdSets: AgdSets = AgdSets.getInstance(this._injector);
    //
    // public uwGegevens: FormTabBuilder = FormTabBuilder.newBuilder(this._injector)
    //     .setLabel('Uw gegevens')
    //     .setUrl('uw-gegevens')
    //     .setIdp(0)
    //     .setChildren([
    //         this.genericFieldSets.getNewDefaultTabSet()
    //             .setLabel('Uw gegevens')
    //             .setChildren([
    //                 this.agdSets.naamOrganisatie,
    //                 this.digidImplementation.digidMainSet,
    //             ])
    //     ])
    //     .setID('agd-digidTab') as FormTabBuilder;
    //
    // public gegevensGoederen: FormTabBuilder = FormTabBuilder.newBuilder(this._injector)
    //     .setLabel('Gegevens van de diensten of goederen')
    //     .setUrl('gegevens-goederen-diensten')
    //     .setChildren([
    //         this.genericFieldSets.getNewDefaultTabSet()
    //             .setLabel('Gegevens van de diensten of goederen')
    //             .setChildren([
    //                 this.agdSets.gegevensGoederen,
    //             ])
    // 	])
    //     .setID('gegevensGoederen') as FormTabBuilder;
    //
    // public gegevensGoederenEherk: FormTabBuilder = FormTabBuilder.newBuilder(this._injector)
    //     .setLabel('Gegevens van de diensten of goederen')
    //     .setUrl('gegevens-goederen-diensten')
    //     .setChildren([
    //         this.genericFieldSets.getNewDefaultTabSet()
    //             .setLabel('Gegevens van de diensten of goederen')
    //             .setChildren([
    //                 this.agdSets.gegevensGoederen,
    //             ])
    // 	])
    //     .setID('gegevensGoederenEherk') as FormTabBuilder;

}
