import {FormConfigBuilder, IFormConfig} from '../../../form-components/2.0/0-routing/formConfig';
import {LoginTileBuilder} from '../../../form-components/2.0/0-routing/form-routing/login-portal/login-tile/login-tile';
import {AgdForms} from './agd.forms';
import {AgdTabs} from './agdTabs';
import {AgdFieldSets} from './agd.fieldSets';
import {AgdControlSets} from './agd.sets';
import {AgdFields} from './agd.fields';

export class AgdLogin {

    public getConfig(): IFormConfig {
        return FormConfigBuilder.newBuilder()
            .setComponents(new AgdForms().getConfig())
            .setComponents(new AgdTabs().getConfig())
            .setComponents(new AgdFieldSets().getConfig())
            .setComponents(new AgdControlSets().getConfig())
            .setComponents(new AgdFields().getConfig())
            .setLoginTile(LoginTileBuilder.newBuilder()
                .setTitle('Log in met digid')
                .setText(
                    '<p>Bent u inwoner of wilt u als particulier inloggen? Log dan in met DigiD.</p>' +
                    '<br>' +
                    '<p>Heeft u nog geen DigiD? Vraag het dan aan via <a href="https://www.digid.nl/" target="blank">DigiD.nl</a></p>'
                )
                .setButtonText('Log in met DigiD')
                .setLoginUrl('digid')
                .build()
            )
            .setForm('digidForm')
            .setId('agdDigidFormConfig')
            .setClassName('FormConfig')
            .build() as IFormConfig;
    }
}