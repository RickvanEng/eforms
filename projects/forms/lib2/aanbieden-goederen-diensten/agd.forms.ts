import {FormBuilder, IForm} from '../../../form-components/2.0/1-form/Form';

export class AgdForms {

    public getConfig(): IForm[] {
        return [
            FormBuilder.newBuilder()
                .setTabs([
                    'uwGegevensTab'
                ])
                .setId('digidForm')
                .setClassName('Form2')
                .build() as IForm
        ];
    }


    //
    //
    // private static instance: AgdForms;
    //
    // public static getInstance(injector: Injector): AgdForms {
    //     if (!AgdForms.instance) {
    //         AgdForms.instance = new AgdForms(injector);
    //     }
    //     return AgdForms.instance;
    // }
    //
    // private constructor(private injector: Injector) {
    // }
    //
    // private genericTabs: GenericTabs = GenericTabs.getInstance(this.injector);
    // private digidImplementation: DigidImplementation = DigidImplementation.getInstance(this.injector);
    // private agdTabs: AgdTabs = new AgdTabs(this.injector);
    // private agdFields: AgdFields = AgdFields.getInstance(this.injector);
    // private digidFields: DigidFields = DigidFields.getInstance(this.injector);
    // private eherkFields: EherkFields = EherkFields.getInstance(this.injector);
    //
    // private eherkImplementation: EherkImplementation = EherkImplementation.getInstance(this.injector);
    //
    // private _digidFormConfig: FormConfig;
    // public get digidFormConfig(): FormConfig {
    //     if (!this._digidFormConfig) {
    //         this._digidFormConfig = this.digidImplementation.digidConfig
    //             .setFormID(FormEnum.AANBIEDEN_GOEDEREN_DIENSTEN_DIGID)
    //             .setForm(this.getDigidForm()
    //                 .setId(FormEnum.AANBIEDEN_GOEDEREN_DIENSTEN_DIGID))
    //             .build();
    //     }
    //     return this._digidFormConfig;
    // }
    //
    // public getDigidForm(): FormBuilder {
    //     return new GenericForms(this.injector).digidForm
    //         .setTitle('Aanbieden goederen en diensten')
    //         .setZaakTypeCode('LP00000052')
    //         .setTabs([
    //             this.agdTabs.uwGegevens,
    //             // this.agdTabs.gegevensGoederen,
    //             // this.genericTabs.akkoordVersturen
    //         ])
    //         .setDankschermInfo({
    //             naam: this.digidFields.digidVoorletters,
    //             tussenvoegsel: this.digidFields.digidVoorvoegsel,
    // 			achternaam: this.digidFields.digidGeslachtsnaam,
    //             label: 'aanvraag voor het aanbieden van goederen en diensten',
    //             reactietermijn: '2 weken',
    //             html: '',
    //         })
    //         .setVerseonOmschrijving([
    //             {
    //                 showcondition: undefined, info: [
    //                     {prefix: '', field: this.agdFields.legUit}
    //                 ]
    //             }
    //         ])
    // }
    //
    // private _eherkFormConfig: FormConfig;
    // public get eherkFormConfig(): FormConfig {
    //     if (!this._eherkFormConfig) {
    //         this._eherkFormConfig = this.eherkImplementation.eherkConfig
    //             .setFormID(FormEnum.AANBIEDEN_GOEDEREN_DIENSTEN_EHERK)
    //             .setForm(this.getEherkForm()
    //                 .setId(FormEnum.AANBIEDEN_GOEDEREN_DIENSTEN_EHERK))
    //             .build();
    //     }
    //     return this._eherkFormConfig;
    // }
    //
    // public getEherkForm(): FormBuilder {
    //     return new GenericForms(this.injector).eherkForm
    //         .setTitle('Aanbieden goederen en diensten')
    //         .setZaakTypeCode('LP00000052')
    //         .setTabs([
    //             this.eherkImplementation.eherkVestigingenTab,
    //             this.eherkImplementation.eherkContactTab,
    //             this.agdTabs.gegevensGoederenEherk,
    //             this.genericTabs.akkoordVersturen
    //         ])
    //         .setDankschermInfo({
    //             naam: this.eherkFields.cp_voorletters,
    //             tussenvoegsel: this.eherkFields.cp_tussenvoegsel,
    //             achternaam: this.eherkFields.cp_achternaam,
    //             label: 'aanvraag voor het aanbieden van goederen en diensten',
    //             reactietermijn: '2 weken',
    //             html:''
    //         })
    //         .setVerseonOmschrijving([
    //             {
    //                 showcondition: undefined, info: [
    //                     {prefix: '', field: this.agdFields.legUit}
    //                 ]
    //             }
    //         ])
    // }

}
