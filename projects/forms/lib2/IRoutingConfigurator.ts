import {IFormRouting} from "../../form-components/2.0/0-routing/form-routing/IFormRouting";

export interface IRoutingConfigurator {
    getConfig(): IFormRouting;
}