import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {InlogPortalModule} from "../form-components/1.0/framework/form/inlog-portal/inlog-portal.module";

@NgModule({
    declarations: [],
    imports: [
        RouterModule,
        InlogPortalModule,
    ],
    providers: [],
    bootstrap: [],
    exports: [
        InlogPortalModule,
    ]
})
export class HlmFormsModule {}
