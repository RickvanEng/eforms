export interface IEherkenningInterface {
  tradeNames: {
    businessName: string,
    currentTradeNames: string,
    shortBusinessName: string,
  },
  kvkNumber: string,
  addresses: {
    bagId: string,
    city: string,
    country: string,
    houseNumber: string,
    houseNumberAddition: string,
    postalCode: string,
    street: string,
    type: string,
  },
  branchNumber: string,
  businessActivities: {
    sbiCodeDescription: string
  },
  legalForm: string
}
