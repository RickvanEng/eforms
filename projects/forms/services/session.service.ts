import {Injectable, Injector} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {BehaviorSubject, Observable} from "rxjs";
import {environment} from "../../../src/environments/environment";

@Injectable()
export class SessionService {
    public hasSession: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);

    public setJSession(injector: Injector): Observable<any> {
        return injector.get(HttpClient).get(environment.baseUrl + 'eforms-api/controller/login', {observe: 'response'});
    }
}
