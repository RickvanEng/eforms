import {Injectable} from '@angular/core';
import {Subject} from 'rxjs';

@Injectable()
export class LoaderService {
    isLoading = new Subject<boolean>();

    private amountOfCalls: number = 0;

    show() {
        this.amountOfCalls++;
        this.isLoading.next(true);
    }

    hide() {
        if (this.amountOfCalls > 0) {
            this.amountOfCalls--;
        }
        if (this.amountOfCalls === 0) {
            this.isLoading.next(false);
        }

    }
}
