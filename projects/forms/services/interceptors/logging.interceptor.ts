import {HttpClient, HttpErrorResponse, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {catchError} from 'rxjs/operators';
import {Observable, throwError} from 'rxjs';

@Injectable()
export class LoggingInterceptor implements HttpInterceptor {
	constructor(private http: HttpClient) {
	}
	
	intercept(req: HttpRequest<any>, next: HttpHandler): Observable<any> {
		return next.handle(req).pipe(
			catchError((error: HttpErrorResponse) => {
				console.log(req.url);
				if (req.url.indexOf('eforms-api/logging/frontend') === -1) {
					// this.http.post(environment.baseUrl + 'eforms-api/logging/frontend', {
					// 	message: 'errorCode: ' + error.status + ', ' + error.error.error,
					// 	type: 'error'
					// }).subscribe(res => {
					// 	console.log(res);
					// });
				}
				
				return throwError(error);
			})
		);
	}
}
