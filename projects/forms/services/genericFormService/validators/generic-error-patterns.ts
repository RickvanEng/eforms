export class GenericErrorPatterns {
  public static readonly voorletters = '^([a-zA-ZÀ-ÿ]\\.? ?)+$';
  public static readonly voorlettersErrorMessage = 'Vul alleen uw voorletters in en een punt.';

  public static readonly voorvoegsel = '^[a-zA-ZÀ-ÿ \'\\.-]+$';
  public static readonly voorvoegselErrorMessage = 'Uw tussenvoegsels bestaan uit letters.';

  public static readonly geslachtsnaam = '^([a-zA-ZÀ-ÿ-]\\.? ?)+$';
  public static readonly geslachtsnaamErrorMessage = 'Uw achternaam bestaat uit letters.';

  public static readonly voornaam = '^([a-zA-ZÀ-ÿ]\\.? ?)+$';
  public static readonly voornaamErrorMessage = 'Uw voornaam bestaat uit letters.';

  public static readonly postcode = '^[1-9][0-9]{3} ?[A-Za-z]{2}$';
  public static readonly postcodeErrorMessage = 'Uw postcode is een Nederlandse postcode en bestaat uit 4 cijfers en 2 letters.';

  public static readonly straatnaam = '^([a-zA-ZÀ-ÿ!0-9,@#\\\'\\"\\$%\\^\\&*\\)\\(+=._-]\\.? ?)+$';
  public static readonly straatnaamErrorMessage = 'Uw straatnaam bestaat uit letters.';

  public static readonly huisnummer = '[0-9]+';
  public static readonly huisnummerErrorMessage = 'Uw huisnummer bestaat uit getallen.';

  public static readonly huisnummerToevoeging = '^[0-9a-zA-ZÀ-ÿ -]+$';
  public static readonly huisnummerToevoegingErrorMessage = 'Uw huisnummertoevoeging bestaat uit cijfers en letters.';

  public static readonly plaats = '^[a-zA-ZÀ-ÿ- \']+$';
  public static readonly plaatsErrorMessage = 'Uw plaats bestaat uit letters. ';

  public static readonly telefoon = '^[0-9]*$';
  public static readonly telefoonErrorMessage = 'Uw telefoonnummer is onvolledig of onjuist ingevuld. Vul een geldig telefoonnummer in.';

  public static readonly telefoon_buitenland = '^\\+?[^-][\\d\\-]+$';
  public static readonly telefoon_buitenlandErrorMessage = 'Uw telefoonnummer is onvolledig of onjuist ingevuld. Vul een geldig telefoonnummer in.';

  public static readonly iban = '^NL[0-9]{2}[A-Z]{4}0[0-9]{9}$';
  public static readonly ibanErrorMessage = 'Het rekeningnummer is onvolledig of onjuist ingevuld. Vul en een geldig rekeningnummer in.';

  public static readonly opdrachtnummer = '^[0-9]*$';

  public static readonly bsn = '^[0-9]{9}$';
  public static readonly bsnErrorMessage = 'Het BSN-nummer is onvolledig of onjuist ingevuld. Vul een geldig BSN-nummer in.';

  public static readonly emailErrorMessage = 'Uw e-mailadres is onvolledig of onjuist ingevuld. Vul een geldig e-mailadres in.';

  public static readonly currencyInput = '^[0-9,]+$';
  public static readonly numberOnly = '[0-9,]+$';
  public static readonly roundNumberOnly = '[0-9]+$';
  public static readonly numbersOnlyNoComma = '[0-9]+$';
  public static readonly lettersAndSpecial = '^[a-zA-ZÀ-ÿ \'\\.-]+$';
  public static readonly lettersNumbersAndSpecial = '^[a-zA-ZÀ-ÿ0-9 \'\\.-]+$';
  public static readonly lettersAndNumbers = '^[a-zA-ZÀ-ÿ0-9- ]+$';
  public static readonly onlyLetters = '[a-zA-ZÀ-ÿ-_\'() ]*$';
  public static readonly time = '^[0-9:]*$';
  //public static readonly textareaValidator = '^[a-zA-ZÀ-ÿ0-9 \'\\.,-\?\!\@\&()]+$';
  public static readonly textareaValidator = '^[a-zA-ZÀ-ÿ0-9 \'\\\\/\.,\+\-\=\?\!\@\&\(\)\#\%\*\n\"\$\`\~\^\&\_]+$';
  // public static readonly kilometers = '^([0-9]{1,4} ?(\.[0-9]{1,1})?)$';
  public static readonly kilometers = '^[0-9]{1,4}(?:\,[0-9])?$';
  public static readonly kilometersErrorMessage = 'U kunt maximaal 4 cijfers invoeren en 1 decimaal';

  public static readonly meters = '^[0-9]{1,6}(?:\,[0-9])?$';
  public static readonly metersErrorMessage = 'U kunt maximaal 6 cijfers invoeren en 1 decimaal';

  public static readonly dagenPerMaand = '^(([1-9])|([1-2][0-9])|(3[01]))$';
  public static readonly dagenPerMaandErrorMessage = 'U kunt minimaal 1 en maximaal 31 dagen invoeren';

  //public static readonly kentekens = '(([a-zA-Z]{3}[0-9]{3})|(\w{2}-\w{2}-\w{2})|([0-9]{2}-[a-zA-Z]{3}-[0-9]{1})|([0-9]{1}-[a-zA-Z]{3}-[0-9]{2})|([a-zA-Z]{1}-[0-9]{3}-[a-zA-Z]{2}))';
  public static readonly kentekens = '^[a-zA-Z0-9- ]+$';
  public static readonly kentekensErrorMessage = 'U kunt alleen cijfers, letters en streepjes invoeren.';

}
