import {FormRouting2} from '../../../form-components/2.0/0-routing/form-routing/formRouting';

export interface IOldForms {
	url: string;
	form: any;
}

export class VariableFormService {
	get routersOld(): IOldForms[] {
		return this._routersOld;
	}
	
	set routersOld(value: IOldForms[]) {
		this._routersOld = value;
	}
	
	public get routers(): FormRouting2[] {
		return this._routers;
	}
	
	public set routers(forms: FormRouting2[]) {
		this._routers = forms;
	}
	
	public formRouting: any;
	public formRoutingNew: FormRouting2;
	
	private _routersOld: IOldForms[] = [];
	private _routers: FormRouting2[] = [];
	
	private static instance: VariableFormService;
	
	public static getInstance(): VariableFormService {
		if (!VariableFormService.instance) {
			VariableFormService.instance = new VariableFormService();
		}
		return VariableFormService.instance;
	}
	
	private constructor() {
	}
}
