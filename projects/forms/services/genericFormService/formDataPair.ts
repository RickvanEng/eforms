import {Injector} from "@angular/core";

export class FormFactory {
    create<T>(type: (new (params: Injector) => T), injector: Injector): T {
        return new type(injector);
    }
}
