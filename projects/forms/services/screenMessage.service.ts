import {Injectable} from '@angular/core';
import {BehaviorSubject} from "rxjs";

export class ScreenMessage {
    public type;
    public message;

    constructor(type: string, message: string) {
        this.type = type;
        this.message = message;
    }
}

@Injectable()
export class ScreenMessageService {

    private messages: BehaviorSubject<ScreenMessage[]> = new BehaviorSubject<ScreenMessage[]>([]);

    public setError(message: ScreenMessage): void {
        if (this.messages.getValue().indexOf(message) === -1) {
            this.messages.getValue().push(message);
        }
    }

    public get(): BehaviorSubject<ScreenMessage[]> {
        return this.messages;
    }

    public remove(message: ScreenMessage) {
        if (this.messages.getValue().indexOf(message) >= 0) {
            this.messages.getValue().splice(this.messages.getValue().indexOf(message), 1);
        }
    }

    public clear() {
        this.messages.getValue().pop();
    }
}
