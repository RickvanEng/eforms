export interface DigidInterface {
  PRS: {
    aNummer: number,
    bsnNummer: number,
    voornamen: string,
    voorletters: string,
    geslachtsnaam: string
    geboortedatum: string,
    geboorteplaats: string,
    omschrijvingGeboorteland: string,
    geslachtsaanduiding: string,
    indicatieGeheim: string,
    codeGemeenteVanInschrijving: number,
    omschrijvingGemeenteVanInschrijving: string,
    burgelijkeStaat: string,
    aanduidingNaamgebruik: string,
    PRSADRVBL: {
      postcode: string,
      woonplaatsnaam: string,
      straatnaam: string,
      huisnummer: number
    },
    PRSNAT: {
      omschrijving: string
    }
  }
}

export interface EherkenningInterface {
  handelsnaam: string,
  kvkNummer: number,
  veststraat: string,
  vesthuisnummer: number,
  vesthuisnummertoevoeging: string,
  vestpostcode: string,
  vestwoonplaats: string,
  bestuurslidEen: string,
  bestuurslidTwee: string,
  bestuurslidDrie: string
}

export class MockData {
  public digid: DigidInterface = {
    PRS: {
      aNummer: 123,
      bsnNummer: 170241002,
      voornamen: 'Jan',
      voorletters: 'J.',
      geslachtsnaam: 'Bakker',
      geboortedatum: '18-11-1989',
      geboorteplaats: 'Hoofddorp',
      omschrijvingGeboorteland: 'Spaarne Gasthuis Hoofddorp',
      geslachtsaanduiding: 'Man',
      indicatieGeheim: 'Geen idee',
      codeGemeenteVanInschrijving: 3333,
      omschrijvingGemeenteVanInschrijving: 'Ergens in Nederland',
      burgelijkeStaat: 'Getrouwd',
      aanduidingNaamgebruik: '',
      PRSADRVBL: {
        postcode: '2132 TZ',
        woonplaatsnaam: 'Hoofddorp',
        straatnaam: 'Raadhuisplein',
        huisnummer: 1
      },
      PRSNAT: {
        omschrijving: ''
      }
    }
  };
}

export class MockDataEherkenning {
  public eherkenning: EherkenningInterface = {
    handelsnaam: 'Haarlemmermeer B.V.',
    kvkNummer: 21346587,
    veststraat: 'Raadhuisplein',
    vesthuisnummer: 1,
    vesthuisnummertoevoeging: '',
    vestpostcode: '2132 TZ',
    vestwoonplaats: 'Hoofddorp',
    bestuurslidEen: 'Rick van Engelenburg',
    bestuurslidTwee: 'Bouke Elzenga',
    bestuurslidDrie: 'Mert Tamer'
  };
}
