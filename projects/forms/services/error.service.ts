import {Router} from "@angular/router";
import {VariableFormService} from "./genericFormService/variableFormService";
import {Injectable} from "@angular/core";
import {ScreenMessage, ScreenMessageService} from "./screenMessage.service";

export class ScreenErrorMessages {
    public static http_403 = new ScreenMessage('danger', 'Uw sessie is verlopen. Log opnieuw in.');
    public static http_403_refresh = new ScreenMessage('danger', 'Uw sessie is verlopen. Om verder te gaan moet u het formulier refreshen / opnieuw laden.');
}

@Injectable()
export class ErrorService {

    private varFormService: VariableFormService;
    
    constructor(public router: Router,
                private screenMessageService: ScreenMessageService) {
        this.varFormService = VariableFormService.getInstance();
    }


    public handleError(error: any) {
        if (error) {
            for (let fieldName of error.blocking) {
                for (let matchedValue of this.varFormService.formRouting.getSelectedForm().activeTabs) {
                    if (matchedValue.form.contains(fieldName)) {
                        console.log('set error on: ', matchedValue.form.get(fieldName));
                        matchedValue.form.get(fieldName).setErrors({'backEndValidation': true});
                    }
                }
            }
        }
    }
}
