import {NgModule} from '@angular/core';
import {StatusDashboardModule} from 'projects/dashboard/status-dashboard.module';

@NgModule({
    declarations: [],
    imports: [
        StatusDashboardModule,
    ],
    providers: [],
    bootstrap: [],
    exports: [
        StatusDashboardModule,
    ]
})
export class HlmFormsModule {}
