import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {EStatus, IFormRoutingCMS} from '../../form-components/2.0/0-routing/form-routing/formRouting';
import {SelectionModel} from '@angular/cdk/collections';
import {TableRow} from '../status-dashboard.component';

@Component({
    selector: 'dashboard-dialog',
    templateUrl: 'dashboard-dialog.component.html',
    styleUrls: ['./dashboard-dialog.component.scss'],
})

export class DashboardDialogComponent implements OnInit {
  	
    constructor(public dialogRef: MatDialogRef<DashboardDialogComponent>, @Inject(MAT_DIALOG_DATA) public data: {selected: SelectionModel<TableRow>, form: IFormRoutingCMS}) {}
	
	ngOnInit(): void {
 
	}
	
	public getStatus(): string[] {
    	let result: string[] = [];
    	Object.keys(EStatus).forEach(key => {
    		result.push(key)
		});
    	return result;
	}
	
}
