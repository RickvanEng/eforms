import {HttpClient} from '@angular/common/http';
import {Component, Injector, OnInit, ViewChild} from '@angular/core';
import {MatDialog, MatSort, MatTableDataSource} from '@angular/material';
import {Router} from '@angular/router';
import {environment} from 'src/environments/environment';
import {TitleService} from 'src/title.service';
import {DashboardDialogComponent} from './dashboard-dialog/dashboard-dialog.component';
import {IOldForms, VariableFormService} from '../forms/services/genericFormService/variableFormService';
import {FormRouting2, IFormDashConfig} from '../form-components/2.0/0-routing/form-routing/formRouting';
import {SelectionModel} from '@angular/cdk/collections';
import {DashboardDialogOfflineMsgComponent} from './dashboard-dialog-offline-msg/dashboard-dialog-offline-msg.component';
import {EnvironmentService, IEnvrionmentData} from '../form-components/2.0/services/environment.service';
import {faExternalLinkAlt, faInfo, faTimes, faWifi} from '@fortawesome/free-solid-svg-icons';

export interface IFilterOption {
	label: string;
	checked: boolean;
}

export interface IFilterGroup {
	filterGroup: string;
	options: IFilterOption[]
}

export interface TableRow {
	name: string,
	routing: FormRouting2,
}

@Component({
	selector: 'dashboard',
	templateUrl: './status-dashboard.component.html',
	styleUrls: ['./status-dashboard.component.scss'],
})
export class StatusDashboardComponent implements OnInit {
	public faInfo = faInfo;

	panelOpenState = false;

	public oldForms: IOldForms[] = [];
	public showTable: boolean = false;
	public environmentData: IEnvrionmentData;
	
	@ViewChild(MatSort) sort: MatSort;
	
	private baseUrl = environment.baseUrl;
	public info: TableRow;
	public showInfo: boolean = false;
	
	public dataSource: MatTableDataSource<TableRow> = new MatTableDataSource([]);
	
	public form;
	public filterGroups: IFilterGroup[];

	public columnsToDisplay = ['select', 'actions', 'name', 'status'];
	public filterValues = {
		name: '',
		url: '',
		status: '',
		logins: ''
	};
	
	public selection = new SelectionModel<TableRow>(true, []);
	public offlineMessage: string = '';
	
	constructor(private injector: Injector, private http: HttpClient, public router: Router, public dialog: MatDialog) {
	}
	
	ngOnInit() {
		this.injector.get(EnvironmentService).environment.subscribe(env => {
			this.environmentData = env;
		});
		
		// before listing the 1.5 forms, get all config from BE
		let promises: Promise<any>[] = [];
		VariableFormService.getInstance().routers.forEach(router => {
			router.forms.forEach(form => {
				promises.push(form.getDashConfig());
			})
		});
		
		Promise.all(promises).then(() => {
			// After the calls are done, load it in the dash
			let tablerows: TableRow[] = [];
			VariableFormService.getInstance().routers.forEach(router => {
				tablerows.push({
					name: router.formName,
					routing: router,
				});
			});
			
			this.showTable = true;
			this.sortRows(tablerows);
			this.dataSource.data = tablerows;
			
			this.getServicesStatus();
			this.filterGroups = [
				{
					filterGroup: 'status',
					options: [
						{label: 'public', checked: false},
						{label: 'private', checked: false},
						{label: 'offline', checked: false},
					]
				},
				{
					filterGroup: 'logins',
					options: [
						{label: 'digid', checked: false},
						{label: 'eherk', checked: false},
						{label: 'no-login', checked: false},
					]
				},
				{
					filterGroup: 'sendMethod',
					options: [
						{label: 'verseon', checked: false},
						{label: 'mail', checked: false},
					]
				}
			];
			
			this.injector.get(TitleService).setTitle('eFormulieren');
			this.dataSource.filterPredicate = this.createFilter();
			this.dataSource.sort = this.sort;
		});
	}
	
	updateFilter() {
		this.dataSource.filter = JSON.stringify(this.filterValues);
	}
	
	filterForms(searchTerm: string) {
		if (searchTerm === '') {
			this.dataSource.filter = '';
		} else {
			this.filterValues.name = searchTerm.toLocaleLowerCase();
			this.dataSource.filter = JSON.stringify(this.filterValues);
		}
	}
	
	createFilter(): (data: any, filter: string) => boolean {
		let filterFunction = (data, filter): boolean => {
			let searchTerms = JSON.parse(filter);
			return data.name.toLowerCase().indexOf(searchTerms.name) !== -1
				&& data.routing.url.toString().toLowerCase().indexOf(searchTerms.name) !== -1
				&& data.routing.forms[0].dashConfig.status.toLowerCase().indexOf(searchTerms.status) !== -1
				//&& data.routing.forms.some(form => form.getIdpLoginUrl().toLowerCase() === searchTerms.logins)
		};
		return filterFunction;
	}
	
	public isAllSelected(): boolean {
		const numSelected = this.selection.selected.length;
		const numRows = this.dataSource.data.length;
		return numSelected === numRows;
	}
	
	masterToggle() {
		if (this.isAllSelected()) {
			this.selection.clear();
			return;
		}
		
		this.selection.select(...this.dataSource.data);
	}
	
	checkboxLabel(row?: TableRow): string {
		if (!row) {
			return `${this.isAllSelected() ? 'deselect' : 'select'} all`;
		}
		return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row`;
	}
	
	getServicesStatus() {


		// this.http.get(this.baseUrl + 'eforms-api/controller/alerts').subscribe(resp => {
		// 	this.services = resp;
		// });
	}
	
	serviceClasses(service) {
		return (service.status == 'UP') ? 'online' : 'offline';
	}
	
	checkboxChanged(group: IFilterGroup) {
		if (group.filterGroup === 'status') {
			this.filterValues.status = group.options.filter(x => x.checked === true).map(x => x.label).join(", ");
			this.dataSource.filter = JSON.stringify(this.filterValues);
		} else if (group.filterGroup === 'logins') {
			this.filterValues.logins = group.options.filter(x => x.checked === true).map(x => x.label).join(", ");
			this.dataSource.filter = JSON.stringify(this.filterValues);
		} else {
			this.dataSource.filter = '';
		}
	}
	
	public navigateTo(url: string): void {
		this.router.navigate([url]);
	}
	
	openDialog2() {
		let form;
		new Promise<any>(resolve => {
			
			if (this.selection.selected.length === 1) {
				this.selection.selected[0].routing.forms[0].getDashConfig().then(res => {
					form = JSON.parse(JSON.stringify(res));
					resolve();
				})
			} else {
				form = {
					id: '',
					status: '',
					designUrl: '',
					description: '',
					message: '',
				};
				resolve();
			}
		}).then(() => {
			let dialogRef = this.dialog.open(DashboardDialogComponent, {
				data: {
					selected: this.selection,
					form: form,
				},
				width: '50%',
				height: 'auto',
				autoFocus: false,
			},);
			
			dialogRef.afterClosed().subscribe((result: { selected: SelectionModel<TableRow>, form: IFormDashConfig }) => {
				// If the user cancels the modal, the result is undefined
				if (result) {
					for (let routing of result.selected.selected) {
						for (let form of routing.routing.forms) {
							form.update(result.form)
						}
					}
				}
			});
		});
	}
	
	openGlobalMessageDialog() {
		const dialogRef = this.dialog.open(DashboardDialogOfflineMsgComponent, {
			data: {message: this.offlineMessage},
			width: '50%',
			height: 'auto',
			autoFocus: false,
		},);
		
		dialogRef.afterClosed().subscribe((msg: any) => {
			if (msg) {
				console.log(msg)
				
				this.http.put(this.baseUrl + 'eforms-api/controller/form/status/globals', {message: msg}).subscribe(resp => {
					console.log(resp)
				});
			}
		});
	}
	
	private sortRows(rows: TableRow[]): void {
		rows.sort((a, b) => {
			if (a.name < b.name) {
				return -1;
			}
			
			if (a.name > b.name) {
				return 1;
			}
			return 0;
		});
	}
}
