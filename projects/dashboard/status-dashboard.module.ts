import {NgModule} from '@angular/core';
import {ReactiveFormsModule} from '@angular/forms';
import {CommonModule} from '@angular/common';
import {BrowserModule} from '@angular/platform-browser';
import {RouterModule} from '@angular/router';
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';
import {StatusDashboardComponent} from './status-dashboard.component';
import {ComponentsModule} from 'projects/form-components/1.0/src/components.module';
import {MaterialModule} from 'src/app/material.module';
import {DashboardDialogComponent} from './dashboard-dialog/dashboard-dialog.component';
import {MatSelectModule} from '@angular/material';
import {DashboardDialogOfflineMsgComponent} from './dashboard-dialog-offline-msg/dashboard-dialog-offline-msg.component';

@NgModule({
	declarations: [
		StatusDashboardComponent,
		DashboardDialogComponent,
		DashboardDialogOfflineMsgComponent,
	],
	entryComponents: [
		DashboardDialogComponent,
		DashboardDialogOfflineMsgComponent,
	],
	imports: [
		ReactiveFormsModule,
		CommonModule,
		BrowserModule,
		ComponentsModule,
		FontAwesomeModule,
		MaterialModule,
		RouterModule.forRoot([{path: 'dashboard', component: StatusDashboardComponent},]),
		MatSelectModule,
	],
	providers: [],
	exports: [StatusDashboardComponent]
})
export class StatusDashboardModule {
}
