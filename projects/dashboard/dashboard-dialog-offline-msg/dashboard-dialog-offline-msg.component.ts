import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';

@Component({
    selector: 'dashboard-dialog-offline-msg',
    templateUrl: 'dashboard-dialog-offline-msg.component.html',
    styleUrls: ['./dashboard-dialog-offline-msg.component.scss'],
})

export class DashboardDialogOfflineMsgComponent implements OnInit {
  	
    constructor(public dialogRef: MatDialogRef<DashboardDialogOfflineMsgComponent>, @Inject(MAT_DIALOG_DATA) public data: any) {}
	
	ngOnInit(): void {}
	
}
