import {FormField, FormFieldBuilder} from "../5-fields/form-field/FormField";
import {FormFieldOption} from "../5-fields/form-field/multipleChoiceField/options/FieldOption";

export class PartnerCondition {
	set field(value: FormField) {
		this._field = value;
	}
	
	get field(): FormField {
		return this._field;
	}
	
	get fieldBuilder(): FormFieldBuilder {
		return this._fieldBuilder;
	}
	
	get option(): FormFieldOption {
		return this._option;
	}
	
	private _fieldBuilder: FormFieldBuilder;
	private _field: FormField;
	private _option: FormFieldOption;

	constructor(field: FormFieldBuilder, option?: FormFieldOption) {
		this._fieldBuilder = field;
		this._option = option;
	}
}
