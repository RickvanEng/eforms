import {Injectable} from '@angular/core';
import {BehaviorSubject} from 'rxjs';
import {FormField} from '../5-fields/form-field/FormField';
import {FormFieldWithOptions} from '../5-fields/form-field/multipleChoiceField/MultipleChoiceField';
import {PartnerCondition} from './partnerCondition';

@Injectable({
	providedIn: 'root'
})
export class PartnerConditionService {
	
	public getPartnerSubscription(partnerConditions: PartnerCondition[]): BehaviorSubject<boolean> {

		let hasPartner: BehaviorSubject<boolean> = new BehaviorSubject(false);
        const fieldsToCheck: { field: FormField, conditions: PartnerCondition, conditionsMet: boolean }[] = [];

		partnerConditions.forEach((i) => {
			fieldsToCheck.push({field: i.field, conditions: i, conditionsMet: false});
		});

		fieldsToCheck.forEach(obj => {
			if (obj.conditions.option) {
				let fieldToCheck: FormFieldWithOptions = obj.field as FormFieldWithOptions;
				
				// TODO check of deze werkt runtime
				fieldToCheck.getOptions.subscribe(options => {
					options.forEach(option => {
						// option.changeDetector.subscribe(trigger => {
						// 	if (trigger) {
						// 		obj.conditionsMet = option === obj.conditions.option;
						// 		this.checkConditions(hasPartner, fieldsToCheck);
						// 	}
						// })
					})
				});
			}
		});

		return hasPartner;
	}

	private checkConditions(obs: BehaviorSubject<boolean>, fieldsToCheck: { field: FormField, conditions: PartnerCondition, conditionsMet: boolean }[]): void {
		let partner: boolean;	
		for (const fieldToCheck of fieldsToCheck) {
			partner = !fieldToCheck.conditionsMet ? false : true;
		}
		obs.next(partner)
	}

}
