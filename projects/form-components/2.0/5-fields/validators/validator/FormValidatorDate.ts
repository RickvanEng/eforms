// import {Injector} from '@angular/core';
// import {DatePickerField} from '../../form-field/date-picker2/DatePickerField';
// import * as moment from 'moment';
// import {DateDirectionTypes, DateTypes} from '../../form-field/date-picker2/date-type';
//
// export class FormValidatorDateBuilder extends FormValidatorBuilder {
// 	private _amount: number = 0;
// 	private _unit: DateTypes = 'days';
// 	private _direction: DateDirectionTypes = 'min';
//
// 	get amount(): number {
// 		return this._amount;
// 	}
//
// 	get unit(): DateTypes {
// 		return this._unit;
// 	}
//
// 	get direction(): DateDirectionTypes {
// 		return this._direction;
// 	}
//
// 	// date om tegen te valideren, plus groter of kleiner
// 	constructor(injector: Injector, amount: number, unit: DateTypes, direction: DateDirectionTypes) {
// 		super(injector);
// 		this._amount = amount;
// 		this._unit = unit;
// 		this._direction = direction;
// 	}
//
// 	public static newBuilder(injector: Injector, amount: number = 0, unit: DateTypes = 'days', direction: DateDirectionTypes = 'min'): FormValidatorDateBuilder {
// 		return new FormValidatorDateBuilder(injector, amount, unit, direction);
// 	}
//
// 	public build(): FormValidatorDate {
// 		return new FormValidatorDate(this);
// 	}
// }
//
// export class FormValidatorDate extends FormValidator {
// 	private _amount: number = 0;
// 	private _unit: DateTypes = 'days';
//
// 	private _direction: DateDirectionTypes = 'min';
//
// 	public dateField: DatePickerField;
//
// 	protected _injector: Injector;
//
// 	get amount(): number {
// 		return this._amount;
// 	}
//
// 	get unit(): DateTypes {
// 		return this._unit;
// 	}
//
// 	get direction(): DateTypes {
// 		return this._unit;
// 	}
//
// 	// set params
// 	constructor(b: FormValidatorDateBuilder) {
// 		super(b);
// 		this._amount = b.amount;
// 		this._unit = b.unit;
// 		this._direction = b.direction;
// 	}
//
// 	public isValid(body: any): Promise<IFormValidatorResponse> {
// 		return new Promise<IFormValidatorResponse>(resolve => {
// 			this.executeValidateLogic(body).then(data => {
// 				resolve(data);
// 			})
// 		});
// 	}
//
// 	public hasDate(date: string): boolean {
// 		return date && date !== '' && date !== 'Invalid date';
// 	}
//
// 	// Override this in extensions
// 	protected executeValidateLogic(body: any): Promise<IFormValidatorResponse> {
// 		return new Promise<IFormValidatorResponse>(resolve => {
// 			let errorResult: IFormValidatorResponse = {
// 				error: false,
// 				statusTekst: '',
// 			};
//
// 			if (this.hasDate(body) && this.dateField && this.hasDate(this.dateField.getValue())) {
// 				const fieldDate = moment(body, 'YYYY-MM-DD');
// 				const minDate = moment(this.dateField.getValue(), 'YYYY-MM-DD').add(this._amount, this._unit);
// 				if (this._direction === 'min') {
// 					errorResult.error = !fieldDate.isSameOrAfter(minDate);
// 				} else {
// 					errorResult.error = !fieldDate.isSameOrBefore(minDate);
// 				}
// 			}
//
// 			resolve(errorResult);
// 		});
// 	}
// }
