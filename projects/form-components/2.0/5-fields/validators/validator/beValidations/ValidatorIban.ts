import {AbstractControl, AsyncValidatorFn, ValidationErrors} from '@angular/forms';
import {AsyncValidatorService} from './async-validator.service';
import {Injector} from '@angular/core';
import {IValidator} from '../../../../validators/IValidator';

export class ValidatorIban {
	static createValidator(inj: Injector, params: IValidator): AsyncValidatorFn {
		return (control: AbstractControl): Promise<ValidationErrors> => {
			return new Promise<ValidationErrors>(resolve => {
				inj.get(AsyncValidatorService).validate(
					'get',
					'eforms-api/controller/zaaksysteem/controle/iban/'.concat(control.value)
				).then(res => {
					resolve(!res.error ? null : {iban: true});
				});
			});
		};
	}
}
