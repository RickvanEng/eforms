import { AsyncValidatorService } from './async-validator.service';
import { AsyncValidatorFn, ValidationErrors } from '@angular/forms';
import { Injector } from '@angular/core';
import * as moment from 'moment';
import {IValidator, IValidatorResponse} from '../../../../validators/IValidator';
import { EFormsAbstractControl } from '../../../../base/eFormsAbstractControl';

export enum EBirtdayResponseRange {
    TOO_YOUNG = -1,
    CORRECT = 0,
    TOO_OLD = 1,
}

export interface IBirthDayResponse {
    error: boolean;
    leeftijdBereik: number;
    statusCode: number;
    statusTekst: string;
}

export class BirthdayValidator {
    static createValidator(inj: Injector, params: IValidator): AsyncValidatorFn {
        let asyncValidatorService: AsyncValidatorService = inj.get(AsyncValidatorService);

        return (control: EFormsAbstractControl): Promise<ValidationErrors> => {
            return new Promise<ValidationErrors>(resolve => {
                asyncValidatorService.validate(
                    'post',
                    'eforms-api/controller/validation/custom/age',
                    {
                        geboortedatum: moment(control.value).format('YYYY-MM-DD'),
                        min: params.params.min,
                        max: params.params.max,
                    }).then((res: IBirthDayResponse) => {

                        params.responses.forEach(response => {
                            control.deleteNotification(response.throwId);
                        });

                        if (res.error) {
                            let matchingResponse: IValidatorResponse = params.responses.find(response => response.responseCode === res.leeftijdBereik);
                            if (matchingResponse.warning) {
                                control.addNotification(matchingResponse.throwId, matchingResponse);
                                resolve(null);
                            } else {
                                let errorObj: any = {
                                    custom: true,
                                    msg: params.useBeMsg ? res.statusTekst : matchingResponse.msg,
                                };
                                params.useBeMsg ? errorObj.msg = res.statusTekst : errorObj.msg = matchingResponse.msg;
                                resolve({ [matchingResponse.throwId]: errorObj });
                            }
                        } else {
                            resolve(null);
                        }
                    },
                );
            });
        };
    }
}
