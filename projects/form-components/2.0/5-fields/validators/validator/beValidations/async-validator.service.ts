import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../../../src/environments/environment';

export interface IValidationResponseBody {
	custom: boolean;
	msg: string;
}

@Injectable({
	providedIn: 'root'
})
export class AsyncValidatorService {
	
	constructor(private http: HttpClient) {
	
	}
	
	public validate(action: 'get' | 'post', url: string, body?: any): Promise<any> {
		switch (action) {
			case 'get': {
				return this.http.get(environment.baseUrl + url).toPromise()
			}
			case 'post': {
				return this.http.post(environment.baseUrl + url, body).toPromise();
			}
		}
	}
}
