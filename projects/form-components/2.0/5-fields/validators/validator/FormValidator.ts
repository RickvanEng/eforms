// import {Injector} from '@angular/core';
//
// export interface IFormValidatorResponse {
// 	error: boolean;
// 	statusTekst: string;
// }
//
// export class FormValidatorBuilder {
//
// 	get injector(): Injector {
// 		return this._injector;
// 	}
//
// 	constructor(private _injector: Injector) {
// 	}
//
// 	public static newBuilder(injector: Injector): FormValidatorBuilder {
// 		return new FormValidatorBuilder(injector);
// 	}
//
// 	public build(): FormValidator {
// 		return new FormValidator(this);
// 	}
// }
//
// export class FormValidator {
//
// 	protected _injector: Injector;
//
// 	constructor(b: FormValidatorBuilder) {
// 		this._injector = b.injector;
//
// 	}
//
// 	public isValid(body: any): Promise<IFormValidatorResponse> {
// 		return new Promise<IFormValidatorResponse>(resolve => {
// 			this.executeValidateLogic(body).then((data: IFormValidatorResponse) => {
// 				resolve(data);
// 			})
// 		});
// 	}
//
// 	public updateVariableContent(target: string, replacement: string): void {
//
// 	}
//
// 	// Override this in extensions
// 	protected executeValidateLogic(body: any): Promise<IFormValidatorResponse> {
// 		return new Promise<IFormValidatorResponse>(resolve => {
// 			console.warn('Override this method, validator does not work without override');
// 			resolve(undefined);
// 		});
// 	}
// }
