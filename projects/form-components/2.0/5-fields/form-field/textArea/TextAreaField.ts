import {Injector} from '@angular/core';
import {FormField, FormFieldBuilder} from '../FormField';

export class TextAreaFieldFactory extends FormFieldBuilder {
	// get maxLength(): number {
	// 	return this._maxLength;
	// }
	//
	// private _maxLength: number = 500;
	//
	// /** override */
	// public static newFactory(injector: Injector): TextAreaFieldFactory {
	// 	return new TextAreaFieldFactory(injector);
	// }
	//
	// public setMaxInput(value: number): TextAreaFieldFactory {
	// 	this._maxLength = value;
	// 	return this;
	// }
	//
	// _buildingClass: any = TextAreaField;
}

export class TextAreaField extends FormField {
	// get maxLength(): number {
	// 	return this._maxLength;
	// }
	//
	// private _maxLength: number;
	//
	// constructor(builder: TextAreaFieldFactory) {
	// 	super(builder);
	// 	this._maxLength = builder.maxLength;
	// }
	//
	// public onBlur(): void {
	// 	if (this._abstractControl.value) {
	// 		this._abstractControl.setValue(this._abstractControl.value.toString().trim())
	// 	}
	// }
}
