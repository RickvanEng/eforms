import {Component} from '@angular/core';
import {FormFieldComponent} from '../../form-field.component';
import {TextAreaField} from '../TextAreaField';

@Component({
	selector: 'app-large-text-area',
	templateUrl: './large-text-area.component.html',
	styleUrls: ['./large-text-area.component.scss']
})
export class LargeTextAreaComponent extends FormFieldComponent {
	public _component: TextAreaField;
}
