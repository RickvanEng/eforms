import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {LargeTextAreaComponent} from './large-text-area.component';

describe('LargeTextAreaComponent', () => {
  let component: LargeTextAreaComponent;
  let fixture: ComponentFixture<LargeTextAreaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LargeTextAreaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LargeTextAreaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
