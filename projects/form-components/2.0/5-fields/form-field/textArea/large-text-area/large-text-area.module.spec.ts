import {LargeTextAreaModule} from './large-text-area.module';

describe('LargeTextAreaModule', () => {
  let largeTextAreaModule: LargeTextAreaModule;

  beforeEach(() => {
    largeTextAreaModule = new LargeTextAreaModule();
  });

  it('should create an instance', () => {
    expect(largeTextAreaModule).toBeTruthy();
  });
});
