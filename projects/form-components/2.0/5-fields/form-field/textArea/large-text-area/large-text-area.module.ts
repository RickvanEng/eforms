import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {LargeTextAreaComponent} from './large-text-area.component';
import {ReactiveFormsModule} from '@angular/forms';
import {FieldErrors2Module} from '../../../field-errors2/field-errors2.module';

@NgModule({
	imports: [
		CommonModule,
		ReactiveFormsModule,
		FieldErrors2Module
	],
	declarations: [
		LargeTextAreaComponent
	]
})
export class LargeTextAreaModule {
}
