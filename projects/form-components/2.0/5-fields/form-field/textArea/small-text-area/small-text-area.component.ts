import {Component} from '@angular/core';
import {FormFieldComponent} from '../../form-field.component';
import {TextAreaField} from '../TextAreaField';

@Component({
	selector: 'app-small-text-area',
	templateUrl: './small-text-area.component.html',
	styleUrls: ['./small-text-area.component.scss']
})
export class SmallTextAreaComponent extends FormFieldComponent {
	public _component: TextAreaField;
}
