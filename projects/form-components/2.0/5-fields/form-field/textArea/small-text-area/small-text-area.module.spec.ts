import {SmallTextAreaModule} from './small-text-area.module';

describe('SmallTextAreaModule', () => {
  let smallTextAreaModule: SmallTextAreaModule;

  beforeEach(() => {
    smallTextAreaModule = new SmallTextAreaModule();
  });

  it('should create an instance', () => {
    expect(smallTextAreaModule).toBeTruthy();
  });
});
