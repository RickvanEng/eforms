import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {SmallTextAreaComponent} from './small-text-area.component';

describe('SmallTextAreaComponent', () => {
  let component: SmallTextAreaComponent;
  let fixture: ComponentFixture<SmallTextAreaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SmallTextAreaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SmallTextAreaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
