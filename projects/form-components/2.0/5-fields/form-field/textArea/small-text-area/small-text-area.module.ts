import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {SmallTextAreaComponent} from './small-text-area.component';
import {ReactiveFormsModule} from '@angular/forms';
import {FieldErrors2Module} from '../../../field-errors2/field-errors2.module';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { QuestionMarkModule } from '../../question-mark/question-mark.module';

@NgModule({
	imports: [
		CommonModule,
		ReactiveFormsModule,
		QuestionMarkModule,
		FieldErrors2Module
	],
	declarations: [SmallTextAreaComponent]
})
export class SmallTextAreaModule {
}
