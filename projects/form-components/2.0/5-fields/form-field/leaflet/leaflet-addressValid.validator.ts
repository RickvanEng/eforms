import {AbstractControl, ValidatorFn} from '@angular/forms';

/**
 * The required validator for the checkbox component. It counts the FormArray to check if X (minRequired: number) FormControls are true
 * @param minRequired
 * @constructor
 */

export function LeafetAddressValid(): ValidatorFn {
	return function validate(control: AbstractControl) {
		if (control['isAddressValid']) {
			return control['hasBeenClicked'] ? null : {LeafetAddressValid: true};
		} else {
			return control['hasBeenClicked'] ? {LeafetAddressValid: true} : null;
		}
	};
}
