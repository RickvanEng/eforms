import {Component, OnDestroy} from '@angular/core';
import {FormFieldComponent} from '../form-field.component';
import {Leaflet} from './leaflet';
import {LeafletAddress} from './src/leaflet-address';
import * as L from 'leaflet';
import GestureHandling from 'leaflet-gesture-handling';

L.Map.addInitHook("addHandler", "gestureHandling", GestureHandling);

@Component({
	selector: 'app-leaflet',
	templateUrl: './leaflet.component.html',
	styleUrls: ['./leaflet.component.scss']
})
export class LeafletComponent extends FormFieldComponent implements OnDestroy {
	public _component: Leaflet;
	public adres: LeafletAddress;
	public BEValidation: string;
	
	ngOnInit(): void {
		super.ngOnInit();
		
		this._component.selectedAddress.subscribe(adres => {
			this.adres = adres;
		});
		
		this._component.BEvalidation.subscribe(adres => {
			this.BEValidation = adres;
		});
	}
	
	ngOnDestroy(): void {
		this._component
	}
}
