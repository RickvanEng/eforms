import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {LeafletComponent} from './leaflet.component';
import {LeafletModule} from '@asymmetrik/ngx-leaflet';
import {BasicInputFieldActionModule} from '../basic-input-field-action/basic-input-field-action.module';
import {FieldErrors2Module} from '../../field-errors2/field-errors2.module';
import {ReactiveFormsModule} from '@angular/forms';

@NgModule({
	imports: [
		CommonModule,
		LeafletModule,
		BasicInputFieldActionModule,
		FieldErrors2Module,
		ReactiveFormsModule,
	],
	declarations: [
		LeafletComponent
	],
	exports: [
		LeafletComponent
	]
})
export class LeafletFieldModule {
}
