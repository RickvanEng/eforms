import {CircleMarker, circleMarker, LatLng, Map, tileLayer} from 'leaflet';
import {Injector} from '@angular/core';
import {FormField, FormFieldBuilder} from '../FormField';
import {LeafletPointerOptions} from './src/leaflet-pointerOptions';
import {LeafletMapOptions} from './src/leaflet-mapOptions';
import {BehaviorSubject, Observable} from 'rxjs';
import {LeafletAddress} from './src/leaflet-address';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../../src/environments/environment';
import {
	BasicInputFieldAction,
	BasicInputFieldActionBuilder
} from '../basic-input-field-action/basic-input-field-action';
import {Location} from './location';
import {AbstractControl, FormControl, FormGroup} from '@angular/forms';
import {DefaultFormFunctions, DefaultFormFunctionsBuilder} from '../../../base/defaultFormFunctions';
import { EFormsAbstractControl, EFormsFormGroup } from '../../../base/eFormsAbstractControl';

export class LeafletBuilder extends FormFieldBuilder {
	// get inputField(): BasicInputFieldActionBuilder {
	// 	return this._inputField;
	// }
	//
	// public static newFactory(injector: Injector): LeafletBuilder {
	// 	return new LeafletBuilder(injector);
	// }
	//
	// get pointerInfo(): LeafletPointerOptions {
	// 	return this._pointerInfo;
	// }
	//
	// get latLng(): LatLng {
	// 	return this._latLng;
	// }
	//
	// get mapOptions(): LeafletMapOptions {
	// 	return this._mapOptions;
	// }
	//
	// protected _buildingClass: any = Leaflet;
	// private _pointerInfo: LeafletPointerOptions;
	// private _mapOptions: LeafletMapOptions;
	// private _latLng: LatLng;
	// private _inputField: BasicInputFieldActionBuilder;
	//
	// public setPointerOptions(info: LeafletPointerOptions): LeafletBuilder {
	// 	this._pointerInfo = info;
	// 	return this;
	// }
	//
	// public setInputField(field: DefaultFormFunctionsBuilder): LeafletBuilder {
	// 	this._children.push(field);
	// 	this._inputField = field as BasicInputFieldActionBuilder;
	// 	return this;
	// }
	//
	// public setMapOptions(options: LeafletMapOptions): LeafletBuilder {
	// 	this._mapOptions = options;
	// 	return this;
	// }
	//
	// public setLatLng(options: LatLng): LeafletBuilder {
	// 	this._latLng = options;
	// 	return this;
	// }
	
}

export class LatLngXY {
	lat: number;
	lng: number;
	x: number;
	y: number;
}

export class Leaflet extends FormField {
	// get chosenLatLng(): BehaviorSubject<LatLngXY> {
	// 	return this._chosenLatLng;
	// }
	//
	// get BEvalidation(): BehaviorSubject<string> {
	// 	return this._BEvalidation;
	// }
	//
	// get inputField(): BasicInputFieldAction {
	// 	return this._inputField;
	// }
	//
	// get selectedAddress(): BehaviorSubject<LeafletAddress> {
	// 	return this._selectedAddress;
	// }
	//
	// get mapOptions(): LeafletMapOptions {
	// 	return this._mapOptions;
	// }
	//
	// public defaultLatLng: LatLng;
	// private _chosenLatLng: BehaviorSubject<LatLngXY> = new BehaviorSubject(undefined);
	// private _selectedAddress: BehaviorSubject<LeafletAddress> = new BehaviorSubject(undefined);
	// private _BEvalidation: BehaviorSubject<string> = new BehaviorSubject(undefined);
	//
	// private streetMap;
	// private map: Map;
	// private _pointerInfo: LeafletPointerOptions;
	// private _circleMarker: CircleMarker;
	// private _mapOptions: LeafletMapOptions;
	// private _inputField: BasicInputFieldAction;
	//
	// _builder: LeafletBuilder;
	//
	// constructor(b: LeafletBuilder) {
	// 	super(b);
	// 	this.defaultLatLng = b.latLng;
	// 	this._pointerInfo = b.pointerInfo;
	// 	this._circleMarker = circleMarker(this.defaultLatLng, this._pointerInfo);
	// 	this._mapOptions = b.mapOptions;
	// 	this.streetMap = tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {});
	// 	this._mapOptions.setLayers([this.streetMap, this._circleMarker]);
	// }
	//
	// constructFormControl(inj: Injector): EFormsAbstractControl {
	// 	let control: EFormsAbstractControl = super.constructFormControl(inj);
	//
	// 	this._inputField = this._builder.inputField.build();
	//
	// 	this._inputField.abstractControl['isAddressValid'] = false;
	// 	this._inputField.abstractControl['hasBeenClicked'] = false;
	//
	// 	this._inputField.clickedValue.subscribe(value => {
	// 		if (value) {
	// 			this._inputField.abstractControl['hasBeenClicked'] = true;
	// 			this._abstractControl['hasBeenClicked'] = true;
	//
	// 			this.searchByAddress(value).subscribe(res => {
	// 				if (res.responseStatus.code === 0) {
	// 					this._inputField.abstractControl['isAddressValid'] = true;
	//
	// 					const latLngXY = new LatLngXY();
	// 					latLngXY.lat = res.coordinates.lat;
	// 					latLngXY.lng = res.coordinates.lng;
	// 					latLngXY.x = res.coordinates.x;
	// 					latLngXY.y = res.coordinates.y;
	//
	// 					this._chosenLatLng.next(latLngXY);
	// 					this.setAddress(
	// 						res.streetName,
	// 						res.houseNumber.toString(),
	// 						res.postalCode,
	// 						res.city)
	// 				} else {
	// 					this._selectedAddress.next(undefined);
	// 					this._inputField.abstractControl['isAddressValid'] = false;
	// 					// this.BEvalidation.next(res.responseStatus.message);
	// 				}
	// 				this._inputField.abstractControl.updateValueAndValidity();
	// 			});
	// 		}
	// 	});
	//
	// 	this._inputField.getCurrentLocation.subscribe(() => {
	// 		this.getCurrentLocation().then(res => {
	// 			this.searchByCoords(new LatLng(res.lat, res.lng)).subscribe(res => {
	// 				this._abstractControl['hasBeenClicked'] = true;
	//
	// 				const latLngXY = new LatLngXY();
	// 				latLngXY.lat = res.coordinates.lat;
	// 				latLngXY.lng = res.coordinates.lng;
	// 				latLngXY.x = res.coordinates.x;
	// 				latLngXY.y = res.coordinates.y;
	// 				this._chosenLatLng.next(latLngXY);
	// 				this.setAddress(
	// 					res.addresses[0].streetName,
	// 					res.addresses[0].houseNumber,
	// 					res.addresses[0].postalCode,
	// 					res.addresses[0].city)
	// 			}, error1 => {
	// 				console.log(error1);
	// 			});
	// 		})
	// 	});
	//
	// 	return control;
	// }
	//
	// getAbstractControl(inj: Injector): EFormsAbstractControl {
	// 	let control: EFormsFormGroup = new EFormsFormGroup({}, {validators: this.getFormValidators(this._validators)});
	// 	this.children.forEach((child: DefaultFormFunctions) => {
	// 		control.addControl(child.id, child.constructFormControl(inj))
	// 	});
	// 	control['hasBeenClicked'] = false;
	//
	// 	return control;
	// }
	//
	// public onMapReady($event) {
	// 	this.streetMap = tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {});
	// 	this._mapOptions.setLayers([this.streetMap, this._circleMarker]);
	// 	this.map = $event;
	// 	this.setMapPosition(this.defaultLatLng);
	// }
	//
	// public onMapClick($event): void {
	// 	this._chosenLatLng.next($event.latlng);
	// 	this.getAddressByCoords($event.latlng);
	// 	this._abstractControl['hasBeenClicked'] = true;
	// 	this._inputField.abstractControl['hasBeenClicked'] = true;
	// }
	//
	// markAsTouched(): void {
	// 	super.markAsTouched();
	// 	this._abstractControl['hasBeenClicked'] = true;
	// }
	//
	// public setLatLng(latLng: LatLng): void {
	// 	this.setMapPosition(latLng);
	// }
	//
	// getPresentationValue(): { label: string; value: string } {
	// 	return {
	// 		label: this.label,
	// 		value: this._selectedAddress.getValue().street + ' ' + this._selectedAddress.getValue().number
	// 	}
	// }
	//
	// getPdfValue(): any {
	// 	if (!this._sendToBE) {
	// 		return undefined;
	// 	}
	//
	// 	return {
	// 		"veldnaam": this.id,
	// 		"inhoud": this._selectedAddress.getValue().street + ' ' + this._selectedAddress.getValue().number,
	// 		"label": this.label,
	// 		"enkelIntern": this._enkelIntern,
	// 		...this.getIsGeneratedValues(),
	// 	}
	// }
	//
	// private setMapPosition(latlng: LatLng): void {
	// 	this._circleMarker.setLatLng(latlng);
	// 	if (this.map) {
	// 		this.map.panTo(latlng);
	// 	}
	// }
	//
	// private getAddressByCoords(latlng: LatLng): void {
	// 	this.searchByCoords(latlng).subscribe(res => {
	// 		this._inputField.abstractControl['isAddressValid'] = true;
	//
	// 		this.setAddress(
	// 			res.addresses[0].streetName,
	// 			res.addresses[0].houseNumber,
	// 			res.addresses[0].postalCode,
	// 			res.addresses[0].city,
	// 		);
	// 	}, error1 => {
	// 		console.log(error1);
	// 	});
	// }
	//
	// private setAddress(street: string, housenumber: string, postalcode: string, city: string): void {
	// 	let newAddress: LeafletAddress = new LeafletAddress(
	// 		street,
	// 		housenumber,
	// 		postalcode,
	// 		city,
	// 	);
	//
	// 	this._inputField.abstractControl.setValue(newAddress);
	//
	// 	const latLngXY = this._chosenLatLng.getValue();
	// 	this.setMapPosition(new LatLng(latLngXY.lat, latLngXY.lng));
	// 	this.BEvalidation.next(undefined);
	// 	this._inputField.abstractControl.setValue(street + ' ' + housenumber);
	// 	this._selectedAddress.next(newAddress);
	// }
	//
	// private searchByAddress(address: string): Observable<Location> {
	// 	let result = {"streetName": "", "streetHouseNumber": "", "streetHouseNumberSuffix": "", "query": address};
	// 	return this._injector.get(HttpClient).post<Location>(environment.baseUrl + 'eforms-api/controller/bag/search/address', result);
	// }
	//
	// private searchByCoords(latlng: LatLng): Observable<any> {
	// 	let result = {"lat": latlng.lat, "lng": latlng.lng};
	// 	return this._injector.get(HttpClient).post<any>(environment.baseUrl + 'eforms-api/controller/bag/search/geo', result);
	// }
	//
	// private getCurrentLocation(): Promise<LatLngXY> {
	// 	return new Promise((resolve, reject) => {
	// 		navigator.geolocation.getCurrentPosition(resp => {
	// 				const latLngXY = new LatLngXY();
	// 				latLngXY.lat = resp.coords.latitude;
	// 				latLngXY.lng = resp.coords.longitude;
	// 				resolve(latLngXY);
	// 			},
	// 			err => {
	// 				reject(err);
	// 			});
	// 	});
	// }
}
