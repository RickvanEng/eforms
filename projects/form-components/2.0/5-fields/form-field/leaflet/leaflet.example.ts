import {Injector} from '@angular/core';
import {
	ComponentExampleConfigBuilder,
	IComponentExampleConfig
} from '../../../../../component-dash/component-example-config.builder';
import {ComponentExampleBuilder} from '../../../../../component-dash/component-example.builder';
import {GenericFields} from '../../presets/genericFields';
import {BasicInputFieldActionBuilder} from '../basic-input-field-action/basic-input-field-action';
import {ValidatorPreBuildConfigs} from '../../../validators/basic/ValidatorPreBuildConfigs';

export class LeafletExample {
	
	public getField(inj: Injector): IComponentExampleConfig {
		let genericFields: GenericFields = new GenericFields(inj);
		
		return ComponentExampleConfigBuilder.newBuilder()
			.setComponent('Leaflet')
			.setExamples([
				ComponentExampleBuilder.newBuilder()
					.setBuilder(genericFields.leaflet
						.setID('leafet')
						.setInputField(
							BasicInputFieldActionBuilder.newBuilder(inj)
								.setValidation(ValidatorPreBuildConfigs.getInstance().leafletAddressValid())
								.setPlaceholder('Straatnaam 1')
								.setID('StraatHuisnummer')
								.setLabel('Straatnaam en huisnummer')
						)
					)
					.setDescription('test Desc')
					.setState(undefined)
					.setValidateButton(true)
					.data,
			
			])
			.data;
	}
}
