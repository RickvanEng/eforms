export class Location {
	streetName: string;
	houseNumber: number;
	houseNumberSuffix: string;
	postalCode: string;
	city: string;
	coordinates:
		{
			x: number;
			y: number;
			lat: number;
			lng: number;
		};
	responseStatus:
		{
			code: number;
			message: string
		};
}
