export class LeafletAddress {
	get street(): string {
		return this._street;
	}
	
	get number(): string {
		return this._number;
	}
	
	get postcode(): string {
		return this._postcode;
	}
	
	get city(): string {
		return this._city;
	}
	
	private _street: string;
	private _number: string;
	private _postcode: string;
	private _city: string;
	
	constructor(street: string, number: string, postcode: string, city: string) {
		this._street = street;
		this._number = number;
		this._postcode = postcode;
		this._city = city;
	}
}
