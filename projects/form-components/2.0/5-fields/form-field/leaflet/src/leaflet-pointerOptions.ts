export class LeafletPointerOptions {
	get color(): string {
		return this._color;
	}
	
	get fillColor(): string {
		return this._fillColor;
	}
	
	get fillOpacity(): number {
		return this._fillOpacity;
	}
	
	get radius(): number {
		return this._radius;
	}
	
	private _color: string;
	private _fillColor: string;
	private _fillOpacity: number;
	private _radius: number;
	
	constructor(color: string, fillColor: string, fillOpacity: number, radius: number) {
		this._color = color;
		this._fillColor = fillColor;
		this._fillOpacity = fillOpacity;
		this._radius = radius;
	}
}
