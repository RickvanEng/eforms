import {LatLng} from 'leaflet';

export class LeafletMapOptions {
	get layers(): any[] {
		return this._layers;
	}
	
	get zoom(): number {
		return this._zoom;
	}
	
	get center(): LatLng {
		return this._center;
	}
	
	get gestureHandling(): boolean {
		return this._gestureHandling;
	}
	
	private _layers: any[];
	private _zoom: number;
	private _center: LatLng;
	private _gestureHandling: boolean;
	
	constructor(zoom: number, center: LatLng, gestureHandling: boolean) {
		this._zoom = zoom;
		this._center = center;
		this._gestureHandling = gestureHandling;
	}
	
	public setLayers(layers: any[]): void {
		this._layers = layers;
	}
}
