import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {SingleCheckboxComponent} from './single-checkbox.component';
import {ReactiveFormsModule} from '@angular/forms';
import {FieldErrors2Module} from '../../field-errors2/field-errors2.module';

@NgModule({
	imports: [
		CommonModule,
		ReactiveFormsModule,
		FieldErrors2Module
	],
	declarations: [SingleCheckboxComponent],
	exports: [
		SingleCheckboxComponent
	]
})
export class SingleCheckboxModule {
}
