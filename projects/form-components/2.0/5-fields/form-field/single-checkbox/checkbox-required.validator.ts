import {AbstractControl} from '@angular/forms';

export function ChexkboxRequiredValidator(control: AbstractControl) {
	if (!control.value) {
		return { required: true };
	}
	return null;
}
