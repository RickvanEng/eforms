// export class SingleCheckboxBuilder extends FormFieldWithOptionsFactory {
//
// 	public static newBuilder(injector: Injector): SingleCheckboxBuilder {
// 		return new SingleCheckboxBuilder(injector);
// 	}
//
// 	protected _buildingClass: any = SingleCheckbox;
//
// 	private _overVieuwValue: { trueValue: string, falseValue: string } = { trueValue: 'Ja', falseValue: 'Nee' };
// 	get overVieuwValue(): { trueValue: string; falseValue: string } {
// 		return this._overVieuwValue;
// 	}
//
// 	public setOverViewValue(value: { trueValue: string, falseValue: string }): SingleCheckboxBuilder {
// 		this._overVieuwValue = value;
// 		return this;
// 	}
// }

// export class SingleCheckbox extends FormFieldWithOptions {
//
// 	private _overVieuwValue: { trueValue: string, falseValue: string };
//
// 	constructor(builder: SingleCheckboxBuilder) {
// 		super(builder);
// 		this._overVieuwValue = builder.overVieuwValue;
// 	}
//
// 	constructFormControl(inj: Injector): AbstractControl {
// 		let fb: FormBuilder = inj.get(FormBuilder);
// 		let abstractControl: FormArray = fb.array([], [], []);
//
// 		this._options.value.forEach(option => {
// 			abstractControl.push(option.getFormControl(inj))
// 		});
// 		this._abstractControl = abstractControl;
// 		return abstractControl;
// 	}
//
// 	getPdfValue(): any {
// 		if (!this._sendToBE) {
// 			return undefined;
// 		}
//
// 		let hasOptionSelected: boolean = this._options.getValue().some(option => option.isChecked === true);
// 		let tranformedValue: string = hasOptionSelected ? this._overVieuwValue.trueValue : this._overVieuwValue.falseValue;
//
// 		return {
// 			"veldnaam": this.id,
// 			"inhoud": tranformedValue,
// 			"label": this.label,
// 			"enkelIntern": "false",
// 			...this.getIsGeneratedValues(),
// 		}
// 	}
//
// 	// commented because single checkboxes behave the same as normal checkboxes, but just with 1 option
// 	// public getBEValue(): any {
// 	// 	if (!this.sendToBE) {
// 	// 		return undefined;
// 	// 	}
// 	//
// 	// 	const hasOptionSelected: boolean = this._options.getValue().some(option => option.isChecked === true);
// 	// 	return hasOptionSelected ? this._overVieuwValue.trueValue : this._overVieuwValue.falseValue;
// 	// }
//
// 	public getPresentationValue(): { label: string, value: string } {
// 		let hasOptionSelected: boolean = this._options.getValue().some(option => option.isChecked === true);
// 		let tranformedValue: string = hasOptionSelected ? this._overVieuwValue.trueValue : this._overVieuwValue.falseValue;
// 		return {
// 			label: this.label, value: tranformedValue
// 		}
// 	}
// }
