import {SingleCheckboxModule} from './single-checkbox.module';

describe('SingleCheckboxModule', () => {
  let singleCheckboxModule: SingleCheckboxModule;

  beforeEach(() => {
    singleCheckboxModule = new SingleCheckboxModule();
  });

  it('should create an instance', () => {
    expect(singleCheckboxModule).toBeTruthy();
  });
});
