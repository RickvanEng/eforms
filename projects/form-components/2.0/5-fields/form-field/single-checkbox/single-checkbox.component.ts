import {Component} from '@angular/core';
import {MultipleChoiceFieldComponent} from '../multipleChoiceField/multiple-choice-field.component';
import {MultipleChoiceCheckbox} from '../multipleChoiceField/multiple-choice-checkbox/multiple-choice-checkbox';

@Component({
    selector: 'app-single-checkbox',
    templateUrl: './single-checkbox.component.html',
    styleUrls: ['./single-checkbox.component.scss']
})
export class SingleCheckboxComponent extends MultipleChoiceFieldComponent {
    public _component: MultipleChoiceCheckbox;
    // onClickChange(event) {
    // 	this.change(event.target.checked);
    // }
}
