import {Injector} from '@angular/core';
import {
    ComponentExampleConfigBuilder,
    IComponentExampleConfig
} from '../../../../../component-dash/component-example-config.builder';
import {ComponentExampleBuilder} from '../../../../../component-dash/component-example.builder';
import {GenericFields} from '../../presets/genericFields';

export class BasicInputFieldUnitsExample {
    public getField(inj: Injector): IComponentExampleConfig {

        return ComponentExampleConfigBuilder.newBuilder()
            .setComponent('BasicInputFieldUnits')
            .setExamples([
                ComponentExampleBuilder.newBuilder()
                    .setBuilder(new GenericFields(inj).basicFieldUnits
                        .setSuffix('kilometer')
                        .setQuestionMarkTitle('The title of the extra info')
                        .setSaveToState(false)
                        .setLabel('This is a label')
                        .setQuestionMarkText('The extra info text')
                        .setHintText('Example of a hint text')
                        .setID('Example 1')
                    )
                    .setDescription('test Desc')
                    .setState(undefined)
                    .setValidateButton(true)
                    .data,

            ])
            .data;

    }
}
