import {Component} from '@angular/core';
import {BasicInputField2Component} from '../basic-input-field2/basic-input-field2.component';
import {BasicInputFieldUnits} from './basic-input-field-units';

@Component({
	selector: 'app-basic-input-field-units',
	templateUrl: './basic-input-field-units.component.html',
	styleUrls: ['./basic-input-field-units.component.scss']
})
export class BasicInputFieldUnitComponent extends BasicInputField2Component {
	public _component: BasicInputFieldUnits;
}
