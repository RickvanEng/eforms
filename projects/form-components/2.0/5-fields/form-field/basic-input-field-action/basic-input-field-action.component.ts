import {Component} from '@angular/core';
import {BasicInputField2Component} from '../basic-input-field2/basic-input-field2.component';
import {BasicInputFieldAction} from './basic-input-field-action';

@Component({
	selector: 'app-basic-input-field-action',
	templateUrl: './basic-input-field-action.component.html',
	styleUrls: ['./basic-input-field-action.component.scss']
})
export class BasicInputFieldActionComponent extends BasicInputField2Component {
	public _component: BasicInputFieldAction;
}
