import {BasicInputFieldActionModule} from './basic-input-field-action.module';

describe('BasicInputFieldActionModule', () => {
  let basicInputFieldActionModule: BasicInputFieldActionModule;

  beforeEach(() => {
    basicInputFieldActionModule = new BasicInputFieldActionModule();
  });

  it('should create an instance', () => {
    expect(basicInputFieldActionModule).toBeTruthy();
  });
});
