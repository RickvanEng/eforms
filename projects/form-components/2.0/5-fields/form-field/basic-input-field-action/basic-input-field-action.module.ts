import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {BasicInputFieldActionComponent} from './basic-input-field-action.component';
import {QuestionMarkModule} from '../question-mark/question-mark.module';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {ReactiveFormsModule} from '@angular/forms';
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';
import {FieldErrors2Module} from '../../field-errors2/field-errors2.module';

@NgModule({
	imports: [
		CommonModule,
		QuestionMarkModule,
		NgbModule,
		ReactiveFormsModule,
		FontAwesomeModule,
		FieldErrors2Module,
	],
	exports: [
		BasicInputFieldActionComponent
	],
	declarations: [
		BasicInputFieldActionComponent
	]
})
export class BasicInputFieldActionModule {
}
