import {FormField, FormFieldBuilder, IFormField} from '../FormField';
import {Injector} from '@angular/core';
import {BehaviorSubject, Subject} from 'rxjs';
import {IBase} from "../../../base/IBase";

export interface IInputFieldAction extends IFormField {
	placeholder: string;
}

export class BasicInputFieldActionBuilder extends FormFieldBuilder {

	protected _config: IInputFieldAction = {
		buildingClass: 'FormField',
		children: [],
		component: undefined,
		id: '',
		label: '',
		preInits: [],
		sendToBE: false,
		asyncValidators: [],
		basicValidators: [],
		saveToState: false,
		showCondition: {
			all: [],
			single: [],
		},
		showInOverviewSet: false,
		icon: '',
		readonly: false,
		questionMark: undefined,
		hintText: '',
		defaultValue: '',
		placeholder: '',
	};

	public static newBuilder(): BasicInputFieldActionBuilder {
		return new BasicInputFieldActionBuilder();
	}

	public setPlaceholder(placeholder: string): BasicInputFieldActionBuilder {
		this._config.placeholder = placeholder;
		return this;
	}

	protected _buildingClass: any = BasicInputFieldAction;
}

// TODO make click etc configurable
export class BasicInputFieldAction extends FormField {
	
	public get getCurrentLocation(): Subject<any> {
		return this._getCurrentLocation;
	}
	
	public get clickedValue(): BehaviorSubject<string> {
		return this._clickedValue;
	}
	
	public setCurrentLocation(): void {
		this._getCurrentLocation.next(true);
	}
	
	public get placeholder(): string {
		return this._config.placeholder;
	}
	
	private _clickedValue:  BehaviorSubject<string> = new BehaviorSubject(undefined);
	private _getCurrentLocation: Subject<any> = new Subject();
	_config: IInputFieldAction;
	
	constructor(b: IInputFieldAction, inj: Injector) {
		super(b, inj);
	}
	
	public onClick(): void {
		this._clickedValue.next(this._abstractControl.value);
	}
}
