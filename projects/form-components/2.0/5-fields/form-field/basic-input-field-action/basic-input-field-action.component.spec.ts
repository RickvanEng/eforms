import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {BasicInputFieldActionComponent} from './basic-input-field-action.component';

describe('BasicInputFieldActionComponent', () => {
  let component: BasicInputFieldActionComponent;
  let fixture: ComponentFixture<BasicInputFieldActionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BasicInputFieldActionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BasicInputFieldActionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
