import {ButtonAction} from './buttonAction';

export class ButtonActionSendBE extends ButtonAction {
    action(value: any): Promise<any> {
        return new Promise<any>(resolve => {
            console.warn('Override this method!');
            resolve();
        });
    }
}
