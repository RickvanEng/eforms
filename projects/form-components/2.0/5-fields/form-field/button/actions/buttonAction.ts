export class ButtonAction {
    public action(value: any): Promise<any> {
        return new Promise<any>(resolve => {
            console.warn('Override this method!');
            resolve();
        });
    }
}
