import {Component} from '@angular/core';
import {Button} from "./button";
import {FormFieldComponent} from "../form-field.component";
import {faPaperclip, faTrashAlt} from '@fortawesome/free-solid-svg-icons';
@Component({
	selector: 'app-button',
	templateUrl: './button.component.html',
	styleUrls: ['./button.component.scss']
})
export class ButtonComponent extends FormFieldComponent {
	public _component: Button;
}
