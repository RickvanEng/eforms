import {FormField, FormFieldBuilder} from "../FormField";
import {Injector} from "@angular/core";

export class ButtonBuilder extends FormFieldBuilder {
	// get buttonAllignment(): string {
	// 	return this._buttonAllignment;
	// }
	// get buttonClass(): string {
	// 	return this._buttonClass;
	// }
	// get image(): { path: string } {
	// 	return this._image;
	// }
	// get buttonText(): string {
	// 	return this._buttonText;
	// }
	//
	// get action(): any {
	// 	return this._action;
	// }
	//
	// public static newBuilder(injector: Injector): ButtonBuilder {
	// 	return new ButtonBuilder(injector);
	// }
	//
	// protected _buildingClass: any = Button;
	// protected _action: any;
	// private _image: {path: string};
	// private _buttonText: string;
	// private _buttonClass: string = 'btn btn-primary';
	// private _buttonAllignment: string = 'offset-lg-5 col-lg-6 col-md-12 col-sm-12 mb-3';
	//
	// public setButtonClass(classString: string): ButtonBuilder {
	// 	this._buttonClass = classString;
	// 	return this;
	// }
	//
	// public setButtonAllignment(classString: string): ButtonBuilder {
	// 	this._buttonAllignment = classString;
	// 	return this;
	// }
	//
	// public setAction(action: any): ButtonBuilder {
	// 	this._action = action;
	// 	return this;
	// }
	//
	// public setImage(imageInfo: {path: string}): ButtonBuilder {
	// 	this._image = imageInfo;
	// 	return this;
	// }
	//
	// public setButtonText(buttonText: string): ButtonBuilder {
	// 	this._buttonText = buttonText;
	// 	return this;
	// }
}

export class Button extends FormField {
	// get buttonAllignment(): string {
	// 	return this._buttonAllignment;
	// }
	// get buttonClass(): string {
	// 	return this._buttonClass;
	// }
	// get image(): { path: string } {
	// 	return this._image;
	// }
	//
	// get buttonText(): string {
	// 	return this._buttonText;
	// }
	//
	// private _action: () => any;
	// private _image: {path: string};
	// private _buttonText: string;
	// private _buttonClass: string;
	// private _buttonAllignment: string;
	//
	// constructor(builder: ButtonBuilder) {
	// 	super(builder);
	// 	this._action = builder.action;
	// 	this._image = builder.image;
	// 	this._buttonText = builder.buttonText;
	// 	this._buttonClass = builder.buttonClass;
	// 	this._buttonAllignment = builder.buttonAllignment;
	// }
	//
	// registerComponent(): void {
	//
	// }
	//
	// public click(): void {
	// 	if (this._action) {
	// 		this._action();
	// 	}
	// 	this._trigger.next(true);
	// }
}
