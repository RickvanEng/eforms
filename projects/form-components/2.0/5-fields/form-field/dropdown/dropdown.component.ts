import {Component} from '@angular/core';
import {DropdownNormal} from './dropdownNormal';
import {MultipleChoiceFieldComponent} from '../multipleChoiceField/multiple-choice-field.component';

@Component({
    selector: 'app-dropdown',
    templateUrl: './dropdown.component.html',
    styleUrls: ['./dropdown.component.scss']
})
export class DropdownComponent extends MultipleChoiceFieldComponent {
    public _component: DropdownNormal;
}
