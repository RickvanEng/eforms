import {Injector} from '@angular/core';
import {
    ComponentExampleConfigBuilder,
    IComponentExampleConfig
} from '../../../../../component-dash/component-example-config.builder';
import {ComponentExampleBuilder} from '../../../../../component-dash/component-example.builder';
import {GenericFields} from '../../presets/genericFields';
import {FormFieldWithOptionsFactory} from '../multipleChoiceField/MultipleChoiceField';
import {FormFieldOptionFactory} from '../multipleChoiceField/options/FieldOption';

export class DropdownExample {
    public getField(inj: Injector): IComponentExampleConfig {

        return ComponentExampleConfigBuilder.newBuilder()
            .setComponent('Dropdown')
            .setExamples([
                ComponentExampleBuilder.newBuilder()
                    .setBuilder(new GenericFields(inj).dropdownNormal
                        .setValidatorRequired()
                        .setOptions([
                            new FormFieldOptionFactory()
                                .setId('option1')
                                .setData({value: 'option 1'})
                                .build()
                        ])
                        .setQuestionMarkTitle('The title of the extra info')
                        .setSaveToState(false)
                        .setLabel('This is a label')
                        .setQuestionMarkText('The extra info text')
                        .setHintText('Example of a hint text')
                        .setID('Example 1')
                    )
                    .setDescription('The dropdown (select) component' +
                        '<ul>' +
                        '<li>Should show a label</li>' +
                        '<li>Should show a select area</li>' +
                        '<li>Should show a questionmark info text</li>' +
                        '<li>Should show a hint text</li>' +
                        '<li>Should show a option when clicked</li>' +
                        '<li>Should not be able to select the default option</li>' +
                        '<li>Should not be valid if the default is selected</li>' +
                        '</ul>'
                    )
                    .setState(undefined)
                    .setValidateButton(true)
                    .data,

            ])
            .data;

    }
}
