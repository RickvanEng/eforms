import {Injector} from "@angular/core";
import {FormFieldOption, FormFieldOptionFactory} from '../multipleChoiceField/options/FieldOption';
import {FormFieldWithOptions, FormFieldWithOptionsFactory} from '../multipleChoiceField/MultipleChoiceField';

// TODO zet deze in dezelfde map als andere multipleChoice velden

export class DropdownNormalBuilder extends FormFieldWithOptionsFactory {
	
	// public static newFactory(injector: Injector): DropdownNormalBuilder {
	// 	return new DropdownNormalBuilder(injector);
	// }
	//
	// protected _buildingClass: any = DropdownNormal;
}

export class DropdownNormal extends FormFieldWithOptions {
	
	// get no_selection(): FormFieldOption {
	// 	return this._no_selection;
	// }
	//
	// private _no_selection: FormFieldOption = FormFieldOptionFactory.newFactory()
	// 	.setId('no_selection')
	// 	.setData({value: 'selecteer een optie'})
	// 	.build();
	//
	// constructor(builder: DropdownNormalBuilder) {
	// 	super(builder);
	// }
	//
	// getPresentationValue(): { label: string, value: string } {
	//
	// 	if (this._abstractControl.value) {
	// 		let foundOption: FormFieldOption = this._options.getValue().find(option => option.id === this._abstractControl.value);
	// 		if (foundOption) {
	// 			return {
	// 				label: this.label, value: foundOption.data.value
	// 			}
	// 		}
	// 	}
	//
	// 	return {
	// 		label: this.label, value: ''
	// 	}
	// }
	//
	// getTechnicalValue(): any {
	// 	if (!this.sendToBE) {
	// 		return undefined;
	// 	}
	//
	// 	let result: any = {};
	// 	result[this._id] = this._abstractControl.value;
	// 	return result;
	// }
	//
	// public prefillValue(value: any): void {
	// 	this._abstractControl.setValue(value)
	// }
	//
	// isOptionChecked(optionID: string): boolean {
	// 	return this._abstractControl.value === optionID;
	// }
}
