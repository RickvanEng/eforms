import {Injector} from '@angular/core';
import {DefaultFormFunctions, DefaultFormFunctionsBuilder,} from '../../base/defaultFormFunctions';
import {BehaviorSubject} from 'rxjs';
import {EBasicValidators} from '../../validators/basic/basicValidatorMap';
import {IValidator} from '../../validators/IValidator';
import {EFormsAbstractControl, EFormsFormControl} from '../../base/eFormsAbstractControl';
import {IBase} from "../../base/IBase";

export interface IFormField extends IBase {
    icon: string;
    readonly: boolean;
    questionMark: any;
    hintText: string;
    defaultValue: string;
}

export class FormFieldBuilder extends DefaultFormFunctionsBuilder {

    _config: IFormField = {
        className: '',
        buildingClass: 'FormField',
        children: [],
        component: undefined,
        id: '',
        label: '',
        preInits: [],
        sendToBE: false,
        asyncValidators: [],
        basicValidators: [],
        saveToState: false,
        showCondition: {
            all: [],
            single: [],
        },
        showInOverviewSet: false,
        icon: '',
        readonly: false,
        questionMark: undefined,
        hintText: '',
        defaultValue: '',
    };

    public clearBasicValidators(): FormFieldBuilder {
        this._config.basicValidators = [];
        return this;
    }

    public clearAsyncValidators(): FormFieldBuilder {
        this._config.asyncValidators = [];
        return this;
    }

    public setDefaultValue(value: string): FormFieldBuilder {
        this._config.defaultValue = value;
        return this;
    }

    public static newBuilder(): FormFieldBuilder {
        return new FormFieldBuilder();
    }

    public setIcon(icon: string): FormFieldBuilder {
        this._config.icon = icon;
        return this;
    }

    public setReadonly(readOnly: boolean): FormFieldBuilder {
        this._config.readonly = readOnly;
        return this;
    }

    public setHintText(hintText: string): FormFieldBuilder {
        this._config.hintText = hintText;
        return this;
    }

    public setQuestionMark(questionMark: any): FormFieldBuilder {
        this._config.questionMark = questionMark;
        return this;
    }

    build(): IFormField {
        return this._config;
    }
}

export class FormField extends DefaultFormFunctions {
    get showQuestionMark(): boolean {
        return this._config.questionMark;
    }

    get icon(): string {
        return this._config.icon;
    }

    get required(): string {
        let foundValidation: IValidator = this._validators.find(val => val.validatorId === EBasicValidators.REQUIRED);
        return foundValidation ? 'required' : '';
    }

    get readOnly(): boolean {
        return this._config.readonly;
    }

    get hintText(): string {
        return this._config.hintText;
    }

    get keyup(): BehaviorSubject<{ keyCode: number, value: string }> {
        return this._keyup;
    }

    _config: IFormField;
    private _keyup: BehaviorSubject<{ keyCode: number, value: string }> = new BehaviorSubject(null);

    getAbstractControl(inj: Injector): EFormsAbstractControl {
        return new EFormsFormControl(this._config.defaultValue, {
            // validators: this.getFormValidators(this._validators),
            // asyncValidators: this.getAsyncValidators(this._injector, this._asyncValidators)
        });
    }

    getValue(): any {
        return this._abstractControl.value;
    }

    clear(): void {
        this._abstractControl.reset();
    }

    getPdfValue(): any {
        if (!this._sendToBE) {
            return undefined;
        }

        return {
            "veldnaam": this.id,
            "inhoud": this._abstractControl.value,
            "label": this.label,
        }
    }

    /**
     * hides the field and also disables the control so it wont get validated
     */
    checkForShow(): void {
        // TODO Fix the hide if the field is readonly and has no value. Does not work with prefills yet
        // let result: boolean = (this._showConditionsMet.getValue() && this._parentIsVisible.getValue() && !(this.readOnly && this._abstractControl.value === null));
        let result: boolean = (this._showConditionsMet.getValue() && this._parentIsVisible.getValue());
        this.setShow(result);
        // (result && !this.readOnly) ? this._abstractControl.enable() : this._abstractControl.disable()
    }

    getTechnicalValue(): any {
        if (!this.sendToBE) {
            return undefined;
        }

        let result: any = {};
        result[this._id] = this._abstractControl.value;
        return result;
    }

    getPresentationValue(): { label: string, value: string } {
        if (!this._abstractControl) {
            return {
                label: this.label, value: ''
            }
        }

        return {
            label: this.label, value: this._abstractControl.value
        }
    }
}
