import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {BasicInputField2Component} from './basic-input-field2.component';

describe('BasicInputField2Component', () => {
  let component: BasicInputField2Component;
  let fixture: ComponentFixture<BasicInputField2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BasicInputField2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BasicInputField2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
