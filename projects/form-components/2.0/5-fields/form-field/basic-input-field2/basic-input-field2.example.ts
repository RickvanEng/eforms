import {Injector} from '@angular/core';
import {GenericFields} from '../../presets/genericFields';
import {
    ComponentExampleConfigBuilder,
    IComponentExampleConfig
} from '../../../../../component-dash/component-example-config.builder';
import {ComponentExampleBuilder} from '../../../../../component-dash/component-example.builder';
import {ValidatorPreBuildConfigs} from '../../../validators/basic/ValidatorPreBuildConfigs';
import {AsyncValidatorPrebuildConfig} from '../../../validators/async/asyncValidatorPrebuildConfig';

export class BasicInputField2Example {
    public getField(inj: Injector): IComponentExampleConfig {

        return ComponentExampleConfigBuilder.newBuilder()
            .setComponent('BasicFormField')
            .setExamples([
                ComponentExampleBuilder.newBuilder()
                    .setBuilder(new GenericFields(inj).basicField
                        .setIcon('phone')
                        .setQuestionMarkTitle('The title of the extra info')
                        .setSaveToState(false)
                        .setValidation(ValidatorPreBuildConfigs.getInstance().required())
                        .setValidatorRequired()
                        .setLabel('This is a label')
                        .setQuestionMarkText('The extra info text')
                        .setHintText('Example of a hint text')
                        .setID('Example 1')
                    )
                    .setDescription('The basic input field.' +
                        '<ul>' +
                        '<li>Should show a label</li>' +
                        '<li>Should show a input area</li>' +
                        '<li>Should show a questionmark info text</li>' +
                        '<li>Should show a hint text</li>' +
                        '<li>Should show a icon</li>' +
                        '<li>It should show an FORM error when one is thrown (required)</li>' +
                        '</ul>'
                    )
                    .setState(undefined)
                    .setValidateButton(true)
                    .data,
                ComponentExampleBuilder.newBuilder()
                    .setBuilder(new GenericFields(inj).iban
                        .setAsyncValidation(AsyncValidatorPrebuildConfig.getInstance().iban())
                        .setLabel('iban')
                        .setSaveToState(false)
                        .setID('Example2')
                    )
                    .setDescription('Custom validations should only be done if the angular form validation is valid. Example: <ul>' +
                        '<li>The iban field has 2 validations</li>' +
                        '<li>A angular form pattern form validation</li>' +
                        '<li>A custom validation that makes a call to the Back-end</li>' +
                        '</ul>' +
                        'The latter should only be done when the angular form pattern is valid')
                    .setState(undefined)
                    .setValidateButton(true)
                    .data,
            ])
            .data;
    }
}
