import {Component, forwardRef} from '@angular/core';
import {FormFieldComponent} from '../form-field.component';
import {faQuestionCircle} from '@fortawesome/free-regular-svg-icons';
import {NG_VALUE_ACCESSOR} from '@angular/forms';

@Component({
	selector: 'basic-input-field2',
	templateUrl: './basic-input-field2.component.html',
	styleUrls: ['./basic-input-field2.component.scss'],
	providers: [{
		provide: NG_VALUE_ACCESSOR,
		useExisting: forwardRef(() => BasicInputField2Component),
		multi: true
	}]
})

export class BasicInputField2Component extends FormFieldComponent {
	public faQuestionCircle = faQuestionCircle;
	
	public keyup($event): void {
		this._component.keyup.next({keyCode: $event.keyCode, value: $event.target.value});
	}
}
