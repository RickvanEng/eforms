import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {BasicInputField2Component} from './basic-input-field2.component';
import {ReactiveFormsModule} from '@angular/forms';
import {FieldErrors2Module} from '../../field-errors2/field-errors2.module';
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { QuestionMarkModule } from "../question-mark/question-mark.module";
import {FieldLabelModule} from '../../../../../../src/app/components/field-label/field-label.module';
import {HintTextModule} from '../../../../../../src/app/components/hint-text/hint-text.module';

@NgModule({
    imports: [
        CommonModule,
        QuestionMarkModule,
        NgbModule,
        ReactiveFormsModule,
        FontAwesomeModule,
        FieldErrors2Module,
        FieldLabelModule,
        HintTextModule
    ],
	exports: [
		BasicInputField2Component
	],
	declarations: [
		BasicInputField2Component
	]
})
export class BasicInputField2Module {
}
