import {Injector} from '@angular/core';
import {
	ComponentExampleConfigBuilder,
	IComponentExampleConfig
} from '../../../../../component-dash/component-example-config.builder';
import {ComponentExampleBuilder} from '../../../../../component-dash/component-example.builder';
import {GenericFields} from '../../presets/genericFields';

export class TimePickerExample {
    public getField(inj: Injector): IComponentExampleConfig {

        return ComponentExampleConfigBuilder.newBuilder()
            .setComponent('TimePickerField')
            .setExamples([
                ComponentExampleBuilder.newBuilder()
                    .setBuilder(new GenericFields(inj).timePicker
                        .setValidatorRequired()
                        .setSaveToState(false)
                        .setLabel('Timepicker label')
                        .setQuestionMarkTitle('The title of the extra info')
                        .setQuestionMarkText('The extra info text')
                        .setHintText('Example of a hint text')
                        .setID('TimepickerExample1')
                    )
                    .setDescription('test Desc')
                    .setState(undefined)
                    .setValidateButton(true)
                    .data,

            ])
            .data;

    }
}
