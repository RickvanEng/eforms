import {TimePickerModule} from './time-picker.module';

describe('DatePicker2Module', () => {
  let timePicker2Module: TimePickerModule;

  beforeEach(() => {
    timePicker2Module = new TimePickerModule();
  });

  it('should create an instance', () => {
    expect(timePicker2Module).toBeTruthy();
  });
});
