import {FormField, FormFieldBuilder} from '../FormField';
import {Injector} from '@angular/core';

export class TimePickerFieldFactory extends FormFieldBuilder {
	
	// public static newBuilder(injector: Injector): TimePickerFieldFactory {
	// 	return new TimePickerFieldFactory(injector);
	// }
	//
	// protected _buildingClass: any = TimePickerField;
}

export class TimePickerField extends FormField {
	
	// constructor(builder: TimePickerFieldFactory) {
	// 	super(builder);
	// }
	//
	// getPresentationValue(): { label: string, value: string } {
	// 	return {
	// 		label: this.label,
	// 		value: this.getTimeValue()
	// 	}
	// }
	//
	// prefillValue(value: any): void {
	// 	let time: { hour: number, minute: number } = {hour: undefined, minute: undefined};
	// 	const [hours, minutes] = value.split(':');
	// 	time.hour = Number(hours);
	// 	time.minute = Number(minutes);
	// 	this._abstractControl.setValue(time);
	// }
	//
	// getPdfValue(): any {
	// 	if (!this.sendToBE) {
	// 		return undefined;
	// 	}
	//
	// 	return {
	// 		"veldnaam": this.id,
	// 		"inhoud": this.getTimeValue(),
	// 		"label": this.label,
	// 		"enkelIntern": "false"
	// 	}
	// }
	//
	// getTechnicalValue(): any {
	// 	let result: any = {};
	// 	result[this._id] = this.getBEValue();
	// 	return result;
	// }
	//
	// private getBEValue(): any {
	// 	if (!this.sendToBE) {
	// 		return undefined;
	// 	}
	// 	return this.getTimeValue();
	// }
	//
	// private getTimeValue(): string {
	// 	if (this._abstractControl.value) {
	// 		return this._abstractControl.value.hour.toString().padStart(2, '0') + ':' + this._abstractControl.value.minute.toString().padStart(2, '0');
	// 	}
	// 	return '';
	// }
	//
	// getDefaultValue(): any {
	// 	return '10:10';
	// }
	
}
