import {Component, forwardRef} from '@angular/core';
import {FormFieldComponent} from '../form-field.component';
import {NgbDateStruct} from '@ng-bootstrap/ng-bootstrap';
import {NG_VALUE_ACCESSOR} from '@angular/forms';
import {TimePickerField} from './TimePickerField';

@Component({
    selector: 'app-time-picker',
    templateUrl: './time-picker.component.html',
    styleUrls: ['./time-picker.component.scss'],
    providers: [
        {provide: NG_VALUE_ACCESSOR, useExisting: forwardRef(() => TimePickerComponent), multi: true}
    ]
})
export class TimePickerComponent extends FormFieldComponent {
    model: NgbDateStruct;
    public _component: TimePickerField;
}
