import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {TimePickerComponent} from './time-picker.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';
import {BrowserModule} from '@angular/platform-browser';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {FieldErrors2Module} from '../../field-errors2/field-errors2.module';
import { QuestionMarkModule } from '../question-mark/question-mark.module';

@NgModule({
	imports: [
		ReactiveFormsModule,
		FormsModule,
		CommonModule,
		BrowserModule,
		NgbModule,
		FontAwesomeModule,
		FieldErrors2Module,
		QuestionMarkModule,
	],
	providers: [],
	declarations: [TimePickerComponent]
})
export class TimePickerModule {
}
