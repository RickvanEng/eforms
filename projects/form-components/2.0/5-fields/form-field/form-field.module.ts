import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormFieldComponent} from './form-field.component';
import {DynamicFieldDirective} from './dynamicField.directive';
import {BasicInputField2Module} from './basic-input-field2/basic-input-field2.module';
import {BasicInputFieldActionModule} from './basic-input-field-action/basic-input-field-action.module';
import {BasicInputFieldUnitModule} from './basic-input-field-units/basic-input-field-units.module';
import {ButtonModule} from './button/button.module';
import {DatePicker2Module} from './date-picker2/date-picker2.module';
import {DropdownModule} from './dropdown/dropdown.module';
import {FileUploadModule} from './file-upload/file-upload.module';
import {LabelFieldModule} from './label-field/label-field.module';
import {MobStatusFieldModule} from './mob-status/mob-status.module';
import {MultipleChoiceFieldModule} from './multipleChoiceField/multiple-choice-field.module';
import {QuestionMarkModule} from './question-mark/question-mark.module';
import {SingleCheckboxModule} from './single-checkbox/single-checkbox.module';
import {LargeTextAreaModule} from './textArea/large-text-area/large-text-area.module';
import {SmallTextAreaModule} from './textArea/small-text-area/small-text-area.module';
import {TimePickerModule} from './time-picker/time-picker.module';
import {LeafletFieldModule} from './leaflet/leaflet.module';

@NgModule({
	imports: [
		CommonModule,
		BasicInputField2Module,
		BasicInputFieldActionModule,
		BasicInputFieldUnitModule,
		ButtonModule,
		DatePicker2Module,
		DropdownModule,
		FileUploadModule,
		LabelFieldModule,
		LeafletFieldModule,
		MobStatusFieldModule,
		MultipleChoiceFieldModule,
		QuestionMarkModule,
		SingleCheckboxModule,
		LargeTextAreaModule,
		SmallTextAreaModule,
		TimePickerModule,
	],
	declarations: [
		FormFieldComponent,
		DynamicFieldDirective,
	],
	exports: [
		FormFieldComponent,
		DynamicFieldDirective,
	]
})
export class FormFieldModule {
}
