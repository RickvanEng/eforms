import {Component, Injector} from '@angular/core';
import {faPaperclip, faTrashAlt} from "@fortawesome/free-solid-svg-icons";
import {faFile, faQuestionCircle} from "@fortawesome/free-regular-svg-icons";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Observable} from "rxjs";
import {environment} from "../../../../../../src/environments/environment";
import {FormFieldComponent} from "../form-field.component";
import {FileUpload} from "./file-upload";
import {FileIDLabel, FileUpload2Service} from './file-upload2.service';
import {FormService} from '../../../1-form/form.service';

@Component({
	selector: 'app-file-upload',
	templateUrl: './file-upload.component.html',
	styleUrls: ['./file-upload.component.scss']
})
export class FileUploadComponent extends FormFieldComponent {
	
	public _component: FileUpload;
	
	public faPaperclip = faPaperclip;
	public faTrashAlt = faTrashAlt;
	public faFile = faFile;
	public faQuestionCircle = faQuestionCircle;
	public warning: string;
	
	constructor(protected injector: Injector,
				private fileUploadService: FileUpload2Service,
				private http: HttpClient) {
		super(injector);
	}
	
	ngOnInit(): void {
		super.ngOnInit();
	}
	
	public removeFile(file: FileIDLabel): void {
		// remove file from list
		this._component.files.splice(this._component.files.indexOf(file), 1);
		
		//make new call to check mbs
		let body: any = {bijlagen: [], id: 'contact'};
		for (let file of this.fileUploadService.getbijlageIDs()) {
			body.bijlagen.push({id: file.id});
		}
		this.validateFiles(body).subscribe(value => {
			this.warning = undefined;
			this.fileUploadService.remainingMbs = value.statusTekst;
		}, error1 => {
			console.error(error1);
		});
		
		this.registerOnChange(this._component.files);
	}
	
	public addFile(files: FileList): void {
		this.warning = undefined;
		
		let formID: string = FormService.getInstance().selectedForm.id;
		
		let arraySize: number = 0;
		for (let i = 0; i < files.length; i++) {
			arraySize = arraySize + files.item(i).size;
		}
		
		this.warning = undefined;
		this.fileUploadService.uploadFiles(this.http, files, formID).subscribe(response => {
				// Keeps track of new added files
				let newFiles: FileIDLabel[] = [];
				for (let i = 0; i < files.length; i++) {
					let newFile: FileIDLabel = new FileIDLabel(response.statusTekst[i], this._component.id, files.item(i).name, '');
					newFiles.push(newFile);
				}
				
				//  combines new files with already uploaded files for MB check
				let body: any = {bijlagen: [], id: ''};
				for (let file of this.fileUploadService.getbijlageIDs()) {
					body.bijlagen.push({id: file.id});
				}
				
				for (let file of newFiles) {
					body.bijlagen.push({id: file.ID});
				}
				
				this.validateFiles(body).subscribe(value => {
					// if no error comes back, add newFiles to field
					for (let file of newFiles) {
						this._component.files.push(file);
					}
					this.fileUploadService.remainingMbs = value.statusTekst;
					this._component.abstractControl.setValue(this._component.files);
				}, error1 => {
					// this.warning = 'De foto of het document is niet toegevoegd, omdat de limiet van 20MB is bereikt.';
					this.warning = error1.error.statusTekst;
					this.warning = 'De limiet om foto’s of documenten te uploaden is bereikt. Uw laatste foto of document is niet opgeslagen.';
					console.error(error1);
				});
			},
			error => {
				this.warning = error.error.statusTekst;
				console.error(error);
			}
		);
	}
	
	private validateFiles(body: any): Observable<any> {
		let defaultHeaders = {
			'Content-Type': 'application/json; charset=UTF-8',
		};
		
		const httpOptions = {
			headers: new HttpHeaders(defaultHeaders)
		};
		
		body['id'] = FormService.getInstance().selectedForm.id;
		
		return this.http.post(environment.baseUrl + 'eforms-api/controller/validation/upload-remaining', body, httpOptions);
	}
}
