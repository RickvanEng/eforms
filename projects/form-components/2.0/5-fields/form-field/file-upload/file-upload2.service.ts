import {Injectable} from '@angular/core';
import {environment} from '../../../../../../src/environments/environment';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {FileUpload} from './file-upload';
import {FormControl, FormGroup, Validators} from '@angular/forms';

export class FileAndID {
	ID: string;
	fieldName: string;
	fileName: string;

	constructor(ID: string, fieldName: string, fileName: string) {
		this.ID = ID;
		this.fieldName = fieldName;
		this.fileName = fileName;
	}
}


export class FileIDLabel {
	ID: string;
	fieldName: string;
	fileName: string;
	fileLabel: string;

	labelForm: FormGroup;

	constructor(ID: string, fieldName: string, fileName: string, fileLabel: string) {
		this.ID = ID;
		this.fieldName = fieldName;
		this.fileName = fileName;
		this.fileLabel = fileLabel;

		this.labelForm = new FormGroup({
			label: new FormControl('', [])
		});
	}
}

@Injectable({
	providedIn: 'root'
})
export class FileUpload2Service {
	private baseUrl = environment.baseUrl;
	
	public remainingMbs: string;
	
	public registerdFields: FileUpload[] = [];
	
	public registerField(field: FileUpload): void {
		if (this.registerdFields.indexOf(field) === -1) {
			this.registerdFields.push(field);
		}
	}
	
	public unRegisterField(field: FileUpload): void {
		if (this.registerdFields.indexOf(field) !== -1) {
			this.registerdFields.splice(this.registerdFields.indexOf(field));
		}
	}
	
	public getbijlageIDs(): { id: string, fieldName: string, omschrijving: string }[] {
		let results: { id: string, fieldName: string, omschrijving: string }[] = [];
		for (let field of this.registerdFields) {
			results = results.concat(field.getUploads());
		}
		return results;
	}
	
	uploadFiles(http: HttpClient, files: FileList, id: string): Observable<any> {
		let url = this.baseUrl + 'eforms-api/controller/upload/files';
		const formData: FormData = new FormData();
		for (let i = 0; i < files.length; i++) {
			formData.append('bestand', files.item(i), files.item(i).name);
		}
		formData.append('id', id);
		return http.post(url, formData);
	}
}
