import {FormField, FormFieldBuilder} from "../FormField";
import {Injector} from "@angular/core";
import {FileAndID, FileIDLabel, FileUpload2Service} from './file-upload2.service';

export class FileUploadBuilder extends FormFieldBuilder {

    // public static newBuilder(injector: Injector): FileUploadBuilder {
    //     return new FileUploadBuilder(injector);
    // }
    //
    // protected _buildingClass: any = FileUpload;
}

export class FileUpload extends FormField {

    // public files: FileIDLabel[] = [];
    //
    // constructor(builder: FileUploadBuilder) {
    //     super(builder);
    //     this._injector.get(FileUpload2Service).registerField(this);
    // }
    //
    // public getUploads():{id: string, fieldName: string, omschrijving: string}[] {
    //     let result: {id: string, fieldName: string, omschrijving: ''}[] = [];
    //
    //     this.files.forEach(file => {
    //         result = result.concat({id: file.ID, fieldName: this._id, omschrijving: ''});
    //     });
    //
    //     return result;
    // }
    //
    // getPresentationValue(): { label: string; value: string } {
    //     let result: { label: string; value: string } = {label: this.label, value: ''};
    //
    //     let value: FileAndID[] = this.abstractControl.value;
    //     if (value) {
    //         if(value.length > 1) {
    //             value.forEach(file => {
    //                 result.value = result.value.concat(file.fileName) + ', ';
    //             });
    //         } else {
    //             result.value = value[0].fileName;
    //         }
    //     }
    //
    //     return result;
    // }
    //
    // public getPdfValue(): any {
    //     // if (!this._sendToBE) {
    //     //     return undefined;
    //     // }
    //     //
    //     // return {
    //     //     "veldnaam": this.name,
    //     //     "inhoud": this.formControl2.value,
    //     //     "label": this.label,
    //     //     "enkelIntern": "false",
    //     //     ...this.getIsGeneratedValues(),
    //     // }
    // }
    //
    // public destroy(): void {
    //     if (this.generated) {
    //         this._injector.get(FileUpload2Service).unRegisterField(this);
    //     }
    // }

}
