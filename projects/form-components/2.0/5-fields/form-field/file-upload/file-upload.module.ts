import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FileUploadComponent} from './file-upload.component';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {BrowserModule} from "@angular/platform-browser";
import {FontAwesomeModule} from "@fortawesome/angular-fontawesome";
import {NgbModule} from "@ng-bootstrap/ng-bootstrap";
import {FieldErrors2Module} from "../../field-errors2/field-errors2.module";
import {QuestionMarkModule} from "../question-mark/question-mark.module";

@NgModule({
    imports: [
        CommonModule,
        ReactiveFormsModule,
        FormsModule,
        FormsModule,
        BrowserModule,
        FontAwesomeModule,
        NgbModule,
        FontAwesomeModule,
        QuestionMarkModule,
        FieldErrors2Module
    ],
    declarations: [
        FileUploadComponent
    ],
    exports: [
        FileUploadComponent
    ]
})
export class FileUploadModule {
}
