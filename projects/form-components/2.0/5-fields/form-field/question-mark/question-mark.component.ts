import {Component, Input} from '@angular/core';
import {faQuestionCircle} from "@fortawesome/free-regular-svg-icons";
import {FormField} from '../FormField';

@Component({
	selector: 'app-question-mark',
	templateUrl: './question-mark.component.html',
	styleUrls: ['./question-mark.component.scss']
})
export class QuestionMarkComponent {
	@Input() public field: FormField;
	
	public faQuestionCircle = faQuestionCircle;
}
