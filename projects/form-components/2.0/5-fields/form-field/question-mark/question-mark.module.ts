import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {QuestionMarkComponent} from './question-mark.component';
import { FontAwesomeModule } from "@fortawesome/angular-fontawesome";
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";

@NgModule({
	imports: [
		CommonModule,
		NgbModule,
		FontAwesomeModule
	],
	declarations: [QuestionMarkComponent],
	exports: [
		QuestionMarkComponent
	]
})
export class QuestionMarkModule {
}
