import {Component, forwardRef, OnInit} from '@angular/core';
import {FormField} from './FormField';
import {ControlValueAccessor, NG_VALUE_ACCESSOR} from '@angular/forms';
import {BaseComponent} from '../../base/base.component';

@Component({
	selector: 'app-form-field',
	templateUrl: './form-field.component.html',
	styleUrls: ['./form-field.component.scss'],
	providers: [{
		provide: NG_VALUE_ACCESSOR,
		useExisting: forwardRef(() => FormFieldComponent),
		multi: true
	}]
})
export class FormFieldComponent extends BaseComponent implements ControlValueAccessor, OnInit {
	
	public _component: FormField;
	
	ngOnInit() {

	}
	
	public showErrors(): boolean {
		return this._component.abstractControl.touched;
	}
	
	registerOnChange(fn: any): void {
	}
	
	registerOnTouched(fn: any): void {
	}
	
	setDisabledState(isDisabled: boolean): void {
	}
	
	writeValue(obj: any): void {
	}
}
