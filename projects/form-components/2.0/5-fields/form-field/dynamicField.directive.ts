import {ComponentFactoryResolver, Directive, Input, OnInit, ViewContainerRef} from '@angular/core';
import {DefaultFormFunctions} from '../../base/defaultFormFunctions';
import {componentMap} from '../../services/componentMap';

@Directive({
	selector: '[dynamicField]',
})
export class DynamicFieldDirective implements OnInit {
	
	public component: any;
	
	private _comp: DefaultFormFunctions;
	
	@Input()
	public set baseComponent(comp: DefaultFormFunctions) {
		this._comp = comp;
	};
	
	constructor(
		private resolver: ComponentFactoryResolver,
		private container: ViewContainerRef
	) {
	}
	
	ngOnInit(): void {
		console.log(this._comp.id);
		console.log(this._comp.component);
		const factory = this.resolver.resolveComponentFactory<any>(componentMap[this._comp.component]);
		this.component = this.container.createComponent(factory);
		this.component.instance.baseComponent = this._comp;
	}
}
