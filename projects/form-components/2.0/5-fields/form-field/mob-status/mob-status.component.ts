import {Component, OnDestroy} from '@angular/core';
import {FormFieldComponent} from '../form-field.component';
import {MobStatus} from './mob-status';
import * as L from 'leaflet';
import GestureHandling from 'leaflet-gesture-handling';

L.Map.addInitHook("addHandler", "gestureHandling", GestureHandling);

@Component({
	selector: 'app-mob-status',
	templateUrl: './mob-status.component.html',
	styleUrls: ['./mob-status.component.scss']
})
export class MobStatusComponent extends FormFieldComponent implements OnDestroy {
	public _component: MobStatus;
	public BEValidation: string;
	
	ngOnDestroy(): void {
		this._component
	}
}
