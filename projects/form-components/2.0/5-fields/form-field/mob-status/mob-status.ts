import {icon, LatLng, Map, marker, tileLayer} from 'leaflet';
import {Injector} from '@angular/core';
import {FormField, FormFieldBuilder} from '../FormField';
import {LeafletPointerOptions} from './src/leaflet-pointerOptions';
import {LeafletMapOptions} from './src/leaflet-mapOptions';

import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../../src/environments/environment';

import * as moment from 'moment';
import {BasicInputField2Component} from '../basic-input-field2/basic-input-field2.component';
import {Button, ButtonBuilder} from '../button/button';
import {ButtonComponent} from '../button/button.component';
import {first} from 'rxjs/operators';
import {MobData} from './src/mob-data';
import {Observable} from 'rxjs';
import {LabelField, LabelFieldBuilder} from '../label-field/LabelField';
import {WarningType} from '../../WarningType';

export class MobStatusBuilder extends FormFieldBuilder {
	
	// protected _buildingClass: any = MobStatus;
	//
	// public static newFactory(injector: Injector): MobStatusBuilder {
	// 	return new MobStatusBuilder(injector);
	// }
	//
	// get pointerInfo(): LeafletPointerOptions {
	// 	return this._pointerInfo;
	// }
	//
	// private _pointerInfo: LeafletPointerOptions;
	//
	// public setPointerOptions(info: LeafletPointerOptions): MobStatusBuilder {
	// 	this._pointerInfo = info;
	// 	return this;
	// }
	//
	// get mapOptions(): LeafletMapOptions {
	// 	return this._mapOptions;
	// }
	//
	// private _mapOptions: LeafletMapOptions;
	//
	// public setMapOptions(options: LeafletMapOptions): MobStatusBuilder {
	// 	this._mapOptions = options;
	// 	return this;
	// }
	//
	// get latLng(): LatLng {
	// 	return this._latLng;
	// }
	//
	// private _latLng: LatLng;
	//
	// public setLatLng(options: LatLng): MobStatusBuilder {
	// 	this._latLng = options;
	// 	return this;
	// }
	//
}

export class LatLngXY {
	lat: number;
	lng: number;
	x: number;
	y: number;
}

export class MobStatus extends FormField {
	
	// get showMap(): boolean {
	// 	return this._showMap;
	// }
	//
	// get inputField(): FormField {
	// 	return this._inputField;
	// }
	//
	// get searchButton(): Button {
	// 	return this._searchButton;
	// }
	//
	// get labelWarning(): LabelField {
	// 	return this._labelWarning;
	// }
	//
	// get labelError(): LabelField {
	// 	return this._labelError;
	// }
	//
	// get mobData(): MobData {
	// 	return this._mobData;
	// }
	//
	// get mapOptions(): LeafletMapOptions {
	// 	return this._mapOptions;
	// }
	//
	// public defaultLatLng: LatLng;
	// public hasError: boolean = false;
	// public hasWarning: boolean = false;
	//
	// private streetMap;
	// private map: Map;
	// private _pointerInfo: LeafletPointerOptions;
	// private _circleMarker;//: CircleMarker;
	// private _mapOptions: LeafletMapOptions;
	// private _inputField: FormField;
	// private _searchButton: Button;
	// private _labelWarning: LabelField;
	// private _labelError: LabelField;
	//
	// private _mobData: MobData;
	// private _showMap: boolean = false;
	//
	// private destinationIcon = icon({
	// 	iconUrl: 'assets/lokatie_bestemming.svg',
	// 	iconSize: [30, 30], // size of the icon
	// 	iconAnchor: [15, 15], // point of the icon which will correspond to marker's location
	// 	popupAnchor: [0, -15] // point from which the popup should open relative to the iconAnchor
	// });
	//
	// constructor(b: MobStatusBuilder) {
	// 	super(b);
	// 	this.defaultLatLng = b.latLng;
	// 	this._pointerInfo = b.pointerInfo;
	// 	this._circleMarker = marker(this.defaultLatLng, {
	// 		icon: this.destinationIcon
	// 	});
	//
	// 	this._mapOptions = b.mapOptions;
	// 	this.streetMap = tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {});
	// 	this._mapOptions.setLayers([this.streetMap, this._circleMarker]);
	//
	// 	this._labelError = LabelFieldBuilder.newFactory(this._injector)
	// 		.setAsWarning(WarningType.DANGER)
	// 		.setID('error')
	// 		.setLabel('Er is geen melding gevonden. Controleer of u het juiste meldingsnummer heeft ingevuld.')
	// 		.build();
	//
	// 	this._labelWarning = LabelFieldBuilder.newFactory(this._injector)
	// 		.setAsWarning(WarningType.WARNING)
	// 		.setID('warning')
	// 		.setLabel('Wij kunnen niet alle gegevens van de melding tonen. Dit komt door de wetgeving rondom privacy. Daarom ziet u alleen de status van de melding.')
	// 		.build();
	//
	// 	this._inputField = FormFieldBuilder.newBuilder(this._injector)
	// 		.setValidatorMaxLength(100)
	// 		.setComponent(BasicInputField2Component)
	// 		.setID('watIsMeldingnummer')
	// 		.setLabel('Wat is het meldingsnummer?')
	// 		.setQuestionMarkTitle('Meldingsnummer')
	// 		.setQuestionMarkText('Het meldingsnummer vindt u in  de bevestigingsmail die u kreeg na het maken van uw melding.')
	// 		.setValidatorMaxLength(7)
	// 		.setValidatorPattern('^[0-9]+$', 'U kunt alleen nummers invoeren.')
	// 		.setHintText('Een meldingsnummer bestaat uit 7 cijfers.')
	// 		.setValidatorRequired('Dit veld is verplicht!')
	// 		.build();
	//
	// 	this._searchButton = ButtonBuilder.newBuilder(this._injector)
	// 		.setButtonText('Zoek')
	// 		.setComponent(ButtonComponent)
	// 		.setID('btnSearch')
	// 		.build();
	// }
	//
	// init(): Promise<any> {
	// 	return new Promise<any>(resolve => {
	// 		super.init().then(() => {
	//
	// 			this._inputField.init();
	//
	// 			this._searchButton.trigger.subscribe(trigger => {
	// 				if (trigger) {
	// 					if (this._inputField.validate()) {
	// 						this.search(this._inputField.getPdfValue().inhoud);
	// 					}
	// 				}
	// 			});
	//
	// 			this._inputField.keyup.subscribe(keyup => {
	// 				if (keyup && keyup.keyCode === 13 && this._inputField.validate()) {
	// 					this.search(this._inputField.getPdfValue().inhoud)
	// 				}
	// 			});
	//
	// 			resolve(true);
	// 		});
	// 	});
	// }
	//
	// private search(value: string) {
	// 	this.searchByCasenumber(value).pipe(first()).subscribe((mobData: MobData) => {
	// 		this.hasWarning = false;
	// 		if (mobData) {
	// 			this.hasError = false;
	// 			if (mobData.dateStart) {
	// 				mobData.dateStart = moment(mobData.dateStart).format('DD-MM-YYYY')
	// 			}
	//
	// 			this._mobData = mobData;
	// 			if (this._mobData.latitude && this._mobData.latitude) {
	// 				this._showMap = true;
	//
	// 				this.setMapPosition(new LatLng(this._mobData.latitude, this._mobData.longitude));
	// 			} else {
	// 				this._showMap = false;
	// 				this.hasWarning = true;
	// 			}
	// 		} else {
	// 			this.hasError = true;
	// 			this._showMap = false;
	// 			this._mobData = null;
	// 		}
	// 	});
	// }
	//
	// public hasCategory(): boolean {
	// 	return this._mobData.maincategory && this._mobData.subcategory && this._mobData.maincategory !== '' && this._mobData.subcategory !== '';
	// }
	//
	// public hasLocation(): boolean {
	// 	return this._mobData.location && this._mobData.location !== '';
	// }
	//
	// public onMapReady($event) {
	// 	this.streetMap = tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {});
	// 	this._mapOptions.setLayers([this.streetMap, this._circleMarker]);
	// 	this.map = $event;
	// 	if (this._mobData && this._mobData.latitude && this._mobData.latitude) {
	// 		this.setMapPosition(new LatLng(this._mobData.latitude, this._mobData.longitude));
	// 	} else {
	// 		this.setMapPosition(this.defaultLatLng);
	// 	}
	// }
	//
	// public setLatLng(latLng: LatLng): void {
	// 	this.setMapPosition(latLng);
	// }
	//
	// private setMapPosition(latlng: LatLng): void {
	// 	this._circleMarker.setLatLng(latlng);
	//
	// 	if (this.map) {
	// 		this.map.panTo(latlng);
	// 	}
	// }
	//
	// public searchByCasenumber(casenumber: string): Observable<any> {
	// 	console.log('call')
	// 	return this._injector.get(HttpClient).get<any>(environment.baseUrl + 'eforms-api/controller/mob/callDetails/' + casenumber);
	// }
}
