export class MobData {
  public isDetailed: boolean;
	public casenumber: string;
  public dateStart: string;
  public location: string;
  public maincategory: string
  public status: string;
  public subcategory: string;
  public x: number;
  public y: number;
  public latitude: number;
  public longitude: number;
}
