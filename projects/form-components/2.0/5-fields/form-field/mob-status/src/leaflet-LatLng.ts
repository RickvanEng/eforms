export class LeafletLatLng {
	get lat(): number {
		return this._lat;
	}
	
	get lng(): number {
		return this._lng;
	}
	
	private _lat: number;
	private _lng: number;
	
	constructor(lat: number, lng: number) {
		this._lat = lat;
		this._lng = lng;
	}
}
