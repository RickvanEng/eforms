import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {MobStatusComponent} from './mob-status.component';
import {LeafletModule} from '@asymmetrik/ngx-leaflet';
import {FieldErrors2Module} from '../../field-errors2/field-errors2.module';
import {BasicInputField2Module} from '../basic-input-field2/basic-input-field2.module';
import {ButtonModule} from '../button/button.module';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { LabelFieldModule } from '../label-field/label-field.module';

@NgModule({
	imports: [
		CommonModule,
		LeafletModule,
		BasicInputField2Module,
		FontAwesomeModule,
		ButtonModule,
		FieldErrors2Module,
		LabelFieldModule
	],
	declarations: [
		MobStatusComponent
	],
	exports: [
		MobStatusComponent
	]
})
export class MobStatusFieldModule {
}
