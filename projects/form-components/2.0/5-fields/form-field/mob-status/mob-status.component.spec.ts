import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {MobStatusComponent} from './mob-status.component';

describe('MobStatusComponent', () => {
  let component: MobStatusComponent;
  let fixture: ComponentFixture<MobStatusComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MobStatusComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MobStatusComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
