import {Component} from '@angular/core';
import {FormFieldWithOptions} from './MultipleChoiceField';
import {FormFieldOption} from './options/FieldOption';
import {FormFieldComponent} from '../form-field.component';

@Component({
	selector: 'app-multiple-choice-field',
	templateUrl: './multiple-choice-field.component.html',
	styleUrls: ['./multiple-choice-field.component.scss']
})
export class MultipleChoiceFieldComponent extends FormFieldComponent {
	
	public _component: FormFieldWithOptions;
	public options: FormFieldOption[] = [];
	
	ngOnInit(): void {
		super.ngOnInit();
		this._component.getOptions.subscribe(options => {
			this.options = options;
		})
	}
	
}
