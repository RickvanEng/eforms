import {MultipleChoiceImageModule} from './multiple-choice-image.module';

describe('MultipleChoiceImageModule', () => {
  let multipleChoiceImageModule: MultipleChoiceImageModule;

  beforeEach(() => {
    multipleChoiceImageModule = new MultipleChoiceImageModule();
  });

  it('should create an instance', () => {
    expect(multipleChoiceImageModule).toBeTruthy();
  });
});
