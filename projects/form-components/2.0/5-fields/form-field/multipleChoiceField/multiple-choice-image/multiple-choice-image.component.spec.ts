import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {MultipleChoiceImageComponent} from './multiple-choice-image.component';

describe('MultipleChoiceImageComponent', () => {
  let component: MultipleChoiceImageComponent;
  let fixture: ComponentFixture<MultipleChoiceImageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MultipleChoiceImageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MultipleChoiceImageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
