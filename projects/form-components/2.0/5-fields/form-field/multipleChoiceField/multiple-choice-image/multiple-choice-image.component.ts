import {Component} from '@angular/core';
import {MultipleChoiceObjectComponent} from '../multiple-choice-object/multiple-choice-object.component';
import {MultipleChoiceImage} from './multiple-choice-image';
import {FormFieldOption} from '../options/FieldOption';

@Component({
    selector: 'app-multiple-choice-image',
    templateUrl: './multiple-choice-image.component.html',
    styleUrls: ['./multiple-choice-image.component.scss']
})
export class MultipleChoiceImageComponent extends MultipleChoiceObjectComponent {
    public _component: MultipleChoiceImage;

    onClick(option: FormFieldOption) {
        super.onClick(option);

        this._component.abstractControl.setValue(option.id);
    }
}
