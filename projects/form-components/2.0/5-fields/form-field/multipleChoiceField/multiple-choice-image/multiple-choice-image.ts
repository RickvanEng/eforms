import {FormField, FormFieldBuilder} from '../../FormField';
import {FormFieldWithOptions, FormFieldWithOptionsFactory} from '../MultipleChoiceField';
import {OptionsFactory} from '../options/options-factory';
import {FormFieldOption} from '../options/FieldOption';
import { EFormsAbstractControl } from '../../../../base/eFormsAbstractControl';
import { Injector } from '@angular/core';

export class MultipleChoiceImageBuilder extends FormFieldWithOptionsFactory {
	// protected _buildingClass: any = MultipleChoiceImage;
	//
	// get optionsFactory(): OptionsFactory {
	// 	return this._optionsFactory;
	// }
	//
	// private _optionsFactory: OptionsFactory;
	// public setOptionsFactory(factory: OptionsFactory): MultipleChoiceImageBuilder {
	// 	this._optionsFactory = factory;
	// 	return this;
	// }
	//
	// protected _events: any[] = [];
	//
	// get events(): any[] {
	// 	return this._events;
	// }
	//
	// public bindEvents(events: any[]): MultipleChoiceImageBuilder {
	// 	this._events = events;
	// 	return this;
	// }
	//
	// private _fields: FormField[] = [];
	// get fields(): FormField[] {
	// 	return this._fields;
	// }
	//
	// private _fieldsToBuild: FormFieldBuilder[];
	// public setFieldsToBuild(field: FormFieldBuilder[]): MultipleChoiceImageBuilder {
	// 	this._fieldsToBuild = field;
	// 	return this;
	// }
	//
	// protected prebuild(): void {
	// 	super.prebuild();
	// 	if (this._fieldsToBuild) {
	// 		this._fieldsToBuild.forEach(fieldToBuild => {
	// 			this._fields.push(fieldToBuild.build());
	// 		});
	//
	// 	}
	// }
}

export class MultipleChoiceImage extends FormFieldWithOptions {
	// public events: any[] = [];
	// private _fields: FormField[];
	//
	// _builder: MultipleChoiceImageBuilder;
	// constructor(b: MultipleChoiceImageBuilder) {
	// 	super(b);
	// 	this._fields = b.fields;
	// 	this.events = b.events;
	// }
	//
	// getAbstractControl(inj: Injector): EFormsAbstractControl {
	// 	let control = super.getAbstractControl(inj);
	// 	this._builder.optionsFactory.getOptions();
	// 	return control;
	// }
	//
	// init(): Promise<any> {
	// 	return new Promise<any>(resolve => {
	// 		super.init().then(() => {
	// 			this._builder.optionsFactory.options.subscribe(options => {
	// 				this.setNewOptions(options)
	// 			});
	//
	// 			this.handleEvents();
	// 			resolve(true);
	// 		})
	// 	})
	// }
	//
	// getSelectedOptions(): FormFieldOption[] {
	// 	return this._options.value.filter(option => option.id === this._abstractControl.value);
	// }
	//
	// getPresentationValue(): { label: string, value: string } {
	// 	let selectedOption: FormFieldOption = this.getSelectedOptions()[0];
	//
	// 	return {
	// 		label: this.label, value: selectedOption ? selectedOption.data.value : ''
	// 	}
	// }
	//
	// private handleEvents() {
	// 	this.events.forEach(event => {
	// 		let selectedOption: FormFieldOption[] = this._options.getValue().filter(option => option.isChecked === true);
	// 		event(this, selectedOption, this._fields);
	// 	});
	// }
	//
	// setNewOptions(options: FormFieldOption[]): void {
	// 	options.forEach(option => {
	// 		option.getFormControl(this._injector)
	// 	});
	//
	// 	this._options.next(options);
	// }
	//
	// isOptionChecked(optionID: string): boolean {
	// 	return this._abstractControl.value === optionID;
	// }
}
