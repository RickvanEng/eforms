import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {MultipleChoiceImageComponent} from './multiple-choice-image.component';
import {ReactiveFormsModule} from '@angular/forms';
import {FieldErrors2Module} from '../../../field-errors2/field-errors2.module';
import {ImagePickerComponent} from '../options/image-picker/image-picker.component';
import {ImagePickerModule} from '../options/image-picker/image-picker.module';
import {BaseModule} from '../../../../base/base.module';

@NgModule({
	imports: [
		CommonModule,
		ReactiveFormsModule,
		FieldErrors2Module,
		ImagePickerModule,
		BaseModule,
	],
	declarations: [
		MultipleChoiceImageComponent
	],
	exports: [
		MultipleChoiceImageComponent
	],
	entryComponents: [
		ImagePickerComponent
	]
})
export class MultipleChoiceImageModule {
}
