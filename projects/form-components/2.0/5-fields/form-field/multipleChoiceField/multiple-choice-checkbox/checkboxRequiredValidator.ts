import {ValidatorFn} from '@angular/forms';
import { EFormsFormArray } from '../../../../base/eFormsAbstractControl';
import {IValidator} from '../../../../validators/IValidator';

// TODO let minRequired param be dynamic

/**
 * The required validator for the checkbox component. It counts the FormArray to check if X (minRequired: number) FormControls are true
 * @param validator
 * @constructor
 */
export function CheckboxRequiredValidator(validator: IValidator): ValidatorFn {
	return function validate(formArray: EFormsFormArray) {
		let checked = 0;

		formArray.controls.forEach(control => {
			if (control.value === true) {
				checked++;
			}
		});

		return (checked < validator.params) ? {requiredMultipleChoice: true} : null;
	};
}
