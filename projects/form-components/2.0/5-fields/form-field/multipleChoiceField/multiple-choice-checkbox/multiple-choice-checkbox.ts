import { FormFieldWithOptions, FormFieldWithOptionsFactory } from '../MultipleChoiceField';
import { Injector } from '@angular/core';
import { EBasicValidators } from '../../../../validators/basic/basicValidatorMap';
import { ValidatorPreBuildConfigs } from '../../../../validators/basic/ValidatorPreBuildConfigs';
import { IValidator } from '../../../../validators/IValidator';
import { EFormsAbstractControl, EFormsFormArray } from '../../../../base/eFormsAbstractControl';

export class MultipleChoiceCheckboxBuilder extends FormFieldWithOptionsFactory {
    // get isSingleCheckbox(): boolean {
    //     return this._isSingleCheckbox;
    // }
    //
	// get overViewValue(): { trueValue: string; falseValue: string } {
	// 	return this._overViewValue;
	// }
    //
	// private _overViewValue: { trueValue: string, falseValue: string } = { trueValue: 'Ja', falseValue: 'Nee' };
    // private _isSingleCheckbox: boolean = false;
    //
    // _buildingClass: any = MultipleChoiceCheckbox;
    //
    // public static newFactory(injector: Injector): MultipleChoiceCheckboxBuilder {
    //     return new MultipleChoiceCheckboxBuilder(injector);
    // }
    //
	// public setOverViewValue(value: { trueValue: string, falseValue: string }): any {
	// 	this._overViewValue = value;
	// 	return this;
	// }
    //
    // setValidation(config: IValidator): any {
    //     if (config.validatorId === EBasicValidators.REQUIRED) {
    //         config.validatorId = EBasicValidators.REQUIRED_MULTIPLECHOICE;
    //     }
    //     return super.setValidation(config);
    // }
    //
    // setValidatorRequired(msg: string = 'Dit veld is verplicht.'): any {
    //     let validatorConfig: IValidator = ValidatorPreBuildConfigs.getInstance().required(msg);
    //     validatorConfig.validatorId = EBasicValidators.REQUIRED_MULTIPLECHOICE;
    //     validatorConfig.params = 1;
    //     this._validators.push(validatorConfig);
    //     return this;
    // }
    //
    // public SetIsSingleCheckbox(value: boolean): any {
    //     this._isSingleCheckbox = value;
    //     return this;
    // }
}

export class MultipleChoiceCheckbox extends FormFieldWithOptions {

    // get required(): string {
    //     let foundValidation: IValidator = this._validators.find(val => val.validatorId === EBasicValidators.REQUIRED_MULTIPLECHOICE);
    //     return foundValidation ? 'required' : '';
    // }
    //
    // protected _abstractControl: EFormsFormArray;
    // _builder: MultipleChoiceCheckboxBuilder;
    //
    // constructor(b: MultipleChoiceCheckboxBuilder) {
    //     super(b);
    // }
    //
    // getAbstractControl(inj: Injector): EFormsAbstractControl {
    //     let abstractControl: EFormsFormArray = new EFormsFormArray([], this.getFormValidators(this._validators), []);
    //     this._options.value.forEach(option => {
    //         abstractControl.push(option.getFormControl(inj));
    //     });
    //     return abstractControl;
    // }
    //
    // getTechnicalValue(): any {
    //     if (!this.sendToBE) {
    //         return undefined;
    //     }
    //
    //     let result: any = {};
    //
    //     let optionsData: any[] = [];
    //     this.getSelectedOptions().forEach(option => {
    //         optionsData.push({ optionId: option.id, optionText: option.data.value });
    //     });
    //
    //     result[this._id] = optionsData;
    //     return result;
    // }
    //
    // getPresentationValue(): { label: string, value: string } {
    //     if (this._builder.isSingleCheckbox) {
    //         let hasOptionSelected: boolean = this._options.getValue().some(option => option.isChecked === true);
    //         let tranformedValue: string = hasOptionSelected ? this._builder.overViewValue.trueValue : this._builder.overViewValue.falseValue;
    //         return {
    //             label: this.label, value: tranformedValue,
    //         };
    //     } else {
	// 		let value: string = this._options.getValue().filter(option => option.isChecked === true).map(a => a.data.value).join(', ');
    //
	// 		return {
	// 			label: this.label, value: value,
	// 		};
    //     }
    // }
    //
    // getPdfValue(): any {
    //     if (!this.sendToBE) {
    //         return undefined;
    //     }
    //
    //     // Add the value of every selected option
    //     let value: { label: string, value: string } = this.getPresentationValue();
    //
    //     return {
    //         'veldnaam': this.id,
    //         'inhoud': value.value,
    //         'label': value.label,
    //         'enkelIntern': 'false',
    //     };
    // }
    //
    // developPrefill(): void {
    //     this._options.value[0].setCheck(true);
    // }

}
