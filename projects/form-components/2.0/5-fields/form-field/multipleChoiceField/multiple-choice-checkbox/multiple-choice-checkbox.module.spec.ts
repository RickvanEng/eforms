import {MultipleChoiceCheckboxModule} from './multiple-choice-checkbox.module';

describe('MultipleChoiceCheckboxModule', () => {
  let multipleChoiceCheckboxModule: MultipleChoiceCheckboxModule;

  beforeEach(() => {
    multipleChoiceCheckboxModule = new MultipleChoiceCheckboxModule();
  });

  it('should create an instance', () => {
    expect(multipleChoiceCheckboxModule).toBeTruthy();
  });
});
