import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {MultipleChoiceCheckboxComponent} from './multiple-choice-checkbox.component';

describe('MultipleChoiceCheckboxComponent', () => {
  let component: MultipleChoiceCheckboxComponent;
  let fixture: ComponentFixture<MultipleChoiceCheckboxComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MultipleChoiceCheckboxComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MultipleChoiceCheckboxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
