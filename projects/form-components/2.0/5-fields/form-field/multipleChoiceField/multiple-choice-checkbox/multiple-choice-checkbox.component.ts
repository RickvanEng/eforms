import {Component, forwardRef} from '@angular/core';
import {MultipleChoiceFieldComponent} from '../multiple-choice-field.component';
import {MultipleChoiceCheckbox} from './multiple-choice-checkbox';
import {NG_VALUE_ACCESSOR} from '@angular/forms';

@Component({
	selector: 'app-multiple-choice-checkbox',
	templateUrl: './multiple-choice-checkbox.component.html',
	styleUrls: ['./multiple-choice-checkbox.component.scss'],
	providers: [{
		provide: NG_VALUE_ACCESSOR,
		useExisting: forwardRef(() => MultipleChoiceCheckboxComponent),
		multi: true
	}]
})
export class MultipleChoiceCheckboxComponent extends MultipleChoiceFieldComponent {
	public _component: MultipleChoiceCheckbox;
}
