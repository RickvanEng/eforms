import {Injector} from '@angular/core';
import {
	ComponentExampleConfigBuilder,
	IComponentExampleConfig
} from '../../../../../../component-dash/component-example-config.builder';
import {ComponentExampleBuilder} from '../../../../../../component-dash/component-example.builder';
import {GenericFields} from '../../../presets/genericFields';
import {FormFieldOptionFactory} from '../options/FieldOption';
import {ValidatorPreBuildConfigs} from '../../../../validators/basic/ValidatorPreBuildConfigs';

export class MultipleChoiceCheckboxExmaple {
    public getField(inj: Injector): IComponentExampleConfig {

        return ComponentExampleConfigBuilder.newBuilder()
            .setComponent('Checkbox')
            .setExamples([
                ComponentExampleBuilder.newBuilder()
                    .setBuilder(new GenericFields(inj).checkboxField
                        .setOptions([
                            new FormFieldOptionFactory()
                                .setId('option1')
                                .setData({value: 'option 1'})
                                .build(),
                            new FormFieldOptionFactory()
                                .setId('option2')
                                .setData({value: 'option 2'})
                                .build(),
                            new FormFieldOptionFactory()
                                .setId('option3')
                                .setData({value: 'option 3'})
                                .build()
                        ])
                        .setValidation(ValidatorPreBuildConfigs.getInstance().required())
                        .setQuestionMarkTitle('The title of the extra info')
                        .setSaveToState(false)
                        .setLabel('This is a label')
                        .setQuestionMarkText('The extra info text')
                        .setHintText('Example of a hint text')
                        .setID('Example 1')
                    )
                    .setDescription('test Desc')
                    .setState(undefined)
                    .setValidateButton(true)
                    .data,

            ])
            .data;

    }
}
