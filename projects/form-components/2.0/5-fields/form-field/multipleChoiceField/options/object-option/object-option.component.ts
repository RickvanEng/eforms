import {Component} from '@angular/core';
import {FormFieldOption} from '../FieldOption';
import {OptionComponent} from '../option.component';

@Component({
	selector: 'app-object-option',
	templateUrl: './object-option.component.html',
	styleUrls: ['./object-option.component.scss']
})
export class ObjectOptionComponent extends OptionComponent {
	public option: FormFieldOption;
}
