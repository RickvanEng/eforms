import {NgModule} from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {CommonModule} from '@angular/common';
import {BrowserModule} from '@angular/platform-browser';
import {MatCardModule} from '@angular/material';
import {ObjectOptionComponent} from './object-option.component';

@NgModule({
	declarations: [
		ObjectOptionComponent
	],
	imports: [
		ReactiveFormsModule,
		FormsModule,
		CommonModule,
		BrowserModule,
		MatCardModule
	],
	exports: [
		ObjectOptionComponent
	]
})
export class ObjectOptionModule {
}
