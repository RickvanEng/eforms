import {BehaviorSubject} from 'rxjs';
import {AbstractControl, FormBuilder, FormControl} from '@angular/forms';
import {Injector} from '@angular/core';

export enum OptionType {
	ONTHEFFING,
	STANDPLAATS,
	EHERK_VESTIGING,
	PLAATS,
}

export class FormFieldOptionFactory {
	get beData(): any {
		return this._beData;
	}
	get component(): any {
		return this._component;
	}
	get show(): boolean {
		return this._show;
	}
	get optionType(): OptionType {
		return this._optionType;
	}
	
	get data(): any {
		return this._data;
	}
	
	get value(): string {
		return this._value;
	}
	
	get id(): string {
		return this._id;
	}
	
	private _value: string = '';
	private _data: any;
	private _beData: any;
	private _optionType: OptionType;
	private _show: boolean = true;
	private _id: string = '';
	private _component: any;
	
	public static newFactory(): FormFieldOptionFactory {
		return new FormFieldOptionFactory();
	}
	
	public setId(id: string): FormFieldOptionFactory {
		this._id = id;
		return this;
	}
	
	public setData(data: any, beData?: string): FormFieldOptionFactory {
		this._data = data;
		this._beData = beData;
		return this;
	}
	
	public setType(type: OptionType): FormFieldOptionFactory {
		this._optionType = type;
		return this;
	}
	
	public setValue(value: string): FormFieldOptionFactory {
		this._value = value;
		return this;
	}
	
	public setShow(show: boolean): FormFieldOptionFactory {
		this._show = show;
		return this;
	}
	
	public setComponent(comp: any): FormFieldOptionFactory {
		this._component = comp;
		return this;
	}
	
	public build(id: string = this._id): any {
		this._id = id;
		return new FormFieldOption(this);
	}
}

export class FormFieldOption {
	get formControl(): FormControl {
		return this._formControl;
	}
	
	get beData(): any {
		return this._beData ? this._beData : this.data.value;
	}
	
	get component(): any {
		return this._component;
	}
	
	get show(): BehaviorSubject<boolean> {
		return this._show;
	}
	
	get isChecked(): boolean {
		return this._formControl.value;
	}
	
	get data(): any {
		return this._data;
	}
	
	get id(): string {
		return this._id;
	}
	
	getFormControl(inj: Injector): AbstractControl {
		let fb: FormBuilder = inj.get(FormBuilder);
		
		let formControl: FormControl = fb.control(false);
		this._formControl = formControl;
		return formControl;
	}
	
	protected _data: any;
	private _beData: any;
	private _id: string;
	private _isChecked: boolean = false;
	private _show: BehaviorSubject<boolean> = new BehaviorSubject(true);
	private _component: any;
	private _optionType: OptionType;

	private _formControl: FormControl;

	constructor(builder: FormFieldOptionFactory) {
		this._data = builder.data;
		this._beData = builder.beData;
		this._optionType = builder.optionType;
		this._id = builder.id;
		this._show.next(builder.show);
	    this._component = builder.component;
	}

	public setCheck(value: boolean): void {
		this._formControl.setValue(value)
	}
	
	public toggleCheck(): void {
		this._isChecked = !this._isChecked;
	}
}
