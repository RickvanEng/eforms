import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {MatCardModule} from '@angular/material';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {BrowserModule} from '@angular/platform-browser';
import {EherkVestigingComponent} from './eherk-vestiging.component';

@NgModule({
	declarations: [
		EherkVestigingComponent
	],
	imports: [
		ReactiveFormsModule,
		FormsModule,
		CommonModule,
		BrowserModule,
		MatCardModule
	]
})
export class EherkVestigingModule {
}
