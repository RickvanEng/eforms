import {Component} from '@angular/core';
import {OptionComponent} from '../option.component';
import {FormFieldOption} from '../FieldOption';

@Component({
	selector: 'app-eherk-vestiging',
	templateUrl: './eherk-vestiging.component.html',
	styleUrls: ['./eherk-vestiging.component.scss']
})
export class EherkVestigingComponent extends OptionComponent {
	public option: FormFieldOption;
}
