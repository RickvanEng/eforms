import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {EherkVestigingComponent} from './eherk-vestiging.component';

describe('EherkVestigingComponent', () => {
  let component: EherkVestigingComponent;
  let fixture: ComponentFixture<EherkVestigingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EherkVestigingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EherkVestigingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
