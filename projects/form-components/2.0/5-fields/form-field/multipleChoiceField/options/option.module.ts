import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {OptionComponent} from './option.component';
import {ObjectOptionModule} from './object-option/object-option.module';

@NgModule({
	imports: [
		CommonModule,
		ObjectOptionModule,
	],
	declarations: [
		OptionComponent
	]
})
export class OptionModule {
}
