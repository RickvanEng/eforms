import {OptionsFactory} from './options-factory';
import {FormFieldOption} from './FieldOption';
import {ImagePicker, ImagePickerBuilder} from './image-picker/image-picker';
import {ImagePickerComponent} from './image-picker/image-picker.component';

export class OFCategory extends OptionsFactory {
	
	protected buildOptions(parentID: string, data: any): FormFieldOption[] {
		let newOptions: FormFieldOption[] = [];

		if (data.categories) {

			let i: number = 0;
			data.categories.forEach(entry => {
				let newOption: ImagePicker = ImagePickerBuilder.newFactory()
					.setId(parentID + '-option-' + i)
					.setComponent(ImagePickerComponent)
					.setData({value: entry.name, url: entry.imageUrl, id: entry.id.toString()})
					.build() as ImagePicker;

				newOptions.push(newOption);
				i++;
			});
		} else {
			console.error('No MOB Categories are returned! Error: ', data.responseStatus.message)
		}
		
		return newOptions;
	}
}

export class OFSubCategory extends OptionsFactory {
	
	protected buildOptions(parentID: string, data: any): FormFieldOption[] {
		let newOptions: FormFieldOption[] = [];
		
		let i: number = 0;
		data.subCategories.forEach(entry => {
			let newOption: ImagePicker = ImagePickerBuilder.newFactory()
				.setId(parentID + '-option-' + i)
				.setComponent(ImagePickerComponent)
				.setData({value: entry.name, url: entry.imageUrl, id: entry.id.toString(), askForAddress: entry.allowedToAskForAddress, blockingText: entry.blockingText})
				.build() as ImagePicker;
			
			newOptions.push(newOption);
			i++;
		});

		return newOptions;
	}
}
