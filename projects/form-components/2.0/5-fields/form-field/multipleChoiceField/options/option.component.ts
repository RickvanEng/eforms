import {Component, Input, OnInit} from '@angular/core';
import {FormFieldOption} from './FieldOption';

@Component({
	selector: 'app-options',
	templateUrl: './option.component.html',
	styleUrls: ['./option.component.scss']
})
export class OptionComponent implements OnInit {
	
	@Input() public option: FormFieldOption;
	public checked: boolean = false;
	
	constructor() {
	}
	
	ngOnInit() {
		// this.option.changeDetector.subscribe(() => {
		// 	this.checked = this.option.isChecked;
		// });
	}
}
