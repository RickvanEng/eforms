import {FormFieldOption, FormFieldOptionFactory} from '../FieldOption';

export class ImagePickerBuilder extends FormFieldOptionFactory {
	public build(): ImagePicker {
		return new ImagePicker(this);
	}
}

export class ImagePicker extends FormFieldOption {
	
	protected _data: {value: any, url: string, id: string};
	
	get data(): any {
		return this._data;
	}
	
	constructor(b: ImagePickerBuilder) {
		super(b);
	}
	
}
