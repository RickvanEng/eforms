import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ImagePickerComponent} from './image-picker.component';

@NgModule({
    imports: [
        CommonModule
    ],
    declarations: [ImagePickerComponent],
    exports: [
        ImagePickerComponent
    ]
})
export class ImagePickerModule {
}
