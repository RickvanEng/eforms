import {Component, Input} from '@angular/core';
import {OptionComponent} from '../option.component';
import {FormFieldOption} from '../FieldOption';

@Component({
	selector: 'app-image-picker',
	templateUrl: './image-picker.component.html',
	styleUrls: ['./image-picker.component.scss']
})
export class ImagePickerComponent extends OptionComponent {
	@Input() option: FormFieldOption;
}
