import {ImagePickerModule} from './image-picker.module';

describe('ImagePickerModule', () => {
  let imagePickerModule: ImagePickerModule;

  beforeEach(() => {
    imagePickerModule = new ImagePickerModule();
  });

  it('should create an instance', () => {
    expect(imagePickerModule).toBeTruthy();
  });
});
