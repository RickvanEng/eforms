import {FormFieldOption} from './FieldOption';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../../../src/environments/environment';
import {Injector} from '@angular/core';
import {BehaviorSubject} from 'rxjs';
import {FormFieldWithOptionsFactory} from '../MultipleChoiceField';

export class OptionsFactory {
	// get options(): BehaviorSubject<FormFieldOption[]> {
	// 	return this._options;
	// }
	//
	// private _injector: Injector;
	// private _options: BehaviorSubject<FormFieldOption[]> = new BehaviorSubject([]);
	// private _api: string;
	// private _parentID: string;
	//
	// private _fieldToWaitFor: FormFieldWithOptionsFactory;
	//
	// constructor(injector: Injector, parentID: string, options: FormFieldOption[], api?: string, fieldToWaitFor?: FormFieldWithOptionsFactory) {
	// 	this._injector = injector;
	// 	this._options.next(options);
	// 	this._api = api;
	// 	this._parentID = parentID;
	// 	this._fieldToWaitFor = fieldToWaitFor;
	// }
	//
	// public getOptions(): void {
	// 	if (this._fieldToWaitFor) {
	// 		this._fieldToWaitFor.build().abstractControl.statusChanges.subscribe(() => {
	//
	// 			let selectedOptions: FormFieldOption[] = this._fieldToWaitFor.build().getSelectedOptions();
	// 			if (selectedOptions.length > 0) {
	// 				this._injector.get(HttpClient).get(environment.baseUrl + this._api + selectedOptions[0].data.id).subscribe((res: any) => {
	// 					this._options.next(this.buildOptions(this._parentID, res));
	// 				})
	// 			}
	// 		})
	// 	} else if (this._api) {
	// 		this._injector.get(HttpClient).get(environment.baseUrl + this._api).subscribe((res: any) => {
	// 			this._options.next(this.buildOptions(this._parentID, res));
	// 		})
	// 	}
	// }
	//
	// protected buildOptions(parentID: string, data: any): FormFieldOption[] {
	// 	console.warn('Get the implementation of this class!');
	// 	return undefined;
	// }
}
