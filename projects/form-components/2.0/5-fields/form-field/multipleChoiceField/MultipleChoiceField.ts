import {FormField, FormFieldBuilder} from '../FormField';
import {FormFieldOption, FormFieldOptionFactory} from './options/FieldOption';
import {Injector} from '@angular/core';
import {BehaviorSubject} from 'rxjs';
import {ValidatorPreBuildConfigs} from '../../../validators/basic/ValidatorPreBuildConfigs';
import { IValidator } from '../../../validators/IValidator';

export class FormFieldWithOptionsFactory extends FormFieldBuilder {
	// getRepeatingOptions(id: string): FormFieldOption[] {
	// 	if (this._repeatingOptions) {
	// 		let newOptions: FormFieldOption[] = [];
	//
	// 		this._repeatingOptions.forEach(option => {
	// 			newOptions.push(option.build(id));
	// 		});
	//
	// 		return newOptions;
	// 	} else {
	// 		return undefined;
	// 	}
	// }
	//
	// get deselectAble(): boolean {
	// 	return this._deselectAble;
	// }
	//
	// get singleSelect(): boolean {
	// 	return this._singleSelect;
	// }
	//
	// get options(): FormFieldOption[] {
	// 	if (this._singleBuild) {
	// 		return this._options;
	// 	} else {
	// 		let newOptions: FormFieldOption[] = [];
	//
	// 		this._repeatingOptions.forEach(option => {
	// 			newOptions.push(option.build());
	// 		});
	//
	// 		return newOptions;
	// 	}
	// }
	//
	// private _options: FormFieldOption[] = [];
	// private _repeatingOptions: FormFieldOptionFactory[];
	// private _singleSelect: boolean = false;
	// private _deselectAble: boolean = false;
	//
	// public static newFactory(injector: Injector): FormFieldWithOptionsFactory {
	// 	return new FormFieldWithOptionsFactory(injector);
	// }
	//
	// public setOptions(options: FormFieldOption[]): FormFieldWithOptionsFactory {
	// 	this._options = options;
	// 	return this;
	// }
	//
	// public setRepeatingOptions(options: FormFieldOptionFactory[]): FormFieldWithOptionsFactory {
	// 	this._repeatingOptions = options;
	// 	return this;
	// }
	//
	// public setSingleSelect(value: boolean): FormFieldWithOptionsFactory {
	// 	this._singleSelect = value;
	// 	return this;
	// }
	//
	// public setDeselectAble(value: boolean): FormFieldWithOptionsFactory {
	// 	this._deselectAble = value;
	// 	return this;
	// }
	//
	// // setValidatorRequired(msg: string = 'Dit veld is verplicht.'): FormFieldWithOptionsFactory {
	// // 	let validatorConfig: IValidator = ValidatorPreBuildConfigs.getInstance().required(msg);
	// // 	this._validators.push(validatorConfig);
	// // 	return this;
	// // }
	//
	// protected _buildingClass: any = FormFieldWithOptions;
}

export class FormFieldWithOptions extends FormField {
	// get deselectAble(): boolean {
	// 	return this._deselectAble;
	// }
	//
	// get getOptions(): BehaviorSubject<FormFieldOption[]> {
	// 	return this._options;
	// }
	//
	// protected _options: BehaviorSubject<FormFieldOption[]> = new BehaviorSubject([]);
	// protected _singleSelect: boolean;
	// private _deselectAble: boolean;
	//
	// constructor(builder: FormFieldWithOptionsFactory) {
	// 	super(builder);
	// 	this._options.next(builder.options);
	// 	this._singleSelect = builder.singleSelect;
	// 	this._deselectAble = builder.deselectAble;
	// }
	//
	// public getSelectedOptions(): FormFieldOption[] {
	// 	return this._options.getValue().filter(option => option.isChecked === true);
	// }
	//
	// getValue(): any {
	// 	return this.getOptions.getValue();
	// }
	//
	// public prefillValue(value: any): void {
	// 	value.forEach(savedOption => {
	// 		this._options.getValue().find(option => option.id === savedOption.optionId).setCheck(true)
	// 	});
	// }
	//
	// clear(): void {
	// 	this._options.getValue().forEach(option => {
	// 		option.setCheck(false);
	// 	});
	// }
	//
	// getPdfValue(): any {
	// 	if (!this.sendToBE) {
	// 		return undefined;
	// 	}
	//
	// 	// Add the value of every selected option
	// 	let value: { label: string, value: string } = this.getPresentationValue();
	//
	// 	return {
	// 		"veldnaam": this.id,
	// 		"inhoud": value.value,
	// 		"label": value.label,
	// 		"enkelIntern": "false"
	// 	}
	// }
	//
	// getPresentationValue(): { label: string, value: string } {
	// 	let foundOption: FormFieldOption = this._options.getValue().find(option => option.isChecked === true);
	// 	if (foundOption) {
	// 		return {
	// 			label: this.label, value: foundOption.data.value
	// 		}
	// 	} else {
	// 		return {
	// 			label: this.label, value: ''
	// 		}
	// 	}
	// }
	//
	// // TODO maak generiek.
	//
	// public setNewOptions(options: FormFieldOption[]): void {
	// 	// let newOptions: FormFieldOption[] = [];
	// 	// TODO remove all from the formControlArray
	// 	this._options.next(options);
	// }
	//
	// getTechnicalValue(): any {
	// 	if (!this.sendToBE) {
	// 		return undefined;
	// 	}
	//
	// 	let result: any = {};
	//
	// 	let foundOption: FormFieldOption = this._options.getValue().find(option => option.isChecked === true);
	//
	// 	if (foundOption) {
	// 		return {
	// 			optionId: foundOption.id,
	// 			optionText: foundOption.beData,
	// 		};
	// 	}
	//
	// 	result[this._id] = foundOption;
	// 	return result;
	// }
	//
	// public getConfig(minimized: boolean): any {
	// 	let result: any = super.getConfig(minimized);
	//
	// 	let options: any[] = [];
	// 	this._options.getValue().forEach(option => {
	// 		options.push({
	// 			optionId: option.id,
	// 			optionText: option.data.value
	// 		})
	// 	});
	//
	// 	result['options'] = options;
	//
	// 	return result;
	// }
	//
	// public isOptionChecked(optionID: string): boolean {
	// 	return this._options.getValue().find(option => option.id === optionID).isChecked;
	// }
	//
	// developPrefill(): void {
	// 	this._abstractControl.setValue(this._options.value[0].id);
	// }
}
