import {ComponentFactoryResolver, Directive, Input, ViewContainerRef} from '@angular/core';
import {FormFieldOption} from './options/FieldOption';

@Directive({
	selector: '[optionDirective]'
})
export class OptionDirective {
	
	public component: any;
	
	@Input()
	public set option(option: FormFieldOption) {
		this.render(option);
	};
	
	constructor(
		private resolver: ComponentFactoryResolver,
		private container: ViewContainerRef
	) {
	}
	
	public render(option: FormFieldOption) {
		const factory = this.resolver.resolveComponentFactory<any>(option.component);
		this.component = this.container.createComponent(factory);
		this.component.instance.option = option;
	}
	
}
