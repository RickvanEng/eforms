import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {MultipleChoiceFieldComponent} from './multiple-choice-field.component';
import {OptionDirective} from './option.directive';
import {MultipleChoiceCheckboxComponent} from './multiple-choice-checkbox/multiple-choice-checkbox.component';
import {MultipleChoiceRadioComponent} from './multiple-choice-radio/multiple-choice-radio.component';
import {ReactiveFormsModule} from '@angular/forms';
import {FieldErrors2Module} from '../../field-errors2/field-errors2.module';
import {QuestionMarkModule} from '../question-mark/question-mark.module';
import {MultipleChoiceObjectComponent} from './multiple-choice-object/multiple-choice-object.component';
import {ObjectOptionComponent} from './options/object-option/object-option.component';
import {OptionModule} from './options/option.module';
import {EherkVestigingComponent} from './options/eherk-vestiging/eherk-vestiging.component';
import {EherkVestigingModule} from './options/eherk-vestiging/eherk-vestiging.module';
import {HintTextModule} from '../../../../../../src/app/components/hint-text/hint-text.module';
import {MultipleChoiceImageModule} from './multiple-choice-image/multiple-choice-image.module';

@NgModule({
	imports: [
		CommonModule,
		ReactiveFormsModule,
		FieldErrors2Module,
		QuestionMarkModule,
		EherkVestigingModule,
		OptionModule,
		HintTextModule,
		MultipleChoiceImageModule,
	],
	declarations: [
		MultipleChoiceFieldComponent,
		MultipleChoiceCheckboxComponent,
		MultipleChoiceRadioComponent,
		MultipleChoiceObjectComponent,
		OptionDirective,
	],
	exports: [
		MultipleChoiceFieldComponent,
		OptionDirective
	],
	entryComponents: [
		ObjectOptionComponent,
		EherkVestigingComponent,
	]
})
export class MultipleChoiceFieldModule {
}
