import {MultipleChoiceFieldModule} from './multiple-choice-field.module';

describe('MultipleChoiceFieldModule', () => {
  let multipleChoiceFieldModule: MultipleChoiceFieldModule;

  beforeEach(() => {
    multipleChoiceFieldModule = new MultipleChoiceFieldModule();
  });

  it('should create an instance', () => {
    expect(multipleChoiceFieldModule).toBeTruthy();
  });
});
