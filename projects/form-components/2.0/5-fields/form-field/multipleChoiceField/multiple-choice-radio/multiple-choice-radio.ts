import {FormFieldWithOptions, FormFieldWithOptionsFactory} from '../MultipleChoiceField';
import {Injector} from '@angular/core';
import {FormFieldOption} from '../options/FieldOption';

export class MultipleChoiceRadioBuilder extends FormFieldWithOptionsFactory {
	// _buildingClass: any = MultipleChoiceRadio;
	//
	// public static newFactory(injector: Injector): MultipleChoiceRadioBuilder {
	// 	return new MultipleChoiceRadioBuilder(injector);
	// }
}

export class MultipleChoiceRadio extends FormFieldWithOptions {
	
	// constructor(b: MultipleChoiceRadioBuilder) {
	// 	super(b)
	// }
	//
	// getPresentationValue(): { label: string, value: string } {
	// 	let value: string = '';
	// 	if (this._abstractControl.value) {
	// 		let foundOption: FormFieldOption = this._options.getValue().find(option => option.id === this._abstractControl.value);
	// 		value = foundOption.data.value;
	// 	}
	//
	// 	return {
	// 		label: this.label, value: value
	// 	}
	// }
	//
	// public prefillValue(value: any): void {
	// 	this._abstractControl.setValue(value)
	// }
	//
	// getTechnicalValue(): any {
	// 	if (!this.sendToBE) {
	// 		return undefined;
	// 	}
	//
	// 	let result: any = {};
	// 	result[this._id] = this._abstractControl.value;
	// 	return result;
	// }
	//
	// getSelectedOptions(): FormFieldOption[] {
	// 	return this._options.value.filter(option => option.id === this._abstractControl.value);
	// }
	//
	// isOptionChecked(optionID: string): boolean {
	// 	return this._abstractControl.value === optionID;
	// }
}
