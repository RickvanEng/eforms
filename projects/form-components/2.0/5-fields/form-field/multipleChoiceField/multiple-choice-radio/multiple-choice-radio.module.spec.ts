import {MultipleChoiceRadioModule} from './multiple-choice-radio.module';

describe('MultipleChoiceRadioModule', () => {
  let multipleChoiceRadioModule: MultipleChoiceRadioModule;

  beforeEach(() => {
    multipleChoiceRadioModule = new MultipleChoiceRadioModule();
  });

  it('should create an instance', () => {
    expect(multipleChoiceRadioModule).toBeTruthy();
  });
});
