import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {MultipleChoiceRadioComponent} from './multiple-choice-radio.component';

describe('MultipleChoiceRadioComponent', () => {
  let component: MultipleChoiceRadioComponent;
  let fixture: ComponentFixture<MultipleChoiceRadioComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MultipleChoiceRadioComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MultipleChoiceRadioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
