import {Component, forwardRef} from '@angular/core';
import {MultipleChoiceFieldComponent} from '../multiple-choice-field.component';
import {MultipleChoiceRadio} from './multiple-choice-radio';
import {NG_VALUE_ACCESSOR} from '@angular/forms';

@Component({
	selector: 'app-multiple-choice-radio',
	templateUrl: './multiple-choice-radio.component.html',
	styleUrls: ['./multiple-choice-radio.component.scss'],
	providers: [{
		provide: NG_VALUE_ACCESSOR,
		useExisting: forwardRef(() => MultipleChoiceRadioComponent),
		multi: true
	}]
})
export class MultipleChoiceRadioComponent extends MultipleChoiceFieldComponent {
	public _component: MultipleChoiceRadio;
}
