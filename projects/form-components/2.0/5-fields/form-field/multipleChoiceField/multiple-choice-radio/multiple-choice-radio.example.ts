import {Injector} from '@angular/core';
import {
	ComponentExampleConfigBuilder,
	IComponentExampleConfig
} from '../../../../../../component-dash/component-example-config.builder';
import {ComponentExampleBuilder} from '../../../../../../component-dash/component-example.builder';
import {GenericFields} from '../../../presets/genericFields';
import {FormFieldOptionFactory} from '../options/FieldOption';
import {ValidatorPreBuildConfigs} from '../../../../validators/basic/ValidatorPreBuildConfigs';

export class MultipleChoiceRadioExample {
    public getField(inj: Injector): IComponentExampleConfig {

        return ComponentExampleConfigBuilder.newBuilder()
            .setComponent('Radio')
            .setExamples([
                ComponentExampleBuilder.newBuilder()
                    .setBuilder(new GenericFields(inj).radioField
                        .setValidation(ValidatorPreBuildConfigs.getInstance().required())
                        .setOptions([
                            new FormFieldOptionFactory()
                                .setId('option1')
                                .setData({value: 'option 1'})
                                .build(),
                            new FormFieldOptionFactory()
                                .setId('option2')
                                .setData({value: 'option 2'})
                                .build(),
                            new FormFieldOptionFactory()
                                .setId('option3')
                                .setData({value: 'option 3'})
                                .build()
                        ])
                        .setQuestionMarkTitle('The title of the extra info')
                        .setSaveToState(false)
                        .setLabel('This is a label')
                        .setQuestionMarkText('The extra info text')
                        .setHintText('Example of a hint text')
                        .setID('Example 1')
                    )
                    .setDescription('The basis of the radio component.' +
                        '<ul>' +
                        '<li>Should show a label</li>' +
                        '<li>Should show one or more options</li>' +
                        '<li>Should show a questionmark info text</li>' +
                        '<li>Should show a hint text</li>' +
                        '<li>It should show an FORM error when one is thrown (required)</li>' +
                        '</ul>')
                    .setState(undefined)
                    .setValidateButton(true)
                    .data,

            ])
            .data;

    }
}
