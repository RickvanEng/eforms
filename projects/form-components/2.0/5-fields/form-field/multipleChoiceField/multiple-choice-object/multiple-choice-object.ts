import {FormFieldWithOptions, FormFieldWithOptionsFactory} from '../MultipleChoiceField';
import {Injector} from '@angular/core';
import {FormFieldOption} from '../options/FieldOption';
import {AbstractControl, FormArray, FormBuilder} from '@angular/forms';
import {ValidatorPreBuildConfigs} from '../../../../validators/basic/ValidatorPreBuildConfigs';
import {EBasicValidators} from '../../../../validators/basic/basicValidatorMap';
import { IValidator } from '../../../../validators/IValidator';
import { EFormsAbstractControl, EFormsFormArray } from '../../../../base/eFormsAbstractControl';

export class MultipleChoiceObjectBuilder extends FormFieldWithOptionsFactory {
	// _buildingClass: any = MultipleChoiceObject;
	//
	// public static newFactory(injector: Injector): MultipleChoiceObjectBuilder {
	// 	return new MultipleChoiceObjectBuilder(injector);
	// }
	//
	// setValidatorRequired(msg: string = 'Dit veld is verplicht.'): any {
	// 	let validatorConfig: IValidator = ValidatorPreBuildConfigs.getInstance().required(msg);
	// 	validatorConfig.validatorId = EBasicValidators.REQUIRED_MULTIPLECHOICE;
	// 	validatorConfig.params = 1;
	// 	return super.setValidatorRequired(msg);
	// }
}

export class MultipleChoiceObject extends FormFieldWithOptions {
	
	// getAbstractControl(inj: Injector): EFormsAbstractControl {
	// 	let fb: FormBuilder = inj.get(FormBuilder);
	// 	let abstractControl: EFormsFormArray = new EFormsFormArray([], this.getFormValidators(this._validators), []);
	// 	this._options.value.forEach(option => {
	// 		abstractControl.push(option.getFormControl(inj))
	// 	});
	// 	return abstractControl;
	// }
	//
	// getPresentationValue(): {label: string, value: string} {
	// 	let value: string = '';
	//
	// 	let selectedOption: FormFieldOption = this._options.getValue().find(option => option.isChecked === true);
	// 	if (selectedOption) {
	// 		value = selectedOption.data.value;
	// 	}
	//
	// 	return {
	// 		label: this.label, value: value
	// 	}
	// }
	//
	// public setNewOptions(options: FormFieldOption[]): void {
	// 	options.forEach(option => {
	// 		(this._abstractControl as EFormsFormArray).push(option.getFormControl(this._injector))
	// 	});
	//
	// 	this._options.next(options);
	// }
	
}
