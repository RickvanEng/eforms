import {MultipleChoiceObjectModule} from './multiple-choice-object.module';

describe('MultipleChoiceObjectModule', () => {
  let multipleChoiceObjectModule: MultipleChoiceObjectModule;

  beforeEach(() => {
    multipleChoiceObjectModule = new MultipleChoiceObjectModule();
  });

  it('should create an instance', () => {
    expect(multipleChoiceObjectModule).toBeTruthy();
  });
});
