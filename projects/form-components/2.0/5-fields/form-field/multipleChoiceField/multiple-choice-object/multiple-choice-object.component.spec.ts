import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {MultipleChoiceObjectComponent} from './multiple-choice-object.component';

describe('MultipleChoiceObjectComponent', () => {
  let component: MultipleChoiceObjectComponent;
  let fixture: ComponentFixture<MultipleChoiceObjectComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MultipleChoiceObjectComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MultipleChoiceObjectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
