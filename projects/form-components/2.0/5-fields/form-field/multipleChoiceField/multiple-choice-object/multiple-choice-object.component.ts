import {Component} from '@angular/core';
import {FormFieldWithOptions} from '../MultipleChoiceField';
import {MultipleChoiceFieldComponent} from '../multiple-choice-field.component';
import {FormFieldOption} from '../options/FieldOption';

@Component({
	selector: 'app-multiple-choice-object',
	templateUrl: './multiple-choice-object.component.html',
	styleUrls: ['./multiple-choice-object.component.scss']
})
export class MultipleChoiceObjectComponent extends MultipleChoiceFieldComponent {
	public _component: FormFieldWithOptions;


	onClick(option: FormFieldOption) {
		if (!this._component.deselectAble) {
			if (option.isChecked) {
				return;
			}
		}

		this._component.getOptions.getValue().forEach(option => {
			option.setCheck(false);
		});

		option.setCheck(!option.isChecked);
	}
}
