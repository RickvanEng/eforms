import {Component} from '@angular/core';
import {FormFieldComponent} from '../form-field.component';
import {LabelField} from './LabelField';

@Component({
	selector: 'app-label-field',
	templateUrl: './label-field.component.html',
	styleUrls: ['./label-field.component.scss']
})
export class LabelFieldComponent extends FormFieldComponent {
	public _component: LabelField;
}
