import {FormField, FormFieldBuilder} from '../FormField';
import {Injector} from '@angular/core';
import {WarningType} from '../../WarningType';
import {AbstractControl, FormControl} from "@angular/forms";
import {ValidatorPreBuildConfigs} from "../../../validators/basic/ValidatorPreBuildConfigs";

export class LabelFieldBuilder extends FormFieldBuilder {
	// get variableLabel(): { prefix: string; field: FormField }[] {
	// 	return this._variableLabel;
	// }
	// get warningType(): WarningType {
	// 	return this._warningType;
	// }
	//
	// get blocking(): boolean {
	// 	return this._blocking;
	// }
	// get errorObject(): any {
	// 	return this._errorObject;
	// }
	//
	// protected _buildingClass: any = LabelField;
	// private _variableLabelBuilder: {prefix: string, field: FormFieldBuilder}[] = [];
	// private _variableLabel: {prefix: string, field: FormField}[] = [];
	// private _warningType: WarningType;
	// private _blocking: boolean = false;
	// private _errorObject: any;
	//
	// public static newFactory(injector: Injector): LabelFieldBuilder {
	// 	return new LabelFieldBuilder(injector);
	// }
	//
	// public setVariableLabel(elements: {prefix: string, field: FormFieldBuilder}[]): LabelFieldBuilder {
	// 	this._variableLabelBuilder = elements;
	// 	return this;
	// }
	//
	// public setAsWarning(type: WarningType): LabelFieldBuilder {
	// 	this._warningType = type;
	// 	return this;
	// }
	//
	// public setBlocking(val: boolean, error: any): LabelFieldBuilder {
	// 	this._blocking = val;
	// 	this._errorObject = error;
	// 	this._validators.push(ValidatorPreBuildConfigs.getInstance().labelValidator())
	// 	return this;
	// }
	//
	// protected prebuild(): void {
	// 	super.prebuild();
	// 	this._variableLabelBuilder.forEach(builder => {
	// 		if (builder.field) {
	// 			this._variableLabel.push({prefix: builder.prefix, field: builder.field.build()})
	// 		} else {
	// 			this._variableLabel.push({prefix: builder.prefix, field: undefined})
	// 		}
	// 	});
	// }
}

export class LabelField extends FormField {
	// get warningType(): WarningType {
	// 	return this._warningType;
	// }
	//
	// get blocking(): boolean {
	// 	return this._blocking;
	// }
	//
	// get errorObject(): boolean {
	// 	return this._errorObject;
	// }
	//
	// private _warningType: WarningType;
	// private _blocking: boolean;
	// private _errorObject: any;
	// private _variableLabel: {prefix: string, field: FormField}[];
	//
	// constructor(builder: LabelFieldBuilder) {
	// 	super(builder);
	// 	this._warningType = builder.warningType;
	// 	this._blocking = builder.blocking;
	// 	this._errorObject = builder.errorObject;
	// 	this._variableLabel = builder.variableLabel;
	//
	// 	this._variableLabel.forEach(element => {
	// 		if (element.field) {
	// 			element.field.trigger.subscribe(value => {
	// 				if (value) {
	// 					this.checkVariableLabel();
	// 				}
	// 			})
	// 		}
	// 	})
	// }
	//
	// registerComponent(): void {
	//
	// }
	//
	// private checkVariableLabel(): void {
	// 	let newLabel: string = '';
	//
	// 	this._variableLabel.forEach(element => {
	// 		if (element.field) {
	// 			newLabel = newLabel.concat(element.prefix + element.field.getPresentationValue().value);
	// 		} else {
	// 			newLabel = newLabel.concat(element.prefix);
	// 		}
	// 	});
	//
	// 	this._label = newLabel;
	// }
	//
	// public getPdfValue(): any {
	// 	let result: any = super.getPdfValue();
	// 	return result;
	// }
}
