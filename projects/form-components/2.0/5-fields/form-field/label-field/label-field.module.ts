import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {LabelFieldComponent} from './label-field.component';
import {QuestionMarkModule} from '../question-mark/question-mark.module';

@NgModule({
  imports: [
    CommonModule,
    QuestionMarkModule,
  ],
  exports: [LabelFieldComponent],
  declarations: [LabelFieldComponent]
})
export class LabelFieldModule { }
