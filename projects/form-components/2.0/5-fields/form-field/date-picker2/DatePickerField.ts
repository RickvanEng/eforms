import {FormField, FormFieldBuilder} from '../FormField';
import {Injector} from '@angular/core';
import * as moment from 'moment';
import {DateTypes} from './date-type';
import {EBasicValidators} from '../../../validators/basic/basicValidatorMap';
import {ValidatorPreBuildConfigs} from '../../../validators/basic/ValidatorPreBuildConfigs';
import {ComponentSingleton} from '../../../1-form/component.service';
import { IValidator } from '../../../validators/IValidator';
import { EFormsAbstractControl } from '../../../base/eFormsAbstractControl';

export class DatePickerFieldFactory extends FormFieldBuilder {
	
	// get hasDynamicMinValidator(): boolean {
	// 	return this._hasDynamicMinValidator;
	// }
	//
	// get hasDynamicMaxValidator(): boolean {
	// 	return this._hasDynamicMaxValidator;
	// }
	//
	// public get minDate(): Date {
	// 	return this._minDate;
	// }
	//
	// public get maxDate(): Date {
	// 	return this._maxDate;
	// }
	//
	// public get sendIso(): boolean {
	// 	return this._sendIso;
	// }
	//
	// private _minDate: Date;
	// private _maxDate: Date;
	// private _hasDynamicMinValidator: boolean = false;
	// private _hasDynamicMaxValidator: boolean = false;
	//
	// private _sendIso: boolean;
	//
	// public static newFactory(injector: Injector): DatePickerFieldFactory {
	// 	return new DatePickerFieldFactory(injector);
	// }
	//
	// // TODO de _minDate moet ook worden geset in setValidator
	// public setMinDate(value: Date, msg?: string): DatePickerFieldFactory {
	// 	let validatorConfig: IValidator = ValidatorPreBuildConfigs.getInstance().minDate(value);
	//
	// 	if (msg) {
	// 		validatorConfig.responses[0].msg = msg;
	// 	}
	//
	// 	this._validators.push(validatorConfig);
	// 	return this;
	// }
	//
	// public setMaxDate(value: Date, msg?: string): DatePickerFieldFactory {
	// 	let validatorConfig: IValidator = ValidatorPreBuildConfigs.getInstance().maxDate(value);
	//
	// 	if (msg) {
	// 		validatorConfig.responses[0].msg = msg;
	// 	}
	//
	// 	this._validators.push(validatorConfig);
	// 	return this;
	// }
	//
	// public setDynamicMinDate(formfield?: DatePickerFieldFactory, amount: number = 0, unit: DateTypes = 'days'): DatePickerFieldFactory {
	// 	this._hasDynamicMinValidator = true;
	// 	let validatorConfig: IValidator = ValidatorPreBuildConfigs.getInstance().dyanmicMinDate(formfield.id);
	// 	this._validators.push(validatorConfig);
	// 	validatorConfig.fieldToListenTo = formfield.id;
	// 	return this;
	// }
	//
	// public setDynamicMaxDate(formfield: DatePickerFieldFactory, amount: number = 0, unit: DateTypes = 'days'): DatePickerFieldFactory {
	// 	this._hasDynamicMaxValidator = true;
	// 	let validatorConfig: IValidator = ValidatorPreBuildConfigs.getInstance().dyanmicMaxDate(formfield.id);
	// 	validatorConfig.fieldToListenTo = formfield.id;
	// 	this._validators.push(validatorConfig);
	// 	return this;
	// }
	//
	// public setDynamicWarning(formfield: DatePickerFieldFactory, amount: number = 0, unit: DateTypes = 'days', msg: string): DatePickerFieldFactory {
	//
	// 	// let validator: DatePickerCustomValidator = new DatePickerCustomValidatorBuilder(this.injector, formfield, amount, unit, 'max').build();
	// 	// let newValidator: FormFieldValidatorConfigBuilder = FormFieldValidatorConfigBuilder.newBuilder(this.injector)
	// 	// 	.setErrorID('dynamicWarning')
	// 	// 	.setFieldValidator(Validators.pattern(''))
	// 	// 	.setValidator(validator)
	// 	// 	.setLabelType(WarningType.WARNING)
	// 	// 	.setBlocking(false)
	// 	// 	.setMessage(msg)
	// 	// 	.setThrowError(FormErrors.getInstance().dynamicDate);
	// 	//
	// 	// this._validatorsBuilders.push(newValidator);
	//
	// 	return this;
	// }
	//
	// public setSendIso(): DatePickerFieldFactory {
	// 	this._sendIso = true;
	// 	return this;
	// }
	//
	// protected _buildingClass: any = DatePickerField;
}

export class DatePickerField extends FormField {
	// get minDate(): Date {
	// 	let foundValidation: IValidator = this._validators.find(val => val.validatorId === EBasicValidators.MIN_DATE);
	// 	return foundValidation ? foundValidation.params : undefined;
	// }
	//
	// get maxDate(): Date {
	// 	let foundValidation: IValidator = this._validators.find(val => val.validatorId === EBasicValidators.MAX_DATE);
	// 	return foundValidation ? foundValidation.params : undefined;
	// }
	//
	// get sendIso(): boolean {
	// 	return this._sendIso;
	// }
	//
	// public warning: boolean;
	// private _minDate: Date;
	// private _maxDate: Date;
	// private _sendIso: boolean;
	// // private _dynamicWarning: { field: DatePickerField, config: FormValidator, validator: FormValidatorDate };
	//
	// _builder: DatePickerFieldFactory;
	//
	// constructor(builder: DatePickerFieldFactory) {
	// 	super(builder);
	// 	this._minDate = builder.minDate;
	// 	this._maxDate = builder.maxDate;
	// 	this._sendIso = builder.sendIso;
	// }
	//
	// getAbstractControl(inj: Injector): EFormsAbstractControl {
	// 	if (this._builder.hasDynamicMinValidator) {
	//
	// 		let dateValidator: IValidator = this._validators.find(config => config.validatorId === EBasicValidators.MIN_DATE);
	//
	// 		let fieldToListenTo: DatePickerField = ComponentSingleton.getInstance().getComponent(dateValidator.fieldToListenTo) as DatePickerField;
	// 		fieldToListenTo.abstractControl.statusChanges.subscribe(status => {
	// 			if (status === 'VALID') {
	// 				let date: Date = new Date(fieldToListenTo.abstractControl.value);
	// 				dateValidator.responses[0].msg = 'De gekozen datum moet liggen na: ' + moment(date).format('DD-MM-YYYY');
	// 				dateValidator.params = date;
	// 			}
	// 		});
	// 	}
	//
	// 	if (this._builder.hasDynamicMaxValidator) {
	// 		let dateValidator: IValidator = this._validators.find(config => config.validatorId === EBasicValidators.MAX_DATE);
	//
	// 		let fieldToListenTo: DatePickerField = ComponentSingleton.getInstance().getComponent(dateValidator.fieldToListenTo) as DatePickerField;
	// 		fieldToListenTo.abstractControl.statusChanges.subscribe(status => {
	// 			if (status === 'VALID') {
	// 				let date: Date = new Date(fieldToListenTo.abstractControl.value);
	// 				dateValidator.responses[0].msg = 'De gekozen datum moet liggen voor: ' + moment(date).format('DD-MM-YYYY');
	// 				dateValidator.params = date;
	// 			}
	// 		});
	// 	}
	//
	// 	return super.getAbstractControl(inj);
	// }
	//
	// public getValue(): any {
	// 	return moment(this._abstractControl.value).format('YYYY-MM-DD');
	// }
	//
	// getPresentationValue(): { label: string, value: string } {
	// 	if (this._abstractControl.value) {
	// 		return {
	// 			label: this.label, value: moment(this.abstractControl.value, 'YYYY-MM-DD').format('DD-MM-YYYY')
	// 		}
	// 	} else {
	// 		return {
	// 			label: this.label, value: ''
	// 		}
	// 	}
	// }
	//
	// getPdfValue(): any {
	// 	if (!this.sendToBE) {
	// 		return undefined;
	// 	}
	//
	// 	let value: string = '';
	// 	if (this.abstractControl.value) {
	// 		value = moment(this._abstractControl.value, 'DD-MM-YYYY').format('YYYY-MM-DD');
	// 	}
	//
	// 	return {
	// 		"veldnaam": this.id,
	// 		"inhoud": value,
	// 		"label": this.label,
	// 		"enkelIntern": "false"
	// 	}
	// }
	//
	// getTechnicalValue(): any {
	// 	if (!this.sendToBE) {
	// 		return undefined;
	// 	}
	//
	// 	let result: any = {};
	// 	result[this._id] = this._abstractControl.value ? moment(this._abstractControl.value, 'DD-MM-YYYY').format('YYYY-MM-DD') : '';
	// 	return result;
	// }
	//
	// getDefaultValue(): any {
	// 	return new Date();
	// }
}
