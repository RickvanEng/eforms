import {Injector} from '@angular/core';
import {GenericFields} from '../../presets/genericFields';
import {
    ComponentExampleConfigBuilder,
    IComponentExampleConfig
} from '../../../../../component-dash/component-example-config.builder';
import {ComponentExampleBuilder} from '../../../../../component-dash/component-example.builder';
import {DatePickerFieldFactory} from './DatePickerField';
import {FormSetBuilder} from '../../../4-set/FormSet';
import {GenericTabFieldSets} from '../../../3-tab-field-set/presets/genericTabFieldSets';
import {TabFieldSetBuilder} from '../../../3-tab-field-set/tab-field-set';

export class DatePicker2Example {
    
    private fieldToListenTo: DatePickerFieldFactory;
    private mainExampleField: DatePickerFieldFactory;

    private getNewDate(): Date {
        return new Date();
    }

    private getDateInFuture(): Date {
        let date: Date = this.getNewDate();
        date.setDate(date.getDate() + 10);
        return date;
    }

    public getField(inj: Injector): IComponentExampleConfig {
        let genericFieldSets: GenericTabFieldSets = new GenericTabFieldSets(inj);
        
        let tabSet: TabFieldSetBuilder = genericFieldSets.getNewDefaultTabSet()
            .setID('tabSet')
            .setLabel('TabSet');
        
        let set: FormSetBuilder = genericFieldSets.getNewSet()
            .setID('testSet')
            .setLabel('set');
        
        this.fieldToListenTo = new GenericFields(inj).datePicker
            .setLabel('First field!')
            .setValidatorRequired()
            .setSaveToState(false)
            .setHintText('Fill this one to test the dynamic minDate on the next field!')
        
        this.mainExampleField = new GenericFields(inj).datePicker
            // .setAsyncValidation(AsyncValidatorPrebuildConfig.getInstance().birthday())
            .setDynamicMinDate(this.fieldToListenTo)
            // .setValidation(ValidatorPreBuildConfigs.getInstance().minDate(this.getNewDate()))
            // .setValidation(ValidatorPreBuildConfigs.getInstance().maxDate(this.getDateInFuture()))
            .setValidatorRequired()
            .setSaveToState(false)
            .setLabel('Datepicawdawdwadker')
            .setQuestionMarkTitle('The title of the extra info')
            .setQuestionMarkText('The extra info text')
            .setHintText('Example of a hint text')
            .setID('datepickerExample');

        return ComponentExampleConfigBuilder.newBuilder()
            .setComponent('Datepicker')
            .setExamples([
                ComponentExampleBuilder.newBuilder()
                    .setBuilder(tabSet.setChildren([
                        set.setChildren([
                            this.fieldToListenTo,
                            this.mainExampleField,
                        ])
                    ]))
                    .setDescription('The date picker field.' +
                        '<ul>' +
                        '<li>Should show a label</li>' +
                        '<li>Should show a input area</li>' +
                        '<li>Should show a questionmark info text</li>' +
                        '<li>Should show a hint text</li>' +
                        '<li>Should show a calender picker</li>' +
                        '<li>It should show an FORM error when one is thrown (required)</li>' +
                        '</ul>'
                    )
                    .setState(undefined)
                    .setValidateButton(true)
                    .data,
                // ComponentExampleBuilder.newBuilder()
                //     .setBuilder(new GenericFields(inj).datePicker
                //         .setDynamicMinDate(this.mainExampleField)
                //         .setSaveToState(false)
                //         .setLabel('Datepicker')
                //         .setID('dynamicMinDateExample')
                //     )
                //     .setDescription('Dynamic min date. The first example date is set as the' +
                //         'min date for this datepicker')
                //     .setState(undefined)
                //     .setValidateButton(true)
                //     .data,
                // ComponentExampleBuilder.newBuilder()
                //     .setBuilder(new GenericFields(inj).datePicker
                //         .setDynamicMaxDate(this.mainExampleField)
                //         .setSaveToState(false)
                //         .setLabel('Datepicker')
                //         .setID('dyanmicMaxDateExample')
                //     )
                //     .setDescription('Dynamic max date. The first example date is set as the' +
                //         'max date for this datepicker')
                //     .setState(undefined)
                //     .setValidateButton(true)
                //     .data,
            ])
            .data;

    }
}
