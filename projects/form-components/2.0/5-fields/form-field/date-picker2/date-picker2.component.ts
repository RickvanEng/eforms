import { Component, forwardRef } from '@angular/core';
import { FormFieldComponent } from '../form-field.component';
import { NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import { DatePickerField } from './DatePickerField';

@Component({
    selector: 'app-date-picker2',
    templateUrl: './date-picker2.component.html',
    styleUrls: ['./date-picker2.component.scss'],
    providers: [
        { provide: NG_VALUE_ACCESSOR, useExisting: forwardRef(() => DatePicker2Component), multi: true },
    ],
})
export class DatePicker2Component extends FormFieldComponent {
    model: NgbDateStruct;
    public _component: DatePickerField;

    public hasError(key: string): boolean {
        let errors: any = this._component.abstractControl.errors;
        if (errors) {
            for (let error of Object.keys(errors)) {
                if (error === key) {
                    return true;
                }
            }

            return false;
        }
    }
}
