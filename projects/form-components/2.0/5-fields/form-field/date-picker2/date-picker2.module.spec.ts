import {DatePicker2Module} from './date-picker2.module';

describe('DatePicker2Module', () => {
  let datePicker2Module: DatePicker2Module;

  beforeEach(() => {
    datePicker2Module = new DatePicker2Module();
  });

  it('should create an instance', () => {
    expect(datePicker2Module).toBeTruthy();
  });
});
