import {DateDirectionTypes, DateTypes} from './date-type';
import {FormValidatorBuilder, IFormValidatorResponse} from '../../validators/validator/FormValidator';
import {Injector} from '@angular/core';
import {FormValidatorDateBuilder} from '../../validators/validator/FormValidatorDate';
import * as moment from 'moment';
import {DatePickerField} from './DatePickerField';
import {FormFieldBuilder} from '../FormField';

export class DatePickerCustomValidatorBuilder extends FormValidatorBuilder {
    get fieldToListenTo(): FormFieldBuilder {
        return this._fieldToListenTo;
    }
    get amount(): number {
        return this._amount;
    }

    get unit(): DateTypes {
        return this._unit;
    }

    get direction(): DateDirectionTypes {
        return this._direction;
    }

    private _amount: number = 0;
    private _unit: DateTypes = 'days';
    private _direction: DateDirectionTypes = 'min';
    private _fieldToListenTo: FormFieldBuilder;

    // date om tegen te valideren, plus groter of kleiner
    constructor(injector: Injector, fieldToListenTo: FormFieldBuilder, amount: number, unit: DateTypes, direction: DateDirectionTypes) {
        super(injector);
        this._amount = amount;
        this._fieldToListenTo = fieldToListenTo;
        this._unit = unit;
        this._direction = direction;
    }

    public static newBuilder(injector: Injector, amount: number = 0, unit: DateTypes = 'days', direction: DateDirectionTypes = 'min'): FormValidatorDateBuilder {
        return new FormValidatorDateBuilder(injector, amount, unit, direction);
    }

    public build(): DatePickerCustomValidator {
        return new DatePickerCustomValidator(this);
    }
}

export class DatePickerCustomValidator extends FormValidator {
    private _amount: number = 0;
    private _unit: DateTypes = 'days';
    private _direction: DateDirectionTypes = 'min';
    private _fieldToListenTo: DatePickerField;

    protected _injector: Injector;

    get amount(): number {
        return this._amount;
    }

    get unit(): DateTypes {
        return this._unit;
    }

    get direction(): DateTypes {
        return this._unit;
    }

    // set params
    constructor(b: DatePickerCustomValidatorBuilder) {
        super(b);
        this._amount = b.amount;
        this._fieldToListenTo = b.fieldToListenTo.build();
        this._unit = b.unit;
        this._direction = b.direction;
    }

    public isValid(body: any): Promise<IFormValidatorResponse> {
        return new Promise<IFormValidatorResponse>(resolve => {
            this.executeValidateLogic(body).then(data => {
                resolve(data);
            })
        });
    }

    public hasDate(date: string): boolean {
        return date && date !== '' && date !== 'Invalid date';
    }

    protected executeValidateLogic(body: any): Promise<IFormValidatorResponse> {
        return new Promise<IFormValidatorResponse>(resolve => {
            // check for nulls
            let errorResult: IFormValidatorResponse = {
                error: false,
                statusTekst: '',
            };
    
            if (this.hasDate(body) && this._fieldToListenTo && this.hasDate(this._fieldToListenTo.getValue())) {
                // Date input op huidige veld
                const fieldDate = moment(body, 'YYYY-MM-DD');
    
                // Date van externe veld
                const minDate = moment(this._fieldToListenTo.getValue(), 'YYYY-MM-DD').add(this._amount, this._unit);
                if (this._direction === 'min') {
                    errorResult.error = !fieldDate.isSameOrAfter(minDate);
                } else {
                    errorResult.error = !fieldDate.isSameOrBefore(minDate);
                }
            }
    
            resolve(errorResult);
        });
    }
}
