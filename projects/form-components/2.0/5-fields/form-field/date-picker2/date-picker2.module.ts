import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {DatePicker2Component} from './date-picker2.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';
import {BrowserModule} from '@angular/platform-browser';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {MatDatepickerModule} from '@angular/material';
import {FieldErrors2Module} from '../../field-errors2/field-errors2.module';
import { QuestionMarkModule } from '../question-mark/question-mark.module';
import {NgbDateNLParserFormatter} from './ngb-date-nl-parser-formatter';

@NgModule({
	imports: [
		ReactiveFormsModule,
		FormsModule,
		CommonModule,
		BrowserModule,
		NgbModule,
		MatDatepickerModule,
		FontAwesomeModule,
		FieldErrors2Module,
		QuestionMarkModule,
	],
	providers: [NgbDateNLParserFormatter],
	declarations: [DatePicker2Component]
})
export class DatePicker2Module {
}
