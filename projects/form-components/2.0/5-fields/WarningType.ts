export enum WarningType {
	DANGER = 'alert alert-danger',
	WARNING = 'alert alert-warning',
	INFO = 'alert alert-info',
	SUCCES = 'alert alert-success',
	PRIMARY = 'alert alert-primary',
	NONE = ''
}
