import {FormFieldBuilder} from '../form-field/FormField';
import {BasicInputField2Component} from '../form-field/basic-input-field2/basic-input-field2.component';
import {DatePicker2Component} from '../form-field/date-picker2/date-picker2.component';
import {Injector} from '@angular/core';
import {TextAreaFieldFactory} from '../form-field/textArea/TextAreaField';
import {SmallTextAreaComponent} from '../form-field/textArea/small-text-area/small-text-area.component';
import {LargeTextAreaComponent} from '../form-field/textArea/large-text-area/large-text-area.component';
import {LabelFieldBuilder} from '../form-field/label-field/LabelField';
import {LabelFieldComponent} from '../form-field/label-field/label-field.component';
import {GenericErrorPatterns} from '../../../../forms/services/genericFormService/validators/generic-error-patterns';
import {DatePickerFieldFactory} from '../form-field/date-picker2/DatePickerField';
import {FileUploadBuilder} from "../form-field/file-upload/file-upload";
import {MultipleChoiceRadioComponent} from "../form-field/multipleChoiceField/multiple-choice-radio/multiple-choice-radio.component";
import {FileUploadComponent} from "../form-field/file-upload/file-upload.component";
import {MultipleChoiceImageBuilder} from '../form-field/multipleChoiceField/multiple-choice-image/multiple-choice-image';
import {MultipleChoiceImageComponent} from '../form-field/multipleChoiceField/multiple-choice-image/multiple-choice-image.component';
import {MultipleChoiceCheckboxComponent} from '../form-field/multipleChoiceField/multiple-choice-checkbox/multiple-choice-checkbox.component';
import {SingleCheckboxComponent} from '../form-field/single-checkbox/single-checkbox.component';
import {TimePickerComponent} from '../form-field/time-picker/time-picker.component';
import {TimePickerFieldFactory} from '../form-field/time-picker/TimePickerField';
import {BasicInputFieldUnitsBuilder} from '../form-field/basic-input-field-units/basic-input-field-units';
import {BasicInputFieldUnitComponent} from '../form-field/basic-input-field-units/basic-input-field-units.component';
import {MultipleChoiceRadioBuilder} from '../form-field/multipleChoiceField/multiple-choice-radio/multiple-choice-radio';
import {MultipleChoiceCheckboxBuilder} from '../form-field/multipleChoiceField/multiple-choice-checkbox/multiple-choice-checkbox';
import {DropdownNormalBuilder} from '../form-field/dropdown/dropdownNormal';
import {DropdownComponent} from '../form-field/dropdown/dropdown.component';
import {MultipleChoiceObjectBuilder} from '../form-field/multipleChoiceField/multiple-choice-object/multiple-choice-object';
import {FormFieldOptionFactory} from '../form-field/multipleChoiceField/options/FieldOption';
import {ButtonBuilder} from '../form-field/button/button';
import {ButtonComponent} from '../form-field/button/button.component';
import {MultipleChoiceObjectComponent} from '../form-field/multipleChoiceField/multiple-choice-object/multiple-choice-object.component';
import {ValidatorPreBuildConfigs} from '../../validators/basic/ValidatorPreBuildConfigs';
import {LeafletBuilder} from '../form-field/leaflet/leaflet';
import {LeafletPointerOptions} from '../form-field/leaflet/src/leaflet-pointerOptions';
import {LeafletMapOptions} from '../form-field/leaflet/src/leaflet-mapOptions';
import {latLng} from 'leaflet';
import {LeafletComponent} from '../form-field/leaflet/leaflet.component';
import {BasicInputFieldActionBuilder} from '../form-field/basic-input-field-action/basic-input-field-action';
import { AsyncValidatorPrebuildConfig } from '../../validators/async/asyncValidatorPrebuildConfig';

export class GenericFields {

    constructor(private injector: Injector) {
    }

    public get label(): LabelFieldBuilder {
        return LabelFieldBuilder.newFactory(this.injector)
            .setComponent(LabelFieldComponent)
            .setSendToBE(false)
            .setShowInOverviewSet(false) as LabelFieldBuilder
    }

    public get imagePicker(): MultipleChoiceImageBuilder {
        return MultipleChoiceImageBuilder.newBuilder(this.injector)
            .setComponent(MultipleChoiceImageComponent) as MultipleChoiceImageBuilder;
    }

    public button: ButtonBuilder = ButtonBuilder.newBuilder(this.injector)
        .setButtonText('Click me')
        .setComponent(ButtonComponent)
        .setSendToBE(false) as ButtonBuilder;

    public get fileUpload(): FileUploadBuilder {
        return FileUploadBuilder.newBuilder(this.injector)
            .setSaveToState(false)
            .setHintText('U kunt .jpg, .png, .txt, .pdf, .doc of .xlsx bestanden meesturen.')
            // .setQuestionMarkTitle('Bestand uploaden')
            // .setQuestionMarkText('Alleen foto\'s in de bestandsformaten: JPEG, PNG, PDF en GIF zijn toegestaan. De foto mag maximaal 4mb zijn in een raster van 1980 bij 1020 pixels.')
            // .setShowQuestionMark(true)
            .setComponent(FileUploadComponent)
            .setSendToBE(false) as FileUploadBuilder;
    }

    public get basicField(): FormFieldBuilder {
        return FormFieldBuilder.newBuilder(this.injector)
            .setValidatorMaxLength(100)
            .setComponent(BasicInputField2Component)
    }

    public get basicFieldUnits(): BasicInputFieldUnitsBuilder {
        return BasicInputFieldUnitsBuilder.newBuilder(this.injector)
            .setValidatorMaxLength(100)
            .setComponent(BasicInputFieldUnitComponent) as BasicInputFieldUnitsBuilder;
    }

    public get basicFieldKilometer(): BasicInputFieldUnitsBuilder {
        return BasicInputFieldUnitsBuilder.newBuilder(this.injector)
            .setSuffix('kilometer')
            .setCommaToDot()
            .setValidatorPattern(GenericErrorPatterns.kilometers, GenericErrorPatterns.kilometersErrorMessage)
            .setComponent(BasicInputFieldUnitComponent) as BasicInputFieldUnitsBuilder;
    }

    public get basicFieldMeter(): BasicInputFieldUnitsBuilder {
        return BasicInputFieldUnitsBuilder.newBuilder(this.injector)
            .setSuffix('meter')
            .setCommaToDot()
            .setValidatorPattern(GenericErrorPatterns.meters, GenericErrorPatterns.metersErrorMessage)
            .setComponent(BasicInputFieldUnitComponent) as BasicInputFieldUnitsBuilder;
    }

    public get radioField(): MultipleChoiceRadioBuilder {
        return MultipleChoiceRadioBuilder.newFactory(this.injector)
            .setSingleSelect(true)
            .setComponent(MultipleChoiceRadioComponent) as MultipleChoiceRadioBuilder;
    }

    public get multipleChoiceObjectField(): MultipleChoiceObjectBuilder {
        return MultipleChoiceObjectBuilder.newFactory(this.injector)
            .setComponent(MultipleChoiceObjectComponent) as MultipleChoiceObjectBuilder;
    }

    public get checkboxField(): MultipleChoiceCheckboxBuilder {
        return MultipleChoiceCheckboxBuilder.newFactory(this.injector)
            .setComponent(MultipleChoiceCheckboxComponent) as MultipleChoiceCheckboxBuilder;
    }

    public get singleCheckboxField(): MultipleChoiceCheckboxBuilder {
        return MultipleChoiceCheckboxBuilder.newFactory(this.injector)
            .SetIsSingleCheckbox(true)
            .setComponent(SingleCheckboxComponent) as MultipleChoiceCheckboxBuilder;
    }

    public get bsn(): FormFieldBuilder {
        return FormFieldBuilder.newBuilder(this.injector)
            .setID('bsn')
            .setLabel('Burgerservicenummer (BSN)')
            .setValidatorPattern(GenericErrorPatterns.bsn, GenericErrorPatterns.bsnErrorMessage)
            .setComponent(BasicInputField2Component)
    }

    public get voorletters(): FormFieldBuilder {
        return FormFieldBuilder.newBuilder(this.injector)
            .setID('voorletters')
            .setLabel('Voorletter(s)')
            .setComponent(BasicInputField2Component)
            .setValidatorMaxLength(20)
            .setValidatorPattern(GenericErrorPatterns.voorletters, GenericErrorPatterns.voorlettersErrorMessage)
    }

    public get voorvoegsel(): FormFieldBuilder {
        return FormFieldBuilder.newBuilder(this.injector)
            .setID('voorvoegselGeslachtsnaam')
            .setLabel('Tussenvoegsel(s)')
            .setComponent(BasicInputField2Component)
            .setValidatorMaxLength(10)
    }

    public get geslachtsnaam(): FormFieldBuilder {
        return FormFieldBuilder.newBuilder(this.injector)
            .setID('geslachtsnaam')
            .setLabel('Achternaam')
            .setComponent(BasicInputField2Component)
            .setValidatorMaxLength(100)
            .setValidatorPattern(GenericErrorPatterns.geslachtsnaam, GenericErrorPatterns.geslachtsnaamErrorMessage)
    }

    public get datePicker(): DatePickerFieldFactory {
        return DatePickerFieldFactory.newFactory(this.injector)
            .setID('datepicker')
            .setLabel('Date Picker')
            .setComponent(DatePicker2Component)
            .setHintText('Bijvoorbeeld 01-10-2004') as DatePickerFieldFactory;
    }

    public get timePicker(): TimePickerFieldFactory {
        return TimePickerFieldFactory.newBuilder(this.injector)
            .setComponent(TimePickerComponent) as TimePickerFieldFactory;
    }

    public get straat(): FormFieldBuilder {
        return FormFieldBuilder.newBuilder(this.injector)
            .setID('straatnaam')
            .setLabel('Straatnaam')
            .setComponent(BasicInputField2Component)
            .setValidatorMaxLength(100)
            .setValidatorPattern(GenericErrorPatterns.straatnaam, GenericErrorPatterns.straatnaamErrorMessage)
    }

    public get plaats(): FormFieldBuilder {
        return FormFieldBuilder.newBuilder(this.injector)
            .setID('plaats')
            .setLabel('Plaats')
            .setComponent(BasicInputField2Component)
            .setValidatorMinLength(2)
            .setValidatorMaxLength(100)
            .setValidatorPattern(GenericErrorPatterns.plaats, GenericErrorPatterns.plaatsErrorMessage)
    }

    public get huisnummer(): FormFieldBuilder {
        return FormFieldBuilder.newBuilder(this.injector)
            .setID('huisnummer')
            .setLabel('Huisnummer')
            .setComponent(BasicInputField2Component)
            .setValidatorMaxLength(6)
            .setValidatorPattern(GenericErrorPatterns.huisnummer, GenericErrorPatterns.huisnummerErrorMessage)
    }

    public get huisnummerletter(): FormFieldBuilder {
        return FormFieldBuilder.newBuilder(this.injector)
            .setID('verblijfhuisnummerletter')
            .setLabel('Huisnummerletter')
            .setComponent(BasicInputField2Component)
    }

    public get huisnummertoevoeging(): FormFieldBuilder {
        return FormFieldBuilder.newBuilder(this.injector)
            .setID('verblijfhuisnummertoevoeging')
            .setLabel('Huisnummertoevoeging')
            .setComponent(BasicInputField2Component)
            .setValidatorMaxLength(10)
    }

    public get postcode(): FormFieldBuilder {
        return FormFieldBuilder.newBuilder(this.injector)
            .setID('verblijfpostcode')
            .setLabel('Postcode')
            .setComponent(BasicInputField2Component)
            .setValidatorPattern('^[1-9][0-9]{3} ?[A-Za-z]{2}$', 'Uw postcode is een Nederlandse postcode en bestaat uit 4 cijfers en 2 letters.')
            .setHintText('Bijvoorbeeld 1234AA')

    }

    public get telefoon(): FormFieldBuilder {
        return FormFieldBuilder.newBuilder(this.injector)
            .setID('telefoonnummer')
            .setLabel('Telefoonnummer')
            .setComponent(BasicInputField2Component)
            .setValidatorMinLength(10)
            .setValidatorMaxLength(10)
            .setValidatorPattern(GenericErrorPatterns.telefoon, GenericErrorPatterns.telefoonErrorMessage)
            .setHintText('Een telefoonnummer bestaat uit 10 cijfers zonder streepje ertussen, bijvoorbeeld 0612345678')
            .setIcon('phone')
    }

    public get email(): FormFieldBuilder {
        return FormFieldBuilder.newBuilder(this.injector)
            .setID('email')
            .setLabel('E-mailadres')
            .setComponent(BasicInputField2Component)
            .setValidatorEmail()
            .setHintText('Een e-mailadres ziet er zo uit: naam@bedrijf.nl')
            .setIcon('envelope')
    }

    public get dropdownNormal(): DropdownNormalBuilder {
        return DropdownNormalBuilder.newFactory(this.injector)
            .setComponent(DropdownComponent) as DropdownNormalBuilder
    }

    public get smallTextArea(): TextAreaFieldFactory {
        return TextAreaFieldFactory.newFactory(this.injector)
            .setMaxInput(500)
            .setComponent(SmallTextAreaComponent) as TextAreaFieldFactory
    }

    public get largeTextArea(): TextAreaFieldFactory {
        return TextAreaFieldFactory.newFactory(this.injector)
            .setMaxInput(1000)
            .setComponent(LargeTextAreaComponent) as TextAreaFieldFactory
    }

    public get waarheidCheckbox(): MultipleChoiceCheckboxBuilder {
        return this.singleCheckboxField
            .setOverViewValue({trueValue: 'Ja', falseValue: 'Nee'})
            .setOptions([
                FormFieldOptionFactory.newFactory()
                    .setId('waarheidAkkoord_option')
                    .setData({value: 'Ik verklaar dat ik deze aanvraag naar waarheid heb ingevuld en geen informatie heb verzwegen.'},
                        'Ik verklaar dat ik deze aanvraag naar waarheid heb ingevuld en geen informatie heb verzwegen.')
                    .build()
            ])
            .setValidatorRequired('U moet akkoord gaan met de verklaring om het formulier te versturen.')
            .setShowInOverviewSet(false) as MultipleChoiceCheckboxBuilder;
    }

    public get privacyCheckbox(): MultipleChoiceCheckboxBuilder {
        return this.singleCheckboxField
            .setOptions([
                FormFieldOptionFactory.newFactory()
                    .setId('privacyAkkoord_option')
                    .setData({value: 'Ja, ik ga akkoord met het verwerken van mijn gegevens zoals beschreven staat in de <a href="https://haarlemmermeergemeente.nl/privacyverklaring" target="blank">privacyverklaring.</a>'},
                        'Ja, ik ga akkoord met het verwerken van mijn gegevens')
                    .build()
            ])
            .setValidatorRequired('U moet akkoord gaan met de privacyverklaring om het formulier te versturen.')
            .setShowInOverviewSet(false) as MultipleChoiceCheckboxBuilder
    }

    public get numbersOnlyField(): FormFieldBuilder {
        return FormFieldBuilder.newBuilder(this.injector)
            .setValidatorMaxLength(6)
            .setValidatorPattern('^[0-9]+$', 'U kunt alleen nummers invoeren.')
            .setComponent(BasicInputField2Component)
            .setHintText('Vul alleen cijfers in, geen komma\'s of punten.')
    }

    public get currencyField(): FormFieldBuilder {
        return BasicInputFieldUnitsBuilder.newBuilder(this.injector)
            .setSuffix('Euro')
            .setValidatorMaxLength(6)
            .setValidatorPattern('^[0-9]+$', 'U kunt alleen nummers invoeren.')
            .setComponent(BasicInputField2Component)
            .setHintText('Vul alleen cijfers in, geen komma\'s of punten. Rond uw bedragen altijd af in hele euro\'s, bijvoorbeeld 1025.')
            .setIcon('euro-sign')
    }

    public get currencyFieldDecimal(): FormFieldBuilder {
        return FormFieldBuilder.newBuilder(this.injector)
            .setValidatorMaxLength(6)
            .setValidatorPattern('^[0-9]+(,[0-9]{1,2})?$', 'Vul alleen cijfers in. U kan maximaal twee cijfers achter de comma invoeren.')
            .setComponent(BasicInputField2Component)
            .setHintText('Vul alleen cijfers in.')
            .setIcon('euro-sign')
    }

    public get iban(): FormFieldBuilder {
        return FormFieldBuilder.newBuilder(this.injector)
            .setValidation(ValidatorPreBuildConfigs.getInstance().minLength(18))
            .setValidation(ValidatorPreBuildConfigs.getInstance().maxLength(18))
            .setAsyncValidation(AsyncValidatorPrebuildConfig.getInstance().iban())
            .setComponent(BasicInputField2Component)
            .setHintText('Een IBAN-nummer heeft geen spaties en ziet er zo uit: NL91ABNA0417164300')
    }

    public get kvk(): FormFieldBuilder {
        return FormFieldBuilder.newBuilder(this.injector)
            .setLabel('Kamer van Koophandel nummer')
            .setValidatorMinLength(8)
            .setValidatorMaxLength(8)
            .setValidatorPattern(GenericErrorPatterns.numberOnly, 'Uw KVK-nummer bestaat alleen uit cijfers.')
            .setComponent(BasicInputField2Component)
            .setHintText('')
    }

    public get percentageField(): FormFieldBuilder {
        return BasicInputFieldUnitsBuilder.newBuilder(this.injector)
            .setSuffix('%')
            .setValidatorMaxLength(6)
            .setValidatorPattern('^[0-9]+$', 'U kan alleen nummers invoeren')
            .setComponent(BasicInputFieldUnitComponent)
    }

    public get leaflet(): LeafletBuilder {
        return LeafletBuilder.newFactory(this.injector)
            .setInputField(
                BasicInputFieldActionBuilder.newBuilder(this.injector)
                    .setValidation(ValidatorPreBuildConfigs.getInstance().leafletAddressValid())
                    .setPlaceholder('Straatnaam 1')
                    .setID('StraatHuisnummer')
                    .setLabel('Straatnaam en huisnummer')
            )
            .setValidation(ValidatorPreBuildConfigs.getInstance().combinedFieldAction())
            .setPointerOptions(new LeafletPointerOptions('red', '#f03', 0.5, 6))
            .setMapOptions(new LeafletMapOptions(16, latLng(52.301521, 4.695950), true))
            .setLatLng(latLng(52.301521, 4.695950))
            .setLabel('Locatie')
            .setValidatorRequired('U moet een locatie kiezen.')
            .setComponent(LeafletComponent)
    }

    public get generatorAddButton(): ButtonBuilder {
        return ButtonBuilder.newBuilder(this.injector)
            .setSingleBuild(false)
            .setButtonClass('btn btn-mfg')
            .setButtonAllignment('full-height')
            .setComponent(ButtonComponent)
            .setIcon('plus')
            .setID('addButton')
            .setSendToBE(false) as ButtonBuilder;
    }

    public get generatorRemoveButton(): ButtonBuilder {
        return ButtonBuilder.newBuilder(this.injector)
            .setSingleBuild(false)
            .setButtonClass('btn btn-mfg')
            .setButtonAllignment('full-height')
            .setComponent(ButtonComponent)
            .setIcon('minus')
            .setID('removeButton')
            .setSendToBE(false) as ButtonBuilder;
    }
}
