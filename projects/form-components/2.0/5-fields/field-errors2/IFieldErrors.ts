import {DatePipe} from "@angular/common";
import {Moment} from 'moment';

export class FiedErrorBuilder {
	get iban(): string {
		return this._iban;
	}
	get maxLengthAmount(): number {
		return this._maxLengthAmount;
	}
	get matDatepickerParse(): string {
		return this._matDatepickerParse;
	}
	get matDatepickerMax(): string {
		return this._matDatepickerMax;
	}
	get matDatepickerMin(): string {
		return this._matDatepickerMin;
	}
	get min(): string {
		return this._min;
	}
	
	get max(): string {
		return this._max;
	}
	
	get minLength(): string {
		return this._minLength;
	}
	
	get maxLength(): string {
		return this._maxLength;
	}
	
	get pattern(): string {
		return this._pattern;
	}
	
	get email(): string {
		return this._email;
	}
	
	get required(): string {
		return this._required;
	}
	
	get mindate(): string {
		return this._mindate;
	}
	
	get maxdate(): string {
		return this._maxdate;
	}
	
	get dateparse(): string {
		return this._dateparse;
	}
	
	private _min = '';
	private _max = '';
	private _minLength = '';
	private _maxLength = '';
	private _maxLengthAmount: number = 10022;
	private _pattern = '';
	private _email = 'Uw e-mailadres is onvolledig of onjuist ingevuld. Vul een geldig e-mailadres in.';
	private _required = 'Dit veld is verplicht.';
	private _mindate = '';
	private _maxdate = '';
	private _dateparse = '';
	private _matDatepickerMin = '_matDatepickerMin';
	private _matDatepickerMax = '_matDatepickerMax';
	private _matDatepickerParse = 'Dit is geen geldige datum.';
	private _iban = 'JAAAAAAAAAAAAAAAAAAAAA';
	
	public static getBaseErrors(): FiedErrorBuilder {
		return new FiedErrorBuilder();
	}
	
	public setMin(amount: number): FiedErrorBuilder {
		this._min = 'U moet minimaal ' + amount + ' invullen.';
		return this;
	}
	
	public setMax(amount: number): FiedErrorBuilder {
		this._max = 'U kunt minimaal ' + amount + ' invullen.';
		return this;
	}
	
	public setMinLength(amount: number): FiedErrorBuilder {
		this._minLength = 'U moet minimaal ' + amount + ' tekens invullen.';
		return this;
	}
	
	public setMaxLength(amount: number): FiedErrorBuilder {
		this._maxLength = 'U kunt maximaal ' + amount + ' tekens invullen.';
		this._maxLengthAmount = amount;
		return this;
	}
	
	public setPattern(errorMessage: string): FiedErrorBuilder {
		this._pattern = errorMessage;
		return this;
	}
	
	public setMinDate(date: Moment): FiedErrorBuilder {
		this._matDatepickerMin = 'De gekozen datum moet liggen na: ' + new DatePipe('nl-NL').transform(date, 'YYYY-MM-DD');
		return this;
	}
	
	public setMaxDate(date: Moment, msg?: string): FiedErrorBuilder {
		this._matDatepickerMax = 'De gekozen datum moet liggen voor: ' + new DatePipe('nl-NL').transform(date, 'YYYY-MM-DD');
		if (msg) {
			this._matDatepickerMax = msg;
		}
		return this;
	}

	public setRequired(errorMessage: string): FiedErrorBuilder {
		this._required = errorMessage;
		return this;
	}
	
	public build(): FieldErrors {
		return new FieldErrors(this);
	}
}

export class FieldErrors {
	min = '';
	max = '';
	minLength = '';
	minlength = '';
	maxLength = '';
	maxLengthAmount: number;
	maxlength = '';
	pattern = '';
	email = 'Uw e-mailadres is onvolledig of onjuist ingevuld. Vul een geldig e-mailadres in.';
	required = 'Dit veld is verplicht.';
	mindate = '';
	maxdate = '';
	matDatepickerMin = '';
	matDatepickerMax = '';
	matDatepickerParse = '';
	
	constructor(builder: FiedErrorBuilder) {
		this.min = builder.min;
		this.max = builder.max;
		this.minLength = builder.minLength;
		this.minlength = builder.minLength;
		this.maxLength = builder.maxLength;
		this.maxLengthAmount = builder.maxLengthAmount;
		this.maxlength = builder.maxLength;
		this.pattern = builder.pattern;
		this.mindate = builder.mindate;
		this.maxdate = builder.maxdate;
		this.matDatepickerMin = builder.matDatepickerMin;
		this.matDatepickerMax = builder.matDatepickerMax;
		this.matDatepickerParse = builder.matDatepickerParse;
		this.required = builder.required;
	}
}
