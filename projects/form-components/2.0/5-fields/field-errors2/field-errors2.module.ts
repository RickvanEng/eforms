import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FieldErrors2Component} from './field-errors2.component';

@NgModule({
	imports: [
		CommonModule
	],
	declarations: [FieldErrors2Component],
	exports: [
		FieldErrors2Component
	]
})
export class FieldErrors2Module {
}
