import { Component, Input, OnInit } from '@angular/core';
import { EFormsAbstractControl } from '../../base/eFormsAbstractControl';
import {IValidator} from '../../validators/IValidator';

@Component({
	selector: 'app-field-errors2',
	templateUrl: './field-errors2.component.html',
	styleUrls: ['./field-errors2.component.scss']
})
export class FieldErrors2Component {
	@Input() public configs: IValidator[];
	@Input() control: EFormsAbstractControl;

	public getProps(obj: any): string[] {
		if (obj) {
			return Object.keys(obj);
		} else {
			return [];
		}
	}

	public getErrorMsg(error: any): string {
		let errorObj: any = this.control.errors[error];
		return errorObj.custom ? errorObj.msg : this.getError(error).responses[0].msg;
	}

	private getError(error: string): IValidator {
		return this.configs.find(validator => validator.validatorId === error);
	}
}
