import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {FieldErrors2Component} from './field-errors2.component';

describe('FieldErrors2Component', () => {
  let component: FieldErrors2Component;
  let fixture: ComponentFixture<FieldErrors2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FieldErrors2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FieldErrors2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
