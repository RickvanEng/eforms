import {ICmsComponent} from "./ICmsComponents";

export class CmsComponent {

    public get id(): string {
        return this._config.id;
    }

    protected _config: ICmsComponent;

    constructor(config: ICmsComponent) {
        this._config = config;
    }
}