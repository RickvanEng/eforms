import {TabFieldSetComponent} from '../3-tab-field-set/tab-field-set.component';

export const componentMap: Record<string, any> = {
    'TabFieldSetComponent': TabFieldSetComponent,
};
