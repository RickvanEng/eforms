import {environment} from '../../../../src/environments/environment';
import {Injectable, Injector} from '@angular/core';
import {DEFAULT_INTERRUPTSOURCES, Idle} from '@ng-idle/core';
import {Keepalive} from '@ng-idle/keepalive';
import {ErrorService} from '../../../forms/services/error.service';
import {HttpClient} from '@angular/common/http';
import {Auth2Service, TokenObject} from './auth2.service';

@Injectable({
	providedIn: 'root'
})
export class KeepAlive2Service {
	private baseUrl = environment.baseUrl;
	private idleState = 'Not started.';
	private timedOut = false;
	private lastPing: Date = null;
	
	constructor(private injector: Injector,
				private idle: Idle,
				private keepalive: Keepalive) {}
	
	public setKeepAlive() {
		let token: TokenObject = this.injector.get(Auth2Service).loginToken.getValue();
		this.idle.setIdle(600);
		this.idle.setTimeout(120);
		this.idle.setInterrupts(DEFAULT_INTERRUPTSOURCES);
		
		this.idle.onIdleEnd.subscribe(() => {
			console.warn('User has resumed activity');
			this.idleState = 'No longer idle.'
		});
		this.idle.onIdleStart.subscribe(() => {
			console.warn('User has become Idle');
			this.idleState = 'idle'
		});
		this.idle.onTimeout.subscribe(() => {
			this.idleState = 'Timed out!';
			this.timedOut = true;
			localStorage.setItem('timedOut', 'true');
			localStorage.removeItem('token');
			location.reload();
		});
		
		this.keepalive.interval(150);
		this.keepalive.onPing.subscribe(() => {
			this.lastPing = new Date();
			
			this.injector.get(HttpClient).get(this.baseUrl + 'eforms-api/controller/session/refresh').subscribe((value: any) => {
			
			}, error => {
				console.log(console.log('error: ' + error));
				this.injector.get(ErrorService).handleError(error);
			});
			
			if (token) {
				this.injector.get(HttpClient).get(this.baseUrl + 'adapter/session/token/' + token.value + '/refresh').subscribe((value: any) => {
					localStorage.setItem('token', JSON.stringify({
						value: token.value,
						status: token.status,
						idp: token.idp,
						ttl: value.expirationDate
					}));
				}, error => {
					console.log(console.log('error: ' + error));
					this.injector.get(ErrorService).handleError(error);
				});
			}
			
		});
		this.reset();
	}
	
	reset() {
		this.idle.watch();
		this.idleState = 'Started.';
		this.timedOut = false;
	}
}
