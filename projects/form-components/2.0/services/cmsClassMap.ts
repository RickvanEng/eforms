import {FormTab2} from '../2-tab/FormTab2';
import {TabFieldSet} from '../3-tab-field-set/tab-field-set';
import {FormSet2} from '../4-set/FormSet';
import {FormField} from '../5-fields/form-field/FormField';
import {Form2} from '../1-form/Form';

export const cmsClassMap: Record<string, any> = {
    'Form2': Form2,
    'FormTab2': FormTab2,
    'TabFieldSet': TabFieldSet,
    'FormSet2': FormSet2,
    'FormField': FormField,
};
