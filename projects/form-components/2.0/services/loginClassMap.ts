import {FormConfig} from '../0-routing/formConfig';
import {LoginTile} from '../0-routing/form-routing/login-portal/login-tile/login-tile';

export const loginClassMap: Record<string, any> = {
    'FormConfig': FormConfig,
    'LoginTile': LoginTile,
};
