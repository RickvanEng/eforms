import {Injectable} from '@angular/core';
import {CanActivate, Router} from '@angular/router';
import {Auth2Service} from './auth2.service';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';

@Injectable()
export class AuthGuardMaintenance implements CanActivate {

    constructor(private authService: Auth2Service, private router: Router) {
    }

    canActivate(): Observable<boolean> | Promise<boolean> | boolean {
        return this.authService.inMaintenance.pipe(map(res => {
            if (res.maintenance) {
                this.router.navigate(['maintenance']);
            }
            return !!res;
        }));
    }
}
