import {Injectable} from '@angular/core';
import {BehaviorSubject} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../src/environments/environment';

export interface IEnvrionmentData {
    prod: boolean;
    vpn: boolean;
}

@Injectable({
    providedIn: 'root'
})
export class EnvironmentService {
    get environment(): BehaviorSubject<IEnvrionmentData> {
        return this._environment;
    }
    private _environment: BehaviorSubject<IEnvrionmentData> = new BehaviorSubject({prod: false, vpn: false});

    constructor(private http: HttpClient) {
        this._environment.next({
            prod: false,
            vpn: true,
        });

        // http.get(environment.baseUrl + 'eforms-api/controller/form/status/environment').toPromise().then((res: IEnvrionmentData) => {
        //     this._environment.next(res);
        // });
    }
}
