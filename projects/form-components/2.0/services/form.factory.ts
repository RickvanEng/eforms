import {ICmsComponent} from '../ICmsComponents';
import {CmsComponent} from '../cmsComponent';
import {CmsAbstractControl} from '../cmsAbstractControl';


class GenericBuilder {
    static create<T>(type: (new (params: ICmsComponent) => T), builder: ICmsComponent): T {
        return new type(builder);
    }
}

export class CmsComponentFactory {
    public static build(configs: ICmsComponent[], map: Record<string, any>): CmsComponent[] {
        return configs.map(conf => GenericBuilder.create(map[conf.className], conf));
    }
}

export class FormComponentFactory {
    public static build(configs: ICmsComponent[], map: Record<string, any>): CmsAbstractControl[] {
        return configs.map(conf => GenericBuilder.create(map[conf.className], conf));
    }
}
