import {Inject, Injectable} from '@angular/core';
import {environment} from '../../../../src/environments/environment';
import {DOCUMENT} from '@angular/platform-browser';
import {HttpClient} from '@angular/common/http';
import {Params} from '@angular/router';
import {BehaviorSubject, Observable} from 'rxjs';
import {tap} from 'rxjs/operators';
import {ELoginTypes2} from '../../../forms/src/formImplementations/IForm';

export class TokenObject {
    value: string;
    status: string;
    idp: string;
    ttl: any
}

@Injectable({
    providedIn: 'root'
})
export class Auth2Service {

    private baseUrl = environment.baseUrl;
    public token: string = 'token';
    public status: string;
    public maintenance: boolean;

    get loginToken(): BehaviorSubject<TokenObject> {
        return this._loginToken;
    }

    private _loginToken: BehaviorSubject<TokenObject> = new BehaviorSubject(undefined);

    constructor(@Inject(DOCUMENT) private document: Document,
                private http: HttpClient) {
        let oldToken: TokenObject = JSON.parse(localStorage.getItem('token'));

        if (oldToken) {
            if (this.checkTokenTTL(oldToken)) {
                this.http.get(this.baseUrl + 'adapter/session/token/' + oldToken.value + '/refresh').subscribe((value: any) => {
                    oldToken.ttl = value.expirationDate;
                    this.loginToken.next(oldToken);
                    localStorage.setItem('token', JSON.stringify(oldToken));
                }, error1 => {
                    console.warn('session expired');
                    localStorage.removeItem('token');
                })
            } else {
                console.warn('session expired');
                localStorage.removeItem('token');
            }
        }
    }

    public partnerLogin(formName: string, progressURL: string, type: ELoginTypes2): void {
        this.document.location.href = this.baseUrl + 'adapter/login/secondary';
    }

    public loginLdap(id: string) {
        this.document.location.href = this.baseUrl + 'adapter/ldaplogin' + '?form=' + id;
    }

    public logout(): void {
        this.document.location.href = this.baseUrl + 'adapter/logout';
    }

    public saveQueryParams(params: Params): Promise<void> {
        return new Promise<void>(resolve => {

            let tokenObject: any = {
                value: params['token'],
                status: params['status'],
                idp: params['idp'],
                ttl: ''
            };

            console.log('save the Query params');
            this.loginToken.next(tokenObject);
            resolve();

            // if (!params.status) {
            //     resolve();
            // }
            //
            // if (params.hasOwnProperty('status')) {
            //     switch (params['status']) {
            //         case 'authResponseLoggedIn' : {
            //             let tokenObject: any = {
            //                 value: params['token'],
            //                 status: params['status'],
            //                 idp: params['idp'],
            //                 ttl: ''
            //             };
            //
            //             console.log('set that shit')
            //             this.loginToken.next(tokenObject);
            //
            //             // localStorage.setItem('token', JSON.stringify(tokenObject));
            //
            //             // this.http.get(this.baseUrl + 'adapter/session/token/' + tokenObject.value + '/refresh').subscribe((value: any) => {
            //             //     tokenObject.ttl = value.expirationDate;
            //             //     this.loginToken.next(tokenObject);
            //             //     localStorage.setItem('token', JSON.stringify(tokenObject));
            //             //     resolve();
            //             // }, error1 => {
            //             //     console.warn(error1);
            //             // });
            //
            //             break;
            //         }
            //         default: {
            //             console.log('default');
            //             break;
            //         }
            //     }
            // }
        });
    }

    public getLdapBsnTokenData(bsn: number): Observable<any> {

        return new Observable<any>();
    }

    public checkTokenTTL(token: TokenObject): boolean {
        let tokenDate = new Date(token.ttl);
        let currentDate = new Date();

        if (currentDate > tokenDate) {
            localStorage.clear();
            return false;
        }
        return true;
    }

    get inMaintenance(): Observable<any> {
        return this.http.get(environment.baseUrl + 'eforms-api/status').pipe(tap(data => this.maintenance = data));
    }

}
