import {Component} from '@angular/core';
import {FormSet2} from './FormSet';
import {BaseComponent} from '../base/base.component';

@Component({
	selector: 'app-set',
	templateUrl: './set.component.html',
	styleUrls: ['./set.component.scss']
})
export class SetComponent extends BaseComponent {
	public _component: FormSet2;
}
