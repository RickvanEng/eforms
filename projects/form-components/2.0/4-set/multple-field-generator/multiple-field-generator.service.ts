import {Injectable, Injector} from '@angular/core';
import {GeneratorFieldConfig} from './reactive-generator/generatorFieldConfig';
import {GeneratedFieldResult} from './reactive-generator/generatedFieldResult';
import {FormField} from '../../5-fields/form-field/FormField';

@Injectable({
	providedIn: 'root'
})
export class MultipleFieldGeneratorService {
	
	public generateFields(generatorSetConfig: GeneratorFieldConfig, inj: Injector): GeneratedFieldResult {
		
		let newFields: FormField[] = generatorSetConfig.fields.map(field => field.build());
		newFields.forEach(field => {
			field.init()
		});
		
		return new GeneratedFieldResult(
			inj,
			newFields,
			generatorSetConfig.deleteButton.build()
		);
	}
}
