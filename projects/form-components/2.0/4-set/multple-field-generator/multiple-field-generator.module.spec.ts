import {MultipleFieldGeneratorModule} from './multiple-field-generator.module';

describe('CheckboxFieldGeneratorModule', () => {
  let checkboxFieldGeneratorModule: MultipleFieldGeneratorModule;

  beforeEach(() => {
    checkboxFieldGeneratorModule = new MultipleFieldGeneratorModule();
  });

  it('should create an instance', () => {
    expect(checkboxFieldGeneratorModule).toBeTruthy();
  });
});
