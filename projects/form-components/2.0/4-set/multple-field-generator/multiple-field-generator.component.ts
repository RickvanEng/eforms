import {Component, OnInit} from '@angular/core';
import {SetComponent} from '../set.component';
import {MultipleFieldGeneratorSet} from './multiple-field-generator';
import {Subscription} from 'rxjs';
import {GeneratedFieldResult} from './reactive-generator/generatedFieldResult';

@Component({
	selector: 'app-multiple-field-generator',
	templateUrl: './multiple-field-generator.component.html',
	styleUrls: ['./multiple-field-generator.component.scss']
})
export class MultipleFieldGeneratorComponent extends SetComponent implements OnInit {
	
	public _component: MultipleFieldGeneratorSet;
	public generatedFields: GeneratedFieldResult[] = [];
	public subscription: Subscription;
	
	ngOnInit(): void {
		this._component.show.subscribe(value => {
			if (value) {
				this.subscription = this._component.generatedFields.subscribe((res: GeneratedFieldResult[]) => {
					this.generatedFields = res;
				});
			}
		});
	}
}
