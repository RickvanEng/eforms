import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ReactiveGeneratorComponent} from './reactive-generator.component';
import {BaseModule} from '../../../base/base.module';
import {FieldLabelModule} from '../../../../../../src/app/components/field-label/field-label.module';
import {FormsModule} from '@angular/forms';
import {FormFieldModule} from '../../../5-fields/form-field/form-field.module';

@NgModule({
    imports: [
        CommonModule,
        BaseModule,
        FieldLabelModule,
        FormsModule,
        FormFieldModule
    ],
	declarations: [
		ReactiveGeneratorComponent
	],
	exports: [
		ReactiveGeneratorComponent
	]
})
export class ReactiveGeneratorModule {
}
