import {FormSet2} from '../../FormSet';
import {ReactiveGeneratorBuilder} from './reactive-generator.builder';
import {MultipleFieldGeneratorService} from '../multiple-field-generator.service';
import {GeneratedFieldResult} from './generatedFieldResult';
import {BehaviorSubject} from 'rxjs';
import {FormField} from '../../../5-fields/form-field/FormField';
import {LabelFieldBuilder} from '../../../5-fields/form-field/label-field/LabelField';
import {AbstractControl, FormArray, FormBuilder, FormGroup} from '@angular/forms';
import {Injector} from '@angular/core';
import { EFormsAbstractControl, EFormsFormArray, EFormsFormGroup } from '../../../base/eFormsAbstractControl';

export class ReactiveGenerator extends FormSet2 {
	
	get subject(): string {
		return this._builder.listenToGenerator.subject;
	}
	
	get children(): FormField[] {
		let fieldsToReturn: FormField[] = this._children as FormField[];
		this._generatedSets.getValue().forEach((fieldSet, index) => {
			// TODO Dit moet generieker.
			fieldsToReturn = fieldsToReturn.concat(
				new LabelFieldBuilder(this._injector)
					.setShowInOverviewSet(true)
					.setSendToBE(false)
					.setID('pdfGroupDelimiter')
					.setLabel(this._builder.listenToGenerator.subject + ' ' + (index + 1).toString())
					.build()
			);
			fieldsToReturn = fieldsToReturn.concat(fieldSet.fields);
		});
		
		return fieldsToReturn;
	}
	
	get generatedSets(): BehaviorSubject<GeneratedFieldResult[]> {
		return this._generatedSets;
	}
	
	_builder: ReactiveGeneratorBuilder;
	_abstractControl: EFormsFormGroup;
	formArray: EFormsFormArray;
	
	private _generatedSets: BehaviorSubject<GeneratedFieldResult[]> = new BehaviorSubject([]);
	
	constructor(b: ReactiveGeneratorBuilder) {
		super(b);
	}
	
	init(): Promise<any> {
		return new Promise<any>(resolve => {
			super.init().then(() => {
				this._builder.listenToGenerator.generatedFields.subscribe(res => {
					let newResults: GeneratedFieldResult[] = [];
					for (let i = 0; i < res.length; i ++) {
						let newSet: GeneratedFieldResult = this._injector.get(MultipleFieldGeneratorService).generateFields(this._builder.setToBuildConfig, this._injector);
						this.formArray.push(newSet.formGroup);

						this.replacePlaceholders(newSet, (i + 1));
						newResults.push(newSet);
					}
					this._generatedSets.next(newResults);
				});

				resolve();
			});
		});
	}

	constructFormControl(inj: Injector): EFormsAbstractControl {
		let group: EFormsFormGroup = super.constructFormControl(inj) as EFormsFormGroup;
		this.formArray = new EFormsFormArray([]);
		group.addControl('array', this.formArray);

		this._abstractControl = group;
		return group;
	}
	
	/**
	 * The data.fields will be iterated and the labels {{index}} interpolated with the index value
	 * @param data
	 * @param index
	 */
	private replacePlaceholders(data: GeneratedFieldResult, index: number): void {
		for (let field of data.fields) {
			field.interpolateLabel('{{index}}', index.toString())
		}
	}
	
}
