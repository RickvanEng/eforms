import {Component, OnInit} from '@angular/core';
import {SetComponent} from '../../set.component';
import {ReactiveGenerator} from './reactive-generator';
import {GeneratedFieldResult} from './generatedFieldResult';

@Component({
	selector: 'app-reactive-generator',
	templateUrl: './reactive-generator.component.html',
	styleUrls: ['./reactive-generator.component.scss']
})
export class ReactiveGeneratorComponent extends SetComponent implements OnInit {
	public _component: ReactiveGenerator;
	
	public generatedConfigs: GeneratedFieldResult[] = [];
	
	ngOnInit(): void {
		this._component.generatedSets.subscribe(sets => {
			this.generatedConfigs = sets;
		});
	}
}
