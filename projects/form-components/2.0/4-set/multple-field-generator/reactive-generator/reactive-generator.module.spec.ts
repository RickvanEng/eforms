import {ReactiveGeneratorModule} from './reactive-generator.module';

describe('ReactiveGeneratorModule', () => {
  let reactiveGeneratorModule: ReactiveGeneratorModule;

  beforeEach(() => {
    reactiveGeneratorModule = new ReactiveGeneratorModule();
  });

  it('should create an instance', () => {
    expect(reactiveGeneratorModule).toBeTruthy();
  });
});
