import {Injector} from '@angular/core';
import {
	ComponentExampleConfigBuilder,
	IComponentExampleConfig
} from '../../../../../component-dash/component-example-config.builder';
import {GenericFields} from '../../../5-fields/presets/genericFields';
import {GeneratorFieldConfig} from './generatorFieldConfig';
import {ReactiveGeneratorComponent} from './reactive-generator.component';
import {ReactiveGeneratorBuilder} from './reactive-generator.builder';
import {ComponentExampleBuilder} from '../../../../../component-dash/component-example.builder';
import {MultipleFieldGeneratorSetBuilder} from '../multiple-field-generator';
import {ButtonBuilder} from '../../../5-fields/form-field/button/button';
import {ButtonComponent} from '../../../5-fields/form-field/button/button.component';
import {MultipleFieldGeneratorComponent} from '../multiple-field-generator.component';
import {ValidatorPreBuildConfigs} from '../../../validators/basic/ValidatorPreBuildConfigs';

export class ReactiveGeneratorExample {
	
	public getField(inj: Injector): IComponentExampleConfig {
		
		let genericFields: GenericFields = new GenericFields(inj);
		let testGeneratorSet: MultipleFieldGeneratorSetBuilder = MultipleFieldGeneratorSetBuilder.newBuilder(inj)
			.setLabel('Generator set')
			.setChildren([
				genericFields.label
					.setID('labelField')
					.setLabel('Standaard labelField'),
				genericFields.basicField
					.setID('basicField_1')
					.setLabel('Standaard basicField')
					.setValidation(ValidatorPreBuildConfigs.getInstance().required())
					.setValidation(ValidatorPreBuildConfigs.getInstance().minLength(2))
			])
			.setSubject('Set')
			.setGeneratorConfig(new GeneratorFieldConfig([
					genericFields.basicField
						.setValidation(ValidatorPreBuildConfigs.getInstance().required())
						.setSaveToState(false)
						.setSingleBuild(false)
						.setLabel('Generated basicfield')
						.setID('generatedBasic'),
					genericFields.datePicker
						.setValidation(ValidatorPreBuildConfigs.getInstance().required())
						.setSaveToState(false)
						.setSingleBuild(false)
						.setLabel('generatedDatepicker')
						.setID('generatedDate')
				],
				ButtonBuilder.newBuilder(inj)
					.setSingleBuild(false)
					.setButtonClass('btn btn-mfg')
					.setButtonAllignment('full-height')
					.setIcon('minus')
					.setButtonText('Verwijder set')
					.setComponent(ButtonComponent)
					.setID('removeButton')
					.setSendToBE(false))
			)
			.setButtons(ButtonBuilder.newBuilder(inj)
				.setSingleBuild(false)
				.setButtonClass('btn btn-mfg')
				.setButtonAllignment('full-height')
				.setIcon('plus')
				.setButtonText('Voeg set toe')
				.setComponent(ButtonComponent)
				.setID('addButton')
				.setSendToBE(false)
			)
			.setMinAmountToAdd(1)
			.setSaveToState(false)
			.setComponent(MultipleFieldGeneratorComponent)
			.setID('generator')
			.setLabel('Field generator label');
		
		return ComponentExampleConfigBuilder.newBuilder()
			.setComponent('ReactiveFieldGenerator')
			.setExamples([
				ComponentExampleBuilder.newBuilder()
					.setBuilder(testGeneratorSet)
					.setDescription(
						'<ul>' +
						'<li>The set title should be visible</li>' +
						'<li>By default, one generated set should be vivisble</li>' +
						'<li>If there is one set it can\'t be removed and the remove button should not be visible</li>' +
						'<li>If you click the add button, a new clean set should appear</li>' +
						'<li>If you validate the set, all de generated fields should also be validated</li>' +
						'</ul>')
					.setState(undefined)
					.setValidateButton(true)
					.data,
				
				ComponentExampleBuilder.newBuilder()
					.setBuilder(ReactiveGeneratorBuilder.newBuilder(inj)
						.setGeneratorToListenTo(testGeneratorSet.build())
						.setSetBuildConfig(new GeneratorFieldConfig(
							[
								genericFields.basicField
									.setValidatorRequired()
									.setSaveToState(false)
									.setSingleBuild(false)
									.setLabel('Voornaam')
									.setID('generatedBasic1'),
								genericFields.basicField
									.setValidatorRequired()
									.setSaveToState(false)
									.setSingleBuild(false)
									.setLabel('Achternaam')
									.setID('generatedBasic2'),
							],
							ButtonBuilder.newBuilder(inj)
								.setSingleBuild(false)
								.setButtonClass('btn btn-mfg')
								.setButtonAllignment('full-height')
								.setIcon('plus')
								.setButtonText('Voeg set toe')
								.setComponent(ButtonComponent)
								.setID('addButton')
								.setSendToBE(false),
						))
						.setComponent(ReactiveGeneratorComponent)
						.setLabel('reactiveGeneratorSet')
						.setID('reactiveGeneratorSet')
						.setChildren([]))
					.setDescription(
						'<ul>' +
						'<li>test</li>' +
						'</ul>')
					.setState(undefined)
					.setValidateButton(true)
					.data
			])
			.data;
	}
}
