import {ButtonBuilder} from '../../../5-fields/form-field/button/button';
import {FormFieldBuilder} from '../../../5-fields/form-field/FormField';

export class GeneratorFieldConfig {
	get fields(): FormFieldBuilder[] {
		return this._fields;
	}
	
	get deleteButton(): ButtonBuilder {
		return this._deleteButton;
	}
	
	private _fields: FormFieldBuilder[];
	private _deleteButton: ButtonBuilder;
	
	constructor(fields: FormFieldBuilder[], deleteButton?: ButtonBuilder) {
		this._fields = fields;
		this._deleteButton = deleteButton;
	}
}
