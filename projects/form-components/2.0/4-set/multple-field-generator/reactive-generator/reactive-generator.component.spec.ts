import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ReactiveGeneratorComponent} from './reactive-generator.component';

describe('ReactiveGeneratorComponent', () => {
  let component: ReactiveGeneratorComponent;
  let fixture: ComponentFixture<ReactiveGeneratorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReactiveGeneratorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReactiveGeneratorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
