import {Button} from '../../../5-fields/form-field/button/button';
import {FormField} from '../../../5-fields/form-field/FormField';
import {FormGroup} from '@angular/forms';
import {Injector} from '@angular/core';

export class GeneratedFieldResult {
	get formGroup(): FormGroup {
		return this._formGroup;
	}

	get fields(): FormField[] {
		return this._fields;
	}
	
	get deleteButton(): Button {
		return this._deleteButton;
	}
	
	private _fields: FormField[];
	private _deleteButton: Button;
	private _formGroup: FormGroup;
	
	constructor(inj: Injector, fields: FormField[], deleteButton: Button) {
		this._fields = fields;
		this._deleteButton = deleteButton;
		this._formGroup = new FormGroup({});

		this._fields.forEach(field => {
			this._formGroup.addControl(field.id, field.constructFormControl(inj));
			
			// TODO CHeck dit. Is dit nodig?
			// field.subscribeOnChange();
		});
	}
}
