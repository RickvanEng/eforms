import {FormSetBuilder} from '../../FormSet';
import {ReactiveGenerator} from './reactive-generator';
import {Injector} from '@angular/core';
import {GeneratorFieldConfig} from './generatorFieldConfig';
import {MultipleFieldGeneratorSet, MultipleFieldGeneratorSetBuilder} from '../multiple-field-generator';

/**
 * This component repeats times X based on a repeating component.
 * In the labels of the fields passed into this component you can define {{index}} in the label of the field.
 * This {{index}} gets interpolated with the number of iteration.
 */

export class ReactiveGeneratorBuilder extends FormSetBuilder {
	get listenToGenerator(): MultipleFieldGeneratorSet {
		return this._listenToGenerator;
	}
	
	get setToBuildConfig(): GeneratorFieldConfig {
		return this._setToBuildConfig;
	}
	
	protected _buildingClass: any = ReactiveGenerator;
	
	private _setToBuildConfig: GeneratorFieldConfig;
	private _listenToGenerator: MultipleFieldGeneratorSet;
	
	public static newBuilder(injector: Injector): ReactiveGeneratorBuilder {
		return new ReactiveGeneratorBuilder(injector);
	}
	
	public setSetBuildConfig(data: GeneratorFieldConfig): ReactiveGeneratorBuilder {
		this._setToBuildConfig = data;
		return this;
	}
	
	public setGeneratorToListenTo(data: MultipleFieldGeneratorSet): ReactiveGeneratorBuilder {
		this._listenToGenerator = data;
		return this;
	}
}
