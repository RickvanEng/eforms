import {FormField} from '../../5-fields/form-field/FormField';
import {FormSet2, FormSetBuilder} from '../FormSet';
import {Injector} from '@angular/core';
import {BehaviorSubject} from 'rxjs';
import {Button, ButtonBuilder} from '../../5-fields/form-field/button/button';
import {GeneratorFieldConfig} from './reactive-generator/generatorFieldConfig';
import {MultipleFieldGeneratorService} from './multiple-field-generator.service';
import {GeneratedFieldResult} from './reactive-generator/generatedFieldResult';
import {LabelFieldBuilder} from '../../5-fields/form-field/label-field/LabelField';
import {AbstractControl, FormArray, FormBuilder, FormGroup} from '@angular/forms';
import { EFormsAbstractControl, EFormsFormArray, EFormsFormGroup } from '../../base/eFormsAbstractControl';

export class MultipleFieldGeneratorSetBuilder extends FormSetBuilder {
	get subject(): string {
		return this._subject;
	}
	
	get addButton(): ButtonBuilder {
		return this._addButton;
	}
	
	get minAmountToAdd(): number {
		return this._minAmountToAdd;
	}
	
	get generatorConfig(): GeneratorFieldConfig {
		return this._generatorConfig;
	}
	
	private _generatorConfig: GeneratorFieldConfig;
	private _addButton: ButtonBuilder;
	private _minAmountToAdd: number = 0;
	private _subject: string;

	public static newBuilder(injector: Injector): MultipleFieldGeneratorSetBuilder {
		return new MultipleFieldGeneratorSetBuilder(injector);
	}
	
	public setSubject(value: string): MultipleFieldGeneratorSetBuilder {
		this._subject = value;
		return this;
	}
	
	public setMinAmountToAdd(amount: number): MultipleFieldGeneratorSetBuilder {
		this._minAmountToAdd = amount;
		return this;
	}
	
	public setGeneratorConfig(generatorConfig: GeneratorFieldConfig): MultipleFieldGeneratorSetBuilder {
		this._generatorConfig = generatorConfig;
		return this;
	}

	public setButtons(addButton: ButtonBuilder) {
		this._addButton = addButton;
		return this;
	}
	
	protected _buildingClass: any = MultipleFieldGeneratorSet;
}

export class MultipleFieldGeneratorSet extends FormSet2 {
	get subject(): string {
		return this._builder.subject;
	}
	
	get addButton(): Button {
		return this._addButton;
	}
	
	get removeButton(): ButtonBuilder {
		return undefined;
	}
	
	get childrenClean(): FormField[] {
		return this._children as FormField[];
	}
	
	get children(): FormField[] {
		let fieldsToReturn: FormField[] = this._children as FormField[];
		this._generatedFields.getValue().forEach((fieldSet, index) => {
			// TODO Dit moet generieker.
			fieldsToReturn = fieldsToReturn.concat(
				new LabelFieldBuilder(this._injector)
					.setShowInOverviewSet(true)
					.setID('pdfGroupDelimiter')
					.setLabel(this.subject + ' ' + (index + 1).toString())
					.build()
			);
			fieldsToReturn = fieldsToReturn.concat(fieldSet.fields);
		});

		return fieldsToReturn;
	}

	get generatedFields(): BehaviorSubject<GeneratedFieldResult[]> {
		return this._generatedFields;
	}
	
	_builder: MultipleFieldGeneratorSetBuilder;

	private _addButton: Button;
	private _generatorService: MultipleFieldGeneratorService;
	private _generatedFields: BehaviorSubject<GeneratedFieldResult[]> = new BehaviorSubject([]);
	private formArray: EFormsFormArray;

	constructor(b: MultipleFieldGeneratorSetBuilder) {
		super(b);
		this._addButton = b.addButton.build();
		this._generatorService = this._injector.get(MultipleFieldGeneratorService);
	}
	
	init(): Promise<any> {
		return new Promise<any>(resolve => {
			super.init().then(() => {

				// TODO Moet nog mooier worden gemaakt
				// if (state) {
				// 	const value: any[] = state.getComponentValue(this.id);
				// 	if (value) {
				// 		if (value.length === 0) {
				// 			for (let i = 0; i < this._builder.minAmountToAdd; i++) {
				// 				this.addNewFieldsSet(this.generateFields());
				// 			}
				// 		}
				// 	} else {
				// 		for (let i = 0; i < this._builder.minAmountToAdd; i++) {
				// 			this.addNewFieldsSet(this.generateFields());
				// 		}
				// 	}
				// } else {
				// 	for (let i = 0; i < this._builder.minAmountToAdd; i++) {
				// 		this.addNewFieldsSet(this.generateFields());
				// 	}
				// }

				this.addButton.trigger.subscribe(trigger => {
					if (trigger) {
						this.addNewFieldsSet(this.generateFields());
					}
				});

				resolve();
			});
		});
	}

	constructFormControl(inj: Injector): EFormsAbstractControl {
		let group: EFormsFormGroup = super.constructFormControl(inj) as EFormsFormGroup;
		this.formArray = new EFormsFormArray([]);
		group.addControl('array', this.formArray);

		this._abstractControl = group;
		return group;
	}

	clear(): void {
		super.clear();
		this.generatedFields.next([]);
	}

	markAsTouched(): void {
		super.markAsTouched();
		this.generatedFields.value.forEach(group => {
			group.formGroup.markAsDirty();
			group.fields.forEach(field => {
				field.markAsTouched();
			});
		});
	}

	public addNewFieldsSet(newSet: GeneratedFieldResult): void {
		this.setGeneratedFields([...this.generatedFields.getValue(), ...[newSet]]);
	}

	public removeFieldSet(group: GeneratedFieldResult): void {
		if (this._generatedFields.getValue().length === this._builder.minAmountToAdd) {
			return;
		}

		const currentGeneratedFields: GeneratedFieldResult[] = this.generatedFields.getValue();
		currentGeneratedFields.splice(currentGeneratedFields.indexOf(group), 1);
		this.setGeneratedFields(currentGeneratedFields);
		// this.updateState();
	}

	private generateFields(): GeneratedFieldResult {
		let generated: GeneratedFieldResult = this._generatorService.generateFields(this._builder.generatorConfig, this._injector);
		this.formArray.push(generated.formGroup);

		generated.deleteButton.trigger.subscribe(trigger => {
			if (trigger) {
				this.removeFieldSet(generated);
			}
		});
		return generated;
	}

	getTechnicalValue(): any {
		if (!this.sendToBE) {
			return undefined;
		}

		let result: any = {};
		// result[this._id] = this.getBEValue();
		return result;
	}
	
	getPdfValue(): any {
		let result: any[] = [];
		
		this.generatedFields.getValue().forEach(genSet => {
			let body: any = {};
			genSet.fields.forEach(field => {
				body[field.id] = field.getPdfValue();
			});
			result.push(body);
		});

		return result;
	}
	
	private setGeneratedFields(newFields: GeneratedFieldResult[]): void {
		this._generatedFields.next(newFields);
	}
	
	// updateState(): void {
	// 	const body: any[] = [];
	// 	this.generatedFields.getValue().forEach(fieldSet => {
	// 		let setBody: any = {};
	//
	// 		fieldSet.fields.forEach(field => {
	// 			setBody[field.id] = field.getStateValue();
	// 		});
	//
	// 		body.push(setBody);
	// 	});
	//
	// 	if(this._saveToState) {
	// 		this._currentState.updateComponent(this._id, body);
	// 	}
	// }
	
	// prefill(value: any[]): void {
	// 	value.forEach(fieldSet => {
	// 		let generatedFields: GeneratedFieldResult = this.generateFields();
	// 		for (let fieldID of Object.keys(fieldSet)) {
	// 			generatedFields.fields.find(field => field.id === fieldID).prefill(fieldSet[fieldID]);
	// 		}
	//
	// 		this.addNewFieldsSet(generatedFields);
	// 	});
	// }

	getPresentationValue(): { label: string; value: string } {
		return super.getPresentationValue();
	}
}
