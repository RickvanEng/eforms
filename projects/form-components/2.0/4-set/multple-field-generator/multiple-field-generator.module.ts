import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {MultipleFieldGeneratorComponent} from './multiple-field-generator.component';
import {ButtonModule} from '../../5-fields/form-field/button/button.module';
import {BaseModule} from '../../base/base.module';
import {ReactiveGeneratorModule} from './reactive-generator/reactive-generator.module';
import {FieldLabelModule} from '../../../../../src/app/components/field-label/field-label.module';
import {ReactiveFormsModule} from '@angular/forms';
import {FormFieldModule} from '../../5-fields/form-field/form-field.module';
import { FieldErrors2Module } from '../../5-fields/field-errors2/field-errors2.module';

@NgModule({
    imports: [
        CommonModule,
        ButtonModule,
        BaseModule,
        ReactiveGeneratorModule,
        FieldLabelModule,
        ReactiveFormsModule,
        FormFieldModule,
        FieldErrors2Module,
    ],
	declarations: [
		MultipleFieldGeneratorComponent,
	],
	exports: [
		MultipleFieldGeneratorComponent,
		ReactiveGeneratorModule,
	]
})
export class MultipleFieldGeneratorModule {
}
