import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {SetComponent} from './set.component';
import {BasicInputField2Component} from '../5-fields/form-field/basic-input-field2/basic-input-field2.component';
import {BasicInputField2Module} from '../5-fields/form-field/basic-input-field2/basic-input-field2.module';
import {DatePicker2Module} from '../5-fields/form-field/date-picker2/date-picker2.module';
import {DatePicker2Component} from '../5-fields/form-field/date-picker2/date-picker2.component';
import {LargeTextAreaModule} from '../5-fields/form-field/textArea/large-text-area/large-text-area.module';
import {SmallTextAreaModule} from '../5-fields/form-field/textArea/small-text-area/small-text-area.module';
import {SmallTextAreaComponent} from '../5-fields/form-field/textArea/small-text-area/small-text-area.component';
import {LargeTextAreaComponent} from '../5-fields/form-field/textArea/large-text-area/large-text-area.component';
import {LabelFieldModule} from '../5-fields/form-field/label-field/label-field.module';
import {LabelFieldComponent} from '../5-fields/form-field/label-field/label-field.component';
import {SingleCheckboxComponent} from '../5-fields/form-field/single-checkbox/single-checkbox.component';
import {SingleCheckboxModule} from '../5-fields/form-field/single-checkbox/single-checkbox.module';
import {AkkoordVersturenModule} from './akkoord-versturen/akkoord-versturen.module';
import {MultipleChoiceCheckboxComponent} from '../5-fields/form-field/multipleChoiceField/multiple-choice-checkbox/multiple-choice-checkbox.component';
import {MultipleChoiceObjectComponent} from '../5-fields/form-field/multipleChoiceField/multiple-choice-object/multiple-choice-object.component';
import {FormFieldModule} from '../5-fields/form-field/form-field.module';
import {MultipleChoiceRadioComponent} from '../5-fields/form-field/multipleChoiceField/multiple-choice-radio/multiple-choice-radio.component';
import {FileUploadModule} from "../5-fields/form-field/file-upload/file-upload.module";
import {FileUploadComponent} from "../5-fields/form-field/file-upload/file-upload.component";
import {ButtonModule} from "../5-fields/form-field/button/button.module";
import {TimePickerModule} from '../5-fields/form-field/time-picker/time-picker.module';
import {TimePickerComponent} from '../5-fields/form-field/time-picker/time-picker.component';
import {MultipleChoiceImageModule} from '../5-fields/form-field/multipleChoiceField/multiple-choice-image/multiple-choice-image.module';
import {MultipleChoiceImageComponent} from '../5-fields/form-field/multipleChoiceField/multiple-choice-image/multiple-choice-image.component';
import {LeafletFieldModule} from '../5-fields/form-field/leaflet/leaflet.module';
import {LeafletComponent} from '../5-fields/form-field/leaflet/leaflet.component';
import {BasicInputFieldActionModule} from '../5-fields/form-field/basic-input-field-action/basic-input-field-action.module';
import {BasicInputFieldActionComponent} from '../5-fields/form-field/basic-input-field-action/basic-input-field-action.component';
import {BasicInputFieldUnitModule} from '../5-fields/form-field/basic-input-field-units/basic-input-field-units.module';
import {BasicInputFieldUnitComponent} from '../5-fields/form-field/basic-input-field-units/basic-input-field-units.component';
import {ButtonComponent} from '../5-fields/form-field/button/button.component';
import {BaseModule} from '../base/base.module';
import {SumSetConfigComponent} from './sum-set-config/sum-set-config.component';
import {MobStatusComponent} from '../5-fields/form-field/mob-status/mob-status.component';
import {MobStatusFieldModule} from '../5-fields/form-field/mob-status/mob-status.module';
import {DropdownModule} from '../5-fields/form-field/dropdown/dropdown.module';
import {DropdownComponent} from '../5-fields/form-field/dropdown/dropdown.component';
import {FieldErrors2Module} from '../5-fields/field-errors2/field-errors2.module';
import {MultipleChoiceFieldModule} from '../5-fields/form-field/multipleChoiceField/multiple-choice-field.module';
import {ReactiveGeneratorComponent} from './multple-field-generator/reactive-generator/reactive-generator.component';
import {ReactiveFormsModule} from '@angular/forms';
import {CombinedFieldActionModule} from "./combined-field-action/combined-field-action.module";
import {CombinedNumberSetModule} from "./combined-number-set/combined-number-set.module";
import {EerdereVergunningSetModule} from "./eerdere-vergunning-set/eerdere-vergunning-set.module";
import {MultipleFieldGeneratorModule} from "./multple-field-generator/multiple-field-generator.module";
import {SumFieldConfigModule} from "./sum-field-config/sum-field-config.module";
import {ReactiveGeneratorModule} from "./multple-field-generator/reactive-generator/reactive-generator.module";

@NgModule({
	imports: [
		CommonModule,
		ReactiveFormsModule,

		AkkoordVersturenModule,
		CombinedFieldActionModule,
		CombinedNumberSetModule,
		EerdereVergunningSetModule,
		MultipleFieldGeneratorModule,
		SumFieldConfigModule,
		ReactiveGeneratorModule,

		BasicInputField2Module,
		DatePicker2Module,
		SmallTextAreaModule,
		LargeTextAreaModule,
		LabelFieldModule,
		SingleCheckboxModule,
		MultipleChoiceFieldModule,
		FormFieldModule,
		FileUploadModule,
		ButtonModule,
		TimePickerModule,
		MultipleChoiceImageModule,
		LeafletFieldModule,
		MobStatusFieldModule,
		BasicInputFieldActionModule,
		BasicInputFieldUnitModule,
		BaseModule,
		DropdownModule,
		FieldErrors2Module,
	],
	declarations: [
		SetComponent,
		SumSetConfigComponent,
	],
	exports: [
		SetComponent
	],
	entryComponents: [
		BasicInputField2Component,
		DatePicker2Component,
		SmallTextAreaComponent,
		LargeTextAreaComponent,
		LabelFieldComponent,
		SingleCheckboxComponent,
		MultipleChoiceCheckboxComponent,
		MultipleChoiceObjectComponent,
		MultipleChoiceRadioComponent,
		FileUploadComponent,
		ButtonComponent,
		TimePickerComponent,
		DropdownComponent,
		
		MultipleChoiceImageComponent,
		LeafletComponent,
		MobStatusComponent,
		BasicInputFieldActionComponent,
		BasicInputFieldUnitComponent,
		
		ReactiveGeneratorComponent,
	]
})
export class SetModule {
}
