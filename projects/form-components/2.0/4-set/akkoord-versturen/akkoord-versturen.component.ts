import {Component, OnInit} from '@angular/core';
import {SetComponent} from '../set.component';
import {FormTab2} from '../../2-tab/FormTab2';
import {Form2} from '../../1-form/Form';
import {FormService} from '../../1-form/form.service';

@Component({
	selector: 'app-akkoord-versturen',
	templateUrl: './akkoord-versturen.component.html',
	styleUrls: ['./akkoord-versturen.component.scss']
})
export class AkkoordVersturenComponent extends SetComponent implements OnInit {
	
	public tabs: FormTab2[] = [];
	public form: Form2;
	
	ngOnInit() {
		this.form = FormService.getInstance().selectedForm;
		this.tabs = this.form.tabs;
	}
}
