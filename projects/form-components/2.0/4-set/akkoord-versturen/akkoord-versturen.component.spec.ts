import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {AkkoordVersturenComponent} from './akkoord-versturen.component';

describe('AkkoordVersturenComponent', () => {
  let component: AkkoordVersturenComponent;
  let fixture: ComponentFixture<AkkoordVersturenComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AkkoordVersturenComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AkkoordVersturenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
