import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {AkkoordVersturenComponent} from './akkoord-versturen.component';

@NgModule({
	imports: [
		CommonModule
	],
	declarations: [
		AkkoordVersturenComponent
	],
	exports: [
		AkkoordVersturenComponent
	]
})
export class AkkoordVersturenModule {
}
