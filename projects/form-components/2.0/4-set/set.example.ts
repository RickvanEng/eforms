import {Injector} from '@angular/core';
import {
    ComponentExampleConfigBuilder,
    IComponentExampleConfig
} from '../../../component-dash/component-example-config.builder';
import {ComponentExampleBuilder} from '../../../component-dash/component-example.builder';
import {FormSetBuilder} from './FormSet';
import {SetComponent} from './set.component';
import {GenericFields} from '../5-fields/presets/genericFields';

export class SetExample {
    public getField(inj: Injector): IComponentExampleConfig {

        return ComponentExampleConfigBuilder.newBuilder()
            .setComponent('Set')
            .setExamples([
                ComponentExampleBuilder.newBuilder()
                    .setBuilder(FormSetBuilder.newBuilder(inj)
                        .setChildren([
                            new GenericFields(inj).basicField
                                .setValidatorRequired()
                                .setSaveToState(false)
                                .setID('test child')
                                .setLabel('testChild')
                        ])
                        .setComponent(SetComponent)
                        .setLabel('This is a label')
                        .setID('Example1')
                    )
                    .setDescription(
                        '<ul>' +
                        '<li>The title should be visible</li>' +
                        '<li>If it has children they should be visible</li>' +
                        '<li>When validated, all the children should be validated</li>' +
                        '</ul>')
                    .setState(undefined)
                    .setValidateButton(true)
                    .data,
            ])
            .data;
    }
}
