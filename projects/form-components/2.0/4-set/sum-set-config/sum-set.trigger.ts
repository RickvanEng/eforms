import {Formula} from './formulas/formula';
import {FormField, FormFieldBuilder} from '../../5-fields/form-field/FormField';
import {FormSet2, FormSetBuilder} from '../FormSet';
import {BehaviorSubject} from 'rxjs';

export class SumSetTriggerBuilder {
	get fieldToPrefill(): FormField {
		if (this._fieldToPrefill) {
			return this._fieldToPrefill.build();
		}
	}
	
	get range(): { min: number; max: number } {
		return this._range;
	}
	
	get sets(): FormSet2 {
		return this._set.build();
	}
	
	private _range: { min: number, max: number };
	private _set: FormSetBuilder;
	private _fieldToPrefill: FormFieldBuilder;
	
	public static newBuilder(): SumSetTriggerBuilder {
		return new SumSetTriggerBuilder();
	}
	
	public setRange(range: { min: number, max: number }): SumSetTriggerBuilder {
		this._range = range;
		return this;
	}
	
	public setSets(sets: FormSetBuilder): SumSetTriggerBuilder {
		this._set = sets;
		return this;
	}
	
	public setFieldToPrefill(data: FormFieldBuilder): SumSetTriggerBuilder {
		this._fieldToPrefill = data;
		return this;
	}
	
	public build(): SumSetTrigger {
		return new SumSetTrigger(this);
	}
}

export class SumSetTrigger {
	get value(): BehaviorSubject<number> {
		return this._value;
	}
	get triggerSet(): FormSet2 {
		return this._triggerSet;
	}
	
	private _formula: Formula;
	private _range: { min: number, max: number };
	private _triggerSet: FormSet2;
	private _fieldToPrefill: FormField;
	
	private _value: BehaviorSubject<number> = new BehaviorSubject(0);
	
	constructor(b: SumSetTriggerBuilder) {
		this._range = b.range;
		this._triggerSet = b.sets;
		this._fieldToPrefill = b.fieldToPrefill;
	}
	
	public check(value: number): boolean {
		if (this._fieldToPrefill) {
			this._fieldToPrefill.prefillValue(value.toString());
		}
		return (value >= this._range.min && value <= this._range.max);
	}
}
