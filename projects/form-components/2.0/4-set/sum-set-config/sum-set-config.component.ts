import {Component, Input} from '@angular/core';
import {SumSetConfig} from './sum-set-config';
import {SetComponent} from '../set.component';
import {FormSet2} from '../FormSet';

@Component({
	selector: 'app-sum-set-config',
	templateUrl: './sum-set-config.component.html',
	styleUrls: ['./sum-set-config.component.scss']
})
export class SumSetConfigComponent extends SetComponent {
	
	public _component: SumSetConfig;
	public triggerSets: FormSet2[] = [];
	public show: boolean;
	
	@Input() set baseComponent(component: SumSetConfig) {
		this._component = component;
		this._component.show.subscribe(value => {
			this.show = value;
		});
		
		component.triggeredSets.subscribe(sets => {
			this.triggerSets = sets;
		});
	}
}
