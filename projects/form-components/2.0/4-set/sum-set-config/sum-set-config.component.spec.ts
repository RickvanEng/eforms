import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {SumSetConfigComponent} from './sum-set-config.component';

describe('SumSetConfigComponent', () => {
  let component: SumSetConfigComponent;
  let fixture: ComponentFixture<SumSetConfigComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SumSetConfigComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SumSetConfigComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
