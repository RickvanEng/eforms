import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {SumSetComponent} from './sum-set.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [SumSetComponent]
})
export class SumSetModule { }
