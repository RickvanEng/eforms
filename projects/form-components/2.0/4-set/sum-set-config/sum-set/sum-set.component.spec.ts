import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {SumSetComponent} from './sum-set.component';

describe('SumSetComponent', () => {
  let component: SumSetComponent;
  let fixture: ComponentFixture<SumSetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SumSetComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SumSetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
