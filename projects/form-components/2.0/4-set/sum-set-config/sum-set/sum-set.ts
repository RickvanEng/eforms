import {FormSet2, FormSetBuilder} from '../../FormSet';
import {Injector} from '@angular/core';
import {BehaviorSubject} from 'rxjs';

export class SumSetBuilder extends FormSetBuilder {
	protected _buildingClass: any = SumSet;
	
	public static newBuilder(injector: Injector): SumSetBuilder {
		return new SumSetBuilder(injector);
	}
}

export class SumSet extends FormSet2 {
	get allValues(): BehaviorSubject<number[]> {
		return this._allValues;
	}
	
	private _allValues: BehaviorSubject<number[]> = new BehaviorSubject([]);
	
	constructor(b: SumSetBuilder) {
		super(b);
	}
	
	init(): Promise<any> {
		return new Promise<any>(resolve => {
			super.init().then(() => {
				this._children.forEach(field => {
					field.trigger.subscribe((trigger) => {
						if (trigger) {
							this.sumValues();
						}
					});
				});
				
				resolve(true);
			});
		});
	}
	
	private sumValues(): void {
		let totalValue: number[] = [];
		this._children.forEach(field => {
			// TODO temp fix, override in currencyField and validate
			if (parseInt(field.getValue())) {
				totalValue.push(parseInt(field.getValue()));
			}
			// if (field.isValid()) {
			// 	// totalValue.push(parseInt(field.getValue()));
			// }
		});
		this._allValues.next(totalValue);
	}
}
