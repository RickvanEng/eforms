import {SumSet, SumSetBuilder} from './sum-set/sum-set';
import {SumSetTrigger, SumSetTriggerBuilder} from './sum-set.trigger';
import {FormSet2, FormSetBuilder} from '../FormSet';
import {Injector} from '@angular/core';
import {BehaviorSubject} from 'rxjs';
import {FormField, FormFieldBuilder} from '../../5-fields/form-field/FormField';
import {Formula} from './formulas/formula';
import {FormulaAdd} from './formulas/formulaAdd';

export class SumSetConfigBuilder extends FormSetBuilder {
	get triggers(): SumSetTrigger[] {
		const buildTriggers: SumSetTrigger[] = [];
		this._triggers.forEach(trigger => {
			buildTriggers.push(trigger.build())
		});
		return buildTriggers;
	}
	
	get fieldToPrefill(): FormField {
		if (this._fieldToPrefill) {
			return this._fieldToPrefill.build();
		}
	}
	
	get mainSet(): SumSet {
		return this._mainSet.build();
	}
	
	get formula(): Formula {
		return this._formula;
	}
	
	private _mainSet: SumSetBuilder;
	private _formula: Formula = new FormulaAdd();
	private _triggers: SumSetTriggerBuilder[] = [];
	private _fieldToPrefill: FormFieldBuilder;
	
	protected _buildingClass: any = SumSetConfig;
	
	public static newBuilder(injector: Injector): SumSetConfigBuilder {
		return new SumSetConfigBuilder(injector);
	}
	
	public setMainSet(set: SumSetBuilder): SumSetConfigBuilder {
		this._mainSet = set;
		return this;
	}
	
	public setFormula(_formula: Formula): SumSetConfigBuilder {
		this._formula = _formula;
		return this;
	}
	
	public setFieldToPrefill(data: FormFieldBuilder): SumSetConfigBuilder {
		this._fieldToPrefill = data;
		return this;
	}
	
	public setTriggers(trigger: SumSetTriggerBuilder[]): SumSetConfigBuilder {
		this._triggers = trigger;
		return this;
	}
}

export class SumSetConfig extends FormSet2 {
	get totalSetValue(): BehaviorSubject<number> {
		return this._totalSetValue;
	}
	get fields(): FormField[] {
		let allFields: FormField[] = [];
		
		allFields = allFields.concat(this._mainSet.children as FormField[]);
		for (let set of this._triggeredSets.getValue()) {
			allFields = allFields.concat(set.children as FormField[]);
		}
		
		return allFields;
	}
	
	get triggeredSets(): BehaviorSubject<FormSet2[]> {
		return this._triggeredSets;
	}
	
	get mainSet(): SumSet {
		return this._mainSet;
	}
	
	private _mainSet: SumSet;
	private _formula: Formula;
	private _triggers: SumSetTrigger[];
	private _triggeredSets: BehaviorSubject<FormSet2[]> = new BehaviorSubject([]);
	private _totalSetValue: BehaviorSubject<number> = new BehaviorSubject(0);
	private _fieldToPrefill: FormField;
	
	constructor(b: SumSetConfigBuilder) {
		super(b);
		this._mainSet = b.mainSet;
		this._triggers = b.triggers;
		this._formula = b.formula;
		this._fieldToPrefill = b.fieldToPrefill;
	}
	
	init(): Promise<any> {
		return new Promise<any>(resolve => {
			super.init().then(() => {
				
				this._mainSet.init().then(() => {
					this._mainSet.allValues.subscribe((values: number[]) => {
						
						let newValue: number = this._formula.calculate(values);
						this._totalSetValue.next(newValue);
						if (this._fieldToPrefill) {
							this._fieldToPrefill.prefillValue(newValue);
						}
						
						this._triggers.forEach(trigger => {
							if (trigger.check(newValue)) {
								this.addSet(trigger.triggerSet);
							} else {
								this.removeSet(trigger.triggerSet);
							}
						})
					});
					
					resolve(true);
				});
			});
		});
	}
	
	private removeSet(set: FormSet2): void {
		const indexToRemove: number = this._triggeredSets.getValue().indexOf(set);
		if (indexToRemove > -1) {
			const newArray: FormSet2[] = this._triggeredSets.getValue();
			newArray.splice(indexToRemove, 1);
			this._triggeredSets.next(newArray);
		}
	}
	
	private addSet(set: FormSet2): void {
		if (this._triggeredSets.getValue().indexOf(set) < 0) {
			this._triggeredSets.next([...this._triggeredSets.getValue(), ...[set]]);
		}
	}
	
	// isValid(): Promise<ObjValidPair> {
	// 	return new Promise<ObjValidPair>(resolve => {
	// 		let promises: Promise<ObjValidPair>[] = [];
	// 		this._mainSet.children.forEach(child => {
	// 			if (child.show.getValue()) {
	// 				promises.push(child.isValid());
	// 			}
	// 		});
	// 		promises.push(this.validate(this.getValue()));
	//
	// 		Promise.all(promises).then((res: ObjValidPair[]) => {
	// 			// if (res.some(obj => obj.valid === false)) {
	// 			// 	console.warn(this._id, 'is invalid!!')
	// 			// }
	// 			resolve(new ObjValidPair(this, !res.some(obj => obj.valid === false)));
	// 		});
	// 	});
	// }
	
	// TODO fix deze
	// public validate(): boolean {
		// let valid = super.validate();
		//
		// if (!this.show.getValue()) {
		// 	return true;
		// }
		//
		// this._triggeredSets.getValue().forEach(set => {
		// 	if (!set.validate()) {
		// 		console.warn('Set: ', set.id, ', is not valid');
		// 		valid = false;
		// 	}
		// });
		//
		// if (!this._mainSet.validate()) {
		// 	console.warn(this._mainSet.id + ' is false');
		// 	valid = false;
		// }
		
		// return true;
	// }
}
