import {Formula} from './formula';

export class FormulaPercentage extends Formula {
	public calculate(numbers: number[]): number {
		if (numbers.length === 2) {
			let firstValue: number = numbers[0];
			let secondValue: number = numbers[1];
			let totalAmount: number = Math.floor((firstValue - secondValue) / firstValue * 100);
			return totalAmount;
		} else {
			let firstValue: number = numbers[0] + numbers[1];
			let secondValue: number = numbers[2] + numbers[3];
			let totalAmount: number = Math.floor((firstValue - secondValue) / firstValue * 100);
			return totalAmount;
		}
	}
}
