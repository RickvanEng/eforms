import {Formula} from './formula';

export class FormulaAdd extends Formula {
	public calculate(numbers: number[]): number {
		let totalAmount: number = 0;
		numbers.forEach(number => {
			totalAmount = totalAmount + number;
		});
		return totalAmount;
	}
}
