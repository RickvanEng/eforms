import { Formula } from "./formula";

export class FormulaMinusThirtysevenPointOne extends Formula {
	
	public calculate(numbers: number[]): number {
        let firstValue: number = numbers[0];
        let result: number = firstValue - (firstValue*37.1/100);
        return result;

	}
}