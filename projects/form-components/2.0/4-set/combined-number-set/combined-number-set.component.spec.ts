import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {CombinedNumberSetComponent} from './combined-number-set.component';

describe('CombinedNumberFieldComponent', () => {
  let component: CombinedNumberSetComponent;
  let fixture: ComponentFixture<CombinedNumberSetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CombinedNumberSetComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CombinedNumberSetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
