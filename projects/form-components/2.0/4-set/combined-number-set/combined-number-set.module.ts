import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {CombinedNumberSetComponent} from './combined-number-set.component';
import {BaseModule} from '../../base/base.module';
import {FieldErrors2Module} from '../../5-fields/field-errors2/field-errors2.module';

@NgModule({
	imports: [
		CommonModule,
		BaseModule,
		FieldErrors2Module
	],
  declarations: [CombinedNumberSetComponent]
})
export class CombinedNumberSetModule { }
