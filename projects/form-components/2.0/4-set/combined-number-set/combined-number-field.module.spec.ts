import {CombinedNumberSetModule} from './combined-number-set.module';

describe('CombinedNumberFieldModule', () => {
  let combinedNumberFieldModule: CombinedNumberSetModule;

  beforeEach(() => {
    combinedNumberFieldModule = new CombinedNumberSetModule();
  });

  it('should create an instance', () => {
    expect(combinedNumberFieldModule).toBeTruthy();
  });
});
