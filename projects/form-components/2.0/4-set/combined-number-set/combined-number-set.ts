import {Injector} from '@angular/core';
import {SumSetConfig, SumSetConfigBuilder} from '../sum-set-config/sum-set-config';
import {SumSetTrigger, SumSetTriggerBuilder} from '../sum-set-config/sum-set.trigger';
import {FormSet2, FormSetBuilder} from '../FormSet';
import {FormField, FormFieldBuilder} from '../../5-fields/form-field/FormField';
import {Formula} from '../sum-set-config/formulas/formula';
import {FormulaAdd} from '../sum-set-config/formulas/formulaAdd';
import {BehaviorSubject} from 'rxjs';

export class CombinedNumberSetBuilder extends FormSetBuilder {
	get fieldToPrefill(): FormField {
		if (this._fieldToPrefill) {
			return this._fieldToPrefill.build();
		}
	}
	
	get setsToCombine(): SumSetConfig[] {
		const newSets: SumSetConfig[] = [];
		this._setsToCombine.forEach(set => {
			newSets.push(set.build());
		});
		return newSets;
	}
	
	get trigger(): SumSetTrigger[] {
		const buildTriggers: SumSetTrigger[] = [];
		this._triggers.forEach(trigger => {
			buildTriggers.push(trigger.build())
		});
		return buildTriggers;
	}
	
	get formula(): Formula {
		return this._formula;
	}
	
	protected _buildingClass: any = CombinedNumberSet;
	private _setsToCombine: SumSetConfigBuilder[] = [];
	private _fieldToPrefill: FormFieldBuilder;
	private _formula: Formula = new FormulaAdd();
	private _triggers: SumSetTriggerBuilder[] = [];
	
	public static newFactory(injector: Injector): CombinedNumberSetBuilder {
		return new CombinedNumberSetBuilder(injector);
	}
	
	public setRelatedSets(data: SumSetConfigBuilder[]): CombinedNumberSetBuilder {
		this._setsToCombine = data;
		return this;
	}
	
	public setFieldToPrefill(data: FormFieldBuilder): CombinedNumberSetBuilder {
		this._fieldToPrefill = data;
		return this;
	}
	
	public setTriggers(data: SumSetTriggerBuilder[]): CombinedNumberSetBuilder {
		this._triggers = data;
		return this;
	}
	
	public setFormula(_formula: Formula): CombinedNumberSetBuilder {
		this._formula = _formula;
		return this;
	}
}

export class CombinedNumberSet extends FormSet2 {
	get triggeredSets(): BehaviorSubject<FormSet2[]> {
		return this._triggeredSets;
	}
	
	private _triggeredSets: BehaviorSubject<FormSet2[]> = new BehaviorSubject([]);
	private _setsToCombine: SumSetConfig[] = [];
	private _triggers: SumSetTrigger[];
	private _formula: Formula;
	_builder: CombinedNumberSetBuilder;
	
	constructor(b: CombinedNumberSetBuilder) {
		super(b);
		this._setsToCombine = b.setsToCombine;
		this._triggers = b.trigger;
		this._formula = b.formula;
	}
	
	init(): Promise<any> {
		return new Promise<any>(resolve => {
			super.init().then(() => {
				this._setsToCombine.forEach(set => {
					set.totalSetValue.subscribe(() => {
						const newNumbers: number[] = [];
						this._setsToCombine.forEach(set => {
							newNumbers.push(set.totalSetValue.getValue());
						});
						let calculatedNumber: number = this._formula.calculate(newNumbers);
						calculatedNumber = ~~(calculatedNumber);
						if(calculatedNumber <= 0 || isNaN(calculatedNumber)) {
							calculatedNumber = 0;
						}
						this._triggers.forEach(trigger => {
							if (trigger.check(calculatedNumber)) {
								this.addSet(trigger.triggerSet);
							} else {
								this.removeSet(trigger.triggerSet);
							}
						});
						
						this._builder.fieldToPrefill.prefillValue(calculatedNumber);
					});
				});
				
				resolve(true);
			});
		});
	}
	
	private removeSet(set: FormSet2): void {
		const indexToRemove: number = this._triggeredSets.getValue().indexOf(set);
		if (indexToRemove > -1) {
			const newArray: FormSet2[] = this._triggeredSets.getValue();
			newArray.splice(indexToRemove, 1);
			this._triggeredSets.next(newArray);
		}
	}
	
	private addSet(set: FormSet2): void {
		if (this._triggeredSets.getValue().indexOf(set) < 0) {
			this._triggeredSets.next([...this._triggeredSets.getValue(), ...[set]]);
		}
	}
	
	// isValid(): Promise<ObjValidPair> {
	// 	return new Promise<ObjValidPair>(resolve => {
	// 		let promises: Promise<ObjValidPair>[] = [];
	// 		this._children.forEach(child => {
	// 			if (child.show.getValue()) {
	// 				promises.push(child.isValid());
	// 			}
	// 		});
	//
	// 		this._triggeredSets.getValue().forEach(child => {
	// 			if (child.show.getValue()) {
	// 				promises.push(child.isValid());
	// 			}
	// 		});
	// 		promises.push(this.validate(this.getValue()));
	//
	// 		Promise.all(promises).then((res: ObjValidPair[]) => {
	// 			// if (res.some(obj => obj.valid === false)) {
	// 			// 	console.warn(this._id, 'is invalid!!')
	// 			// }
	// 			resolve(new ObjValidPair(this, !res.some(obj => obj.valid === false)));
	// 		});
	// 	});
	// }
	
	// TODO fix deze
	// public validate(): boolean {
	// 	let valid = super.validate();
	//
	// 	if(this.show.value){
	// 		this._triggeredSets.getValue().forEach(set => {
	// 			if (!set.validate()) {
	// 				console.warn('Set: ', set.id, ', is not valid');
	// 				valid = false;
	// 			}
	// 		});
	// 	} else {
	// 		valid = true;
	// 	}
	//
	// 	return valid;
	// }
}
