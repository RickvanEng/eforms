import {Component, Input} from '@angular/core';
import {FormSet2} from '../FormSet';
import {SetComponent} from '../set.component';
import {CombinedNumberSet} from './combined-number-set';

@Component({
	selector: 'app-combined-number-field',
	templateUrl: './combined-number-set.component.html',
	styleUrls: ['./combined-number-set.component.scss']
})
export class CombinedNumberSetComponent extends SetComponent {
	
	public _component: CombinedNumberSet;
	public triggerSets: FormSet2[] = [];
	public show: boolean;
	
	@Input() set baseComponent(component: CombinedNumberSet) {
		this._component = component;
		this._component.show.subscribe(value => {
			this.show = value;
		});
		
		component.triggeredSets.subscribe(sets => {
			this.triggerSets = sets;
		});
	}
}
