import {CFAAction} from './CFA-action';
import {DefaultFormFunctions} from '../../../base/defaultFormFunctions';
import {Injector} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Auth2Service} from '../../../services/auth2.service';
import {environment} from '../../../../../../src/environments/environment';
import { FormService } from '../../../1-form/form.service';

export class CFAActionBECall extends CFAAction {

    private api: string;
    private useFormId: boolean;

    constructor(injector: Injector, api: string, useFormId: boolean = false) {
        super(injector);
        this.api = api;
        this.useFormId = useFormId;
    }

    action(formObjects: DefaultFormFunctions[]): Promise<any> {
        return new Promise<any>(resolve => {
            let headers = {'X-Auth-Token': this.injector.get(Auth2Service).loginToken.value.value};
            this.injector.get(HttpClient).post(environment.baseUrl + this.api, this.createBody(formObjects), {headers: headers}).subscribe(res => {
                resolve(res);
            });
        });
    }

    private createBody(formObjects: DefaultFormFunctions[]): any {
        let result: any = {};

        formObjects.forEach(obj => {
            result[obj.id] = obj.getValue();
        });

        if (this.useFormId) {
            result['id'] = FormService.getInstance().selectedForm.id;
        }
        return result;
    }
}