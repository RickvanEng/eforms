import {DefaultFormFunctions} from '../../../base/defaultFormFunctions';
import {Injector} from '@angular/core';

export class CFAAction {

    constructor(protected injector: Injector) {

    }

    public action(formObjects: DefaultFormFunctions[]): Promise<any> {
        return new Promise<any>(resolve => {
            console.warn('Override this method!');
            resolve();
        });
    }
}
