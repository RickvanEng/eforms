import {CFAAction} from './CFA-action';
import {DefaultFormFunctions} from '../../../base/defaultFormFunctions';
import {Injector} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Auth2Service} from '../../../services/auth2.service';
import {environment} from '../../../../../../src/environments/environment';
import {FormField} from '../../../5-fields/form-field/FormField';
import {FormService} from 'projects/form-components/2.0/1-form/form.service';

export class CFAActionGetBsnWithLdap extends CFAAction {
    
    private saveAs: string;

    constructor(injector: Injector, saveAs: string) {
        super(injector);
        this.saveAs = saveAs;
    }

    action(formObjects: DefaultFormFunctions[]): Promise<any> {
        let bsnField: FormField = formObjects[0] as FormField;
        return new Promise<any>(resolve => {
            this.injector.get(HttpClient).get(environment.baseUrl + 'adapter/session/token/' + this.injector.get(Auth2Service).loginToken.value.value + '/bsn/' + bsnField.getValue())
                .subscribe((res: any) => {
                    if (res.identities) {
                        FormService.getInstance().setFormData(res.identities[0].data, this.saveAs);
                    } else {
                        FormService.getInstance().setFormData({}, this.saveAs);
                    }
                    resolve(res);
                }, error1 => {
                    console.error(error1);
                    FormService.getInstance().setFormData({}, this.saveAs);
                });
        });
    }
}
