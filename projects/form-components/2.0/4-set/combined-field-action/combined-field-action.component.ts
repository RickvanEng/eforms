import {Component} from '@angular/core';
import {SetComponent} from '../set.component';
import {CombinedFieldAction} from './combined-field-action';

@Component({
    selector: 'app-combined-field-action',
    templateUrl: './combined-field-action.component.html',
    styleUrls: ['./combined-field-action.component.scss']
})
export class CombinedFieldActionComponent extends SetComponent {
    public _component: CombinedFieldAction;
    
    public showErrors(): boolean {
        return (this._component._abstractControl.touched && this._component._abstractControl['hasBeenClicked'])
    }
}
