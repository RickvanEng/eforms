import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CombinedFieldActionComponent } from './combined-field-action.component';

describe('CombinedFieldActionComponent', () => {
  let component: CombinedFieldActionComponent;
  let fixture: ComponentFixture<CombinedFieldActionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CombinedFieldActionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CombinedFieldActionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
