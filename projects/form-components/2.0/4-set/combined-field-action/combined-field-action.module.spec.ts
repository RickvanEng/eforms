import { CombinedFieldActionModule } from './combined-field-action.module';

describe('CombinedFieldActionModule', () => {
  let combinedFieldActionModule: CombinedFieldActionModule;

  beforeEach(() => {
    combinedFieldActionModule = new CombinedFieldActionModule();
  });

  it('should create an instance', () => {
    expect(combinedFieldActionModule).toBeTruthy();
  });
});
