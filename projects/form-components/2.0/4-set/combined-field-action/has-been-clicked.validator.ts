import {FormGroup, ValidatorFn} from '@angular/forms';

export function HasBeenClickedValidator(): ValidatorFn {
    return function validate(formGroup: FormGroup) {
        return formGroup['hasBeenClicked'] ? null : {combinedFieldActionValidator: false};
    };
}
