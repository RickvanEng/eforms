import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {CombinedFieldActionComponent} from './combined-field-action.component';
import {FieldErrors2Module} from '../../5-fields/field-errors2/field-errors2.module';
import {BaseModule} from '../../base/base.module';
import {FormFieldModule} from '../../5-fields/form-field/form-field.module';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

@NgModule({
    imports: [
        CommonModule,
        FieldErrors2Module,
        BaseModule,
        FormFieldModule,
        FormsModule,
        ReactiveFormsModule
    ],
    declarations: [
        CombinedFieldActionComponent
    ],
    exports: [
        CombinedFieldActionComponent
    ]
})
export class CombinedFieldActionModule {
}
