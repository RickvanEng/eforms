import {FormSet2, FormSetBuilder} from '../FormSet';
import {Injector} from '@angular/core';
import {Button, ButtonBuilder} from '../../5-fields/form-field/button/button';
import {CFAAction} from './actions/CFA-action';
import {BasicValidationResult} from '../../validators/basic/BasicValidationResult';
import { EFormsFormGroup } from '../../base/eFormsAbstractControl';
import { EBasicValidators } from '../../validators/basic/basicValidatorMap';

export class CombinedFieldActionBuilder extends FormSetBuilder {
    get succesFeedback(): BasicValidationResult {
        return this._succesFeedback;
    }

    get error(): BasicValidationResult {
        return this._error;
    }

    get action(): CFAAction {
        return this._action;
    }

    get actionButton(): Button {
        return this._actionButton.build();
    }

    protected _buildingClass: any = CombinedFieldAction;
    private _actionButton: ButtonBuilder;
    private _action: CFAAction;
    private _error: BasicValidationResult;
    private _succesFeedback: BasicValidationResult;

    public static newBuilder(injector: Injector): CombinedFieldActionBuilder {
        return new CombinedFieldActionBuilder(injector);
    }

    public setActionButton(field: ButtonBuilder): CombinedFieldActionBuilder {
        this._actionButton = field;
        return this;
    }

    public setError(error: BasicValidationResult): CombinedFieldActionBuilder {
        this._validators.find(val => val.validatorId === EBasicValidators.IS_PROP_PRESENT).responses[0].msg = error.message;
        this._error = error;
        return this;
    }

    public setSuccesFeedback(value: BasicValidationResult): CombinedFieldActionBuilder {
        this._succesFeedback = value;
        return this;
    }

    public setAction(action: CFAAction): CombinedFieldActionBuilder {
        this._action = action;
        return this;
    }
}

export class CombinedFieldAction extends FormSet2 {
    get actionButton(): Button {
        return this._actionButton;
    }

    private _actionButton: Button;
    private _action: CFAAction;
    private _errorToThrow: BasicValidationResult;
    private _succesFeedback: BasicValidationResult;
    _abstractControl: EFormsFormGroup;

    constructor(b: CombinedFieldActionBuilder) {
        super(b);
        this._actionButton = b.actionButton;
        this._action = b.action;
        this._errorToThrow = b.error;
        this._succesFeedback = b.succesFeedback;
    }

    init(): Promise<any> {
        this._abstractControl['hasBeenClicked'] = false;

        this._actionButton.trigger.subscribe(trigger => {
            if (trigger) {
                this.markAsTouched();

                // First check if all the fields that are required are valid
                for (let key of Object.keys(this._abstractControl.controls)) {
                    if (!this._abstractControl.get(key).valid) {
                        return;
                    }
                }

                this.callAction().then(() => {
                    this._abstractControl['hasBeenClicked'] = true;
                    this._abstractControl.updateValueAndValidity();
                });
            }
        });

        Object.keys(this._abstractControl.controls).forEach(key => {
            this._abstractControl.get(key).valueChanges.subscribe(() => {
                this._abstractControl['hasBeenClicked'] = false;
            })
        });

        return super.init();
    }

    private callAction(): Promise<any> {
        return new Promise<any>(resolve => {
            this._action.action(this._children).then(res => {
                this._abstractControl['cfaValid'] = !res.error;
                if (res.error) {
                    this._abstractControl.setErrors({cfaValid: false});
                }

                resolve();
            }).catch(error => {
                console.error(error);
            })
        })
    }
}
