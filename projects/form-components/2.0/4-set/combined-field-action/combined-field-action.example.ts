import {Injector} from '@angular/core';
import {
	ComponentExampleConfigBuilder,
	IComponentExampleConfig
} from '../../../../component-dash/component-example-config.builder';
import {GenericFields} from '../../5-fields/presets/genericFields';
import {ComponentExampleBuilder} from '../../../../component-dash/component-example.builder';
import {ButtonBuilder} from '../../5-fields/form-field/button/button';
import {ButtonComponent} from '../../5-fields/form-field/button/button.component';
import {CombinedFieldActionBuilder} from './combined-field-action';
import {CombinedFieldActionComponent} from './combined-field-action.component';
import {BasicValidationResult} from '../../validators/basic/BasicValidationResult';
import {WarningType} from '../../5-fields/WarningType';
import {ValidatorPreBuildConfigs} from '../../validators/basic/ValidatorPreBuildConfigs';
import {CFAActionBECall} from './actions/CFA-action-BECall';
import {GenericTabFieldSets} from '../../3-tab-field-set/presets/genericTabFieldSets';
import {CFAActionGetBsnWithLdap} from './actions/CFA-action-getBsnWithLdap';

export class CombinedFieldActionExample {
    public getField(inj: Injector): IComponentExampleConfig {

        let genericSets: GenericTabFieldSets = new GenericTabFieldSets(inj);
        let genericFields: GenericFields = new GenericFields(inj);

        return ComponentExampleConfigBuilder.newBuilder()
            .setComponent('combinedFieldAction')
            .setExamples([
                ComponentExampleBuilder.newBuilder()
                    .setBuilder(genericSets.getCombinedActionSet()
                        .setAction(new CFAActionGetBsnWithLdap(inj, 'bsnData'))
                        .setError(new BasicValidationResult(
                            'Er is iets fout gegaan. 1. U heeft een verkeerd BSN-nummer ingevuld. Vul een juist BSN-nummer in. 2. De gegevens van het BSN-nummer kunnen niet worden opgehaald. Er is een storing of de persoon is niet goed ingeschreven in de BRP.'))
                        .setActionButton(
                            ButtonBuilder.newBuilder(inj)
                                .setSingleBuild(false)
                                .setButtonClass('btn btn-mfg')
                                .setButtonAllignment('full-height')
                                .setButtonText('Validate')
                                .setComponent(ButtonComponent)
                                .setID('validateButton')
                        )
                        .setID('getBsnData')
                        .setChildren([
                            genericFields.datePicker
                                .setID('leerlingGeboortedatum')
                                .setLabel('Geboortedatum')
                                .setValidatorRequired()
                                .setDefaultValue('18-11-1989'),
                            genericFields.bsn
                                .setID('leerlingBsn')
                                .setEnkelIntern(true)
                                .setLabel('Burgerservicenummer (BSN)')
                                .setValidatorRequired()
                                .setDefaultValue('092759683'),
                        ]),


                        // .setValidation(ValidatorPreBuildConfigs.getInstance().combinedFieldAction())
                        // .setAction(new CFAActionBECall(inj, 'eforms-api/controller/validation/custom/bsn-date-of-birth'))
                        // .setError(new BasicValidationResult(
                        //     'Deze match is niet goed',
                        //     true,
                        //     WarningType.DANGER
                        // ))
                        // .setActionButton(
                        // ButtonBuilder.newBuilder(inj)
                        //     .setSingleBuild(false)
                        //     .setButtonClass('btn btn-mfg')
                        //     .setButtonAllignment('full-height')
                        //     .setButtonText('Validate')
                        //     .setComponent(ButtonComponent)
                        //     .setID('validateButton'))
                        // .setComponent(CombinedFieldActionComponent)
                        // .setChildren([
                        //
                        // ])
                        // .setID('callToActionExample')
                        // .setLabel('Combined action field!')
                    )
                    .setDescription(
                        '<ul>' +
                        '<li>The set title should be visible</li>' +
                        '<li>By default, one generated set should be vivisble</li>' +
                        '<li>If there is one set it can\'t be removed and the remove button should not be visible</li>' +
                        '<li>If you click the add button, a new clean set should appear</li>' +
                        '<li>If you validate the set, all de generated fields should also be validated</li>' +
                        '</ul>')
                    .setState(undefined)
                    .setValidateButton(true)
                    .data,
            ])
            .data;
    }
}
