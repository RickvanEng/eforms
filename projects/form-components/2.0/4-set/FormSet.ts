import {DefaultFormFunctions, DefaultFormFunctionsBuilder} from '../base/defaultFormFunctions';
import {ICmsControlset} from './ICmsControlset';

export class FormSetBuilder extends DefaultFormFunctionsBuilder {
    public static newBuilder(): FormSetBuilder {
        return new FormSetBuilder();
    }

    build(): ICmsControlset {
        return this._config
    }

}

export class FormSet2 extends DefaultFormFunctions {

}
