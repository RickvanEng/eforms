import {FormSet2, FormSetBuilder} from '../../FormSet';
import {Injector} from '@angular/core';
import {LabelField, LabelFieldBuilder} from '../../../5-fields/form-field/label-field/LabelField';
import {FormField, FormFieldBuilder} from '../../../5-fields/form-field/FormField';
import {BehaviorSubject} from 'rxjs';
import {FormState} from '../../../1-form/formStateManager';
import {DefaultFormFunctions} from '../../../base/defaultFormFunctions';
import {AbstractControl, FormGroup} from '@angular/forms';
import {EFormsAbstractControl, EFormsFormGroup} from '../../../base/eFormsAbstractControl';

export class SumFieldsSetBuilder extends FormSetBuilder {
    get fieldsPartner(): FormField[] {
        return this._fieldsPartner;
    }

    get extraFields(): FormField[] {
        return this._extraFields;
    }

    get extraFieldsPartner(): FormField[] {
        return this._extraFieldsPartner;
    }

    get max(): number {
        return this._max;
    }

    get maxSingle(): number {
        return this._maxSingle;
    }

    get maxPartner(): number {
        return this._maxPartner;
    }

    get maxDefaultPartner(): number {
        return this._maxDefaultPartner;
    }

    get warningLabel(): LabelField {
        return this._warningLabel;
    }

    get warningLabelDefaultPartner(): LabelField {
        return this._warningLabelDefaultPartner;
    }

    get warningLabelSingle(): LabelField {
        return this._warningLabelSingle;
    }

    get warningLabelPartner(): LabelField {
        return this._warningLabelPartner;
    }

    protected _fieldsPartner: FormField[] = [];
    protected _fieldBuildersPartner: FormFieldBuilder[] = [];

    protected _extraFields: FormField[] = [];
    protected _extraFieldsPartner: FormField[] = [];
    protected _extraFieldsBuilders: FormFieldBuilder[] = [];
    protected _extraFieldsBuildersPartner: FormFieldBuilder[] = [];

    protected _buildingClass: any = SumFieldsSet;
    protected _warningLabelBuild: LabelFieldBuilder;
    protected _warningLabelDefaultPartnerBuild: LabelFieldBuilder;
    protected _warningLabelSingleBuild: LabelFieldBuilder;
    protected _warningLabelPartnerBuild: LabelFieldBuilder;
    protected _warningLabel: LabelField;
    protected _warningLabelDefaultPartner: LabelField;
    protected _warningLabelSingle: LabelField;
    protected _warningLabelPartner: LabelField;

    protected _max: number;
    protected _maxSingle: number;
    protected _maxPartner: number;
    protected _maxDefaultPartner: number;

    public static newBuilder(injector: Injector): SumFieldsSetBuilder {
        return new SumFieldsSetBuilder(injector);
    }

    public setLabelDefault(field: LabelFieldBuilder, max: number) {
        this._warningLabelBuild = field;
        this._maxSingle = max;
        return this;
    }

    public setLabelDefaultPartner(field: LabelFieldBuilder, max: number) {
        this._warningLabelDefaultPartnerBuild = field;
        this._maxSingle = max;
        return this;
    }

    public setLabelSingle(field: LabelFieldBuilder, maxSingle: number) {
        this._warningLabelSingleBuild = field;
        this._maxSingle = maxSingle;
        return this;
    }

    public setLabelPartner(field: LabelFieldBuilder, maxPartner: number) {
        this._warningLabelPartnerBuild = field;
        this._maxPartner = maxPartner;
        return this;
    }

    public setFieldsPartner(fields: FormFieldBuilder[]): FormSetBuilder {
        this._fieldBuildersPartner = fields;
        return this;
    }

    public setExtraFields(fields: FormFieldBuilder[]): FormSetBuilder {
        this._extraFieldsBuilders = fields;
        return this;
    }

    public setExtraFieldsPartner(fields: FormFieldBuilder[]): FormSetBuilder {
        this._extraFieldsBuildersPartner = fields;
        return this;
    }


    prebuild() {
        super.prebuild();
        if (this._warningLabelBuild) {
            this._warningLabel = this._warningLabelBuild.build();
        }
        if (this._warningLabelDefaultPartnerBuild) {
            this._warningLabelDefaultPartner = this._warningLabelDefaultPartnerBuild.build();
        }
        if (this._warningLabelSingleBuild) {
            this._warningLabelSingle = this._warningLabelSingleBuild.build();
        }
        if (this._warningLabelPartnerBuild) {
            this._warningLabelPartner = this._warningLabelPartnerBuild.build();
        }

        if (this._extraFieldsBuilders) {
            this._extraFieldsBuilders.forEach(builder => {
                this._extraFields.push(builder.build());
            });
        }
        if (this._extraFieldsBuildersPartner) {
            this._extraFieldsBuildersPartner.forEach(builder => {
                this._extraFieldsPartner.push(builder.build());
            });
        }

        this._fieldBuildersPartner.forEach(builder => {
            this._fieldsPartner.push(builder.build());
        });
    };
}

export class SumFieldsSet extends FormSet2 {
    get totalValue(): BehaviorSubject<number> {
        return this._totalValue;
    }

    private _totalValue: BehaviorSubject<number> = new BehaviorSubject(0);

    constructor(b: SumFieldsSetBuilder) {
        super(b);
    }

    constructFormControl(inj: Injector): EFormsAbstractControl {
        let newGroup: EFormsAbstractControl = super.constructFormControl(inj);

        newGroup.valueChanges.subscribe(() => {
            this.sumValues(newGroup as EFormsFormGroup);
        });

        return newGroup;
    }

    private sumValues(formGroup: FormGroup) {

        let result: number = 0;

        Object.keys(formGroup.controls).forEach(key => {
            result = result + ~~(formGroup.get(key).value);
        });

        this._totalValue.next(result);
    }

    // updateVariableContent(target: string, replacement: string) {
    //     super.updateVariableContent(target, replacement);
    //     for (let field of this.fieldsPartner) {
    //         field.updateVariableContent(target, replacement)
    //     }
    //     for (let field of this.extraFields) {
    //         field.updateVariableContent(target, replacement)
    //     }
    //     for (let field of this.extraFieldsPartner) {
    //         field.updateVariableContent(target, replacement)
    //     }
    // }

}
