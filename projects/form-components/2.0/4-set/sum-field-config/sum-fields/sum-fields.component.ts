import {Component} from '@angular/core';
import {SetComponent} from '../../set.component';
import {SumFieldsSet} from './sum-fields';

@Component({
    selector: 'app-sum-fields',
    templateUrl: './sum-fields.component.html',
    styleUrls: ['./sum-fields.component.scss']
})
export class SumFieldsComponent extends SetComponent {
    public _component: SumFieldsSet;
}
