import {
    ComponentExampleConfigBuilder,
    IComponentExampleConfig
} from '../../../../../component-dash/component-example-config.builder';
import {Injector} from '@angular/core';
import {ComponentExampleBuilder} from '../../../../../component-dash/component-example.builder';
import {SumFieldsSetBuilder} from './sum-fields';
import {SumFieldsComponent} from './sum-fields.component';
import {GenericFields} from '../../../5-fields/presets/genericFields';

export class SumFieldsExample {
    public getField(inj: Injector): IComponentExampleConfig {

        let genericFields: GenericFields = new GenericFields(inj);

        return ComponentExampleConfigBuilder.newBuilder()
            .setComponent('SumFieldsExample')
            .setExamples([
                ComponentExampleBuilder.newBuilder()
                    .setBuilder(SumFieldsSetBuilder.newBuilder(inj)
                        .setLabelDefault(genericFields.label
                            .setLabel('label 1')
                            .setID('label1'), 1000)
                        .setChildren([
                            genericFields.numbersOnlyField
                                .setID('simfield1')
                                .setLabel('field 1'),
                            genericFields.numbersOnlyField
                                .setID('simfield2')
                                .setLabel('field 2'),
                            genericFields.numbersOnlyField
                                .setID('simfield3')
                                .setLabel('field 3'),
                        ])
                        .setComponent(SumFieldsComponent)
                        .setID('sumFieldsSet'))
                    .setDescription(
                        '<ul>' +
                        '<li>The title should be visible</li>' +
                        '<li>If it has children they should be visible</li>' +
                        '<li>When validated, all the children should be validated</li>' +
                        '</ul>')
                    .setState(undefined)
                    .setValidateButton(true)
                    .data,
            ])
            .data;
    }
}
