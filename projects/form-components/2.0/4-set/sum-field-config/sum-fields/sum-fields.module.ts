import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {SumFieldsComponent} from './sum-fields.component';
import {BaseModule} from '../../../base/base.module';
import {FormFieldModule} from '../../../5-fields/form-field/form-field.module';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { FieldErrors2Module } from '../../../5-fields/field-errors2/field-errors2.module';

@NgModule({
    imports: [
        CommonModule,
        BaseModule,
        FormFieldModule,
        FormsModule,
        ReactiveFormsModule,
        FieldErrors2Module,
    ],
    declarations: [
       SumFieldsComponent
    ],
    exports: [
        SumFieldsComponent
    ]
})
export class SumFieldsModule {
}
