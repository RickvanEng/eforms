import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SumFieldConfigComponent } from './sum-field-config.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {FormFieldModule} from '../../5-fields/form-field/form-field.module';
import {FieldErrors2Module} from '../../5-fields/field-errors2/field-errors2.module';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        FormFieldModule,
        ReactiveFormsModule,
        FieldErrors2Module
    ],
  declarations: [SumFieldConfigComponent]
})
export class SumFieldConfigModule { }
