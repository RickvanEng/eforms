import {Injector} from '@angular/core';
import {DefaultFormFunctionsBuilder} from '../../base/defaultFormFunctions';
import {LabelField} from '../../5-fields/form-field/label-field/LabelField';
import {SumFieldConfig} from './sum-field-config';

export interface ISumFielTrigger {
    label: LabelField;
    min: number;
    max: number;
}

export class SumFieldConfigBuilder extends DefaultFormFunctionsBuilder {
    get triggerFields(): ISumFielTrigger[] {
        return this._triggerFields;
    }

    private _triggerFields: ISumFielTrigger[] = [];

    protected _buildingClass: any = SumFieldConfig;

    public static newBuilder(injector: Injector): SumFieldConfigBuilder {
        return new SumFieldConfigBuilder(injector);
    }

    public setTriggers(triggerFields: ISumFielTrigger[]): SumFieldConfigBuilder {
        this._triggerFields = triggerFields;
        return this;
    }
}
