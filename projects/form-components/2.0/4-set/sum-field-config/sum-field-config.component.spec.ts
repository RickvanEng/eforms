import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SumFieldConfigComponent } from './sum-field-config.component';

describe('SumFieldConfigComponent', () => {
  let component: SumFieldConfigComponent;
  let fixture: ComponentFixture<SumFieldConfigComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SumFieldConfigComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SumFieldConfigComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
