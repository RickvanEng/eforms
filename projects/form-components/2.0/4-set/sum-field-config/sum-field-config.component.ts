import {Component} from '@angular/core';
import {BaseComponent} from '../../base/base.component';
import {SumFieldConfig} from './sum-field-config';

@Component({
    selector: 'app-sum-field-config',
    templateUrl: './sum-field-config.component.html',
    styleUrls: ['./sum-field-config.component.scss']
})
export class SumFieldConfigComponent extends BaseComponent {
    public _component: SumFieldConfig;
}
