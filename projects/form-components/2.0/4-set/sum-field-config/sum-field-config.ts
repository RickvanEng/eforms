import {DefaultFormFunctions} from '../../base/defaultFormFunctions';
import {ISumFielTrigger, SumFieldConfigBuilder} from './sum-field-config.builder';
import {SumFieldsSet} from './sum-fields/sum-fields';
import {EFormsAbstractControl, EFormsFormArray} from '../../base/eFormsAbstractControl';
import {Injector} from '@angular/core';

export class SumFieldConfig extends DefaultFormFunctions {
    get children(): DefaultFormFunctions[] {
        let result: DefaultFormFunctions[] = [];

        this._children.forEach(setChild => {
            setChild.children.forEach(child => {
                result.push(child);
            })
        });

        return result;
    }

    get formSetChildren(): SumFieldsSet[] {
        return this._children;
    }

    get triggers(): ISumFielTrigger[] {
        return this._builder.triggerFields;
    }

    _builder: SumFieldConfigBuilder;
    _children: SumFieldsSet[];
    _abstractControl: EFormsFormArray;

    getAbstractControl(inj: Injector): EFormsAbstractControl {
        let control: EFormsFormArray = new EFormsFormArray([], {
            validators: this.getFormValidators(this._validators),
            asyncValidators: this.getAsyncValidators(this._injector, this._asyncValidators)
        });

        this._builder.triggerFields.forEach(trigger => {
            trigger.label.constructFormControl(this._injector);
        });

        this.formSetChildren.forEach((child: SumFieldsSet) => {
            control.push(child.constructFormControl(inj));
            child.totalValue.subscribe(() => {
                this.checkTriggers(this.count());
            })
        });

        return control;
    }

    private checkTriggers(value: number): void {
        let foundTrigger: ISumFielTrigger = undefined;

        for (let trigger of this._builder.triggerFields) {
            trigger.label.setShow(false);
            if (trigger.max) {
                if (value > trigger.min && value < trigger.max) {
                    foundTrigger = trigger;
                }
            } else {
                if (value > trigger.min) {
                    foundTrigger = trigger;
                }
            }

        }

        if (foundTrigger) {
            foundTrigger.label.setShow(true);
        }
    }

    private count(): number {
        let result: number = 0;
        this._children.forEach(child => {
            result = result + child.totalValue.value;
        });
        return result;
    }
}
