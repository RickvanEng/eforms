import {Component} from '@angular/core';
import {EerdereVergunningenSet} from '../../_presets/vergunningen/eerdere-vergunningen.set';
import {BaseComponent} from '../../base/base.component';

@Component({
	selector: 'app-eerdere-vergunning-set',
	templateUrl: './eerdere-vergunning-set.component.html',
	styleUrls: ['./eerdere-vergunning-set.component.scss']
})
export class EerdereVergunningSetComponent extends BaseComponent {
	public _component: EerdereVergunningenSet;
}
