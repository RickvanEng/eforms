import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {EerdereVergunningSetComponent} from './eerdere-vergunning-set.component';

describe('EerdereVergunningSetComponent', () => {
  let component: EerdereVergunningSetComponent;
  let fixture: ComponentFixture<EerdereVergunningSetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EerdereVergunningSetComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EerdereVergunningSetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
