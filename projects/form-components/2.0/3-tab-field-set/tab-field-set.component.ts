import {Component} from '@angular/core';
import {TabFieldSet} from './tab-field-set';
import {BaseComponent} from '../base/base.component';
import {VariableFormService} from '../../../forms/services/genericFormService/variableFormService';

@Component({
	selector: 'app-tab-field-set',
	templateUrl: './tab-field-set.component.html',
	styleUrls: ['./tab-field-set.component.scss']
})
export class TabFieldSetComponent extends BaseComponent {
	
	public _component: TabFieldSet;
	
	public next(): void {
		VariableFormService.getInstance().formRouting.selectedForm.toNextTab();
	}
}
