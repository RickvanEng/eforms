import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {TabFieldSetComponent} from './tab-field-set.component';
import {SetModule} from '../4-set/set.module';
import {SetComponent} from '../4-set/set.component';
import {MultipleFieldGeneratorModule} from "../4-set/multple-field-generator/multiple-field-generator.module";
import {MultipleFieldGeneratorComponent} from "../4-set/multple-field-generator/multiple-field-generator.component";;
import {SumFieldsModule} from '../4-set/sum-field-config/sum-fields/sum-fields.module';
import {SumFieldsComponent} from '../4-set/sum-field-config/sum-fields/sum-fields.component';
import {EerdereVergunningSetModule} from '../4-set/eerdere-vergunning-set/eerdere-vergunning-set.module';
import {EerdereVergunningSetComponent} from '../4-set/eerdere-vergunning-set/eerdere-vergunning-set.component';
import {BaseModule} from '../base/base.module';
import {SumSetConfigComponent} from '../4-set/sum-set-config/sum-set-config.component';
import {CombinedNumberSetComponent} from '../4-set/combined-number-set/combined-number-set.component';
import {CombinedNumberSetModule} from '../4-set/combined-number-set/combined-number-set.module';
import {CombinedFieldActionModule} from '../4-set/combined-field-action/combined-field-action.module';
import {CombinedFieldActionComponent} from '../4-set/combined-field-action/combined-field-action.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {FormFieldModule} from '../5-fields/form-field/form-field.module';
import { FieldErrors2Module } from '../5-fields/field-errors2/field-errors2.module';
import {SumFieldConfigComponent} from '../4-set/sum-field-config/sum-field-config.component';
import {SumFieldConfigModule} from '../4-set/sum-field-config/sum-field-config.module';

@NgModule({
    imports: [
        CommonModule,
        SetModule,
        SumFieldConfigModule,
        MultipleFieldGeneratorModule,
        SumFieldsModule,
        EerdereVergunningSetModule,
        BaseModule,
        CombinedNumberSetModule,
        CombinedFieldActionModule,
        FormsModule,
        FormFieldModule,
        ReactiveFormsModule,
        FieldErrors2Module,
    ],
    declarations: [
        TabFieldSetComponent
    ],
    exports: [
        TabFieldSetComponent
    ],
    entryComponents: [
        SetComponent,
        MultipleFieldGeneratorComponent,
        SumFieldsComponent,
        EerdereVergunningSetComponent,
		SumSetConfigComponent,
		CombinedNumberSetComponent,
        CombinedFieldActionComponent,
        SumFieldConfigComponent,
    ]
})
export class TabFieldSetModule {
}
