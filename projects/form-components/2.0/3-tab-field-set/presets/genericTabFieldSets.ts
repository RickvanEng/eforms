import {Injector} from '@angular/core';
import {TabFieldSetBuilder} from '../tab-field-set';
import {TabFieldSetComponent} from '../tab-field-set.component';
import {FormSetBuilder} from '../../4-set/FormSet';
import {SetComponent} from '../../4-set/set.component';
import {DigidSet2Builder} from '../../_presets/digid/digidSet2';
import { CombinedFieldActionBuilder } from '../../4-set/combined-field-action/combined-field-action';
import { CombinedFieldActionComponent } from '../../4-set/combined-field-action/combined-field-action.component';
import { ValidatorPreBuildConfigs } from '../../validators/basic/ValidatorPreBuildConfigs';
import { ValidatorResponseBuilder } from '../../validators/validatorResponse.builder';

export class GenericTabFieldSets {
	
	constructor(private injector: Injector) {
	}
	
	public getNewDefaultTabSet(): TabFieldSetBuilder {
		return TabFieldSetBuilder.newBuilder(this.injector)
			.setComponent(TabFieldSetComponent)
			.setShowInOverviewSet(false)
			.setID('basicTabFieldSet') as TabFieldSetBuilder
	}
	
	public getNewSet(): FormSetBuilder {
		return FormSetBuilder.newBuilder(this.injector)
			.setComponent(SetComponent) as FormSetBuilder
	}
	
	public getNewDigidSet(): DigidSet2Builder {
		return DigidSet2Builder.newBuilder(this.injector)
			.setComponent(SetComponent) as DigidSet2Builder
	}

	public getCombinedActionSet(): CombinedFieldActionBuilder {
		return CombinedFieldActionBuilder.newBuilder(this.injector)
			.setComponent(CombinedFieldActionComponent)
			.setValidation(ValidatorPreBuildConfigs.getInstance().isPropPresent('cfaValid', {responses: [
					ValidatorResponseBuilder.newBuilder()
						.setMessage('Generic isPropPresent error.')
						.build()
				]}))
	}
}
