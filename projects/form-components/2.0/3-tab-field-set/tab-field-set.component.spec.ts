import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {TabFieldSetComponent} from './tab-field-set.component';

describe('TabFieldSetComponent', () => {
  let component: TabFieldSetComponent;
  let fixture: ComponentFixture<TabFieldSetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TabFieldSetComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TabFieldSetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
