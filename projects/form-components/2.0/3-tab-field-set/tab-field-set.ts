import {DefaultFormFunctions, DefaultFormFunctionsBuilder} from '../base/defaultFormFunctions';
import {IFormFieldset} from './IFormFieldset';


export class TabFieldSetBuilder extends DefaultFormFunctionsBuilder {
    public static newBuilder(): TabFieldSetBuilder {
        return new TabFieldSetBuilder();
    }

    build(): IFormFieldset {
        return this._config
    }
}

export class TabFieldSet extends DefaultFormFunctions {

}
