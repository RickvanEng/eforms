import {ICmsComponent} from "./ICmsComponents";

export class CmsComponentBuilder {

    _config: ICmsComponent = {
        id: undefined,
        className: undefined,
    };

    public static newBuilder(): CmsComponentBuilder {
        return new CmsComponentBuilder();
    }

    public setId(id: string): CmsComponentBuilder {
        this._config.id = id;
        return this;
    }

    public setClassName(name: string): CmsComponentBuilder {
        this._config.className = name;
        return this;
    }

    public build(): ICmsComponent {
        // if (!this._config.id) {
        //     throw 'Cms constructor error. Please set an ID for this config: ' + JSON.stringify(this._config);
        // }
        return this._config;
    }
}