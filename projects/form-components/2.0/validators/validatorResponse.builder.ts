import { IValidatorResponse } from './IValidator';
import { WarningType } from '../5-fields/WarningType';

export class ValidatorResponseBuilder {

    private _config: IValidatorResponse = {
        responseCode: undefined,
        warning: false,
        msg: '',
        warningType: WarningType.DANGER,
        show: true,
        throwId: undefined,
    };

    public static newBuilder(): ValidatorResponseBuilder {
        return new ValidatorResponseBuilder();
    }

    public setWarning(value: boolean): ValidatorResponseBuilder {
        this._config.warningType = WarningType.WARNING;
        this._config.warning = value;
        return this;
    }

    public setThrowId(id: string): ValidatorResponseBuilder {
        this._config.throwId = id;
        return this;
    }

    public setShow(value: boolean): ValidatorResponseBuilder {
        this._config.show = value;
        return this;
    }

    public setError(value: boolean): ValidatorResponseBuilder {
        this._config.error = value;
        return this;
    }

    public setResponseId(id: number): ValidatorResponseBuilder {
        this._config.responseCode = id;
        return this;
    }

    public setMessage(value: string): ValidatorResponseBuilder {
        this._config.msg = value;
        return this;
    }

    public setLabelType(value: WarningType): ValidatorResponseBuilder {
        this._config.warningType = value;
        return this;
    }

    public build(): IValidatorResponse {
        return this._config;
    }
}
