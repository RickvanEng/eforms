import {IValidator, IValidatorResponse} from '../IValidator';
import {EAsyncValidators} from './asyncValidatorMap';
import {ValidatorResponseBuilder} from '../validatorResponse.builder';
import {EBirtdayResponseRange} from '../../5-fields/validators/validator/beValidations/birthday.validator';
import {FormErrors} from '../../_presets/formErrors';

/**
 * This is were the most used configs are pre-defined. This is not required, but usefull for developers.
 * All the properties van be overridden if need be.
 */

export class AsyncValidatorPrebuildConfig {

    private static instance: AsyncValidatorPrebuildConfig;

    public static getInstance(): AsyncValidatorPrebuildConfig {
        if (!AsyncValidatorPrebuildConfig.instance) {
            AsyncValidatorPrebuildConfig.instance = new AsyncValidatorPrebuildConfig();
        }
        return AsyncValidatorPrebuildConfig.instance;
    }

    private constructor() {
    }

    public iban(params?: IValidator): IValidator {
        let originalParams: IValidator = {
            validatorId: EAsyncValidators.IBAN,
            responses: [
                ValidatorResponseBuilder.newBuilder()
                    .setMessage('Dit is geen geldige Iban.')
                    .build(),
            ],
        };

        return this.updateParams(originalParams, params);
    }

    public birthday(min: number, max: number, params?: IValidator): IValidator {
        let originalParams: IValidator = {
            validatorId: EAsyncValidators.BIRTHDAY,
            params: {
                min: min,
                max: max,
            },
            responses: [
                ValidatorResponseBuilder.newBuilder()
                    .setResponseId(EBirtdayResponseRange.TOO_YOUNG)
                    .setMessage('Leeftijd is niet tussen de ' + min + ' en ' + max + ' jaar')
                    .build(),
                ValidatorResponseBuilder.newBuilder()
                    .setResponseId(EBirtdayResponseRange.CORRECT)
                    .setMessage('')
                    .build(),
                ValidatorResponseBuilder.newBuilder()
                    .setThrowId(FormErrors.getInstance().birthDateOld.id)
                    .setResponseId(EBirtdayResponseRange.TOO_OLD)
                    .setMessage('Leeftijd is niet tussen de ' + min + ' en ' + max + ' jaar')
                    .build(),
            ],
        };

        return this.updateParams(originalParams, params);
    }

    public postcodeCall(responses: IValidatorResponse[], params?: IValidator): IValidator {
        let originalParams: IValidator = {
            validatorId: EAsyncValidators.POSTAL_CODE,
            responses: responses,
        };

        return this.updateParams(originalParams, params);
    }

    public tozoCalls(params?: IValidator): IValidator {
        let originalParams: IValidator = {
            validatorId: EAsyncValidators.TOZO,
            params: {},
            responses: [
                ValidatorResponseBuilder.newBuilder()
                    .setResponseId(EBirtdayResponseRange.TOO_YOUNG)
                    .setMessage('Leeftijd is niet tussen de 0 en 27 jaar')
                    .build(),
            ],
        };
        return this.updateParams(originalParams, params);
    }

    /**
     * Gets called before returning an IValidator interface. Overrides the old params with the new ones if there are any set.
     * @param oldParams
     * @param newParams
     */
    private updateParams(oldParams: IValidator, newParams: IValidator): IValidator {
        if (newParams) {
            for (const key of Object.keys(newParams)) {
                oldParams[key] = newParams[key]
            }
        }
        return oldParams;
    }
}
