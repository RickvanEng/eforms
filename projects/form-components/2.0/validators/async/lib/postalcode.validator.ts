import {Injector} from '@angular/core';
import {AsyncValidatorFn, ValidationErrors} from '@angular/forms';
import {AsyncValidatorService} from '../../../5-fields/validators/validator/beValidations/async-validator.service';
import {EFormsAbstractControl} from '../../../base/eFormsAbstractControl';
import {IValidator, IValidatorResponse} from '../../IValidator';

export interface IPostalCodeValidatorReponse {
    statusCode: number;
    statusTekst: string;
    error: boolean;
}

export class PostalcodeValidator {
    static createValidator(inj: Injector, params: IValidator): AsyncValidatorFn {
        let asyncValidatorService: AsyncValidatorService = inj.get(AsyncValidatorService);

        return (control: EFormsAbstractControl): Promise<ValidationErrors> => {
            return new Promise<ValidationErrors>(resolve => {
                asyncValidatorService.validate(
                    'post',
                    'eforms-api/controller/bag/residency/postcode',
                    {postcode: control.value}).then((res: IPostalCodeValidatorReponse) => {
                        params.responses.forEach(response => {
                            control.deleteNotification(response.throwId);
                        });

                        let matchingResponse: IValidatorResponse = params.responses.find(response => response.error === res.error);
                        if (matchingResponse) {
                            if (matchingResponse.warning) {
                                control.addNotification(matchingResponse.throwId, matchingResponse);
                                resolve(null);
                            } else {
                                let errorObj: any = {
                                    custom: true,
                                    msg: res.statusTekst,
                                };
                                params.useBeMsg ? errorObj.msg = res.statusTekst : errorObj.msg = matchingResponse.msg;
                                resolve({[matchingResponse.throwId]: errorObj});
                            }
                        } else {
                            resolve(null);
                        }
                    },
                );
            });
        };
    }
}
