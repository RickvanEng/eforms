import {AsyncValidatorFn} from '@angular/forms';
import {IdValidatorPair} from '../idValidatorPair';
import {ValidatorIban} from '../../5-fields/validators/validator/beValidations/ValidatorIban';
import {Injector} from '@angular/core';
import {BirthdayValidator} from '../../5-fields/validators/validator/beValidations/birthday.validator';
import {PostalcodeValidator} from './lib/postalcode.validator';
import {IValidator} from '../IValidator';

/**
 * This map is used to construct the validator when the Form components are build.
 */

export enum EAsyncValidators {
	NONE = 'none',
	BIRTHDAY = 'birthday',
	IBAN = 'iban',
	TOZO = 'tozo',
	POSTAL_CODE = 'postalCcode',
}

export class AsyncValidatorMap {
	
	constructor(private inj: Injector) {
	
	}
	
	/**
	 * get the matching validator (id) from the map and instantiates it with the params
	 * @param params
	 */
	public getValidator(params: IValidator): AsyncValidatorFn {
		return this.map[params.validatorId](this.inj, params);
	}
	
	private map: IdValidatorPair = {
		[EAsyncValidators.IBAN]: ValidatorIban.createValidator,
		[EAsyncValidators.BIRTHDAY]: BirthdayValidator.createValidator,
		[EAsyncValidators.POSTAL_CODE]: PostalcodeValidator.createValidator,
	}
}
