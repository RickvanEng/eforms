import {IValidator} from "../../IValidator";
import {ValidatorFn} from "@angular/forms";
import {EFormsAbstractControl} from "../../../base/eFormsAbstractControl";

export class MaxLengthValidator {
    static createValidator(validator: IValidator): ValidatorFn {
        return function validate(control: EFormsAbstractControl) {
            validator.responses.forEach(response => {
                control.deleteNotification(response.throwId);
            });

            if (control.value) {
                if (validator.responses[0].warning) {
                    ~~control.value.toString().length > validator.params ? control.addNotification(validator.responses[0].throwId, validator.responses[0]) : null;
                    return null;
                } else {
                    return ~~control.value.toString().length > validator.params ? {maxlength: false} : null;
                }
            } else {
                return null;
            }
        };
    }
}
