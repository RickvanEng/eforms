import {AbstractControl, ValidatorFn} from "@angular/forms";

/**
 * This validator is for LabelsComponents that are blocking. When the label is active, is throws an error.
 */
export class LabelValidator {
    static createValidator(): ValidatorFn {
        return function validate(control: AbstractControl) {
            return {labelBlock: true};
        };
    }
}
