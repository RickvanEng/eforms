import {IValidator} from "../../IValidator";
import {ValidatorFn} from "@angular/forms";
import {EFormsAbstractControl} from "../../../base/eFormsAbstractControl";

export class PatternValidator {
    static createValidator(validator: IValidator): ValidatorFn {
        return function validate(control: EFormsAbstractControl) {
            validator.responses.forEach(response => {
                control.deleteNotification(response.throwId);
            });

            let reg = new RegExp(validator.params);

            if (validator.responses[0].warning) {
                reg.test(control.value) ? control.addNotification(validator.responses[0].throwId, validator.responses[0]) : null;
                return null;
            } else {
                return reg.test(control.value) ? null : {pattern: false};
            }
        };
    }
}
