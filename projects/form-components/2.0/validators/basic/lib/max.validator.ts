import {ValidatorFn} from '@angular/forms';
import {IValidator} from '../../IValidator';
import {EFormsAbstractControl} from '../../../base/eFormsAbstractControl';

export class MaxValidator {
    static createValidator(validator: IValidator): ValidatorFn {
        return function validate(control: EFormsAbstractControl) {
            validator.responses.forEach(response => {
                control.deleteNotification(response.throwId);
            });

            if (validator.responses[0].warning) {
                ~~control.value > validator.params ? control.addNotification(validator.responses[0].throwId, validator.responses[0]) : null;
                return null;
            } else {
                return ~~control.value > validator.params ? {max: false} : null;
            }

        };
    }
}
