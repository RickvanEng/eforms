import {AbstractControl, ValidatorFn} from '@angular/forms';
import {IValidator} from '../../IValidator';

/**
 * This validator can be used to check if a property is present on the control. This has to be used with
 * a custom piece of code inside the component that sets a property. I.E: The combinedFieldAction.
 */
export class IsPropPresentValidator {
	static createValidator(validator: IValidator): ValidatorFn {
		return function validate(control: AbstractControl) {
			return control[validator.params] ? null : {IsPropPresentValidator: false};
		};
	}
}
