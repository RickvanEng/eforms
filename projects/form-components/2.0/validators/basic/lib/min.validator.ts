import {IValidator} from "../../IValidator";
import {ValidatorFn} from "@angular/forms";
import {EFormsAbstractControl} from "../../../base/eFormsAbstractControl";

export class MinValidator {
    static createValidator(validator: IValidator): ValidatorFn {
        return function validate(control: EFormsAbstractControl) {
            validator.responses.forEach(response => {
                control.deleteNotification(response.throwId);
            });

            if (validator.responses[0].warning) {
                ~~control.value.toString().length < validator.params ? control.addNotification(validator.responses[0].throwId, validator.responses[0]) : null;
                return null;
            } else {
                return ~~control.value.toString().length < validator.params ? {max: false} : null;
            }

        };
    }
}
