import {ValidatorFn, Validators} from '@angular/forms';
import {CheckboxRequiredValidator} from '../../5-fields/form-field/multipleChoiceField/multiple-choice-checkbox/checkboxRequiredValidator';
import {HasBeenClickedValidator} from '../../4-set/combined-field-action/has-been-clicked.validator';
import {LeafetAddressValid} from '../../5-fields/form-field/leaflet/leaflet-addressValid.validator';
import {DigidPartnerloginValidator} from '../../_presets/digid/digid-partnerlogin.validator';
import {IdValidatorPair} from '../idValidatorPair';
import {IsPropPresentValidator} from './lib/isPropPresent.validator';
import {LabelValidator} from "./lib/label.validator";
import {IValidator} from '../IValidator';
import {MaxValidator} from './lib/max.validator';
import {MinValidator} from "./lib/min.validator";
import {MinLengthValidator} from "./lib/minLength.validator";
import {MaxLengthValidator} from "./lib/maxLength.validator";
import {PatternValidator} from "./lib/pattern.validator";

/**
 * This map is used to construct the validator when the Form components are build.
 */

export enum EBasicValidators {
	NONE = 'none',
	PATTERN = 'pattern',
	MAX_DATE = 'matDatepickerMax',
	MIN_DATE = 'matDatepickerMin',
	REQUIRED = 'required',
	REQUIRED_MULTIPLECHOICE = 'requiredMultipleChoice',
	COMBINED_FIELD_ACTION = 'combinedFieldActionValidator',
	LEAFLET_ADDRESS_VALID = 'LeafetAddressValid',
	MIN_LENGTH = 'minlength',
	MAX_LENGTH = 'maxlength',
	MIN = 'min',
	MAX = 'max',
	IS_PROP_PRESENT = 'IsPropPresentValidator',
	LABEL_VALIDATOR = 'LabelValidator',
}

export class BasicValidatorMap {
	
	/**
	 * get the matching validator (id) from the map and instantiates it with the params
	 * @param validator
	 */
	public getValidator(validator: IValidator): ValidatorFn {
		return validator.params ? this.map[validator.validatorId](validator) : this.map[validator.validatorId];
	}
	
	private map: IdValidatorPair = {
		[EBasicValidators.REQUIRED]: Validators.required,

		[EBasicValidators.PATTERN]: PatternValidator.createValidator,
		[EBasicValidators.MIN_LENGTH]: MinLengthValidator.createValidator,
		[EBasicValidators.MAX_LENGTH]: MaxLengthValidator.createValidator,
		[EBasicValidators.MIN]: MinValidator.createValidator,
		[EBasicValidators.MAX]: MaxValidator.createValidator,
		[EBasicValidators.REQUIRED_MULTIPLECHOICE]: CheckboxRequiredValidator,
		[EBasicValidators.IS_PROP_PRESENT]: IsPropPresentValidator.createValidator,
		[EBasicValidators.COMBINED_FIELD_ACTION]: HasBeenClickedValidator(),
		[EBasicValidators.LEAFLET_ADDRESS_VALID]: LeafetAddressValid(),
		[EBasicValidators.LABEL_VALIDATOR]: LabelValidator.createValidator(),
	}
}
