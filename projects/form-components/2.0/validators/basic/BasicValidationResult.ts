import {WarningType} from '../../5-fields/WarningType';

export class BasicValidationResult {
	
	get labelType(): WarningType {
		return this._labelType;
	}
	
	get message(): string {
		return this._message;
	}
	
	get show(): boolean {
		return this._show;
	}
	
	set msg(msg: string) {
		this._message = msg;
	}
	
	// should the error / warning be shown
	private _show: boolean;
	// the message to present
	private _message: string;
	// type of error / warning
	private _labelType: WarningType;
	
	constructor(
		message: string,
		show: boolean = true,
		labelType: WarningType = WarningType.DANGER,
	) {
		this._message = message;
		this._labelType = labelType;
		this._show = show;
	}
}
