import {EBasicValidators} from './basicValidatorMap';
import {IValidator} from '../IValidator';
import {DatePipe} from '@angular/common';
import { ValidatorResponseBuilder } from '../validatorResponse.builder';
import {FormErrors} from '../../_presets/formErrors';

/**
 * WARNING: The default angular Validators cannot be configured as warnings. They will al throw errors
 */

export class ValidatorPreBuildConfigs {
	
	private static instance: ValidatorPreBuildConfigs;
	
	public static getInstance(): ValidatorPreBuildConfigs {
		if (!ValidatorPreBuildConfigs.instance) {
			ValidatorPreBuildConfigs.instance = new ValidatorPreBuildConfigs();
		}
		return ValidatorPreBuildConfigs.instance;
	}
	
	private constructor() {
	}
	
	public required(msg?: string, params?: IValidator): IValidator {
		let originalParams: IValidator = {
			validatorId: EBasicValidators.REQUIRED,
			responses: [
				ValidatorResponseBuilder.newBuilder()
					.setMessage(msg)
					.build(),
			],
		};
		
		return this.updateParams(originalParams, params);
	}
	
	public pattern(pattern: string, msg: string, params?: IValidator): IValidator {
		let originalParams: IValidator = {
			validatorId: EBasicValidators.PATTERN,
			params: pattern,
			responses: [
				ValidatorResponseBuilder.newBuilder()
					.setThrowId(FormErrors.getInstance().pattern.id)
					.setMessage(msg)
					.build(),
			],
		};

		return this.updateParams(originalParams, params);
	}
	
	public email(params?: IValidator): IValidator {
		let originalParams: IValidator = {
			validatorId: EBasicValidators.PATTERN,
			params: '^.+@.+\\..+$',
			responses: [
				ValidatorResponseBuilder.newBuilder()
					.setMessage('Uw e-mailadres is onvolledig of onjuist ingevuld. Vul een geldig e-mailadres in.')
					.build(),
			],
		};

		return this.updateParams(originalParams, params);
	}
	
	public minLength(amount: number, params?: IValidator): IValidator {
		let originalParams: IValidator = {
			validatorId: EBasicValidators.MIN_LENGTH,
			params: amount,
			responses: [
				ValidatorResponseBuilder.newBuilder()
					.setMessage('U moet minimaal ' + amount + ' tekens invullen.')
					.setThrowId(FormErrors.getInstance().minLength.id)
					.build(),
			],
		};

		return this.updateParams(originalParams, params);
	}
	
	public maxLength(amount: number, params?: IValidator): IValidator {
		let originalParams: IValidator = {
			validatorId: EBasicValidators.MAX_LENGTH,
			params: amount,
			responses: [
				ValidatorResponseBuilder.newBuilder()
					.setMessage('U kunt maximaal ' + amount + ' tekens invullen.')
					.setThrowId(FormErrors.getInstance().maxLength.id)
					.build(),
			],
		};

		return this.updateParams(originalParams, params);
	}
	
	public min(amount: number, params?: IValidator): IValidator {
		let originalParams: IValidator = {
			validatorId: EBasicValidators.MIN,
			params: amount,
			responses: [
				ValidatorResponseBuilder.newBuilder()
					.setMessage('U moet minimaal ' + amount + ' invullen.')
					.setThrowId(FormErrors.getInstance().min.id)
					.build(),
			],
		};

		return this.updateParams(originalParams, params);
	}
	
	public max(amount: number, params?: IValidator): IValidator {
		let originalParams: IValidator = {
			validatorId: EBasicValidators.MAX,
			params: amount,
			responses: [
				ValidatorResponseBuilder.newBuilder()
					.setMessage('U kunt maximaal ' + amount + ' invullen.')
					.setThrowId(FormErrors.getInstance().max.id)
					.build(),
			],
		};

		return this.updateParams(originalParams, params);
	}

	public minDate(date: Date, params?: IValidator): IValidator {
		let originalParams: IValidator = {
			validatorId: EBasicValidators.MIN_DATE,
			params: date,
			responses: [
				ValidatorResponseBuilder.newBuilder()
					.setMessage('De gekozen datum moet liggen na: ' + new DatePipe('nl-NL').transform(date, 'dd-MM-yyyy'))
					.build(),
			],
		};

		return this.updateParams(originalParams, params);
	}
	
	public dyanmicMinDate(fieldToListenTo: string, params?: IValidator): IValidator {
		let originalParams: IValidator = {
			validatorId: EBasicValidators.MIN_DATE,
			responses: [
				ValidatorResponseBuilder.newBuilder()
					.setMessage('')
					.build(),
			],
		};

		return this.updateParams(originalParams, params);
	}
	
	public dyanmicMaxDate(fieldToListenTo: string, params?: IValidator): IValidator {
		let originalParams: IValidator = {
			validatorId: EBasicValidators.MAX_DATE,
			responses: [
				ValidatorResponseBuilder.newBuilder()
					.setMessage('')
					.build(),
			],
		};

		return this.updateParams(originalParams, params);
	}

	public maxDate(date: Date, params?: IValidator): IValidator {
		let originalParams: IValidator = {
			validatorId: EBasicValidators.MAX_DATE,
			params: date,
			responses: [
				ValidatorResponseBuilder.newBuilder()
					.setMessage('De gekozen datum moet liggen voor: ' + new DatePipe('nl-NL').transform(date, 'dd-MM-yyyy'))
					.build(),
			],
		};

		return this.updateParams(originalParams, params);
	}

	public combinedFieldAction(msg?: 'U moet een locatie kiezen.', params?: IValidator): IValidator {
		let originalParams: IValidator = {
			validatorId: EBasicValidators.COMBINED_FIELD_ACTION,
			responses: [
				ValidatorResponseBuilder.newBuilder()
					.setMessage(msg)
					.build(),
			],
		};

		return this.updateParams(originalParams, params);
	}
	
	public leafletAddressValid(params?: IValidator): IValidator {
		let originalParams: IValidator = {
			validatorId: EBasicValidators.LEAFLET_ADDRESS_VALID,
			responses: [
				ValidatorResponseBuilder.newBuilder()
					.setMessage('leafletAddressValid error message')
					.build(),
			],
		};

		return this.updateParams(originalParams, params);
	}
	
	public isPropPresent(prop: string, params?: IValidator): IValidator {
		let originalParams: IValidator = {
			validatorId: EBasicValidators.IS_PROP_PRESENT,
			params: prop,
			responses: [
				ValidatorResponseBuilder.newBuilder()
					.setMessage('Generic isPropPresent error.')
					.build(),
			],
		};

		return this.updateParams(originalParams, params);
	}

	public labelValidator(params?: IValidator): IValidator {
		let originalParams: IValidator = {
			validatorId: EBasicValidators.LABEL_VALIDATOR,
			responses: [
				ValidatorResponseBuilder.newBuilder()
					.setMessage('Generic labelValidator error.')
					.build(),
			],
		};

		return this.updateParams(originalParams, params);
	}

	/**
	 * overrides the old params with the new ones if there are any set.
	 * @param oldParams
	 * @param newParams
	 */
	private updateParams(oldParams: IValidator, newParams: IValidator): IValidator {
		if (newParams) {
			for (const key of Object.keys(newParams)) {
				oldParams[key] = newParams[key]
			}
		}
		return oldParams;
	}
}
