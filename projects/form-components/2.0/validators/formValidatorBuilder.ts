import { Injector } from '@angular/core';
import { IValidator, IValidatorResponse } from './IValidator';

export class FormValidatorConfigBuilder {
    protected _injector: Injector;

    private _config: IValidator = {
        validatorId: undefined,
        params: undefined,
        responses: [],
        useBeMsg: false,
    }

    public static newBuilder(injector: Injector): FormValidatorConfigBuilder {
        return new FormValidatorConfigBuilder(injector);
    }

    constructor(injector: Injector) {
        this._injector = injector;
    }

    public setValidator(validator: string): FormValidatorConfigBuilder {
        this._config.validatorId = validator;
        return this;
    }

    public setParams(params: any): FormValidatorConfigBuilder {
        this._config.params = params;
        return this;
    }

    public setResponse(response: IValidatorResponse): FormValidatorConfigBuilder {
        this._config.responses.push(response);
        return this;
    }

    public setUseBeMsg(value: boolean): FormValidatorConfigBuilder {
        this._config.useBeMsg = value;
        return this;
    }

    public build(): IValidator {
        return this._config;
    }
}