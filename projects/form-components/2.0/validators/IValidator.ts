import {WarningType} from '../5-fields/WarningType';

/**
 * The different types of responses depending on the feedback of the call. Must be configured specificly for each call.
 * For an example see: AsyncValidatorPrebuildConfig.ts
 */
export interface IValidatorResponse {
	error?: boolean;
	responseCode?: number;
	throwId?: string;
	warning?: boolean;
	msg?: string;
	warningType?: WarningType;
	show?: boolean;
}

/**
 * Interface the will be used to build the FormValidator.ts
 */
export interface IValidator {
	validatorId?: string;
	params?: any;
	responses?: IValidatorResponse[];

	useBeMsg?: boolean;

	fieldToListenTo?: string;
}
