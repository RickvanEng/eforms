// import { BasicValidationResult } from './basic/BasicValidationResult';
// import { IValidator, IValidatorResponse } from './IValidator';
//
// export class FormValidator {
//
//     get useBeMsg(): boolean {
//         return this._config.useBeMsg;
//     }
//
//     get fieldToListenTo(): string {
//         return this._config.fieldToListenTo;
//     }
//
//     get validatorId(): string {
//         return this._config.validatorId;
//     }
//
//     get params(): any {
//         return this._config.params;
//     }
//
//     public getResponses(): IValidatorResponse[] {
//         return this._config.responses;
//     }
//
//     /**
//      * Used if the params need to be changed runtime
//      * @param value
//      */
//     set params(value: any) {
//         this._config.params = value;
//     }
//
//     /**
//      * Used if the msg need to be changed runtime
//      * @param msg
//      */
//     set msg(msg: string) {
//         this._validationResult.msg = msg;
//     }
//
//     protected _validationResult: BasicValidationResult;
//     protected _config: IValidator;
//
//     constructor(config: IValidator) {
//         this._config = config;
//     }
//
//     /**
//      * This is a utility function used to get the config in an JSON export.
//      */
//     public getConfig(): IValidator {
//         return this._config;
//     }
// }
