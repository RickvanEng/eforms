import {CmsComponent} from './cmsComponent';
import {Injector} from '@angular/core';
import {EFormsAbstractControl, EFormsFormGroup} from './base/eFormsAbstractControl';
import {IValidator} from './validators/IValidator';
import {AsyncValidatorFn, ValidatorFn} from '@angular/forms';
import {BasicValidatorMap, EBasicValidators} from './validators/basic/basicValidatorMap';
import {AsyncValidatorMap} from './validators/async/asyncValidatorMap';
import {ICmsAbstractControl} from './ICmsAbstractControl';

export class CmsAbstractControl extends CmsComponent {

    get abstractControl(): EFormsAbstractControl {
        return this._abstractControl;
    }

    protected _abstractControl: EFormsAbstractControl;
    _config: ICmsAbstractControl;

    constructor(config: ICmsAbstractControl) {
        super(config);
    }

    /**
     * Construct the FormGroup when the FormComponent is loaded. The injector is needed to build custom validators
     * @param inj
     */
    public constructFormControl(inj: Injector): EFormsAbstractControl {
        if (this._abstractControl) {
            return this._abstractControl;
        }

        let control: EFormsAbstractControl = this.getAbstractControl(inj);

        this._abstractControl = control;
        return control;
    }

    /**
     * Can be overridden when extented. Return correct type of AbstractControl for that component
     * @param inj
     */
    protected getAbstractControl(inj: Injector): EFormsAbstractControl {
        let control: EFormsFormGroup = new EFormsFormGroup({}, {
            // validators: this.getFormValidators(this._validators),
            // asyncValidators: this.getAsyncValidators(this._injector, this._asyncValidators)
        });

        return control;
    }

    /**
     * Loads in the config and gets the corresponding BASIC validators with the params
     * @param configs
     */
    protected getFormValidators(configs: IValidator[]): ValidatorFn[] {
        let validatorMap: BasicValidatorMap = new BasicValidatorMap();

        let result: ValidatorFn[] = [];
        configs.forEach(validatorConfig => {
            // TODO die min date etc moet eruit. Moet generieker.
            if (validatorConfig.validatorId !== EBasicValidators.MIN_DATE && validatorConfig.validatorId !== EBasicValidators.MAX_DATE) {
                result.push(validatorMap.getValidator(validatorConfig));
            }
        });

        return result;
    }

    /**
     * Loads in the config and gets the corresponding ASYNC validators with the params
     * @param inj
     * @param configs
     */
    protected getAsyncValidators(inj: Injector, configs: IValidator[]): AsyncValidatorFn[] {
        let validatorMap: AsyncValidatorMap = new AsyncValidatorMap(inj);

        let result: AsyncValidatorFn[] = [];
        configs.forEach(validatorConfig => {
            let validator: AsyncValidatorFn | null = validatorMap.getValidator(validatorConfig);
            if (validator) {
                result.push(validator);
            }
        });

        return result;
    }

    public validate(): boolean {
        this.markAsTouched();
        return this._abstractControl.valid;
    }

    /**
     * Marks the abstractControl and the control touched. Used to show validations.
     */
    markAsTouched(): void {
        this._abstractControl.markAsTouched();
    }
}
