export class ThrowableError {
	get id(): string {
		return this._id;
	}
	
	private _id: string;
	
	constructor(id: string) {
		this._id = id;
	}
}
