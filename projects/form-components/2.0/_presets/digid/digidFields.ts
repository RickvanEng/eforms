import {FormFieldBuilder} from '../../5-fields/form-field/FormField';
import {GenericFields} from '../../5-fields/presets/genericFields';
import {Injector} from '@angular/core';
import {DatePickerFieldFactory} from "../../5-fields/form-field/date-picker2/DatePickerField";
import {DatePicker2Component} from "../../5-fields/form-field/date-picker2/date-picker2.component";
import {
	FormFieldOption,
	FormFieldOptionFactory
} from "../../5-fields/form-field/multipleChoiceField/options/FieldOption";
import {FormFieldWithOptionsFactory} from "../../5-fields/form-field/multipleChoiceField/MultipleChoiceField";
import {ButtonBuilder} from '../../5-fields/form-field/button/button';
import {ButtonComponent} from '../../5-fields/form-field/button/button.component';
import {environment} from '../../../../../src/environments/environment';
import {FormErrors} from '../formErrors';

export class DigidFields {
	
	private static instance: DigidFields;
	
	public static getInstance(injector: Injector): DigidFields {
		if (!DigidFields.instance) {
			DigidFields.instance = new DigidFields(injector);
		}
		return DigidFields.instance;
	}
	
	constructor(private injector: Injector) {
	}
	
	private genericFields: GenericFields = new GenericFields(this.injector);
	
	public get digidBSN(): FormFieldBuilder {
		return this.genericFields.bsn
			.clearValidators()
			.setID('bsnNummer')
			.setReadonly(true)
			.setSaveToState(false)
			.setSendToBE(false) as FormFieldBuilder;
	}
	
	public get digidVoorletters(): FormFieldBuilder {
		return this.genericFields.voorletters
			.clearValidators()
			.setID('voorletters')
			.setReadonly(true)
			.setSaveToState(false)
			.setSendToBE(false) as FormFieldBuilder;
	}
	
	public get digidVoorvoegsel(): FormFieldBuilder {
		return this.genericFields.voorvoegsel
			.clearValidators()
			.setID('voorvoegselGeslachtsnaam')
			.setReadonly(true)
			.setSaveToState(false)
			.setSendToBE(false) as FormFieldBuilder;
	}
	
	public get digidGeslachtsnaam(): FormFieldBuilder {
		return this.genericFields.geslachtsnaam
			.clearValidators()
			.setID('geslachtsnaam')
			.setReadonly(true)
			.setSaveToState(false)
			.setSendToBE(false) as FormFieldBuilder;
	}
	
	public get digidGeboortedatum(): DatePickerFieldFactory {
		return DatePickerFieldFactory.newFactory(this.injector)
			.clearValidators()
			.setComponent(DatePicker2Component)
			.setID('geboortedatum')
			.setLabel('Geboortedatum')
			.setReadonly(true)
			.setSaveToState(false)
			.setSendToBE(false) as DatePickerFieldFactory;
	}
	
	public get digidStraat(): FormFieldBuilder {
		return this.genericFields.straat
			.clearValidators()
			.setID('straatnaam')
			.setReadonly(true)
			.setSaveToState(false)
			.setSendToBE(false) as FormFieldBuilder;
	}
	
	public get digidHuisnummer(): FormFieldBuilder {
		return this.genericFields.huisnummer
			.clearValidators()
			.setID('huisnummer')
			.setReadonly(true)
			.setSaveToState(false)
			.setSendToBE(false) as FormFieldBuilder;
	}
	
	public get digidHuisnummerletter(): FormFieldBuilder {
		return this.genericFields.huisnummerletter
			.clearValidators()
			.setID('huisletter')
			.setReadonly(true)
			.setSaveToState(false)
			.setSendToBE(false) as FormFieldBuilder;
	}
	
	public get digidHuisnummertoevoeging(): FormFieldBuilder {
		return this.genericFields.huisnummertoevoeging
			.clearValidators()
			.setID('huisnummertoevoeging')
			.setReadonly(true)
			.setSaveToState(false)
			.setSendToBE(false) as FormFieldBuilder;
	}
	
	public get digidWoonplaats(): FormFieldBuilder {
		return this.genericFields.plaats
			.clearValidators()
			.setID('woonplaatsnaam')
			.setReadonly(true)
			.setSaveToState(false)
			.setSendToBE(false) as FormFieldBuilder;
	}
	
	public get digidPostcode(): FormFieldBuilder {
		return this.genericFields.postcode
			.clearValidators()
			.setID('postcode')
			.setReadonly(true)
			.setSaveToState(false)
			.setSendToBE(false) as FormFieldBuilder;
	}
	
	public get digidTelefoon(): FormFieldBuilder {
		return this.genericFields.telefoon
			.setValidatorRequired() as FormFieldBuilder;
	}
	
	public get digidEmail(): FormFieldBuilder {
		return this.genericFields.email
			.setValidatorRequired();
	}
	
	/** Fields for gezinsituatie */
	
	public alleenstaand: FormFieldOption = FormFieldOptionFactory.newFactory()
		.setId('alleenstaand-id')
		.setData({value: 'Alleenstaand'})
		.build();
	public getrouwd: FormFieldOption = FormFieldOptionFactory.newFactory()
		.setId('getrouwdSamenwonend-id')
		.setData({value: 'Getrouwd of samenwonend'})
		.build();
	public alleenstaandMetKids: FormFieldOption = FormFieldOptionFactory.newFactory()
		.setId('alleenstaandeThuiswonende-id')
		.setData({value: 'Alleenstaande ouder met thuiswonende kinderen jonger dan 18 jaar'})
		.build();
	public gezinSituatie: FormFieldWithOptionsFactory = this.genericFields.radioField
		.setOptions([
			this.alleenstaand,
			this.getrouwd,
			this.alleenstaandMetKids
		])
		.setID('gezinSituatie')
		.setLabel('Bent u?')
		.setValidatorRequired() as FormFieldWithOptionsFactory;

	
	/** Fields for the partner */
	
	public samenPartner_ja: FormFieldOption = FormFieldOptionFactory.newFactory()
		.setId('samenPartner-ja')
		.setData({value: 'Ja'})
		.build();
	public samenPartner_nee: FormFieldOption = FormFieldOptionFactory.newFactory()
		.setId('samenPartner-nee')
		.setData({value: 'Nee'})
		.build();
	public samenPartner: FormFieldWithOptionsFactory = this.genericFields.radioField
		.setOptions([
			this.samenPartner_ja,
			this.samenPartner_nee,
		])
		.setID('samenPartner')
		.setLabel('Heeft u een partner?')
		.setValidatorRequired()
		.setShowQuestionMark(true)
		.setQuestionMarkTitle('Partner')
		.setQuestionMarkText('<dl><dt><strong>U heeft een partner als u:</strong></dt>' +
			'<dd>- getrouwd bent of geregistreerd partners bent.</dd>' +
			'<dt><strong>U heeft ook een partner als u op hetzelfde adres woont en:</strong></dt>' +
			'<dd>- samen op hetzelfde adres ingeschreven staat.</dd>' +
			'<dd>- ex-echtgenoten of ex-partners bent.</dd>' +
			'<dd>- samen een kind hebt.</dd>' +
			'<dd>- u het kind van uw partner hebt erkend.</dd>' +
			'<dd>- uw partner uw kind heeft erkend.</dd>' +
			'<dd>- u ergens anders als partners geregistreerd staat, bijvoorbeeld bij de Belastingdienst of als meeverzekerde bij een zorgverzekering.</dd></dl>') as FormFieldWithOptionsFactory;
	
	private parnterLogin(): void {
		window.location.href = environment.baseUrl + 'adapter/login/secondary';
	}
	public partnerLoginButton: ButtonBuilder = ButtonBuilder.newBuilder(this.injector)
		.setImage({path: 'assets/digid-logo.svg'})
		.setAction(this.parnterLogin)
		.setButtonText('Inloggen met DigiD partner')
		.setComponent(ButtonComponent)
		.setID('partnerLoginButton')
		.setSendToBE(false) as ButtonBuilder;
	
	private _digidPartnerBSN: FormFieldBuilder;
	public get digidPartnerBSN(): FormFieldBuilder {
		if (!this._digidPartnerBSN) {
			this._digidPartnerBSN = this.genericFields.bsn
				.clearValidators()
				.setID('bsn-partner')
				.setReadonly(true)
				.setEnkelIntern(true)
				.setSaveToState(false)
				.setSendToBE(false) as FormFieldBuilder;
		}
		return this._digidPartnerBSN;
	}
	
	private _digidPartnerVoorletters: FormFieldBuilder;
	public get digidPartnerVoorletters(): FormFieldBuilder {
		if (!this._digidPartnerVoorletters) {
			this._digidPartnerVoorletters = this.genericFields.voorletters
				.clearValidators()
				.setID('voorletters-partner')
				.setSaveToState(false)
				.setReadonly(true)
				.setSendToBE(false) as FormFieldBuilder;
		}
		return this._digidPartnerVoorletters;
	}
	
	private _digidPartnerVoorvoegsel: FormFieldBuilder;
	public get digidPartnerVoorvoegsel(): FormFieldBuilder {
		if (!this._digidPartnerVoorvoegsel) {
			this._digidPartnerVoorvoegsel = this.genericFields.voorvoegsel
				.clearValidators()
				.setID('voorvoegselGeslachtsnaam-partner')
				.setSaveToState(false)
				.setReadonly(true)
				.setSendToBE(false) as FormFieldBuilder;
		}
		return this._digidPartnerVoorvoegsel;
	}
	
	private _digidPartnerGeslachtsnaam: FormFieldBuilder;
	public get digidPartnerGeslachtsnaam(): FormFieldBuilder {
		if (!this._digidPartnerGeslachtsnaam) {
			this._digidPartnerGeslachtsnaam = this.genericFields.geslachtsnaam
				.clearValidators()
				.setID('geslachtsnaam-partner')
				.setReadonly(true)
				.setSaveToState(false)
				.setSendToBE(false) as FormFieldBuilder;
		}
		return this._digidPartnerGeslachtsnaam;
	}
	
	private _digidPartnerGeboortedatum: DatePickerFieldFactory;
	public get digidPartnerGeboortedatum(): DatePickerFieldFactory {
		if (!this._digidPartnerGeboortedatum) {
			this._digidPartnerGeboortedatum = DatePickerFieldFactory.newFactory(this.injector)
				.clearValidators()
				.setComponent(DatePicker2Component)
				.setID('geboortedatum-partner')
				.setLabel('Geboortedatum')
				.setReadonly(true)
				.setSaveToState(false)
				.setSendToBE(false) as DatePickerFieldFactory;
		}
		return this._digidPartnerGeboortedatum;
	}
	
	private _digidPartnerStraat: FormFieldBuilder;
	public get digidPartnerStraat(): FormFieldBuilder {
		if (!this._digidPartnerStraat) {
			this._digidPartnerStraat = this.genericFields.straat
				.clearValidators()
				.setID('verblijfstraat-partner')
				.setReadonly(true)
				.setSaveToState(false)
				.setSendToBE(false) as FormFieldBuilder;
		}
		return this._digidPartnerStraat;
	}
	
	private _digidPartnerHuisnummer: FormFieldBuilder;
	public get digidPartnerHuisnummer(): FormFieldBuilder {
		if (!this._digidPartnerHuisnummer) {
			this._digidPartnerHuisnummer = this.genericFields.huisnummer
				.clearValidators()
				.setID('verblijfhuisnummer-partner')
				.setReadonly(true)
				.setSaveToState(false)
				.setSendToBE(false) as FormFieldBuilder;
		}
		return this._digidPartnerHuisnummer;
	}
	
	private _digidPartnerHuisnummerletter: FormFieldBuilder;
	public get digidPartnerHuisnummerletter(): FormFieldBuilder {
		if (!this._digidPartnerHuisnummerletter) {
			this._digidPartnerHuisnummerletter = this.genericFields.huisnummerletter
				.clearValidators()
				.setID('verblijfhuisnummerletter-partner')
				.setReadonly(true)
				.setSaveToState(false)
				.setSendToBE(false) as FormFieldBuilder;
		}
		return this._digidPartnerHuisnummerletter;
	}
	
	private _digidPartnerHuisnummertoevoeging: FormFieldBuilder;
	public get digidPartnerHuisnummertoevoeging(): FormFieldBuilder {
		if (!this._digidPartnerHuisnummertoevoeging) {
			this._digidPartnerHuisnummertoevoeging = this.genericFields.huisnummertoevoeging
				.clearValidators()
				.setID('verblijfhuisnummertoevoeging-partner')
				.setReadonly(true)
				.setSaveToState(false)
				.setSendToBE(false) as FormFieldBuilder;
		}
		return this._digidPartnerHuisnummertoevoeging;
	}
	
	private _digidPartnerWoonplaats: FormFieldBuilder;
	public get digidPartnerWoonplaats(): FormFieldBuilder {
		if (!this._digidPartnerWoonplaats) {
			this._digidPartnerWoonplaats = this.genericFields.plaats
				.clearValidators()
				.setID('verblijfwoonplaats-partner')
				.setReadonly(true)
				.setSaveToState(false)
				.setSendToBE(false) as FormFieldBuilder;
		}
		return this._digidPartnerWoonplaats;
	}
	
	private _digidPartnerPostcode: FormFieldBuilder;
	public get digidPartnerPostcode(): FormFieldBuilder {
		if (!this._digidPartnerPostcode) {
			this._digidPartnerPostcode = this.genericFields.postcode
				.clearValidators()
				.setID('verblijfpostcode-partner')
				.setSaveToState(false)
				.setReadonly(true)
				.setSendToBE(false) as FormFieldBuilder;
		}
		return this._digidPartnerPostcode;
	}
	
	/** Fields for the digidReadOnlySet */
	
	private _digidReadOnlyTelefoon: FormFieldBuilder;
	public get digidReadOnlyTelefoon(): FormFieldBuilder {
		if (!this._digidReadOnlyTelefoon) {
			this._digidReadOnlyTelefoon = this.genericFields.telefoon
				.setReadonly(true);
		}
		return this._digidReadOnlyTelefoon;
	}
	
	private _digidReadOnlyEmail: FormFieldBuilder;
	public get digidReadOnlyEmail(): FormFieldBuilder {
		if (!this._digidReadOnlyEmail) {
			this._digidReadOnlyEmail = this.genericFields.email
				.setReadonly(true);
		}
		return this._digidReadOnlyEmail;
	}

}
