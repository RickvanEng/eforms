import {FormSet2, FormSetBuilder} from '../../4-set/FormSet';
import {IDigidInterface} from '../../../../forms/services/prefill/IDigiD';
import {Injector} from '@angular/core';
import {FormField} from '../../5-fields/form-field/FormField';
import {Digid2Service} from './digid2.service';

export class DigidSet2Builder extends FormSetBuilder {
	
	protected _buildingClass: any = DigidSet2;
	
	public static newBuilder(injector: Injector): DigidSet2Builder {
		return new DigidSet2Builder(injector);
	}
}

export class DigidSet2 extends FormSet2 {
	
	// Prefill is here by ID. in this case I dont need a ref to the field
	private personArray: string[] = [
		'bsnNummer',
		'voorletters',
		'voorvoegselGeslachtsnaam',
		'geslachtsnaam',
		'geboortedatum',
		
		'verblijfstraat',
		'verblijfhuisnummer',
		'verblijfhuisnummerletter',
		'verblijfhuisnummertoevoeging',
		'verblijfwoonplaats',
		'verblijfpostcode',
	];
	
	private addressArray: string[] = [
		'straatnaam',
		'huisnummer',
		'huisletter',
		'huisnummertoevoeging',
		'woonplaatsnaam',
		'postcode',
	];
	
	constructor(builder: FormSetBuilder) {
		super(builder);
	}
	
	init(): Promise<any> {
		return new Promise<any>(resolve => {
			super.init().then(() => {
				this._injector.get(Digid2Service).tokenData.subscribe(value => {
					if (value) {
						this.preFillDigid(this._children as FormField[], value.identities.find(obj => obj.identityOrder === 0).data as IDigidInterface);
					}
					
					resolve(true);
				});
			});
		})
	}
	
	public preFillDigid(fields: FormField[], data: IDigidInterface): void {
		this.personArray.forEach(entry => {
			let field: FormField = fields.find(field => field.id === entry);
			if (field) {
				field.prefillValue(data[entry]);
			}
		});
		
		this.addressArray.forEach(entry => {
			let field: FormField = fields.find(field => field.id === entry);
			if (field) {
				field.prefillValue(data.verblijfplaatsAdres[entry]);
			}
		});
	}
}
