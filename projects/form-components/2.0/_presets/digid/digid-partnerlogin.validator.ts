import {AbstractControl, AsyncValidatorFn, ValidationErrors} from '@angular/forms';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {Injector} from '@angular/core';
import {Digid2Service} from './digid2.service';

export class DigidPartnerloginValidator {
    static createValidator(inj: Injector): AsyncValidatorFn {
        return (control: AbstractControl): Observable<ValidationErrors> => {
            return inj.get(Digid2Service).tokenData.pipe(
                map((res: any) => res.identities.find(obj => obj.identityOrder === 1) ? {partnerError: true} : {partnerError: true})
            )
        };
    }
}
