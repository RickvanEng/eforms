import {BehaviorSubject, Observable} from 'rxjs';
import {environment} from '../../../../../src/environments/environment';
import {Injectable, Injector} from '@angular/core';
import {Auth2Service, TokenObject} from '../../services/auth2.service';
import {HttpClient} from '@angular/common/http';

@Injectable({
    providedIn: 'root'
})
export class Digid2Service {
    get tokenData(): BehaviorSubject<any> {
        return this._tokenData;
    }

    private tokenDataURl: string = environment.baseUrl + 'adapter/session/token/';
    private _tokenData: BehaviorSubject<any> = new BehaviorSubject(undefined);

    constructor(private injector: Injector) {
        this.injector.get(Auth2Service).loginToken.subscribe((token: TokenObject) => {
            this._tokenData.next({
                "identities": [{
                    "data": {
                        "voornamen": "Henk de Tank Jr",
                        "bsnNummer": "900174316",
                        "voorletters": "H.D.T.J.",
                        "voorvoegselGeslachtsnaam": "van der",
                        "geslachtsnaam": "Tester",
                        "geboortedatum": "1903-01-17",
                        "geboorteplaats": "Haarlemmermeer",
                        "omschrijvingGeboorteland": "Nederland",
                        "geslachtsaanduiding": "Man",
                        "datumInschrijvingGemeente": "20070102",
                        "correspondentieAdres": null,
                        "inschrijvingAdres": null,
                        "verblijfplaatsAdres": {
                            "adresBuitenland1": null,
                            "adresBuitenland2": null,
                            "adresBuitenland3": null,
                            "postcode": "2132TZ",
                            "woonplaatsnaam": "Hoofddorp",
                            "straatnaam": "Raadhuisplein",
                            "huisnummer": "1",
                            "huisletter": "A",
                            "huisnummertoevoeging": "Testerde test",
                            "aanduidingBijHuisnummer": null,
                            "locatieomschrijving": null,
                            "locatieadresnummer": "0000007191",
                            "ingangsdatum": null,
                            "einddatum": null,
                            "straatcode": "7191"
                        },
                        "nummerAanduiding": null,
                        "geboortedatumOnbekend": false
                    }, "registerType": "BRP", "identityOrder": 0
                }],
                "registerType": "NONE",
                "ldapUserPrincipal": null,
                "expirationDate": "2021-10-02T22:05:47.190+02:00",
                "ttl": 1633205147190,
                "error": null
            });
        })
    }

    public getDigidToken(): string {
        console.log(JSON.parse(localStorage.getItem('token')).value.value);
        return JSON.parse(localStorage.getItem('token')).value.value;
    }

    private getTokenData(token: string): Observable<any> {
        return this.injector.get(HttpClient).get(this.tokenDataURl + token);
    }
}
