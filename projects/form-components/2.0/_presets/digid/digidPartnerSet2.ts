import {FormSet2, FormSetBuilder} from "../../4-set/FormSet";
import {Injector} from "@angular/core";
import {Digid2Service} from "./digid2.service";
import {FormField} from "../../5-fields/form-field/FormField";
import {FormFieldOption} from "../../5-fields/form-field/multipleChoiceField/options/FieldOption";
import {BehaviorSubject} from "rxjs";
import {
	FormFieldWithOptions,
	FormFieldWithOptionsFactory
} from '../../5-fields/form-field/multipleChoiceField/MultipleChoiceField';
import {DigidFields} from './digidFields';
import {BasicValidationResult} from '../../validators/basic/BasicValidationResult';
import {IDigidInterface} from '../../../../forms/services/prefill/IDigiD';

export class DigidPartnerSet2Builder extends FormSetBuilder {
	get partnerField(): { field: FormFieldWithOptions, option: FormFieldOption } {
		return {field: this._partnerField.field.build(), option: this._partnerField.option};
	}
	
	protected _buildingClass: any = DigidPartnerSet2;
	private _partnerField: { field: FormFieldWithOptionsFactory, option: FormFieldOption };
	
	public static newBuilder(injector: Injector): DigidPartnerSet2Builder {
		return new DigidPartnerSet2Builder(injector);
	}
	
	public setPartnerField(config: { field: FormFieldWithOptionsFactory, option: FormFieldOption }): DigidPartnerSet2Builder {
		this._partnerField = config;
		return this;
	}
}

export class DigidPartnerSet2 extends FormSet2 {
	
	private partnerData: BehaviorSubject<any> = new BehaviorSubject(undefined);

	// private _partnerField: { field: FormFieldWithOptions, option: FormFieldOption };
	private digidFields: DigidFields = DigidFields.getInstance(this._injector);

	private partnerNoLoginError: BasicValidationResult;
	
	constructor(builder: DigidPartnerSet2Builder) {
		super(builder);
		// this._partnerField = builder.partnerField;

		this.partnerNoLoginError = new BasicValidationResult(
			// FormErrors.getInstance().partnerLoginError,
			'je moet inloggen',
			// true,
			// WarningType.DANGER,
			// false
		)
	}
	
	init(): Promise<any> {
		return new Promise<any>(resolve => {
			super.init().then(() => {

				// TODO zet als preinit
				this._injector.get(Digid2Service).tokenData.subscribe(value => {
					if (value) {
						let partnerData: any = value.identities.find(obj => obj.identityOrder === 1);
						if (partnerData) {
							this.partnerData.next(partnerData);
							this.preFillDigid(this._children as FormField[], partnerData.data as IDigidInterface);

							//trigger the subscription below
							// this._partnerField.field.trigger.next(true);
						}
					}
				});

				// this._partnerField.field.abstractControl.statusChanges.subscribe(() => {
				// 	this.checkForGegevensShow(this._partnerField.field.isOptionChecked(this._partnerField.option.id))
				// });

				resolve(true);
			});
		});
	}
	
	private checkForGegevensShow(optionSelected: boolean): void {
		console.log('Button click');
		console.log(optionSelected);
		// console.log((optionSelected && this.partnerData.getValue()));
		this.setShow((optionSelected));

		// if (optionSelected && !this.partnerData.getValue()) {
		// 	this.addError(this.partnerNoLoginError);
		// } else {
		// 	this.removeError(this.partnerNoLoginError);
		// }
	}
	
	public preFillDigid(fields: FormField[], data: IDigidInterface): void {
		this.digidFields.digidPartnerBSN.build().prefillValue(data.bsnNummer.toString());
		this.digidFields.digidPartnerVoorletters.build().prefillValue(data.voorletters);
		this.digidFields.digidPartnerVoorvoegsel.build().prefillValue(data.voorvoegselGeslachtsnaam);
		this.digidFields.digidPartnerGeslachtsnaam.build().prefillValue(data.geslachtsnaam);
		this.digidFields.digidPartnerGeboortedatum.build().prefillValue(data.geboortedatum);
		this.digidFields.digidPartnerStraat.build().prefillValue(data.verblijfplaatsAdres.straatnaam);
		this.digidFields.digidPartnerHuisnummer.build().prefillValue(data.verblijfplaatsAdres.huisnummer);
		this.digidFields.digidPartnerHuisnummerletter.build().prefillValue(data.verblijfplaatsAdres.huisletter);
		this.digidFields.digidPartnerHuisnummertoevoeging.build().prefillValue(data.verblijfplaatsAdres.huisnummertoevoeging);
		this.digidFields.digidPartnerWoonplaats.build().prefillValue(data.verblijfplaatsAdres.woonplaatsnaam);
		this.digidFields.digidPartnerPostcode.build().prefillValue(data.verblijfplaatsAdres.postcode);
	}
}
