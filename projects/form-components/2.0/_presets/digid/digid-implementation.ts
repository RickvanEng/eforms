import {Injector} from '@angular/core';
import {DigidFields} from './digidFields';
import {DigidSet2Builder} from './digidSet2';
import {SetComponent} from '../../4-set/set.component';
import {FormTabBuilder} from '../../2-tab/FormTab2';
import {GenericTabFieldSets} from '../../3-tab-field-set/presets/genericTabFieldSets';
import {DigidPartnerSet2Builder} from "./digidPartnerSet2";
import {FormConfigBuilder} from '../../0-routing/formConfig';
import {ELoginTypes2} from '../../../../forms/src/formImplementations/IForm';
import {GenericTiles} from '../../0-routing/form-routing/login-portal/login-tile/presets/generic-tiles';
import {DigidPartnerInlogSetBuilder} from './digidPartnerInlogSet';
import {ShowCondition} from '../../showconditions/showCondition';

export class DigidImplementation {
	
	private static instance: DigidImplementation;
	
	public static getInstance(injector: Injector): DigidImplementation {
		if (!DigidImplementation.instance) {
			DigidImplementation.instance = new DigidImplementation(injector);
		}
		return DigidImplementation.instance;
	}
	
	constructor(private injector: Injector) {
	}
	
	private genericFieldSets: GenericTabFieldSets = new GenericTabFieldSets(this.injector);
	private digidFields: DigidFields = DigidFields.getInstance(this.injector);
	
	private _digidPartnerInlogSet: DigidPartnerInlogSetBuilder = DigidPartnerInlogSetBuilder.newBuilder(this.injector)
		.setPartnerField({field: this.digidFields.samenPartner, option: this.digidFields.samenPartner_ja})
		.setButton(this.digidFields.partnerLoginButton)
		.setChildren([
			this.digidFields.samenPartner,
			this.digidFields.partnerLoginButton
				.setShowConditions({
					all: [],
					single: [new ShowCondition(this.digidFields.samenPartner, this.digidFields.samenPartner_ja)],
				}),
		])
		.setComponent(SetComponent)
		.setID('digidGezinSituatieInlogSet') as DigidPartnerInlogSetBuilder;
	
	public get digidPartnerInlogSet(): DigidPartnerInlogSetBuilder {
		return this._digidPartnerInlogSet;
	}
	
	public get digidMainSet(): DigidSet2Builder {
		return DigidSet2Builder.newBuilder(this.injector)
			.setChildren([
				this.digidFields.digidBSN,
				this.digidFields.digidVoorletters,
				this.digidFields.digidVoorvoegsel,
				this.digidFields.digidGeslachtsnaam,
				this.digidFields.digidGeboortedatum,
				this.digidFields.digidStraat,
				this.digidFields.digidHuisnummer,
				this.digidFields.digidHuisnummerletter,
				this.digidFields.digidHuisnummertoevoeging,
				this.digidFields.digidPostcode,
				this.digidFields.digidWoonplaats,
				this.digidFields.digidTelefoon,
				this.digidFields.digidEmail,
			])
			.setComponent(SetComponent)
			.setID('digidMainSet') as DigidSet2Builder;
	}
	
	public get digidMainTab(): FormTabBuilder {
		return FormTabBuilder.newBuilder(this.injector)
			.setID('digidMainTab')
			.setLabel('Uw gegevens')
			.setUrl('uw-gegevens')
			.setIdp(0)
			.setChildren([
				this.genericFieldSets.getNewDefaultTabSet()
					.setLabel('Uw gegevens')
					.setChildren([
						this.digidMainSet
					])
			]) as FormTabBuilder;
	}

	private _digidPartnerSet: DigidPartnerSet2Builder = DigidPartnerSet2Builder.newBuilder(this.injector)
		.setPartnerField({field: this.digidFields.samenPartner, option: this.digidFields.samenPartner_ja})
		// .setValidation(ValidatorPreBuildConfigs.getInstance().partnerLoginError({params: this.injector}))
		.setChildren([
			this.digidFields.digidPartnerBSN,
			this.digidFields.digidPartnerVoorletters,
			this.digidFields.digidPartnerVoorvoegsel,
			this.digidFields.digidPartnerGeslachtsnaam,
			this.digidFields.digidPartnerGeboortedatum,
			this.digidFields.digidPartnerStraat,
			this.digidFields.digidPartnerHuisnummer,
			this.digidFields.digidPartnerHuisnummerletter,
			this.digidFields.digidPartnerHuisnummertoevoeging,
			this.digidFields.digidPartnerWoonplaats,
			this.digidFields.digidPartnerPostcode,
		])
		.setComponent(SetComponent)
		.setShowConditions({
			all: [],
			single: [new ShowCondition(this.digidFields.samenPartner, this.digidFields.samenPartner_ja)],
		})
		.setID('digidPartnerSet') as DigidPartnerSet2Builder;

	public get digidPartnerSet(): DigidPartnerSet2Builder {
		return this._digidPartnerSet;
	}
	
	public get digidGezinSituatieInlogSet(): DigidPartnerInlogSetBuilder {
		return DigidPartnerInlogSetBuilder.newBuilder(this.injector)
			.setPartnerField({field: this.digidFields.gezinSituatie, option: this.digidFields.getrouwd})
			.setButton(this.digidFields.partnerLoginButton)
			.setChildren([
				this.digidFields.gezinSituatie,
				this.digidFields.partnerLoginButton,
			])
			.setComponent(SetComponent)
			.setID('digidGezinSituatieInlogSet') as DigidPartnerInlogSetBuilder;
	}
	//
	// public get digidGezinSituatieSet(): DigidPartnerSet2Builder {
	// 	return DigidPartnerSet2Builder.newBuilder(this.injector)
	// 		.setLabel('Gegevens van uw partner')
	// 		.setPartnerField({field: this.digidFields.gezinSituatie, option: this.digidFields.getrouwd})
	// 		.setChildren([
	// 			this.digidFields.digidPartnerBSN,
	// 			this.digidFields.digidPartnerVoorletters,
	// 			this.digidFields.digidPartnerVoorvoegsel,
	// 			this.digidFields.digidPartnerGeslachtsnaam,
	// 			this.digidFields.digidPartnerGeboortedatum,
	// 			this.digidFields.digidPartnerStraat,
	// 			this.digidFields.digidPartnerHuisnummer,
	// 			this.digidFields.digidPartnerHuisnummerletter,
	// 			this.digidFields.digidPartnerHuisnummertoevoeging,
	// 			this.digidFields.digidPartnerWoonplaats,
	// 			this.digidFields.digidPartnerPostcode,
	// 		])
	// 		.setComponent(SetComponent)
	// 		.setID('digidPartnerSet') as DigidPartnerSet2Builder;
	// }

	public get digidPartnerTab(): FormTabBuilder {
		return FormTabBuilder.newBuilder(this.injector)
			.setLabel('Gegevens partner')
			.setUrl('gegevens-partner')
			.setIdp(1)
			.setChildren([
				this.genericFieldSets.getNewDefaultTabSet()
					.setLabel('Gegevens partner')
					.setChildren([
						this.digidPartnerInlogSet,
						this.digidPartnerSet,
					])
			]) as FormTabBuilder;
	}
	
	public get digidConfig(): FormConfigBuilder {
		return FormConfigBuilder.newBuilder(this.injector)
			.setLoginType(ELoginTypes2.DIGID, new GenericTiles(this.injector).digidTile.build())
			.setLoginUrl('adapter/login');
	}
}
