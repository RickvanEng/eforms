import {FormSet2, FormSetBuilder} from '../../4-set/FormSet';
import {Injector} from '@angular/core';
import {
	FormFieldWithOptions,
	FormFieldWithOptionsFactory
} from '../../5-fields/form-field/multipleChoiceField/MultipleChoiceField';
import {FormFieldOption} from '../../5-fields/form-field/multipleChoiceField/options/FieldOption';
import {FormField, FormFieldBuilder} from '../../5-fields/form-field/FormField';
import {BasicValidationResult} from '../../validators/basic/BasicValidationResult';

export class DigidPartnerInlogSetBuilder extends FormSetBuilder {
	get loginButton(): FormField {
		return this._loginButton.build();
	}
	
	get partnerField(): { field: FormFieldWithOptions, option: FormFieldOption } {
		return {field: this._partnerField.field.build(), option: this._partnerField.option};
	}
	
	protected _buildingClass: any = DigidPartnerInlogSet;
	private _partnerField: { field: FormFieldWithOptionsFactory, option: FormFieldOption };
	private _loginButton: FormFieldBuilder;
	
	public static newBuilder(injector: Injector): DigidPartnerInlogSetBuilder {
		return new DigidPartnerInlogSetBuilder(injector);
	}
	
	public setPartnerField(config: { field: FormFieldWithOptionsFactory, option: FormFieldOption }): DigidPartnerInlogSetBuilder {
		this._partnerField = config;
		return this;
	}
	
	public setButton(field: FormFieldBuilder): DigidPartnerInlogSetBuilder {
		this._loginButton = field;
		return this;
	}
}

export class DigidPartnerInlogSet extends FormSet2 {
	
	private _partnerField: { field: FormFieldWithOptions, option: FormFieldOption };
	private _button: FormField;
	private partnerLoginError: BasicValidationResult;
	
	constructor(b: DigidPartnerInlogSetBuilder) {
		super(b);
		this._partnerField = b.partnerField;
		this._button = b.loginButton;
		
		this.partnerLoginError = new BasicValidationResult(
			// FormErrors.getInstance().partnerLoginError,
			'U moet met de DigiD van uw partner inloggen.');
	}
	
	init(): Promise<any> {
		return new Promise<any>(resolve => {
			super.init().then(() => {
				this._partnerField.field.trigger.subscribe(value => {
					let partnerOptionSelected: boolean = this._partnerField.field.isOptionChecked(this._partnerField.option.id);
					this._button.setShow(partnerOptionSelected);

					if (!partnerOptionSelected) {
						// this.removeError(this.partnerLoginError);
					}
				});

				resolve(true);
			});
		});
	}
}
