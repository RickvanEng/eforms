import {BehaviorSubject, Observable} from 'rxjs';
import {environment} from '../../../../../src/environments/environment';
import {Injectable, Injector} from '@angular/core';
import {Auth2Service, TokenObject} from '../../services/auth2.service';
import {HttpClient} from '@angular/common/http';
import {FormFieldOption} from '../../5-fields/form-field/multipleChoiceField/options/FieldOption';

@Injectable({
	providedIn: 'root'
})
export class Eherk2Service {
	get selectedVestiging(): BehaviorSubject<FormFieldOption> {
		return this._selectedVestiging;
	}
	
	get tokenData(): BehaviorSubject<any> {
		return this._tokenData;
	}
	
	private tokenDataURl: string = environment.baseUrl + 'adapter/session/token/';
	
	private _tokenData: BehaviorSubject<any> = new BehaviorSubject(undefined);
	private _selectedVestiging: BehaviorSubject<FormFieldOption> = new BehaviorSubject(undefined);
	
	constructor(private injector: Injector) {
		this.injector.get(Auth2Service).loginToken.subscribe((token: TokenObject) => {
			if (token) {
				console.log(token)
				this.getTokenData(token.value).subscribe(value => {
					this._tokenData.next(value);
				});
			}
		});
	}

	public setVestiging(option: FormFieldOption): void {
		this._selectedVestiging.next(option);
	}
	
	private getTokenData(token: string): Observable<any> {
		return this.injector.get(HttpClient).get(this.tokenDataURl + token);
	}
}
