import {Injector} from '@angular/core';
import {GenericFields} from '../../5-fields/presets/genericFields';
import {FormFieldBuilder} from '../../5-fields/form-field/FormField';
import {BasicInputField2Component} from '../../5-fields/form-field/basic-input-field2/basic-input-field2.component';
import {FormFieldWithOptionsFactory} from '../../5-fields/form-field/multipleChoiceField/MultipleChoiceField';
import {
	FormFieldOption,
	FormFieldOptionFactory
} from '../../5-fields/form-field/multipleChoiceField/options/FieldOption';
import {ShowCondition} from '../../showconditions/showCondition';
import {MultipleChoiceObjectBuilder} from '../../5-fields/form-field/multipleChoiceField/multiple-choice-object/multiple-choice-object';
import {PreInitEherkVestigingen} from '../../base/preInit/preInitEherkVestigingen';

export class EherkFields {

	private static instance: EherkFields;

	public static getInstance(injector: Injector): EherkFields {
		if (!EherkFields.instance) {
			EherkFields.instance = new EherkFields(injector);
		}
		return EherkFields.instance;
	}
	
	constructor(private injector: Injector) {
	}
	
	private genericFields: GenericFields = new GenericFields(this.injector);

	public eherkLabel: FormFieldBuilder = this.genericFields.label
		.setID('eherkLabel')
		.setLabel('Klik op uw vestiging en daarna op Volgende om door te gaan.');
	
	public vestigingSelection: MultipleChoiceObjectBuilder = this.genericFields.multipleChoiceObjectField
		.setPreInit(new PreInitEherkVestigingen(this.injector))
		.setDeselectAble(false)
		.setValidatorRequired()
		.setID('vestigingSelection') as MultipleChoiceObjectBuilder;
	
	public naamOrganisatie: FormFieldBuilder = FormFieldBuilder.newBuilder(this.injector)
		.setID('handelsnaam')
		.setReadonly(true)
		.setLabel('Naam van de organisatie')
		.setSaveToState(false)
		.setComponent(BasicInputField2Component)
		.setSendToBE(false) as FormFieldBuilder;
	
	public vestiging: FormFieldBuilder = FormFieldBuilder.newBuilder(this.injector)
		.setID('vestiging')
		.setReadonly(true)
		.setLabel('Vestiging')
		.setSaveToState(false)
		.setComponent(BasicInputField2Component)
		.setSendToBE(false) as FormFieldBuilder;
	
	public vestigingsnummer: FormFieldBuilder = FormFieldBuilder.newBuilder(this.injector)
		.setID('vestigingsNummer')
		.setReadonly(true)
		.setLabel('Vestigingsnummer')
		.setSaveToState(false)
		.setComponent(BasicInputField2Component) as FormFieldBuilder;
	
	public kvkNummer: FormFieldBuilder = FormFieldBuilder.newBuilder(this.injector)
		.setID('kvkNumber')
		.setReadonly(true)
		.setLabel('Kamer van Koophandel-nummer')
		.setSaveToState(false)
		.setComponent(BasicInputField2Component)
		.setSendToBE(false) as FormFieldBuilder;
	
	public straat: FormFieldBuilder = FormFieldBuilder.newBuilder(this.injector)
		.setID('verblijfstraat')
		.setReadonly(true)
		.setLabel('Straatnaam')
		.setSaveToState(false)
		.setComponent(BasicInputField2Component)
		.setSendToBE(false) as FormFieldBuilder;
	
	public huisnummer: FormFieldBuilder = FormFieldBuilder.newBuilder(this.injector)
		.setID('verblijfhuisnummer')
		.setReadonly(true)
		.setLabel('Huisnummer')
		.setSaveToState(false)
		.setComponent(BasicInputField2Component)
		.setSendToBE(false) as FormFieldBuilder;
	
	public postcode: FormFieldBuilder = FormFieldBuilder.newBuilder(this.injector)
		.setID('verblijfpostcode')
		.setReadonly(true)
		.setLabel('Postcode')
		.setSaveToState(false)
		.setComponent(BasicInputField2Component)
		.setSendToBE(false) as FormFieldBuilder;
	
	public plaats: FormFieldBuilder = FormFieldBuilder.newBuilder(this.injector)
		.setID('verblijfwoonplaats')
		.setReadonly(true)
		.setLabel('Plaats')
		.setSaveToState(false)
		.setComponent(BasicInputField2Component)
		.setSendToBE(false) as FormFieldBuilder;
	
	public get eherkTelefoon(): FormFieldBuilder {
		return this.genericFields.telefoon
			.setValidatorRequired()
	}
	
	public get eherkEmail(): FormFieldBuilder {
		return this.genericFields.email
			.setValidatorRequired();
	}
	
	public pa_vestigingAdresJn_Ja: FormFieldOption = FormFieldOptionFactory.newFactory()
		.setId('postVestigingAdresJn-ja')
		.setData({value: 'Ja'})
		.build();
	public pa_vestigingAdresJn_Nee: FormFieldOption = FormFieldOptionFactory.newFactory()
		.setId('postVestigingAdresJn-nee')
		.setData({value: 'Nee'})
		.build();
	public pa_vestigingAdresJn: FormFieldWithOptionsFactory = this.genericFields.radioField
		.setOptions([
			this.pa_vestigingAdresJn_Ja,
			this.pa_vestigingAdresJn_Nee,
		])
		.setID('pa-vestigingAdresJn')
		.setLabel('Is het postadres hetzelfde als het vestigingsadres?')
		.setValidatorRequired() as FormFieldWithOptionsFactory;

	public pa_postadres: FormFieldBuilder = this.genericFields.basicField
		.setID('pa-postadres')
		.setLabel('Straat of Postbus')
		.setValidatorRequired()
		.setShowConditions({
			all: [new ShowCondition(this.pa_vestigingAdresJn, this.pa_vestigingAdresJn_Nee)],
			single:[]
		}) as FormFieldBuilder;
	
	public pa_huisnummer: FormFieldBuilder = this.genericFields.huisnummer
		.setID('pa-huisnummer')
		.setLabel('Huisnummer of postbusnummer')
		.setComponent(BasicInputField2Component)
		.setValidatorRequired()
		.setShowConditions({
			all: [new ShowCondition(this.pa_vestigingAdresJn, this.pa_vestigingAdresJn_Nee)],
			single:[]
		}) as FormFieldBuilder;

	public pa_huisnummertoevoeging: FormFieldBuilder = this.genericFields.huisnummertoevoeging
		.setID('pa-huisnummertoevoeging')
		.setLabel('Huisnummertoevoeging')
		.setComponent(BasicInputField2Component)
		.setShowConditions({
			all: [new ShowCondition(this.pa_vestigingAdresJn, this.pa_vestigingAdresJn_Nee)],
			single:[]
		}) as FormFieldBuilder;

	public pa_postcode: FormFieldBuilder = this.genericFields.postcode
		.setID('pa-postcode')
		.setLabel('Postcode')
		.setComponent(BasicInputField2Component)
		.setValidatorRequired()
		.setShowConditions({
			all: [new ShowCondition(this.pa_vestigingAdresJn, this.pa_vestigingAdresJn_Nee)],
			single:[]
		}) as FormFieldBuilder;
	
	public pa_plaats: FormFieldBuilder = this.genericFields.plaats
		.setID('pa-plaats')
		.setLabel('Plaats')
		.setComponent(BasicInputField2Component)
		.setValidatorRequired()
		.setShowConditions({
			all: [new ShowCondition(this.pa_vestigingAdresJn, this.pa_vestigingAdresJn_Nee)],
			single:[]
		}) as FormFieldBuilder;
		
	public cp_label: FormFieldBuilder = this.genericFields.label
		.setID('cp-label')
		.setLabel('Wat is uw naam, telefoonnummer en e-mailadres?');
	
	public cp_voorletters: FormFieldBuilder = this.genericFields.voorletters
		.setID('cp-voorletters')
		.setValidatorRequired();
	public cp_tussenvoegsel: FormFieldBuilder = this.genericFields.voorvoegsel
		.setID('cp-tussenvoegsel');
	public cp_achternaam: FormFieldBuilder = this.genericFields.geslachtsnaam
		.setID('cp-achternaam')
		.setValidatorRequired();
	public cp_telefoon: FormFieldBuilder = this.genericFields.telefoon
		.setID('cp-telefoon')
		.setValidatorRequired();
	public cp_email: FormFieldBuilder = this.genericFields.email
		.setID('cp-email')
		.setValidatorRequired()
	
}
