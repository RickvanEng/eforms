import {FormSet2, FormSetBuilder} from '../../4-set/FormSet';
import {Injector} from '@angular/core';
import {Eherk2Service} from './eherk2.service';

export class EherkGegevensSet2Builder extends FormSetBuilder {
	
	public static newBuilder(injector: Injector): EherkGegevensSet2Builder {
		return new EherkGegevensSet2Builder(injector);
	}
	
	protected _buildingClass: any = EherkGegevensSet2;
}

export class EherkGegevensSet2 extends FormSet2 {
	constructor(builder: FormSetBuilder) {
		super(builder);
	}

	init(): Promise<any> {
		return new Promise<any>(resolve => {
			super.init().then(() => {
				this._injector.get(Eherk2Service).selectedVestiging.subscribe(option => {
					if (option) {
						this.prefillValue(option.data);
					}
				});

				resolve(true);
			})
		})
	}

	prefillValue(value: any): void {
		if (value.tradeNames.shortBusinessName) {
			this._children.find(field => field.id === 'handelsnaam').prefillValue(value.tradeNames.shortBusinessName);
		} else {
			this._children.find(field => field.id === 'handelsnaam').prefillValue(value.tradeNames.businessName);
		}
		
		this._children.find(field => field.id === 'vestiging').prefillValue(value.tradeNames.businessName);
		this._children.find(field => field.id === 'vestigingsNummer').prefillValue(value.branchNumber);
		this._children.find(field => field.id === 'kvkNumber').prefillValue(value.kvkNumber);
		
		if (Array.isArray(value.addresses)) {
			this._children.find(field => field.id === 'verblijfstraat').prefillValue(value.addresses[0].street);
			this._children.find(field => field.id === 'verblijfhuisnummer').prefillValue(value.addresses[0].houseNumber);
			this._children.find(field => field.id === 'verblijfhuisnummertoevoeging').prefillValue(value.addresses[0].houseNumberAddition);
			this._children.find(field => field.id === 'verblijfpostcode').prefillValue(value.addresses[0].postalCode);
			this._children.find(field => field.id === 'verblijfwoonplaats').prefillValue(value.addresses[0].city);
		} else {
			this._children.find(field => field.id === 'verblijfstraat').prefillValue(value.addresses.street);
			this._children.find(field => field.id === 'verblijfhuisnummer').prefillValue(value.addresses.houseNumber);
			this._children.find(field => field.id === 'verblijfpostcode').prefillValue(value.addresses.postalCode);
			this._children.find(field => field.id === 'verblijfwoonplaats').prefillValue(value.addresses.city);
		}
	}
}
