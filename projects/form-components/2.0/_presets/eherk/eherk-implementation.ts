import {Injector} from '@angular/core';
import {SetComponent} from '../../4-set/set.component';
import {EherkFields} from './eherkFields';
import {EherkGegevensSet2Builder} from './eherkGegevensSet2';
import {EherkVestigingenSet2Builder} from './eherkVestigingenSet2';
import {FormTabBuilder} from '../../2-tab/FormTab2';
import {BackendTasks} from '../../../../forms/services/sendform/sendForm.service';
import {GenericTabFieldSets} from '../../3-tab-field-set/presets/genericTabFieldSets';
import {FormConfigBuilder} from '../../0-routing/formConfig';
import {ELoginTypes2} from '../../../../forms/src/formImplementations/IForm';
import {GenericTiles} from '../../0-routing/form-routing/login-portal/login-tile/presets/generic-tiles';
import {PrefillVergunningEherkSetBuilder} from "../vergunningen/prefillVergunningEherk.set";
import {FormSetBuilder} from '../../4-set/FormSet';

export class EherkImplementation {

    private static instance: EherkImplementation;

    public static getInstance(injector: Injector): EherkImplementation {
        if (!EherkImplementation.instance) {
            EherkImplementation.instance = new EherkImplementation(injector);
        }
        return EherkImplementation.instance;
    }

    constructor(private injector: Injector) {
    }

    private eherkFields: EherkFields = new EherkFields(this.injector);
    private genericFieldSets: GenericTabFieldSets = new GenericTabFieldSets(this.injector);

    private _eherkVestigingenSet = EherkVestigingenSet2Builder.newBuilder(this.injector)
        .setComponent(SetComponent)
        .setChildren([
            this.eherkFields.eherkLabel,
            this.eherkFields.vestigingSelection
        ])
        .setID('eherkVestigingenSet') as EherkVestigingenSet2Builder;
    public get eherkVestigingenSet(): EherkVestigingenSet2Builder {
        return this._eherkVestigingenSet;
    }

    private _eherkVestigingenAPVSet = PrefillVergunningEherkSetBuilder.newBuilder(this.injector)
        .setComponent(SetComponent)
        .setChildren([
            this.eherkFields.eherkLabel,
            this.eherkFields.vestigingSelection
        ])
        .setID('eherkVestigingenAPVSet') as PrefillVergunningEherkSetBuilder;
    public get eherkVestigingenAPVSet(): PrefillVergunningEherkSetBuilder {
        return this._eherkVestigingenAPVSet;
    }

    private _eherkVestigingenTab: FormTabBuilder = FormTabBuilder.newBuilder(this.injector)
        .setLabel('Vestigingen')
        .setBackendTask(BackendTasks.NONE)
        .setUrl('vestigingen')
        .setChildren([
            this.genericFieldSets.getNewDefaultTabSet()
                .setLabel('Vestigingen')
                .setChildren([
                    this._eherkVestigingenSet
                ])
        ])
        .setShowInOverviewSet(false)
        .setSendToBE(false)
        .setID('eherkVestigingenTab') as FormTabBuilder;
    public get eherkVestigingenTab(): FormTabBuilder {
        return this._eherkVestigingenTab;
    }

    private _eherkVestigingenAPVTab: FormTabBuilder = FormTabBuilder.newBuilder(this.injector)
        .setLabel('Vestigingen')
        .setBackendTask(BackendTasks.NONE)
        .setUrl('vestigingen')
        .setChildren([
            this.genericFieldSets.getNewDefaultTabSet()
                .setLabel('Vestigingen')
                .setChildren([
                    this._eherkVestigingenAPVSet
                ])
        ])
        .setShowInOverviewSet(false)
        .setSendToBE(false) as FormTabBuilder;
    public get eherkVestigingenAPVTab(): FormTabBuilder {
        return this._eherkVestigingenAPVTab;
    }

    private _eherkGegevensSet: EherkGegevensSet2Builder = EherkGegevensSet2Builder.newBuilder(this.injector)
        .setComponent(SetComponent)
        .setChildren([
            this.eherkFields.naamOrganisatie,
            this.eherkFields.vestiging,
            this.eherkFields.vestigingsnummer,
            this.eherkFields.kvkNummer,
            this.eherkFields.straat,
            this.eherkFields.huisnummer,
            this.eherkFields.postcode,
            this.eherkFields.plaats,
            this.eherkFields.eherkTelefoon,
            this.eherkFields.eherkEmail,
        ])
        .setID('eherkGegevensSet') as EherkGegevensSet2Builder;
    public get eherkGegevensSet(): EherkGegevensSet2Builder {
        return this._eherkGegevensSet;
    }

    private _eherkGegevensTab: FormTabBuilder = FormTabBuilder.newBuilder(this.injector)
        .setLabel('Uw gegevens')
        .setIdp(0)
        .setUrl('uw-gegevens')
        .setChildren([
            this.genericFieldSets.getNewDefaultTabSet()
                .setLabel('Uw gegevens')
                .setChildren([
                    this._eherkGegevensSet
                ])
        ])
        .setID('eherkGegevensTab') as FormTabBuilder;
    public get eherkGegevensTab(): FormTabBuilder {
        return this._eherkGegevensTab;
    }

    /////////////////////////////////

    private _eherkGegevensContactSet: EherkGegevensSet2Builder = EherkGegevensSet2Builder.newBuilder(this.injector)
        .setComponent(SetComponent)
        .setChildren([
            this.eherkFields.naamOrganisatie,
            this.eherkFields.vestiging,
            this.eherkFields.vestigingsnummer,
            this.eherkFields.kvkNummer,
            this.eherkFields.straat,
            this.eherkFields.huisnummer,
            this.eherkFields.postcode,
            this.eherkFields.plaats,
        ])
        .setID('eherkGegevensSet') as EherkGegevensSet2Builder;
    public get eherkGegevensContactSet(): EherkGegevensSet2Builder {
        return this._eherkGegevensContactSet;
    }

    private _eherkPostadresSet: FormSetBuilder = FormSetBuilder.newBuilder(this.injector)
        .setComponent(SetComponent)
        .setLabel('Postadres')
        .setChildren([
            this.eherkFields.pa_vestigingAdresJn,
            this.eherkFields.pa_postadres,
            this.eherkFields.pa_huisnummer,
            this.eherkFields.pa_huisnummertoevoeging,
            this.eherkFields.pa_postcode,
            this.eherkFields.pa_plaats,
        ])
        .setID('eherkGegevensSet') as FormSetBuilder;
    public get eherkPostadresSet(): FormSetBuilder {
        return this._eherkPostadresSet;
    }

    private _eherkContactpersoonSet: FormSetBuilder = FormSetBuilder.newBuilder(this.injector)
        .setComponent(SetComponent)
        .setLabel('Contactpersoon')
        .setChildren([
            this.eherkFields.cp_label,
            this.eherkFields.cp_voorletters,
            this.eherkFields.cp_tussenvoegsel,
            this.eherkFields.cp_achternaam,
            this.eherkFields.cp_telefoon,
            this.eherkFields.cp_email,
        ])
        .setID('eherkGegevensSet') as FormSetBuilder;

    public get eherkContactpersoonSet(): FormSetBuilder {
        return this._eherkContactpersoonSet;
    }

    private _eherkContactTab: FormTabBuilder = FormTabBuilder.newBuilder(this.injector)
        .setLabel('Uw gegevens')
        .setIdp(0)
        .setUrl('uw-gegevens')
        .setChildren([
            this.genericFieldSets.getNewDefaultTabSet()
                .setLabel('Uw gegevens')
                .setChildren([
                    this._eherkGegevensContactSet,
                    this._eherkContactpersoonSet
                ])
        ]) as FormTabBuilder;
    public get eherkContactTab(): FormTabBuilder {
        return this._eherkContactTab;
    }

    private _eherkPostContactTab: FormTabBuilder = FormTabBuilder.newBuilder(this.injector)
        .setLabel('Uw gegevens')
        .setIdp(0)
        .setUrl('uw-gegevens')
        .setChildren([
            this.genericFieldSets.getNewDefaultTabSet()
                .setLabel('Uw gegevens')
                .setChildren([
                    this._eherkGegevensContactSet,
                    this._eherkPostadresSet,
                    this._eherkContactpersoonSet
                ])
        ]) as FormTabBuilder;
    public get eherkPostContactTab(): FormTabBuilder {
        return this._eherkPostContactTab;
    }

    private _eherkConfig: FormConfigBuilder = FormConfigBuilder.newBuilder(this.injector)
        .setLoginType(ELoginTypes2.EHERK, new GenericTiles(this.injector).eherkTile.build())
        .setLoginUrl('adapter/login?idp=eherkenning');
    public get eherkConfig(): FormConfigBuilder {
        return this._eherkConfig;
    }

}
