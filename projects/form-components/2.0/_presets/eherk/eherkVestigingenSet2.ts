import {FormSet2, FormSetBuilder} from '../../4-set/FormSet';
import {Injector} from '@angular/core';
import {FormFieldWithOptions} from '../../5-fields/form-field/multipleChoiceField/MultipleChoiceField';
import {Eherk2Service} from './eherk2.service';

export class EherkVestigingenSet2Builder extends FormSetBuilder {

    public static newBuilder(injector: Injector): EherkVestigingenSet2Builder {
        return new EherkVestigingenSet2Builder(injector);
    }

    protected _buildingClass: any = EherkVestigingenSet2;
}

export class EherkVestigingenSet2 extends FormSet2 {
    constructor(builder: FormSetBuilder) {
        super(builder);
    }

    init(): Promise<any> {
        return new Promise<any>(resolve => {
            super.init().then(() => {

                let vestigingenField: FormFieldWithOptions = this._children[1] as FormFieldWithOptions;

                // saves the selected option
                vestigingenField.abstractControl.statusChanges.subscribe(() => {
                    vestigingenField.getOptions.getValue().forEach(option => {
                        if (option.isChecked) {
                            this._injector.get(Eherk2Service).setVestiging(option.data);
                        }
                    });
                });

                resolve(true);
            });
        });
    }
}
