import {TokenObject} from '../services/auth2.service';
import {Subject} from 'rxjs';

export class IdpDataService {
	protected _token$: Subject<TokenObject> = new Subject();
	
	setToken(token: TokenObject): void {
		this._token$.next(token);
	}
}
