import {Injector} from '@angular/core';
import {ELoginTypes2} from 'projects/forms/src/formImplementations/IForm';
import {FormConfigBuilder} from '../../0-routing/formConfig';
import {GenericTiles} from '../../0-routing/form-routing/login-portal/login-tile/presets/generic-tiles';

export class NoLoginImplementation {

	private static instance: NoLoginImplementation;
	
	public static getInstance(injector: Injector): NoLoginImplementation {
		if (!NoLoginImplementation.instance) {
			NoLoginImplementation.instance = new NoLoginImplementation(injector);
		}
		return NoLoginImplementation.instance;
	}

	constructor(private injector: Injector) {
	}

	private _noLoginConfig: FormConfigBuilder = FormConfigBuilder.newBuilder(this.injector)
		.setLoginType(ELoginTypes2.NONE, new GenericTiles(this.injector).noLoginTile.build())
		.setLoginUrl('adapter/login');

	public get noLoginConfig(): FormConfigBuilder {
		return this._noLoginConfig;
	}


	
}
