import {FormConfigBuilder} from '../../0-routing/formConfig';
import {Injector} from '@angular/core';
import {ELoginTypes2} from "../../../../forms/src/formImplementations/IForm";
import {GenericTiles} from "../../0-routing/form-routing/login-portal/login-tile/presets/generic-tiles";

export class NoLoginConfig {

    private static instance: NoLoginConfig;

    public static getInstance(injector: Injector): NoLoginConfig {
        if (!NoLoginConfig.instance) {
            NoLoginConfig.instance = new NoLoginConfig(injector);
        }
        return NoLoginConfig.instance;
    }

    constructor(private injector: Injector) {
    }

    private _config: FormConfigBuilder = FormConfigBuilder.newBuilder(this.injector)
        .setLoginType(ELoginTypes2.NONE, new GenericTiles(this.injector).noLoginTile.build())
        .setLoginUrl('no-login');
    public get config(): FormConfigBuilder {
        return this._config;
    }
}
