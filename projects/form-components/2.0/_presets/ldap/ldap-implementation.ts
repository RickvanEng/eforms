import {Injector} from '@angular/core';
import {FormConfigBuilder} from '../../0-routing/formConfig';
import {ELoginTypes2} from '../../../../forms/src/formImplementations/IForm';
import {GenericTiles} from '../../0-routing/form-routing/login-portal/login-tile/presets/generic-tiles';

export class LdapImplementation {

    private static instance: LdapImplementation;

    public static getInstance(injector: Injector): LdapImplementation {
        if (!LdapImplementation.instance) {
            LdapImplementation.instance = new LdapImplementation(injector);
        }
        return LdapImplementation.instance;
    }

    constructor(private injector: Injector) {
    }

    public get ldapConfig(): FormConfigBuilder {
        return FormConfigBuilder.newBuilder(this.injector)
            .setLoginType(ELoginTypes2.LDAP, new GenericTiles(this.injector).ldapTile.build())
            .setLoginUrl('adapter/ldaplogin?form=');
    }
}
