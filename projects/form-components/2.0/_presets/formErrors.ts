import {ThrowableError} from './ThrowableError';

export class FormErrors {

    private static instance: FormErrors;

    public static getInstance(): FormErrors {
        if (!FormErrors.instance) {
            FormErrors.instance = new FormErrors();
        }
        return FormErrors.instance;
    }

    private constructor() {
    }

    public email: ThrowableError = new ThrowableError('email');
    public date: ThrowableError = new ThrowableError('date');
    public minDate: ThrowableError = new ThrowableError('minData');
    public maxDate: ThrowableError = new ThrowableError('maxDate');
    public required: ThrowableError = new ThrowableError('required');
    public minLength: ThrowableError = new ThrowableError('minLength');
    public partnerLoginError: ThrowableError = new ThrowableError('partnerLoginError');
    // public ageErrorTooOld: ThrowableError = new ThrowableError('ageErrorTooOld');
    public ageErrorTooYoung: ThrowableError = new ThrowableError('ageErrorTooYoung');
    public postcodeCheck: ThrowableError = new ThrowableError('postcodeCheck');
    public bsn: ThrowableError = new ThrowableError('bsn');
    public zaakNummer: ThrowableError = new ThrowableError('zaakNummer');
	public tozoAanvraag: ThrowableError = new ThrowableError('tozoAanvraag');
	public tozoFilled: ThrowableError = new ThrowableError('tozoFilled');
	public tonkAlreadyFilled: ThrowableError = new ThrowableError('tonkAlreadyFilled');
    public bbzAanvraag: ThrowableError = new ThrowableError('bbzAanvraag');

    public iban: ThrowableError = new ThrowableError('iban');
    public birthDateYoung: ThrowableError = new ThrowableError('birthDate_Young');
    public birthDateOld: ThrowableError = new ThrowableError('birthDate-Old');
    public pattern: ThrowableError = new ThrowableError('pattern');
    public maxLength: ThrowableError = new ThrowableError('maxLength');
    public min: ThrowableError = new ThrowableError('min');
    public max: ThrowableError = new ThrowableError('max');
    public dynamicDate: ThrowableError = new ThrowableError('dynamicDate');
}
