import {FormSet2, FormSetBuilder} from '../../4-set/FormSet';
import {Injector} from '@angular/core';
import {
	FormFieldWithOptions,
	FormFieldWithOptionsFactory
} from '../../5-fields/form-field/multipleChoiceField/MultipleChoiceField';
import {FormFieldOption} from '../../5-fields/form-field/multipleChoiceField/options/FieldOption';
import {VergunningenService} from './vergunningen.service';

export class EerdereVergunningenSetBuilder extends FormSetBuilder {
	get vergunningField(): FormFieldWithOptions {
		return this._vergunningField.build();
	}
	
	get endpoint(): string {
		return this._endpoint;
	}
	
	protected _buildingClass: any = EerdereVergunningenSet;
	private _vergunningField: FormFieldWithOptionsFactory;
	private _endpoint: string;
	
	public static newBuilder(injector: Injector): EerdereVergunningenSetBuilder {
		return new EerdereVergunningenSetBuilder(injector);
	}
	
	public setVergunningChoiceField(field: FormFieldWithOptionsFactory): EerdereVergunningenSetBuilder {
		this._vergunningField = field;
		return this;
	}
	
	public setEndPoint(url: string): EerdereVergunningenSetBuilder {
		this._endpoint = url;
		return this;
	}
}

export class EerdereVergunningenSet extends FormSet2 {
	
	protected _builder: EerdereVergunningenSetBuilder;
	private _endpoint: string;
	private _vergunningField: FormFieldWithOptions;
	
	constructor(builder: EerdereVergunningenSetBuilder) {
		super(builder);
		this._endpoint = builder.endpoint;
		this._vergunningField = builder.vergunningField;
	}
	
	init(): Promise<any> {
		return new Promise<any>(resolve => {
			super.init().then(() => {
				this._vergunningField.trigger.subscribe(trigger => {
					if (trigger) {
						let selectedOption: FormFieldOption = this._vergunningField.getOptions.getValue().find(option => option.isChecked === true);
						this._injector.get(VergunningenService).selectedOption.next(selectedOption);
						// this._currentState.updateComponent(this.id, {vergunningChoice: selectedOption.data})
					}
				});

				resolve(true);
			});
		});

		// this._vergunningField.getOptions.getValue().forEach(option => {
		// 	option.changeDetector.subscribe(trigger => {
		// 		if (trigger) {
		// 			// TODO check deze
		// 			this._currentState.updateSet(this.id, {vergunningChoice: option.data})
		// 		}
		// 	})
		// });
		
		// if (this._oldState.getSetInfo(this.id)) {
		// 	this._injector.get(VergunningenService).stateToFill = this._oldState;
		//
		// 	const option: FormFieldOption = this._injector.get(VergunningenService).options.getValue().find(option =>
		// 		option.data.data.zaaknummer ===
		// 		this._oldState.getSetInfo(this.id).vergunningChoice.data.zaaknummer);
		// 	option.changeDetector.next(true);
		// }
	}
}
