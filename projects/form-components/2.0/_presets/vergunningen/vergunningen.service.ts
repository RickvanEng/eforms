import {Injectable} from '@angular/core';
import {BehaviorSubject} from 'rxjs';
import {FormFieldOption} from '../../5-fields/form-field/multipleChoiceField/options/FieldOption';
import {FormState} from '../../1-form/formStateManager';

@Injectable({
	providedIn: 'root'
})
export class VergunningenService {
	set stateToFill(value: FormState) {
		this._stateToFill = value;
	}
	get stateToFill(): FormState {
		return this._stateToFill;
	}
	get selectedOption(): BehaviorSubject<any> {
		return this._selectedOption;
	}
	get options(): BehaviorSubject<FormFieldOption[]> {
		return this._options;
	}
	
	private _stateToFill: FormState;
	private _options: BehaviorSubject<FormFieldOption[]> = new BehaviorSubject([]);
	private _selectedOption: BehaviorSubject<any> = new BehaviorSubject(undefined);
}
