import {FormSet2, FormSetBuilder} from '../../4-set/FormSet';
import {Injector} from '@angular/core';

export class PrefillVergunningSetBuilder extends FormSetBuilder {
	
	public static newBuilder(injector: Injector): PrefillVergunningSetBuilder {
		return new PrefillVergunningSetBuilder(injector);
	}
	
	protected _buildingClass: any = PrefillVergunningSet;
}

export class PrefillVergunningSet extends FormSet2 {

	constructor(builder: PrefillVergunningSetBuilder) {
		super(builder);
	}
	
	// init(state: FormState): Promise<any> {
	// 	return new Promise<any>(resolve => {
	// 		super.init(state).then(() => {
	// 			this._injector.get(VergunningenService).selectedOption.subscribe((option: FormFieldOption) => {
	// 				if (option) {
	// 					this.clear();
	// 					this.prefillForm(this._children, option.data.data);
	// 					if (this._injector.get(VergunningenService).stateToFill) {
	// 						// this.stateReset(this._injector.get(VergunningenService).stateToFill);
	// 						// this._injector.get(VergunningenService).stateToFill = undefined;
	// 					}
	// 				} else {
	// 					this.clear();
	// 				}
	// 			});
	//
	// 			resolve(true);
	// 		});
	// 	});
	// }
}
