import {Injector} from "@angular/core";
import {VergunningenService} from "./vergunningen.service";
import {FormFieldOption} from "../../5-fields/form-field/multipleChoiceField/options/FieldOption";
import {FormFieldWithOptions} from "../../5-fields/form-field/multipleChoiceField/MultipleChoiceField";
import {EherkVestigingenSet2, EherkVestigingenSet2Builder} from "../eherk/eherkVestigingenSet2";

export class PrefillVergunningEherkSetBuilder extends EherkVestigingenSet2Builder {

    public static newBuilder(injector: Injector): PrefillVergunningEherkSetBuilder {
        return new PrefillVergunningEherkSetBuilder(injector);
    }

    protected _buildingClass: any = PrefillVergunningEherkSet;
}

export class PrefillVergunningEherkSet extends EherkVestigingenSet2 {
    constructor(builder: PrefillVergunningEherkSetBuilder) {
        super(builder);
        this._injector.get(VergunningenService).selectedOption.subscribe((option: FormFieldOption) => {
            if (option) {
                this.clear();
                this.prefillForm(option.data.selected);
            } else {
                this.clear();
            }
        })
    }

    private prefillForm(data: any): void {
        let dataToPrefill: string = data['vestigingsnummer'];
        let vestigingSelection: FormFieldWithOptions = this._children.find(field => field.id === 'vestigingSelection') as FormFieldWithOptions;
        if (vestigingSelection) {
            let option: FormFieldOption = vestigingSelection.getOptions.value.find(option => option.data.value.branchNumber === dataToPrefill);
            if (option) {
                // option.changeDetector.next(true);
            }
        }
    }
}
