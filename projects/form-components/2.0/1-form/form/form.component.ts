import {Component, Injector, Input, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {Form2} from '../Form';
import {FormTab2} from '../../2-tab/FormTab2';
import {FormBuilder, FormGroup} from '@angular/forms';

@Component({
    selector: 'app-form-component',
    templateUrl: './form.component.html',
    styleUrls: ['./form.component.scss']
})
export class FormComponent implements OnInit {

    @Input() public form: Form2;
    public rdy: boolean = false;


    // public forms: FormConfig[];
    // public logoutLink: string;
    public activeTab: FormTab2;
    //
    // private dev: boolean = true;
    //
    public formGroup: FormGroup;

    //
    constructor(public router: Router,
                private activeRoute: ActivatedRoute,
                private fb: FormBuilder,
                private inj: Injector) {
    }

    ngOnInit() {
        // this.logoutLink = environment.baseUrl + 'adapter/logout';

        let formToLoad: Form2 = this.form;
        // FormService.getInstance().selectedForm = this.formConfig.form;
        // // step 1: Init the form. All preinits
        formToLoad.initForm('').then(() => {
            // step 2: construct controls
            this.formGroup = this.constructFormGroup(formToLoad);

            // step 4: subscribe on other fields
            // formToLoad.setupShowConditions();

            // step 3: prefill the state
            // let oldState: string = this.getState(formToLoad.id);
            // if (oldState) {
            //     formToLoad.tabs.forEach(tab => {
            //         tab.prefillState(JSON.parse(oldState));
            //         formToLoad.navigateToTab(formToLoad.tabs.find(tab => tab.id === (JSON.parse(oldState) as IFormState).activeTab))
            //     });
            // }

            // step 5: save changes in state
            // this.formGroup.statusChanges.subscribe(() => {
            //     this.updateState();
            // });

            this.form = formToLoad;
            this.form.activeTab.subscribe(tab => {
                this.activeTab = tab;
                // this.updateState();
            });

        });

    }

    public navFromSideMenu(targetTab: FormTab2): void {
        // this.form.navigateToTab(targetTab)
    }

    public isTabActive(tab: FormTab2): boolean {
        return tab === this.activeTab;
    }

    private constructFormGroup(form: Form2): FormGroup {
        let formGroup: FormGroup = this.fb.group({});

        form.tabs.forEach((tab: FormTab2) => {
            formGroup.addControl(tab.id, tab.constructFormControl(this.inj))
        });

        return formGroup;
    }

    // private getState(id: string): any {
    //     return localStorage.getItem(id);
    // }
    //
    // private updateState(): void {
    //     let stateOBJ: IFormState = {
    //         formID: this.form.id,
    //         activeTab: this.activeTab.id,
    //         ttl: new Date().toString(),
    //         components: this.form.getKeyValueFromFields(),
    //     };
    //     localStorage.setItem(stateOBJ.formID, JSON.stringify(stateOBJ));
    // }
    //
    // public logForm(): void {
    //     console.log(this.formGroup);
    // }
}
