import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormComponent} from './form.component';
import {RouterModule} from '@angular/router';
import {TabModule} from '../../2-tab/tab.module';
import {DankSchermModule} from '../dank-scherm/dank-scherm.module';
import {DankScherm2Component} from '../dank-scherm/dank-scherm2.component';
import {FormFieldModule} from '../../5-fields/form-field/form-field.module';
import {ReactiveFormsModule} from '@angular/forms';

@NgModule({
	imports: [
		CommonModule,
		RouterModule,
		TabModule,
		DankSchermModule,
		FormFieldModule,
		ReactiveFormsModule,
	],
	declarations: [
		FormComponent,
	],
	exports: [
		FormComponent,
	],
	entryComponents: [
		DankScherm2Component,
	]
})
export class MultiPageForm2Module {
}
