import {Injector} from '@angular/core';
import {FormBuilder} from '../Form';
import {ELoginTypes2} from '../../../../forms/src/formImplementations/IForm';
import {PreInitDigiDLogin} from '../../base/preInit/preInitDigiDLogin';
import {PreInitEherkLogin} from '../../base/preInit/preInitEherkLogin';

export class GenericForms {
	
	constructor(private injector: Injector){}
	
	public digidForm: FormBuilder = FormBuilder.newBuilder(this.injector)
		.setLoginType(ELoginTypes2.DIGID)
		.setPreInit(new PreInitDigiDLogin(this.injector))
		.setUrl('digid');
	
	public eherkForm: FormBuilder = FormBuilder.newBuilder(this.injector)
		.setPreInit(new PreInitEherkLogin(this.injector))
		.setLoginType(ELoginTypes2.EHERK)
		.setUrl('eherk');
	
	public ldapForm: FormBuilder = FormBuilder.newBuilder(this.injector)
		.setLoginType(ELoginTypes2.LDAP)
		.setUrl('ldap');
	
	public nologinForm: FormBuilder = FormBuilder.newBuilder(this.injector)
		.setLoginType(ELoginTypes2.NONE)
		.setUrl('no-login');
}
