import { Injector } from '@angular/core';
import { ELoginTypes2 } from '../../../../forms/src/formImplementations/IForm';
import { VariableValueFormBuilder } from '../Form.variableValue';

export class VariableValueForms {

    constructor(private injector: Injector) {
    }

    public digidForm: VariableValueFormBuilder = VariableValueFormBuilder.newBuilder(this.injector)
        .setLoginType(ELoginTypes2.DIGID)
        .setUrl('digid') as VariableValueFormBuilder;

    public eherkForm: VariableValueFormBuilder = VariableValueFormBuilder.newBuilder(this.injector)
        .setLoginType(ELoginTypes2.EHERK)
        .setUrl('adapter/login?idp=eherkenning')
        .setUrl('eherk') as VariableValueFormBuilder;
}
