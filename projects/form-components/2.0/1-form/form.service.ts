import {BehaviorSubject} from 'rxjs';
import {Form2} from './Form';

// TODO deze kan weg?
export class FormService {
	
	private static instance: FormService;
	
	public static getInstance(): FormService {
		if (!FormService.instance) {
			FormService.instance = new FormService();
		}
		return FormService.instance;
	}
	
	private constructor() {
	}
	
	get formData(): BehaviorSubject<any> {
		return this._formData;
	}
	
	setFormData(value: any, id: string) {
		this._formData.value[id] = value;
		this._formData.next(this._formData.value);
	}
	
	get selectedForm(): Form2 {
		return this._selectedForm;
	}
	
	set selectedForm(value: Form2) {
		this._selectedForm = value;
	}
	
	private _selectedForm: Form2;
	private _formData: BehaviorSubject<any> = new BehaviorSubject({});
}
