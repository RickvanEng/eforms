import {FormTab2, IFormTab} from '../2-tab/FormTab2';
import {ICmsComponent} from '../ICmsComponents';
import {CmsComponentBuilder} from '../cmsComponent.builder';
import {CmsComponent} from '../cmsComponent';
import {BehaviorSubject} from 'rxjs';
import {ICmsAbstractControl} from '../ICmsAbstractControl';
import {CmsAbstractControl} from '../cmsAbstractControl';

export enum FormSendTypes {
    MAIL,
    VERSEON
}

export interface IForm extends ICmsAbstractControl {
    id: string;
    saveToState: boolean;
    zaakTypeCode: string;
    tabs: string[];
    showMenu: boolean;
    tabWidth: string;

    // dyanmicZaakTypeCode: { showcondition: ShowCondition, code: string }[] = [];
    // dyanmicZaakTypeCodeBuilder: { showcondition: ShowCondition, code: string }[] = [];
    // sendTo: FormSendTypes = FormSendTypes.VERSEON;
    // verseonOmschrijving: { showcondition: ShowCondition, info: { prefix: string, field: FormField, set?: MultipleFieldGeneratorSet }[] }[] = [];
    // verseonOmschrijvingBuilder: { showcondition: ShowCondition, info: { prefix: string, field: FormFieldBuilder, set?: MultipleFieldGeneratorSetBuilder }[] }[] = [];
    // preInits: PreInit[];
    // loginPortalTile: LoginTile = new GenericTiles(this._injector).noLoginTile.build();
    // extraData: { id: string, inhoud: GetDataFromBe[] }[] = [];
    // extraDataBuilder: { id: string, inhoud: GetDataFromBe }[] = [];
    // customProperty: string = 'dynamicFormData';
}

export class FormBuilder extends CmsComponentBuilder {

    _config: IForm = {
        asyncValidators: [],
        basicValidators: [],
        className: undefined,
        id: undefined,
        saveToState: false,
        showMenu: true,
        tabWidth: 'col-lg-9',
        tabs: [],
        zaakTypeCode: ''
    };


    public static newBuilder(): FormBuilder {
        return new FormBuilder();
    }

    /** save this form to localstorage */
    public setSaveToState(value: boolean): FormBuilder {
        this._config.saveToState = value;
        return this;
    }

    public setId(id: string): FormBuilder {
        this._config.id = id;
        return this;
    }


    public setZaakTypeCode(code: string): FormBuilder {
        this._config.zaakTypeCode = code;
        return this;
    }


    public setTabs(tabs: string[]): FormBuilder {
        this._config.tabs = tabs;
        return this;
    }

    public build(): IForm {
        return this._config;
    }

}

export class Form2 extends CmsAbstractControl {
    get activeTab(): BehaviorSubject<FormTab2> {
        return this._activeTab;
    }

    get zaaknummer(): string {
        return this._config.zaakTypeCode;
    }

    get tabWidth(): string {
        return this._config.tabWidth;
    }

    get id(): string {
        return this._config.id;
    }

    get zaakTypeCode(): string {
        return this._config.zaakTypeCode;
    }

    get showMenu(): boolean {
        return this._config.showMenu;
    }

    get tabs(): FormTab2[] {
        return this._tabs;
    }

    _config: IForm;
    private _tabs: FormTab2[] = [];
    private _activeTab: BehaviorSubject<FormTab2>;

    constructor(config: IForm) {
        super(config);
        // this._activeTab = new BehaviorSubject()
        // this._tabs = config.tabs.map(tab => ComponentSingleton.getInstance().getComponent(tab)) as FormTab2[];
        // console.log(this._tabs);
    }

    public initForm(parentUrl: string): Promise<void> {
        return new Promise<void>(resolve => {
            resolve();
            // this._parentUrl = parentUrl;
            //
            // // List of actions that needs to be done before building the form
            // let promises: Promise<boolean>[] = [];
            // this._preInits.forEach(obj => {
            //     promises.push(obj.activateForm(this));
            // });
            //
            // Promise.all(promises).then(() => {
            //
            //     // Initializes the tabs
            //     this._tabs.forEach(tab => {
            //         tab.init();
            //     });
            //
            //     this._activeTab.next(this.tabs[0]);
            //     resolve();
            // });
        })
    }

    // /** navigates to previous tab if there is a previous one */
    // public previousTab(): void {
    //     let currentIndex: number = this.tabs.indexOf(this._activeTab.getValue());
    //     if (currentIndex > 0) {
    //         let previousTabIndex: number = currentIndex - 1;
    //         if (this.tabs[previousTabIndex].show.getValue()) {
    //             this.setActiveTab(this.tabs[previousTabIndex]);
    //         } else {
    //             this.setActiveTab(this.tabs[this.getPreviousTabIndex(this.tabs)]);
    //         }
    //     }
    // }
    //
    // /**
    //  * Navigates to the next tab. It gives the active tab as param to know where to nav to.
    //  * @param activeTab
    //  */
    // public toNextTab(activeTab: FormTab2): void {
    //     let currentIndex: number = this.tabs.indexOf(activeTab);
    //     if (currentIndex < this.tabs.length - 1) {
    //         let nextTabIndex: number = currentIndex + 1;
    //         if (this.tabs[nextTabIndex].show.getValue()) {
    //             this.navigateToTab(this.tabs[nextTabIndex]);
    //         } else {
    //             this.navigateToTab(this.tabs[this.getNextTabIndex(nextTabIndex, this._tabs)]);
    //         }
    //     } else {
    //         console.log('akkoord versturen valid: ', this._activeTab.value.valid);
    //         if (this._activeTab.getValue().valid) {
    //             this.sendForm();
    //         }
    //     }
    // }
    //
    // private getNextTabIndex(startPosition: number, tabs: FormTab2[]): number {
    //     for (let i = startPosition; i < tabs.length - 1; i++) {
    //         if (tabs[i].show.getValue()) {
    //             return i;
    //         }
    //     }
    //
    //     return tabs.length - 1
    // }
    //
    // private getPreviousTabIndex(tabs): number {
    //     let slicedArray = tabs.slice().reverse().slice((tabs.slice().reverse().indexOf(this._activeTab.getValue()) + 1));
    //     let previousUrl = slicedArray[slicedArray.findIndex(tab => tab.show.getValue() === true)].url;
    //     const index = this.tabs.findIndex(tab => tab.url === previousUrl);
    //     return index === -1 ? -1 : index;
    // }
    //
    // /** Returns the tab that you may navigate to */
    // public navigateToTab(targetTab: FormTab2): void {
    //     if (targetTab === this._activeTab.getValue()) {
    //         return;
    //     }
    //
    //     let targetTabIndex: number = this.tabs.indexOf(targetTab);
    //     let currentTabIndex: number = this.tabs.indexOf(this.activeTab.getValue());
    //
    //     if (targetTabIndex > currentTabIndex) {
    //         if (this._activeTab.getValue().validate()) {
    //             this.setActiveTab(this.getNextInvalidTab(targetTab));
    //         }
    //     } else {
    //         this.setActiveTab(targetTab);
    //     }
    // }
    //
    // private getNextInvalidTab(targetTab: FormTab2): FormTab2 {
    //     for (let tab of this.tabs) {
    //         if (tab === targetTab) {
    //             return targetTab;
    //         }
    //
    //         if (tab.show.getValue()) {
    //             if (!tab.valid) {
    //                 return tab;
    //             }
    //         }
    //     }
    // }
    //
    // // handigheidje om alle id's etc van velden te krijgen
    // public getFormConfig(minimized: boolean): void {
    //     let result: any[] = [];
    //
    //     if (minimized) {
    //         this.tabs.forEach(tab => {
    //             tab.children.forEach(panel => {
    //                 panel.children.forEach(set => {
    //                     set.children.forEach(obj => {
    //                         let field = obj as FormField;
    //
    //                         if (field.sendToBE) {
    //                             result.push(field.getConfig(true))
    //                         }
    //                     })
    //                 })
    //             })
    //         });
    //     } else {
    //         this._tabs.forEach(tab => {
    //             result.push(tab.getConfig(minimized));
    //         })
    //     }
    //
    //     let obj: any = {
    //         formID: this.id,
    //         fields: result
    //     };
    //
    //     this._injector.get(HttpClient).post('check-network-tab', obj).subscribe(res => {
    //
    //     });
    // }
    //
    // public setupShowConditions(): void {
    //     this._tabs.forEach(tab => {
    //         tab.setupShowConditions();
    //     });
    // }
    //
    // public logout(): void {
    //     localStorage.removeItem('token');
    //     localStorage.removeItem('selectedForm2');
    //     location.reload();
    // }
    //
    // public downloadPDF(): void {
    //     let result = {
    //         "formBlocks": [],
    //         "bijlagen": this._injector.get(FileUpload2Service).getbijlageIDs(),
    //         "omschrijving": "",
    //         "id": this.id
    //     };
    //
    //     result.formBlocks = this.fillFormBlocks(this.tabs);
    //
    //     let defaultHeaders = {
    //         'Content-Type': 'application/json; charset=UTF-8',
    //         'Accept': 'application/pdf',
    //     };
    //
    //     let token: any = JSON.parse(window.localStorage.getItem('token'));
    //     if (token) {
    //         defaultHeaders['X-Auth-Token'] = token.value;
    //     }
    //
    //     this._injector.get(HttpClient).post(environment.baseUrl + 'eforms-api/controller/pdf/' + this.zaakTypeCode + '/' + this.zaaknummer, result, {
    //         headers: defaultHeaders,
    //         responseType: 'blob'
    //     }).subscribe((response: any) => {
    //             const blob = new Blob([response], {type: 'application/pdf'});
    //             saveAs(blob, this._title + ' - ' + new DatePipe('nl-NL').transform(new Date(), 'dd-MM-yyyy'));
    //         }, error => {
    //             console.error("Error", error);
    //         }
    //     );
    // }
    //
    // public sendForm(): void {
    //     // TODO Dit moet worden bekeken met de BE. Is dubbel
    //     let promises: Promise<any>[] = [];
    //     this._extraDataObj.forEach(data => {
    //         promises.push(data.inhoud.makeTheCall());
    //     });
    //
    //     Promise.all(promises).then(res => {
    //         let payload = this.getPayload(res);
    //
    //         let defaultHeaders = {
    //             'Content-Type': 'application/json; charset=UTF-8',
    //         };
    //
    //         let token: any = JSON.parse(window.localStorage.getItem('token'));
    //         if (token) {
    //             defaultHeaders['X-Auth-Token'] = token.value;
    //         }
    //
    //         this.sendFormRequest(payload, defaultHeaders);
    //     });
    // }
    //
    // protected clearForm(): void {
    //     this._activeTab.next(this._tabs[0]);
    //     this._tabs.forEach(tab => {
    //         tab.clear();
    //     });
    // }
    //
    // protected getPayload(extraData: any[]): any {
    //     let obj: { [k: string]: any } = {};
    //     obj.formBlocks = this.fillFormBlocks(this.tabs);
    //     obj.bijlagen = this._injector.get(FileUpload2Service).getbijlageIDs();
    //     obj.omschrijving = this.verseonOmschrijving;
    //     obj.id = this.id;
    //
    //     // TODO Moet met Edwin overleggen, kan veel beter in de BE.
    //     obj.extra = this.fillExtraData(extraData);
    //
    //     // If the user want to send extra data to the BE.
    //     if (this._customProperty) {
    //         obj[this._customProperty] = this.getKeyValueFromFields();
    //     }
    //     return obj;
    // }
    //
    // // This is for the techincal values that needs to be send to the DB / TODO validation
    // public getKeyValueFromFields(): any {
    //     let obj: { [k: string]: any } = {};
    //     for (let tab of this.tabs) {
    //         let tabData: any = tab.getTechnicalValue();
    //         Object.keys(tabData).forEach(key => {
    //             obj[key] = tabData[key]
    //         });
    //     }
    //
    //     return obj;
    // }
    //
    // private fillExtraData(data: any[]): any {
    //     let result: any = {};
    //
    //     data.forEach(i => {
    //         Object.keys(i).forEach(key => {
    //             result[key] = i[key];
    //         })
    //     });
    //
    //     return result;
    // }
    //
    // protected sendFormRequest(payload: {}, defaultHeaders: {}): void {
    //     switch (this._sendTo) {
    //         case FormSendTypes.VERSEON: {
    //             this._injector.get(HttpClient).post(environment.baseUrl + 'eforms-api/controller/zaaksysteem/' + this.zaakTypeCode, payload, {headers: defaultHeaders})
    //                 .subscribe((res: any) => {
    //                         this._zaaknummer = res['statusTekst'];
    //                         // this.isFormSend.next(true);
    //
    //                         // TODO These codes need to be fixed. not all calls have the same codes in the response for the scenario
    //                         if (res.statusCode === 201 || res.blocking) {
    //                             this._injector.get(ScreenMessageService).setError({
    //                                 type: 'danger',
    //                                 message: res.statusTekst
    //                             });
    //                             window.scroll(0, 0);
    //                         } else {
    //                             this.disableBackButton();
    //                             this._injector.get(Router).navigate([this._parentUrl + '/dank-scherm']).then(() => {
    //                             	localStorage.removeItem(this.id);
    //                             });
    //                         }
    //                     }, (error) => {
    //                         this._injector.get(ScreenMessageService).setError({
    //                             type: 'danger',
    //                             message: error.error.statusTekst
    //                         });
    //
    //                         window.scroll(0, 0);
    //                     }
    //                 );
    //
    //             break;
    //         }
    //         case FormSendTypes.MAIL: {
    //             this._injector.get(HttpClient).post(environment.baseUrl + 'eforms-api/controller/email', payload, {headers: defaultHeaders})
    //                 .subscribe((res: any) => {
    //                         this._zaaknummer = res['statusTekst'] === 'Verzonden' ? '' : res['statusTekst'];
    //
    //                         // TODO These codes need to be fixed. not all calls have the same codes in the response for the scenario
    //                         if (res.statusCode === 201 || res.blocking) {
    //                             this._injector.get(ScreenMessageService).setError({
    //                                 type: 'danger',
    //                                 message: res.statusTekst
    //                             });
    //                             // window.scroll(0, 0);
    //                         } else {
    //                             this.disableBackButton();
    //                             this._injector.get(Router).navigate([this._parentUrl + '/dank-scherm']).then(() => {
    //                                 localStorage.removeItem(this.id);
    //                             });
    //                         }
    //                     }, error => {
    //                         console.error("Error", error);
    //                     }
    //                 );
    //             break;
    //         }
    //     }
    // }
    //
    // protected disableBackButton() {
    //     window.onload = () => {
    //         window.history.forward()
    //     };
    //     window.onpageshow = (evts) => {
    //         if (evts.persisted) window.history.forward()
    //     };
    //     window.onunload = () => {
    //         void (0)
    //     };
    // }
    //
    // protected fillFormBlocks(tabs: FormTab2[]): {}[] {
    //     let result = [];
    //
    //     for (let tab of tabs) {
    //         // TODO Get object from child, dont go nesting
    //         if (tab.sendToBE && tab.show.getValue()) {
    //
    //             /** everything else than -1 means the BE needs to prefill it again */
    //             let prefill: boolean = false;
    //             if (tab.idp !== -1) {
    //                 prefill = true;
    //             }
    //
    //             let formFields = {
    //                 "formFields": [],
    //                 "titel": tab.label,
    //                 "prefill": prefill,
    //                 'identityOrder': tab.idp,
    //                 "hidden": false
    //             };
    //
    //             for (let fieldSet of tab.children) {
    //                 for (let set of fieldSet.children) {
    //                     if (set.sendToBE && set.show.getValue()) {
    //                         for (let field of set.children) {
    //                             if (field.sendToBE && field.show.getValue()) {
    //                                 // TODO FIX
    //                                 // if (field.commaToDot) {
    //                                 // 	formFields.formFields.push(field.getJsonValueConvert());
    //                                 // } else {
    //                                 // 	formFields.formFields.push(field.getJsonValue());
    //                                 // }
    //                                 formFields.formFields.push(field.getPdfValue());
    //                             }
    //                         }
    //                     }
    //                 }
    //             }
    //             result.push(formFields);
    //         }
    //     }
    //
    //     return result;
    // }
    //
    // public setActiveTab(tab: FormTab2): void {
    //     this._activeTab.next(tab);
    //     this._injector.get(Location).replaceState(this._parentUrl + '/' + this.url + '/' + tab.url);
    //     window.scroll(0, 0);
    // }
}
