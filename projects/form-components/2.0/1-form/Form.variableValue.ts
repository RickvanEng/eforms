import {Injector} from "@angular/core";
import {BehaviorSubject} from "rxjs";
import {Form2, FormBuilder} from "./Form";

export class VariableValueFormBuilder extends FormBuilder {

    public static newBuilder(injector: Injector): VariableValueFormBuilder {
        return new VariableValueFormBuilder(injector);
    }

    get variableValue(): string {
		return this._variableValue;
	}

    public setVariableValue(variableValue: string): FormBuilder {
		this._variableValue = variableValue;
		return this;
    }

    public _variableValue: string;

    public build(): Form2 {
		if (!this.field) {
			this._verseonOmschrijvingBuilder.forEach(entry => {
				let info: any[] = [];
				if (entry.showcondition) {
					entry.showcondition.field = entry.showcondition.fieldBuilder.build();
				}
				entry.info.forEach(obj => {
					if (obj.field) {
						info.push({prefix: obj.prefix, field: obj.field.build()})
					} else {
						info.push({prefix: obj.prefix, field: undefined})
					}
					
				});
				this._verseonOmschrijving.push({showcondition: entry.showcondition, info: info});
			});
			this._tabBuilders.forEach(builder => {
				this._tabs.push(builder.build());
			});
			this.field = new FormVariableValue(this);
		}
		return this.field;
	}
}

class FormVariableValue extends Form2 {

    get variableValue() {
		return this._variableValue;
	}

	//public _variableValue: string;
	protected _variableValue: BehaviorSubject<string>;

    constructor(builder: VariableValueFormBuilder) {
        super(builder);
		this._variableValue = new BehaviorSubject(builder.variableValue);
    }
    
    // TODO deze moet worden aangepast met die interpolate nieuwe functie
	public initForm(parentUrl: string): Promise<void> {
		for (let tab of this.tabs) {
			tab.updateVariableContent('{monthReplace}', this._variableValue.value);
		}
		return super.initForm(parentUrl);
	}
}
