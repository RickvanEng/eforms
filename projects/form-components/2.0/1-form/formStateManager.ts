import {Form2} from './Form';
import {Injectable} from '@angular/core';
import {BehaviorSubject} from 'rxjs';

export class IFormState {
	formID: string;
	activeTab: string;
	components: any;
	ttl: string;
}

export class FormState implements IFormState {
	get components(): any {
		return this._components;
	}
	
	set activeTab(value: string) {
		this._activeTab = value;
		this.saveState();
	}
	
	set formID(value: string) {
		this._formID = value;
	}
	
	get formID(): string {
		return this._formID;
	}
	
	get activeTab(): string {
		return this._activeTab;
	}
	
	get ttl(): string {
		return this._ttl;
	}
	
	private _formID: string;
	private _activeTab: string = '';
	private _components: any = {};
	private _ttl: string = '';
	
	constructor(state?: IFormState) {
		if (state) {
			this._formID = state.formID;
			this._activeTab = state.activeTab;
			this._components = state.components;
			this._ttl = state.ttl;
		}
	}
	
	public getComponentValue(id: string): any {
		return this._components[id];
	}
	
	private saveState(): void {
		localStorage.setItem(this._formID, JSON.stringify(this.getStateAsJSON()));
	}
	
	private getStateAsJSON(): any {
		return {
			formID: this._formID,
			activeTab: this._activeTab,
			components: this._components,
			ttl: this._ttl,
		}
	}
}

@Injectable({
	providedIn: 'root'
})
export class FormStateManager {
	get oldState(): FormState {
		return this._oldState;
	}
	
	get activeState(): FormState {
		return this._activeState;
	}
	
	get isStateLoaded(): BehaviorSubject<boolean> {
		return this._isStateLoaded;
	}
	
	private _isStateLoaded: BehaviorSubject<boolean> = new BehaviorSubject(false);
	
	
	private _oldState: FormState;
	private _activeState: FormState;
	
	public initFormState(form: Form2): FormState {
		if (this.getState(form.id)) {
			const newStateObj: FormState = new FormState(JSON.parse(this.getState(form.id)));
			this._oldState = new FormState(JSON.parse(this.getState(form.id)));
			this._activeState = newStateObj;
			return newStateObj;
		} else {
			const newState = new FormState();
			newState.formID = form.id;
			this._oldState = new FormState();
			this._activeState = newState;
			return newState;
		}
	}
	
	private getState(id: string): any {
		return localStorage.getItem(id);
	}
	
}
