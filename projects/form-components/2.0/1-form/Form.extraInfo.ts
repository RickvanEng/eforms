import {Injector} from "@angular/core";
import {Form2, FormBuilder} from "./Form";

export class ExtraInfoFormBuilder extends FormBuilder {

    public static newBuilder(injector: Injector): ExtraInfoFormBuilder {
        return new ExtraInfoFormBuilder(injector);
    }

    public build(): Form2 {
        if (!this.field) {
            this._verseonOmschrijvingBuilder.forEach(entry => {
                let info: any[] = [];
                if (entry.showcondition) {
                    entry.showcondition.field = entry.showcondition.fieldBuilder.build();
                }
                entry.info.forEach(obj => {
                    if (obj.field) {
                        info.push({prefix: obj.prefix, field: obj.field.build()})
                    } else {
                        info.push({prefix: obj.prefix, field: undefined})
                    }

                });
                this._verseonOmschrijving.push({showcondition: entry.showcondition, info: info});
            });
            this._tabBuilders.forEach(builder => {
                this._tabs.push(builder.build());
            });
            this.field = new FormExtraInfo(this);
        }
        return this.field;
    }
}

class FormExtraInfo extends Form2 {

    // protected _extraDataObj: { id: string, inhoud: GetDataFromBe[]}[];
    //
    // public getPayload(): {} {
    //
    //     return {
    //         formBlocks: this.fillFormBlocks(this.tabs),
    //         bijlagen: this._injector.get(FileUploadService).getbijlageIDs(),
    //         omschrijving: this.verseonOmschrijving,
    //         id: this.id,
    //         extra: this.getExtraData(this._extraDataObj),
    //     };
    // }
}
