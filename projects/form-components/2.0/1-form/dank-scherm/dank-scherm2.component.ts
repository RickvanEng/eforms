import {Component, OnInit} from '@angular/core';
import {Form2} from '../Form';
import {FormService} from '../form.service';
import {faFacebookSquare, faInstagram, faTwitterSquare} from '@fortawesome/free-brands-svg-icons';
import {LocationStrategy} from '@angular/common';

@Component({
	selector: 'app-dank-scherm',
	templateUrl: './dank-scherm2.component.html',
	styleUrls: ['./dank-scherm2.component.scss']
})
export class DankScherm2Component implements OnInit {
	
	public form: Form2;
	public faFacebookSquare = faFacebookSquare;
	public faTwitterSquare = faTwitterSquare;
	public faInstagram = faInstagram;
	
	constructor(private location: LocationStrategy) {
		history.pushState(null, null, window.location.href);
		this.location.onPopState(() => {
			history.pushState(null, null, window.location.href);
		});
	}
	
	ngOnInit() {
		this.form = FormService.getInstance().selectedForm;
	}
	
	public downloadForm(): void {
		this.form.downloadPDF();
	}
	
}
