import {Injectable, Injector} from '@angular/core';
import {ActivatedRoute, CanActivate, Router} from '@angular/router';
import {Observable} from 'rxjs';
import {FormService} from '../form.service';
import {Form2} from '../Form';

@Injectable()
export class DankschermAuthguard implements CanActivate {

    constructor(private inj: Injector,
                private route: ActivatedRoute,
                private router: Router,
    ) {
    }

    canActivate(): Observable<boolean> | Promise<boolean> | boolean {
        let selectedForm: Form2 = FormService.getInstance().selectedForm;

        if (!selectedForm) {
            this.router.navigate([window.location.pathname.split('/')[1]])
        }

        return true;
    }
}
