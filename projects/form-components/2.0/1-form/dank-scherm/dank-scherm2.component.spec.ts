import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {DankScherm2Component} from './dank-scherm2.component';

describe('DankSchermComponent', () => {
  let component: DankScherm2Component;
  let fixture: ComponentFixture<DankScherm2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DankScherm2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DankScherm2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
