import {Injector} from '@angular/core';
import { FormField, FormFieldBuilder } from '../../5-fields/form-field/FormField';

export class DankSchermBuilder {
	get injector(): Injector {
		return this._injector;
	}
	get html(): string {
		return this._html;
	}
	get naam(): FormField {
		return this._naam;
	}
	get tussenvoegsel(): FormField {
		return this._tussenvoegsel;
	}
	get achternaam(): FormField {
		return this._achternaam;
	}
	get label(): string {
		return this._label;
	}
	get extraBody(): string {
		return this._extraBody;
	}
	get reactietermijn(): string {
		return this._reactietermijn;
	}
	
	private _html: string;
	private _naam: FormField;
	private _achternaam: FormField;
	private _naamBuilder: FormFieldBuilder;
	private _tussenvoegsel: FormField;
	private _tussenvoegselBuilder: FormFieldBuilder;
	private _achternaamBuilder: FormFieldBuilder;
	private _label: string;
	private _extraBody: string;
	private _reactietermijn: string;
	
	constructor(private _injector: Injector){}
	
	public static newBuilder(injector: Injector): DankSchermBuilder {
		return new DankSchermBuilder(injector);
	}
	
	public setInfo(info: {naam: FormFieldBuilder, tussenvoegsel?: FormFieldBuilder, achternaam: FormFieldBuilder, label: string, extraBody?: string, reactietermijn: string, html?: string}): DankSchermBuilder {
		this._naamBuilder = info.naam;
		if(info.tussenvoegsel) { this._tussenvoegselBuilder = info.tussenvoegsel; }
		this._achternaamBuilder = info.achternaam;
		this._label = info.label;
		if(info.reactietermijn === '' || info.reactietermijn === undefined) {
			this._reactietermijn = undefined;
		} else {
			this.setReactietermijn(info.reactietermijn);
		}
		if(info.html) { this._html = info.html; }
		if(info.extraBody) { this._extraBody = info.extraBody; }
		return this;
	}

	public setNaamBuilder(buildMe) {
		this._naamBuilder = buildMe;
	}
	public setTussenvoegselBuilder(buildMe) {
		this._tussenvoegselBuilder = buildMe;
	}
	public setAchternaamBuilder(buildMe) {
		this._achternaamBuilder = buildMe;
	}

	public setReactietermijn(reactietermijn: string) {
		this._reactietermijn = reactietermijn;
		return this;
	}
	
	public build(): DankScherm {
		if(this._naamBuilder) {
			this._naam = this._naamBuilder.build();
		}
		if(this._tussenvoegselBuilder) {
			this._tussenvoegsel = this._tussenvoegselBuilder.build();
		}
		if(this._achternaamBuilder) {
			this._achternaam = this._achternaamBuilder.build();
		}
		return new DankScherm(this);
	}
}

export class DankScherm {
	get html(): string {
		return this._html;
	}
	get naam(): FormField {
		return this._naam;
	}
	get achternaam(): FormField {
		return this._achternaam;
	}
	get tussenvoegsel(): FormField {
		return this._tussenvoegsel;
	}
	get label(): string {
		return this._label;
	}
	get extraBody(): string {
		return this._extraBody;
	}
	get reactietermijn(): string {
		return this._reactietermijn;
	}
	
	private _html: string;
	private injector: Injector;
	private _naam: FormField;
	private _tussenvoegsel: FormField;
	private _achternaam: FormField;
	private _label: string;
	private _extraBody: string;
	private _reactietermijn: string;
	
	constructor(builder: DankSchermBuilder) {
		this.injector = builder.injector;
		this._html = builder.html;
		this._naam = builder.naam;
		this._tussenvoegsel = builder.tussenvoegsel;
		this._achternaam = builder.achternaam;
		this._label = builder.label;
		this._extraBody = builder.extraBody;
		this._reactietermijn = builder.reactietermijn;
	}

	public setHtml(info: string) {
		this._html = info;
	}

}
