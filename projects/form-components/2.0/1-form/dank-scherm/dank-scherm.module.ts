import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {DankScherm2Component} from './dank-scherm2.component';
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';

@NgModule({
	imports: [
		CommonModule,
		FontAwesomeModule
	],
	declarations: [
		DankScherm2Component
	],
	exports: [
		DankScherm2Component
	]
})
export class DankSchermModule {
}
