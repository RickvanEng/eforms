import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Injector} from "@angular/core";
import {Observable} from "rxjs";
import {environment} from "src/environments/environment";
import {Auth2Service} from "../services/auth2.service";

export class GetDataFromBeResult {

	set data(value: string) {
		this._data = value;
	}
	
	get data(): string {
		return this._data;
	}

	private _data: string;

	constructor() {
	}
}

// TODO dit hele ding kan weg? Kan veel makkelijker in de BE. Wij halen het nu op, om het vervolgens weer terug te sturen...?
export class GetDataFromBeBuilder {
	get id(): string {
		return this._id;
	}

	get injector(): Injector {
		return this._injector;
	}
	get endPoint(): string {
		return this._endPoint;
	}
	get callType(): string {
		return this._callType;
	}
	get headers(): any {
		return this._headers;
	}

    private _endPoint: string;
    private _id: string;
	private _callType: string = 'post';
	private _headers: HttpHeaders = new HttpHeaders({
		'Content-Type': 'application/json'
    });
    
    constructor(private _injector: Injector) {}
	
	public static newBuilder(injector: Injector): GetDataFromBeBuilder {
		return new GetDataFromBeBuilder(injector);
	}
	
	public setEndPoint(endPoint: string): GetDataFromBeBuilder {
		this._endPoint = endPoint;
		return this;
	}
	
	public setCallType(type: 'get' | 'post'): GetDataFromBeBuilder {
		this._callType = type;
		return this;
	}
	
	public setID(id: string): GetDataFromBeBuilder {
    	this._id = id;
    	return this;
	}
	
	public build(): GetDataFromBe {
		return new GetDataFromBe(this);
	}

}


export class GetDataFromBe {

	protected _injector: Injector;
	protected _endPoint: string;
	protected _id: string;
	protected _callType: string;
	protected _headers: HttpHeaders;
	
	constructor(b: GetDataFromBeBuilder) {
		this._injector = b.injector;
		this._endPoint = b.endPoint;
		this._headers = b.headers;
		this._callType = b.callType;
		this._id = b.id;
	}
	
	public makeTheCall(): Promise<any> {
		switch (this._callType) {
			case 'get': {
				return new Promise(resolve => {
					this.makeGetCall().subscribe(res => {
						//resolve(this.handleResult({"bsn":"900174316","zaaknummer":"2550857","email":"steven.heye@haarlemmermeer.nl","telNummer":"0123456789","periode":["januari februari maart"],"partner":false}));
						resolve(this.handleResult(res.data));
					})
				});
			}
			case 'post': {
				return new Promise(resolve => {
					this.makeGetCall().subscribe(res => {
						resolve(this.handleResult(res.data));
					})
				});
			}
		}
	}
	
	public makeGetCall(): Observable<any> {
		this._endPoint = this._endPoint + this._injector.get(Auth2Service).loginToken.value.value;
		this._headers = this._headers.set('X-Auth-Token', this._injector.get(Auth2Service).loginToken.value.value);
		return this._injector.get(HttpClient).get(environment.baseUrl + this._endPoint, {headers: this._headers});
	}

	protected handleResult(data: any) {
		let result: any = {};
		
		Object.keys(data).forEach(key => {
			result[key] = data[key]
		});
		
		return result;
	}

}
