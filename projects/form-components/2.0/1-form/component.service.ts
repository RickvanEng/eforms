import {CmsComponent} from "../cmsComponent";
import {Injectable, Injector} from '@angular/core';
import {CmsAbstractControl} from '../cmsAbstractControl';

@Injectable({
	providedIn: 'root',
})
export class ComponentService {
	private components: Record<string, CmsComponent> = {};
	private formComponents: Record<string, CmsAbstractControl> = {};

	constructor(private inj: Injector) {

	}
	
	public getComponent(id: string): CmsComponent {
		return this.components[id];
	}

	public getFormComponent(id: string): CmsAbstractControl {
		return this.formComponents[id];
	}

	public registerComponent(newComp: CmsComponent): void {
		if (newComp.id) {
			if (this.components[newComp.id]) {
				console.error('Duplicate comp ID: ', newComp.id);
			} else {
				this.components[newComp.id] = newComp;
			}
		}
	}

	public registerFormComponent(newComp: CmsAbstractControl): void {
		if (newComp.id) {
			if (this.formComponents[newComp.id]) {
				console.error('Duplicate comp ID: ', newComp.id);
			} else {
				console.log(newComp._config.id)
				newComp.constructFormControl(this.inj);
				this.formComponents[newComp.id] = newComp;
			}
		}
	}

	public reset(): void {
		this.components = {};
		this.formComponents = {};
	}
}
