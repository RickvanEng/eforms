import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {LandingComponent} from './landing.component';
import {FormRoutingModule} from "../0-routing/form-routing/form-routing.module";

@NgModule({
    imports: [
        CommonModule,
        FormRoutingModule
    ],
    declarations: [LandingComponent]
})
export class LandingModule {
}
