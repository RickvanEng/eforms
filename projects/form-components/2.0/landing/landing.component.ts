import {Component, OnInit} from '@angular/core';
import {FormRouting2} from "../0-routing/form-routing/formRouting";
import {ActivatedRoute, Router} from "@angular/router";
import {Auth2Service} from "../services/auth2.service";
import {formMaps} from "../../../../src/app/app-inits/formMap";
import {IFormRouting} from "../0-routing/form-routing/IFormRouting";
import {CmsComponentFactory} from '../services/form.factory';
import {loginClassMap} from '../services/loginClassMap';

@Component({
    selector: 'app-landing',
    templateUrl: './landing.component.html',
    styleUrls: ['./landing.component.scss']
})
export class LandingComponent implements OnInit {

    public selectedRouting: FormRouting2;

    constructor(private authS: Auth2Service,
                private route: ActivatedRoute,
                private router: Router) {
    }

    ngOnInit() {
        this.route.queryParams.subscribe(params => {
            if (params.idp) {
                console.log('hij heeft query parms');
                Promise.all([
                        this.authS.saveQueryParams(params),
                    ]
                ).then(() => {
                    this.getRouting();

                    // TODO FIX offline formulieren

                    // let url: string = this.inj.get(Router).url.substring(0, this.inj.get(Router).url.indexOf('?'));
                    // this.inj.get(Router).navigateByUrl(url);

                    // let promises: Promise<any>[] = [];
                    // this.routing.forms.forEach(form => {
                    //     promises.push(form.getFormStatusFromDB());
                    // });
                    //
                    // Promise.all(promises).then((res: any[]) => {
                    //     if (EStatus[res[0].status] === EStatus.OFFLINE) {
                    //         this.inj.get(Router).navigate(['offline'], {relativeTo: this.inj.get(ActivatedRoute)});
                    //     } else {
                    //         this.inj.get(Router).navigate(['login-portal'], {relativeTo: this.inj.get(ActivatedRoute)});
                    //     }
                    // });
                });
            } else {
                console.log('no params');
                this.getRouting();
            }
        });


    }

    private getRouting(): void {
        this.route.params.subscribe((params: any) => {
            if (params.routing) {
                let selectedRouting: IFormRouting = formMaps[params.routing] ? formMaps[params.routing].getConfig() : undefined;
                let loginComponents: any = CmsComponentFactory.build(selectedRouting.loginConfigs, loginClassMap);
                this.selectedRouting = new FormRouting2(selectedRouting, loginComponents);
            }
        });
    }

    onActivate(): void {
        window.scroll(0, 0);
    }
}
