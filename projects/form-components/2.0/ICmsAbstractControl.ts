import {ICmsComponent} from './ICmsComponents';
import {IValidator} from './validators/IValidator';

export interface ICmsAbstractControl extends ICmsComponent {
    basicValidators: IValidator[];
    asyncValidators: IValidator[];
}
