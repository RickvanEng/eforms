import {IShowCondition} from "./IShowCondition";

export class ShowCondition {
    get error(): string {
        return this.config.listenToError;
    }

    get field(): string {
        return this.config.listenToField;
    }

    get option(): string {
        return this.config.listenToOption;
    }

    private config: IShowCondition;

    constructor(config: IShowCondition) {
        this.config = config;
    }
}
