import {IShowCondition} from "./IShowCondition";

export class ShowConditionsBuilder {
    private config: IShowCondition = {
        invert: false,
        listenToError: '',
        listenToField: '',
        listenToOption: ''
    };

    public static newBuilder(): any {
        return new ShowConditionsBuilder();
    }

    public setListenToField(field: string): ShowConditionsBuilder {
        this.config.listenToField = field;
        return this;
    }

    public setListenOption(option: string): ShowConditionsBuilder {
        this.config.listenToOption = option;
        return this;
    }

    public setListenError(error: string): ShowConditionsBuilder {
        this.config.listenToError = error;
        return this;
    }

    public setInvert(value: boolean): ShowConditionsBuilder {
        this.config.invert = value;
        return this;
    }

    public build(): IShowCondition {
        return this.config;
    }
}