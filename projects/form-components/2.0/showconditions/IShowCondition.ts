export class IShowCondition {
    listenToError: string;
    listenToOption: string;
    listenToField: string;
    invert: boolean;
}