import {Injector} from '@angular/core';
import {LoginTile} from './form-routing/login-portal/login-tile/login-tile';
import {Form2, FormBuilder, IForm} from '../1-form/Form';
import {Auth2Service, TokenObject} from '../services/auth2.service';
import {environment} from '../../../../src/environments/environment';
import {FormEnum} from '../../../forms/services/sendform/sendGegevensPerForm';
import {HttpClient} from '@angular/common/http';
import {IFormDashConfig, IFormRoutingCMS} from './form-routing/formRouting';
import {ICmsComponent} from '../ICmsComponents';
import {CmsComponentBuilder} from '../cmsComponent.builder';
import {ILoginTile} from './form-routing/login-portal/login-tile/ILoginTile';
import {CmsComponentFactory} from '../services/form.factory';
import {cmsClassMap} from '../services/cmsClassMap';
import {IBase} from '../base/IBase';
import {ICmsAbstractControl} from '../ICmsAbstractControl';

export enum ELoginTypes2 {
    DIGID = 0,
    EHERK = 2,
    LDAP = 3,
    NONE = -1
}

export interface IFormConfig extends ICmsComponent {
    components: ICmsAbstractControl[];
    loginType: number;
    loginTile: ILoginTile;
    loginURL: string;

    form: string;
}

export class FormConfigBuilder extends CmsComponentBuilder {

    _config: IFormConfig = {
        components: [],
        id: undefined,
        className: undefined,
        form: '',
        loginTile: undefined,
        loginType: 0,
        loginURL: ''
    };

    public static newBuilder(): FormConfigBuilder {
        return new FormConfigBuilder();
    }

    public setLoginTile(value: ILoginTile): FormConfigBuilder {
        this._config.loginTile = value;
        return this;
    }

    public setComponents(components: ICmsAbstractControl[]): FormConfigBuilder {
        this._config.components = this._config.components.concat(...components);
        return this;
    }

    public setLoginUrl(url: string): FormConfigBuilder {
        this._config.loginURL = url;
        return this;
    }

    public setForm(form: string): FormConfigBuilder {
        this._config.form = form;
        return this;
    }

    public build(): IFormConfig {
        return this._config;
    }
}

export class FormConfig {
    // get dashConfig(): IFormDashConfig {
    //     return this._config.dashConfig;
    // }

    get idp(): ELoginTypes2 {
        return this._config.loginType;
    }

    get formID(): string {
        return this._config.form;
    }

    get loginTile(): ILoginTile {
        return this._config.loginTile;
    }

    // get form(): Form2 {
    //     return this.activeForm;
    // }

    get components(): ICmsComponent[] {
        return this._config.components;
    }

    // get loginTile(): LoginTile {
    //     return this._loginTile;
    // }

    public loggedIn: boolean = false;

    private _config: IFormConfig;
    private activeForm: Form2;

    constructor(config: IFormConfig) {
        this._config = config;

        // this._injector.get(Auth2Service).loginToken.subscribe(token => {
        //     if (token) {
        //         this.loggedIn = (token.idp === this.loginType.toString());
        //     } else {
        //         this.loggedIn = false;
        //     }
        // })
    }

    // public getIdpLoginUrl(): string {
    //     return this._formBuilder.url;
    // }

    // TODO moet dit niet met een preinit?
    public isAllowedToLoad(): boolean {
        // if (this.loginType === ELoginTypes2.NONE) {
        //     return true;
        // }
        // let token: TokenObject = this._injector.get(Auth2Service).loginToken.value;
        // return token ? token.idp === this.loginType.toString() : false;
        return true;
    }

    public getFormStatusFromDB(): Promise<IFormRoutingCMS> {


        return new Promise<IFormDashConfig>(resolve => {
            // resolve({
            //     id: this._formID,
            //     status: 'ONLINE',
            //     message: '',
            //     designUrl: '',
            //     description: '',
            // });

            // this._injector.get(HttpClient).get(environment.baseUrl + 'eforms-api/controller/form/status/form/' + this.formID).toPromise().then((res: IFormDashConfig) => {
            //     resolve({
            //         id: this._formID,
            //         status: 'ONLINE',
            //         message: '',
            //         designUrl: '',
            //         description: '',
            //     });
            // });
        });
    }

    public getDashConfig(): Promise<IFormDashConfig> {
        return new Promise<IFormDashConfig>(resolve => {
            // resolve({
            //     id: this._formID,
            //     status: 'ONLINE',
            //     message: '',
            //     designUrl: '',
            //     description: '',
            // });

            // if (this._dashConfig) {
            //     resolve(this._dashConfig);
            // } else {
            //     this._injector.get(HttpClient).get(environment.baseUrl + 'eforms-api/controller/form/dashboard/form/' + this.formID).toPromise().then((res: IFormDashConfig) => {
            //         this._dashConfig = (res);
            //         resolve(res);
            //     });
            // }
        });
    }

    public update(data: IFormDashConfig): void {
        // Object.keys(data).forEach(key => {
        //     if (data[key] && key !== 'id') {
        //         this._dashConfig[key] = data[key];
        //     }
        // });
        //
        // this._injector.get(HttpClient).put(environment.baseUrl + 'eforms-api/controller/form/dashboard/status', {forms: [this._dashConfig]}).subscribe(() => {
        //
        // });
    }

    // public get form(): Form2 {
    //     if (!this._form) {
    //         this._form = this._formBuilder
    //             .setLoginType(this._loginType)
    //             .build();
    //     }
    //     return this._form;
    // }

    // public selectForm(): void {
    // 	if (this._loginType !== ELoginTypes2.NONE) {
    // 		let authService: Auth2Service = this._injector.get(Auth2Service);
    // 		authService.loginToken.subscribe(token => {
    // 			if (token) {
    // 				if (token.idp === this.loginType.toString()) {
    // 					// this.form.initForm();
    // 					return;
    // 				}
    // 			}
    // 			this.login();
    // 		});
    // 	} else {
    // 		// this.form.initForm()
    // 	}
    // }

    public login(): void {
        // if (this.loginType === ELoginTypes2.LDAP) {
        //     document.location.href = environment.baseUrl + this._loginURL + this.formID;
        // } else {
        //     document.location.href = environment.baseUrl + this._loginURL;
        // }
    }
}
