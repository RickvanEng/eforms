import {Component, Input, OnInit} from '@angular/core';
import {ESteps, FormRouting2} from './formRouting';
import {Auth2Service} from '../../services/auth2.service';
import {ActivatedRoute, Router} from '@angular/router';
import {TitleService} from '../../../../../src/title.service';
import {FormConfig} from "../formConfig";
import {Form2} from '../../1-form/Form';
import {CmsComponentFactory, FormComponentFactory} from '../../services/form.factory';
import {cmsClassMap} from '../../services/cmsClassMap';
import {ComponentService} from '../../1-form/component.service';
import {CmsComponent} from '../../cmsComponent';

@Component({
    selector: 'app-form-routing',
    templateUrl: './form-routing.component.html',
    styleUrls: ['./form-routing.component.scss']
})
export class FormRoutingComponent implements OnInit {

    @Input() routing: FormRouting2;
    public selectedForm: Form2;

    public currentPhase: ESteps = ESteps.LOGIN;
    public loginPhase: ESteps = ESteps.LOGIN;
    public formPhase: ESteps = ESteps.FORM;
    public dankPhase: ESteps = ESteps.DANK_SCHERM;

    constructor(private titleService: TitleService,
                private authS: Auth2Service,
                private router: Router,
                private compS: ComponentService,
                private actiRoute: ActivatedRoute) {
    }

    ngOnInit() {
        this.titleService.setTitle(this.routing.formName);
        this.authS.loginToken.subscribe(token => {
            console.log('auth token: ', token);
            if (token) {
                this.actiRoute.params.subscribe(res => {
                    // TODO dit moet nog mooier. Hij navt niet als URL hetzelfde, maar is niet netjes denk ik.
                    this.router.navigateByUrl(res.routing);


                    let foundConfig: FormConfig = this.routing.formConfigs.find(config => config.idp === ~~token.idp);

                    FormComponentFactory.build(foundConfig.components, cmsClassMap).forEach(comp => {
                        this.compS.registerFormComponent(comp)
                    });

                    this.selectedForm = this.compS.getFormComponent(foundConfig.formID) as Form2;
                    this.currentPhase = this.formPhase;
                });
            }
        });

        // this.inj.get(Auth2Service).loginToken.subscribe(token => {
        //     if (token) {
        //         console.log('the token: ', token);
        //         this.selectedRouting = this.routing.forms.find(config => config.loginType.toString() === token.idp);
        //     }
        // })


        // this.routing.activeStep.subscribe(phase => {
        //     this.currentPhase = phase;
        // });
        //
        // this.route.queryParams.subscribe((params: any) => {
        //     console.log(params);
        //     if (params.idp) {
        //         Promise.all([
        //                 this.inj.get(Auth2Service).saveQueryParams(params),
        //             ]
        //         ).then(() => {
        //             console.log('params loaded');
        //             console.log(this.inj.get(Router).navigate(['aanbieden-goederen-diensten-2']))
        //
        //             // TODO FIX offline formulieren
        //
        //             // let url: string = this.inj.get(Router).url.substring(0, this.inj.get(Router).url.indexOf('?'));
        //             // this.inj.get(Router).navigateByUrl(url);
        //
        //             // let promises: Promise<any>[] = [];
        //             // this.routing.forms.forEach(form => {
        //             //     promises.push(form.getFormStatusFromDB());
        //             // });
        //             //
        //             // Promise.all(promises).then((res: any[]) => {
        //             //     if (EStatus[res[0].status] === EStatus.OFFLINE) {
        //             //         this.inj.get(Router).navigate(['offline'], {relativeTo: this.inj.get(ActivatedRoute)});
        //             //     } else {
        //             //         this.inj.get(Router).navigate(['login-portal'], {relativeTo: this.inj.get(ActivatedRoute)});
        //             //     }
        //             // });
        //         });
        //     }
        // });
    }

    // public onOutletLoaded(component: FormComponent): void {
    //     component.forms = this.routing.forms;
    // }
}
