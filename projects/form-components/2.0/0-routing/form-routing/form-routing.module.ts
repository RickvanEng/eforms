import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormRoutingComponent} from './form-routing.component';
import {RouterModule} from '@angular/router';
import {OfflinePageComponent} from './offline-page/offline-page.component';
import { LoginPortalModule } from './login-portal/login-portal.module';
import {MultiPageForm2Module} from "../../1-form/form/multi-page-form2.module";

@NgModule({
	imports: [
		CommonModule,
		RouterModule,
		MultiPageForm2Module,
		LoginPortalModule,
	],
	declarations: [
		FormRoutingComponent,
		OfflinePageComponent,
	],
	exports: [
		FormRoutingComponent
	],
	entryComponents: [
		OfflinePageComponent
	]
})
export class FormRoutingModule {
}
