import {Component, OnInit} from '@angular/core';
import {VariableFormService} from '../../../../../forms/services/genericFormService/variableFormService';
import {IFormRoutingCMS} from '../formRouting';

@Component({
	selector: 'app-offline-page',
	templateUrl: './offline-page.component.html',
	styleUrls: ['./offline-page.component.scss']
})
export class OfflinePageComponent implements OnInit {
	
	public cmsData: IFormRoutingCMS;
	
	constructor() {
	}
	
	ngOnInit() {
		VariableFormService.getInstance().formRoutingNew.forms[0].getFormStatusFromDB().then((res: IFormRoutingCMS) => {
			this.cmsData = res;
		});
	}
	
}
