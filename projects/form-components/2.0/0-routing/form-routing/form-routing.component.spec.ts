import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {FormRoutingComponent} from './form-routing.component';

describe('FormRoutingComponent', () => {
  let component: FormRoutingComponent;
  let fixture: ComponentFixture<FormRoutingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormRoutingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormRoutingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
