import {Form2} from '../../1-form/Form';
import {Injector} from '@angular/core';
import {DankScherm2Component} from '../../1-form/dank-scherm/dank-scherm2.component';
import {FormConfig, IFormConfig} from '../formConfig';
import {WarningType} from 'projects/form-components/2.0/5-fields/WarningType';
import {BehaviorSubject} from 'rxjs';
import { Auth2Service } from '../../services/auth2.service';
import {IFormRouting} from "./IFormRouting";
import {CmsComponentBuilder} from "../../cmsComponent.builder";
import {ICmsComponent} from "../../ICmsComponents";

export interface IStepConfig {
	step: ESteps;
	completed: boolean;
	url: string;
}

export enum ESteps {
	LOGIN,
	FORM,
	DANK_SCHERM,
}

export enum EStatus {
	PUBLIC = 'PUBLIC',
	PRIVATE = 'PRIVATE',
	OFFLINE = 'OFFLINE',
}

export interface IFormRoutingCMS {
	id: string;
	status: string;
	message: string;
}

export interface IFormDashConfig extends IFormRoutingCMS{
	designUrl: string;
	description: string;
}

export class FormRoutingBuilder extends CmsComponentBuilder {
	
	_config: IFormRouting = {
		className: undefined,
		id: undefined,
		formName: undefined,
		loginConfigs: []
	};
	
	public static newBuilder(): FormRoutingBuilder {
		return new FormRoutingBuilder();
	}

	public setFormName(name: string): FormRoutingBuilder {
		this._config.formName = name;
		return this;
	}

	public setFormLoginConfigs(formLoginConfigs: ICmsComponent[]): FormRoutingBuilder {
		this._config.loginConfigs = formLoginConfigs;
		return this;
	}
	
	public build(): IFormRouting {
		return this._config;
	}
}

export class FormRouting2 {
	get formName(): string {
		return this._config.formName;
	}
	
	get formConfigs(): FormConfig[] {
		return this.loginConfigs;
	}

	private StepLogin: IStepConfig =
		{
			step: ESteps.LOGIN,
			completed: false,
			url: 'login-portal',
		};

	private StepForm: IStepConfig =
		{
			step: ESteps.FORM,
			completed: false,
			url: '',
		};

	private StepDankScherm: IStepConfig =
		{
			step: ESteps.DANK_SCHERM,
			completed: false,
			url: 'dank-scherm',
		};

	private steps: IStepConfig[] = [this.StepLogin, this.StepForm, this.StepDankScherm];

	private _config: IFormRouting;
	private loginConfigs: FormConfig[] = [];
	
	constructor(config: IFormRouting, loginConfigs: FormConfig[]) {
		this._config = config;
		this.loginConfigs = loginConfigs;
	}
}
