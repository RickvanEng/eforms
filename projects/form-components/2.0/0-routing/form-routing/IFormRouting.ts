import {ICmsComponent} from "../../ICmsComponents";

export interface IFormRouting extends ICmsComponent {
    formName: string;
    loginConfigs: ICmsComponent[],
}