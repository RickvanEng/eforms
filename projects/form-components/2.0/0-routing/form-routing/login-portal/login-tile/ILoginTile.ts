export interface ILoginTile {
    title: string;
    text: string;
    loginUrl: string;
    buttonText: string;
}
