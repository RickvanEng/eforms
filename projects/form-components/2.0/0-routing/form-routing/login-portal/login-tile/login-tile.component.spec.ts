import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {LoginTileComponent} from './login-tile.component';

describe('LoginNoneComponent', () => {
    let component: LoginTileComponent;
    let fixture: ComponentFixture<LoginTileComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [LoginTileComponent]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(LoginTileComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
