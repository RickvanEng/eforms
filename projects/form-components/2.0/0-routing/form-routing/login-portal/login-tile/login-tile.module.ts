import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {LoginTileComponent} from './login-tile.component';

@NgModule({
    imports: [
        CommonModule
    ],
    declarations: [
        LoginTileComponent
    ],
    exports: [
        LoginTileComponent
    ]
})
export class LoginTileModule {
}
