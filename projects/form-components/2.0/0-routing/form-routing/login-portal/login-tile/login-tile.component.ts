import {Component, Injector, Input, OnInit} from '@angular/core';
import {FormConfig} from '../../../formConfig';
import {Auth2Service} from '../../../../services/auth2.service';
import {ActivatedRoute, Router} from '@angular/router';
import {ILoginTile} from './ILoginTile';

@Component({
    selector: 'app-tile',
    templateUrl: './login-tile.component.html',
    styleUrls: ['./login-tile.component.scss']
})
export class LoginTileComponent implements OnInit {

    @Input() tile: ILoginTile;

    constructor(private injector: Injector) {
    }

    ngOnInit() {
        // let selectedForm: any = localStorage.getItem('selectedForm2');
        // this.injector.get(Auth2Service).loginToken.subscribe(value => {
        //     if (value) {
        //         if ((value.idp === this.formConfig.loginType.toString()) && (selectedForm === this.formConfig.formID)) {
        //             this.injector.get(Router).navigate(['../' + this.formConfig.getIdpLoginUrl()], {relativeTo: this.injector.get(ActivatedRoute)});
        //         }
        //     }
        // });
    }

    public login() {
        // localStorage.setItem('selectedForm2', this.formConfig.formID);
        // this.formConfig.selectForm();

        // if (this.formConfig.loginType !== ELoginTypes2.NONE) {
        // 	let authService: Auth2Service = this.injector.get(Auth2Service);
        // 	authService.loginToken.subscribe(token => {
        // 		if (token) {
        // 			if (token.idp === this.formConfig.loginType.toString()) {
        // 				this.injector.get(Router).navigate(['../' + this.formConfig.getIdpLoginUrl()], {relativeTo: this.injector.get(ActivatedRoute)});
        // 				return;
        // 			}
        // 		}
        // 		this.formConfig.login();
        // 	});
        // } else {
        // 	this.injector.get(Router).navigate(['../' + this.formConfig.getIdpLoginUrl()], {relativeTo: this.injector.get(ActivatedRoute)});
        // }

        this.injector.get(ActivatedRoute).params.subscribe(res => {
            this.injector.get(Router).navigateByUrl(res.routing + '?idp=0');
        });
    }

}
