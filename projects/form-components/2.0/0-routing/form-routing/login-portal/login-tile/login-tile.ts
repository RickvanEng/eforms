import {ILoginTile} from './ILoginTile';

export class LoginTileBuilder {

    _config: ILoginTile = {
        text: '',
        title: '',
        buttonText: '',
        loginUrl: ''
    };

    public static newBuilder(): LoginTileBuilder {
        return new LoginTileBuilder();
    }

    public setText(value: string): LoginTileBuilder {
        this._config.text = value;
        return this;
    }

    public setTitle(value: string): LoginTileBuilder {
        this._config.title = value;
        return this;
    }

    public setButtonText(value: string): LoginTileBuilder {
        this._config.buttonText = value;
        return this;
    }

    public setLoginUrl(value: string): LoginTileBuilder {
        this._config.loginUrl = value;
        return this;
    }

    public build(): ILoginTile {
        return this._config;
    }
}

export class LoginTile {
    // private _injector: Injector;
    //
    // constructor(builder: LoginTileBuilder) {
    //     this._injector = builder.injector;
    //     this._title = builder.title;
    //     this._texts = builder.texts;
    //     this._buttonText = builder.buttonText;
    //     this._alert = builder.alert;
    //     this._buttonTextLoggedIn = builder.buttonTextLoggedIn;
    //     this._img = builder.img;
    //     this._loginHref = builder.loginHref;
    //     this._buttonDisabled = builder.buttonDisabled;
    // }
    //
    // private _title: string;
    //
    // get title(): string {
    //     return this._title;
    // }
    //
    // private _texts: string[];
    //
    // get texts(): string[] {
    //     return this._texts;
    // }
    //
    // private _buttonText: string;
    //
    // get buttonText(): string {
    //     return this._buttonText;
    // }
    //
    // private _buttonTextLoggedIn: string;
    //
    // get buttonTextLoggedIn(): string {
    //     return this._buttonTextLoggedIn;
    // }
    //
    // private _alert: { type: string, message: string };
    //
    // get alert(): { type: string; message: string } {
    //     return this._alert;
    // }
    //
    // private _img: { path: string, width: number, height: number };
    //
    // get img(): { path: string, width: number, height: number } {
    //     return this._img;
    // }
    //
    // private _loginHref: string;
    //
    // get loginHref(): string {
    //     return this._loginHref;
    // }
    //
    // private _buttonDisabled: boolean;
    //
    // get buttonDisabled(): boolean {
    //     return this._buttonDisabled;
    // }
}
