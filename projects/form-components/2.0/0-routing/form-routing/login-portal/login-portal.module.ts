import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {LoginPortalComponent} from './login-portal.component';
import {LoginTileModule} from './login-tile/login-tile.module';

@NgModule({
    imports: [
        CommonModule,
        LoginTileModule
    ],
    declarations: [
        LoginPortalComponent
    ],
    exports: [
        LoginPortalComponent
    ]
})
export class LoginPortalModule {
}
