import {LoginPortalModule} from './login-portal.module';

describe('LoginPortalModule', () => {
    let loginPortalModule: LoginPortalModule;

    beforeEach(() => {
        loginPortalModule = new LoginPortalModule();
    });

    it('should create an instance', () => {
        expect(loginPortalModule).toBeTruthy();
    });
});
