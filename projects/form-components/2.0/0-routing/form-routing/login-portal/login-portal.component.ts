import {Component, Injector, Input, OnInit} from '@angular/core';
import {FormRouting2} from '../formRouting';
import {FormConfig, IFormConfig} from "../../formConfig";

@Component({
    selector: 'app-login-portal',
    templateUrl: './login-portal.component.html',
    styleUrls: ['./login-portal.component.scss']
})
export class LoginPortalComponent implements OnInit {

    @Input() configs: FormConfig[] = [];

    constructor(public inj: Injector) {
    }

    ngOnInit(): void {
        // this.formRoute = VariableFormService.getInstance().formRoutingNew;
        // if (this.formRoute.skipPortal) {
        // 	this.inj.get(Router).navigate([this.formRoute.url + '/' + this.formRoute.forms[0].form.url])
        // }
    }

}
