import {Component, Injector, Input} from '@angular/core';
import {DefaultFormFunctions} from './defaultFormFunctions';

@Component({
	selector: 'app-base',
	templateUrl: './base.component.html',
	styleUrls: ['./base.component.scss'],
})
export class BaseComponent {
	
	public show: boolean;
	public _component: DefaultFormFunctions;
	public children: DefaultFormFunctions[] = [];
	
	@Input() public set baseComponent(component: DefaultFormFunctions) {
		// let compSingle: ComponentSingleton = ComponentSingleton.getInstance();

		this._component = component;
		this._component.show.subscribe(value => {
			this.show = value;
		});

		this._component.children.forEach(child => {
			// this.children.push(compSingle.getComponent(child) as DefaultFormFunctions);
		});
	}
	
	constructor(protected injector: Injector) {
	
	}
	
}
