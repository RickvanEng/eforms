export enum EControlStatus {
	VALID = 'VALID',
	INVALID = 'INVALID',
	PENDING = 'PENDING',
}
