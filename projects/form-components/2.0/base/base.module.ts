import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {BaseComponent} from './base.component';
import {BaseDirective} from './base.directive';

@NgModule({
	imports: [
		CommonModule,
	],
	declarations: [
		BaseComponent,
		BaseDirective
	],
	exports: [
		BaseDirective
	]
})
export class BaseModule {
}
