export interface IPreInitForm {
	activate(): Promise<boolean>;
}
