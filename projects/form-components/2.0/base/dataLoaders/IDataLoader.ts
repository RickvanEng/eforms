import {DefaultFormFunctions} from '../defaultFormFunctions';

export interface IDataLoader {
	loadData(comp: DefaultFormFunctions): void;
}
