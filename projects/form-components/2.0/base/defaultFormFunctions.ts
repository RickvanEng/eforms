import {Injector} from "@angular/core";
import {ShowCondition} from "../showconditions/showCondition";
import {BehaviorSubject} from "rxjs";
import {BasicValidationResult} from "../validators/basic/BasicValidationResult";
import {IFormState} from "../1-form/formStateManager";
import {PreInit} from "./preInit/preInit";
import {ObjValidPair} from "./objValidPair";
import {AsyncValidatorFn, ValidatorFn,} from "@angular/forms";
import {BasicValidatorMap, EBasicValidators,} from "../validators/basic/basicValidatorMap";
import {AsyncValidatorMap} from "../validators/async/asyncValidatorMap";
import {IValidator} from '../validators/IValidator';
import {EFormsAbstractControl, EFormsFormGroup} from './eFormsAbstractControl';
import {IBase} from "./IBase";
import {CmsComponent} from '../cmsComponent';
import {CmsComponentBuilder} from '../cmsComponent.builder';
import {CmsAbstractControl} from '../cmsAbstractControl';

export class DefaultFormFunctionsBuilder extends CmsComponentBuilder {

    _config: IBase = {
        className: undefined,
        id: undefined,
        buildingClass: undefined,
        children: [],
        component: undefined,
        label: '',
        preInits: [],
        sendToBE: false,
        asyncValidators: [],
        basicValidators: [],
        saveToState: false,
        showCondition: {
            all: [],
            single: [],
        },
        showInOverviewSet: false,
    };

    public static newBuilder(): any {
        return new DefaultFormFunctionsBuilder();
    }

    public setLabel(label: string): DefaultFormFunctionsBuilder {
        this._config.label = label;
        return this;
    }

    public setSaveToState(val: boolean): DefaultFormFunctionsBuilder {
        this._config.saveToState = val;
        return this;
    }

    public setPreInit(preInit: PreInit): DefaultFormFunctionsBuilder {
        this._config.preInits.push(preInit);
        return this;
    }

    public setComponent(component: any): DefaultFormFunctionsBuilder {
        this._config.component = component;
        return this;
    }

    public setChildren(children: string[]): DefaultFormFunctionsBuilder {
        this._config.children = children;
        return this;
    }

    public setSendToBE(sendToBE: boolean): DefaultFormFunctionsBuilder {
        this._config.sendToBE = sendToBE;
        return this;
    }

    public setShowInOverviewSet(show: boolean): DefaultFormFunctionsBuilder {
        this._config.showInOverviewSet = show;
        return this;
    }

    public setValidation(config: IValidator): DefaultFormFunctionsBuilder {
        this._config.basicValidators.push(config);
        return this;
    }

    public setAsyncValidation(config: IValidator): DefaultFormFunctionsBuilder {
        this._config.asyncValidators.push(config);
        return this;
    }

    // // TODO FIX show moet invert worden.
    // public setShowConditions(conditions: { all: ShowCondition[], single: ShowCondition[] }, show: boolean = true): any {
    //     let newCondition: any = {
    //         all: conditions.all,
    //         single: conditions.single,
    //         show: show,
    //     };
    //
    //     this._showCondition = newCondition;
    //     return this;
    // }

    public build(): IBase {
        return this._config;
    }
}

export class DefaultFormFunctions extends CmsAbstractControl {
    get validationResult(): ObjValidPair {
        return this._validationResult;
    }

    get errors(): BehaviorSubject<BasicValidationResult[]> {
        return this._errors;
    }

    get trigger(): BehaviorSubject<any> {
        return this._trigger;
    }

    get component(): any {
        return this._config.component;
    }

    get children(): string[] {
        return this._config.children;
    }

    get showInOverviewSet(): boolean {
        return this._showInOverviewSet;
    }

    get sendToBE(): boolean {
        return this._sendToBE;
    }

    get show(): BehaviorSubject<boolean> {
        return this._show;
    }

    // Also sets hide parent on children. So the children also know to hide.
    public setShow(value: boolean): void {
        // this.children.forEach(child => {
        //     child.setParentVisible(value);
        // });
        //
        // // TODO FIX Alle gehide velden moeten disabled zijn.
        // value ? this._abstractControl.enable() : this._abstractControl.disable();
        // this._show.next(value);
    }

    get label(): string {
        return this._config.label;
    }

    get valid(): boolean {
        return this._abstractControl.valid;
    }

    get validators(): IValidator[] {
        return this._validators.concat(this._asyncValidators);
    }

    public setParentVisible(value: boolean): void {
        this._parentIsVisible.next(value);
    }

    private _validationResult: ObjValidPair;

    protected _label: string;
    protected _injector: Injector;
    protected _id: string;
    protected _showInOverviewSet: boolean;
    protected _sendToBE: boolean;
    protected _saveToState: boolean;

    protected _children: DefaultFormFunctions[] = [];
    protected _showCondition: { all: ShowCondition[], single: ShowCondition[], show: boolean };
    protected _show: BehaviorSubject<boolean>;

    // This is used to let other objects now this object has changed.
    protected _trigger: BehaviorSubject<any> = new BehaviorSubject(undefined);
    protected _errors: BehaviorSubject<BasicValidationResult[]> = new BehaviorSubject([]);

    protected _parentIsVisible: BehaviorSubject<boolean> = new BehaviorSubject(true);
    protected _showConditionsMet: BehaviorSubject<boolean> = new BehaviorSubject(true);

    // list with validators that will trigger after each trigger
    protected _validators: IValidator[];
    protected _asyncValidators: IValidator[];

    protected _preInits: PreInit[];
    _config: IBase;
    private _allConditions: BehaviorSubject<{ isValid: boolean }[]> = new BehaviorSubject([]);


    private _component: any;

    constructor(b: IBase) {
        super(b);
        this._show = new BehaviorSubject(true);
    }

    /**
     * Does any checks that needs to be done before the field is used.
     * @param state
     */
    public init(): Promise<any> {
        return new Promise<any>(resolve => {

            // let promises: Promise<boolean>[] = [];
            // this._preInits.forEach(obj => {
            //     promises.push(obj.activate(this));
            // });
            //
            // Promise.all(promises).then(res => {
            //
            //     let childInits: Promise<any>[] = [];
            //     this.children.forEach(child => {
            //         childInits.push(child.init());
            //     });
            //     Promise.all(childInits).then(() => {
            //         resolve(true);
            //     });
            // });
        });
    }

    /**
     * sets up all the showConditions for the current obj and all children
     */
    public setupShowConditions(): void {
        // this._showCondition = this._builder.showCondition;
        //         // this.setShowConditionsOnOptions(this._showCondition.all, this._showCondition.single);
        //         //
        //         // this._allConditions.subscribe(res => {
        //         //     if (this._showCondition.all.length > 0) {
        //         //         this._showConditionsMet.next(!res.find(obj => obj.isValid === false))
        //         //     } else if (this._showCondition.single.length > 0) {
        //         //         this._showConditionsMet.next(!!res.find(obj => obj.isValid === true))
        //         //     }
        //         // });
        //         //
        //         // this._parentIsVisible.subscribe(() => {
        //         //     this.checkForShow();
        //         // });
        //         //
        //         // this._showConditionsMet.subscribe(() => {
        //         //     this.checkForShow();
        //         // });
        //         //
        //         // this._children.forEach(child => {
        //         //     child.setupShowConditions();
        //         // });
    }



    /**
     * Passes the state down to all children. finds the value thats needs to be filled and passes it to: prefillValue().
     * @param state
     */
    public prefillState(state: IFormState): void {
        let stateValue: any = state.components[this.id];
        if (stateValue) {
            this.prefillValue(stateValue);
        }

        this._children.forEach(child => {
            child.prefillState(state);
        });
    }

    /**
     * Prefills a value
     * @param value
     */
    public prefillValue(value: any): void {
        this._abstractControl.setValue(value);
        this.markAsTouched();
    }

    /**
     * Resets the components and all child components.
     */
    public clear(): void {
        this._children.forEach(child => {
            child.clear();
        });
    }

    /**
     * OVERRIDE: gets the value of the component.
     */
    public getValue(): any {
        return '';
    }

    public getPresentationValue(): { label: string, value: string } {
        console.warn('Implement getPresentationValue() for: ', this.id);
        return {label: 'NOT IMPLEMENTED', value: 'NOT IMPLEMENTED'}
    }

    /**
     * OVERRIDE: This function gets called for the values in the PDF.
     */
    public getPdfValue(): any {

    }

    /**
     * Gets the technical value of a component. This will be saved in the DB and the localStorage state
     */
    public getTechnicalValue(): any {
        let result: any = {};
        let allFields: any[] = [];

        // this.children.forEach(res => {
        //     if (res.show.value && res.sendToBE) {
        //         allFields.push(res.getTechnicalValue());
        //     }
        // });

        allFields.forEach(fieldBOdy => {
            if (fieldBOdy) {
                Object.keys(fieldBOdy).forEach(key => {
                    result[key] = fieldBOdy[key];
                })
            }
        });

        return result;
    }

    /**
     * Hides / shows the component
     */
    protected checkForShow(): void {
        this.setShow((this._showConditionsMet.getValue() && this._parentIsVisible.getValue()));
    }



    /**
     * creates an object for each condition. OnChange, the object gets updated and .next() to trigger the check for show.
     * @param showConditionsALL
     * @param showConditionsSINGLE
     */
    private setShowConditionsOnOptions(showConditionsALL: ShowCondition[], showConditionsSINGLE: ShowCondition[]): void {
        // if (showConditionsALL.length > 0) {
        //     showConditionsALL.forEach(condition => {
        //         let conditionResult: { isValid: boolean } = {isValid: false};
        //         this._allConditions.value.push(conditionResult);
        //
        //         if (condition.error) {
        //             condition.field._abstractControl.$notifications().subscribe(res => {
        //                 conditionResult.isValid = !!res[condition.error.id];
        //                 this._allConditions.next(this._allConditions.value)
        //             });
        //         }
        //
        //         if (condition.option) {
        //             condition.field._abstractControl.statusChanges.subscribe(res => {
        //                 if (res !== EControlStatus.PENDING) {
        //                     conditionResult.isValid = condition.field.isOptionChecked(condition.option.id);
        //                     this._allConditions.next(this._allConditions.value)
        //                 }
        //             });
        //         }
        //     });
        // } else {
        //     showConditionsSINGLE.forEach(condition => {
        //         let conditionResult: { isValid: boolean } = {isValid: false};
        //         this._allConditions.value.push(conditionResult);
        //
        //         if (condition.error) {
        //             condition.field._abstractControl.$notifications().subscribe(res => {
        //                 conditionResult.isValid = !!res[condition.error.id];
        //                 this._allConditions.next(this._allConditions.value)
        //             });
        //         }
        //
        //         if (condition.option) {
        //             condition.field._abstractControl.statusChanges.subscribe(res => {
        //                 if (res !== EControlStatus.PENDING) {
        //                     conditionResult.isValid = condition.field.isOptionChecked(condition.option.id);
        //                     this._allConditions.next(this._allConditions.value)
        //                 }
        //             });
        //         }
        //     });
        // }
    }

    /**
     * Replaces a value in the label of the field.
     * @param replaceWith
     * @param searchFor
     */
    public interpolateLabel(searchFor: string, replaceWith: string) {
        if (this._label) {
            return this._label.replace(searchFor, replaceWith);
        }

        return '';
    }
}
