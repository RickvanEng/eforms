import {PreInit} from './preInit';
import {Injector} from '@angular/core';
import {FormService} from '../../1-form/form.service';
import {DefaultFormFunctions} from '../defaultFormFunctions';

export class PreInitGetFormData extends PreInit {
	
	private ids: string[];
	
	constructor(protected injector: Injector, ids: string[]) {
		super(injector);
		this.ids = ids;
	}
	
	activate(obj: DefaultFormFunctions): Promise<boolean> {
		return new Promise<any>(resolve => {
			
			FormService.getInstance().formData.subscribe((val) => {
				let data: any = val;
				// filter through the data
				if (data[this.ids[0]]) {
					this.ids.forEach(id => {
						if (data[id] !== undefined && data[id] !== null) {
							data = data[id];
						} else {
							data = '';
						}
					});
					
					if (data) {
						obj.prefillValue(data);
					} else {
						obj.prefillValue(undefined);
					}
				} else {
					obj.prefillValue(undefined);
				}
			});			

			resolve(true);
		})
	}
}
