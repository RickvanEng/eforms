import {PreInit} from './preInit';
import {Injector} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {DefaultFormFunctions} from '../defaultFormFunctions';
import {environment} from '../../../../../src/environments/environment';
import {Auth2Service} from '../../services/auth2.service';
import {BasicValidationResult} from '../../validators/basic/BasicValidationResult';
import {WarningType} from '../../5-fields/WarningType';

// TODO maak deze generiek, maak ook nieuw type van, een die erros kan thrownen
export class PreInitBbzEerdereAanvraag extends PreInit {
	
	private api: string;
	
	constructor(injector: Injector, api: string) {
		super(injector);
		this.api = api;
	}
	
	private validationResult: BasicValidationResult = new BasicValidationResult(
		'U kunt geen bijstand voor zelfstandigen (bbz) aanvragen in 2021, omdat u geen Tozo 5 heeft ontvangen. U kunt wel bijstand aanvragen via <a href="www.haarlemmermeer.nl/bijstandsuitkering">www.haarlemmermeer.nl/bijstandsuitkering</a>. U kunt dit formulier sluiten.',
		true,
		WarningType.DANGER
	);
	
	activate(obj: DefaultFormFunctions): Promise<boolean> {
		return new Promise<any>(resolve => {
			
			let headers: HttpHeaders = new HttpHeaders({
				'Content-Type': 'application/json',
				'X-Auth-Token': this.injector.get(Auth2Service).loginToken.value.value
			});
			
			this.injector.get(HttpClient).get(environment.baseUrl + this.api,  {headers: headers}).subscribe((res: any) => {
				if (res) {
					// this.validationResult.error = res.error;
					// obj.updateErrors([this.validationResult]);
					resolve(true);
				}
			})
		})
	}
}
