import {DefaultFormFunctions} from '../defaultFormFunctions';
import {Form2} from '../../1-form/Form';
import {Injector} from '@angular/core';

export class PreInit {
	
	constructor(protected injector: Injector) {
	
	}
	
	public updateVariableContent(target: string, replacement: string) {

	}
	
	public activate(obj: DefaultFormFunctions): Promise<boolean> {
		console.warn('Override this method!');
		return new Promise<any>(resolve => {
			setTimeout(() => {
				resolve(true);
			}, 2000);
		})
	}
	
	// TODO remove this, get his own class
	public activateForm(obj: Form2): Promise<boolean> {
		return new Promise<any>(resolve => {
			setTimeout(() => {
				resolve(true);
			}, 2000);
		})
	}

	public getConfig(): any {
		return {
			id: this.constructor.name
		}
	}
}
