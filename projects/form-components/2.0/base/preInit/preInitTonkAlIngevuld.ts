import {PreInit} from './preInit';
import {BasicValidationResult} from '../../validators/basic/BasicValidationResult';
import {DefaultFormFunctions} from '../defaultFormFunctions';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Auth2Service} from '../../services/auth2.service';
import {environment} from '../../../../../src/environments/environment';
import {Injector} from '@angular/core';

export class PreInitTonkAlIngevuld extends PreInit {

    private api: string;

    constructor(injector: Injector, api: string) {
        super(injector);
        this.api = api;
    }

    private validationResult: BasicValidationResult = new BasicValidationResult(
        // FormErrors.getInstance().tonkAlreadyFilled,
        'U of uw partner heeft al een aanvraag voor TONK gedaan. U kunt dit formulier niet nog een keer invullen. U kunt dit formulier sluiten. <br><br>' +
        'Wilt u een verandering doorgeven? Mail dan naar <a href="mailto: tonk@haarlemmermeer.nl">tonk@haarlemmermeer.nl</a>',
    );

    activate(obj: DefaultFormFunctions): Promise<boolean> {
        return new Promise<any>(resolve => {

            let headers: HttpHeaders = new HttpHeaders({
                'Content-Type': 'application/json',
                'X-Auth-Token': this.injector.get(Auth2Service).loginToken.value.value
            });

            this.injector.get(HttpClient).get(environment.baseUrl + this.api,  {headers: headers}).subscribe((res: any) => {
                if (res) {
                    // this.validationResult.error = res.error;
                    // obj.updateErrors([this.validationResult]);
                    resolve(true);
                }
            })
        })
    }
}
