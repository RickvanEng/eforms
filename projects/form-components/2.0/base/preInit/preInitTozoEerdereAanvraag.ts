import {PreInit} from './preInit';
import {Injector} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {DefaultFormFunctions} from '../defaultFormFunctions';
import {environment} from '../../../../../src/environments/environment';
import {Auth2Service} from '../../services/auth2.service';
import {BasicValidationResult} from '../../validators/basic/BasicValidationResult';

// TODO maak deze generiek, maak ook nieuw type van, een die erros kan thrownen
export class PreInitTozoEerdereAanvraag extends PreInit {
	
	private api: string;
	
	constructor(injector: Injector, api: string) {
		super(injector);
		this.api = api;
	}
	
	public updateVariableContent(target: string, replacement: string) {
		this.api = this.api.replace(target, replacement);
	}
	
	private validationResult: BasicValidationResult = new BasicValidationResult(
		// FormErrors.getInstance().tozoAanvraag,
		'',
	);
	
	activate(obj: DefaultFormFunctions): Promise<boolean> {
		return new Promise<any>(resolve => {
			
			let headers: HttpHeaders = new HttpHeaders({
				'Content-Type': 'application/json',
				'X-Auth-Token': this.injector.get(Auth2Service).loginToken.value.value
			});
			
			this.injector.get(HttpClient).get(environment.baseUrl + this.api,  {headers: headers}).subscribe((res: any) => {
				console.log('Tozo block wip');
				obj.abstractControl['TozoBlock'] = true;
				if (res.error) {
					obj.abstractControl['TozoBlock'] = false;
					obj.validate();
				}

				resolve(true);
			})
		})
	}
}
