// export class PreInitMobCategory extends PreInit {
//
// 	private endpoint: string;
// 	private _options: BehaviorSubject<FormFieldOption[]> = new BehaviorSubject([]);
// 	private _parentID: string;
// 	private _fieldToWaitFor: FormFieldWithOptionsFactory;
//
// 	constructor(protected injector: Injector, endpoint: string) {
// 		super(injector);
// 		this.endpoint = endpoint;
// 	}
//
// 	activate(obj: DefaultFormFunctions): Promise<boolean> {
// 		return new Promise<any>(resolve => {
// 			if (this._fieldToWaitFor) {
// 				this._fieldToWaitFor.build().trigger.subscribe(options => {
// 					let selectedOptions: FormFieldOption[] = this._fieldToWaitFor.build().getSelectedOptions();
// 					if (selectedOptions.length > 0) {
// 						this.injector.get(HttpClient).get(environment.baseUrl + this.endpoint + selectedOptions[0].data.id).subscribe((res: any) => {
// 							this._options.next(this.buildOptions(this._parentID, res));
// 						})
// 					}
// 				})
// 			} else if (this._api) {
// 				this.injector.get(HttpClient).get(environment.baseUrl + this.endpoint).subscribe((res: any) => {
// 					this._options.next(this.buildOptions(this._parentID, res));
// 				})
// 			}
// 		})
// 	}
// }
