import {PreInit} from './preInit';
import {Injector} from '@angular/core';
import {BasicValidationResult} from '../../validators/basic/BasicValidationResult';
import {DefaultFormFunctions} from '../defaultFormFunctions';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Auth2Service} from '../../services/auth2.service';
import {environment} from '../../../../../src/environments/environment';

export class PreInitTozo4VerantwoordenAlIngevuld extends PreInit {
	
	private api: string;
	
	constructor(injector: Injector, api: string) {
		super(injector);
		this.api = api;
	}
	
	public updateVariableContent(target: string, replacement: string) {
		this.api = this.api.replace(target, replacement);
	}
	
	private validationResult: BasicValidationResult = new BasicValidationResult(
		// FormErrors.getInstance().tozoFilled,
		'U heeft het formulier voor het verantwoorden van Tozo 4 al ingevuld en verstuurd. U hoeft dit formulier niet nog een keer in te vullen. Wilt u een verandering doorgeven? Doe dat dan via <a target="_top" href="mailto:tozo@haarlemmermeer.nl">tozo@haarlemmermeer.nl</a>.',
	);
	
	activate(obj: DefaultFormFunctions): Promise<boolean> {
		return new Promise<any>(resolve => {
			
			let headers: HttpHeaders = new HttpHeaders({
				'Content-Type': 'application/json',
				'X-Auth-Token': this.injector.get(Auth2Service).loginToken.value.value
			});
			
			this.injector.get(HttpClient).get(environment.baseUrl + this.api,  {headers: headers}).subscribe((res: any) => {
				if (res) {
					// this.validationResult.error = res.error;
					// obj.updateErrors([this.validationResult]);
					resolve(true);
				}
			})
		})
	}
}
