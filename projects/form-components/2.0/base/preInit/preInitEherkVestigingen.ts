import {PreInit} from './preInit';
import {Injector} from '@angular/core';
import {DefaultFormFunctions} from '../defaultFormFunctions';
import {Eherk2Service} from '../../_presets/eherk/eherk2.service';
import {
	FormFieldOption,
	FormFieldOptionFactory
} from '../../5-fields/form-field/multipleChoiceField/options/FieldOption';
import {FormFieldWithOptions} from '../../5-fields/form-field/multipleChoiceField/MultipleChoiceField';
import {EherkVestigingComponent} from '../../5-fields/form-field/multipleChoiceField/options/eherk-vestiging/eherk-vestiging.component';

export class BusinessActivities {
    sbiCodeDescription: string = '';
}

export class TradeNames {
    businessName: string = '';
    shortBusinessName: string = '';
    currentTradeNames: string[] = [];
}

export class Address {
    type: string = '';
    bagId: string = '';
    street: string = '';
    houseNumber: string = '';
    houseNumberAddition: string = '';
    postalCode: string = '';
    city: string = '';
    country: string = '';
}

export class VestigingObject {
    kvkNumber: string = '';
    branchNumber: string = '';
    tradeNames: TradeNames = new TradeNames();
    legalForm: string = '';
    businessActivities: BusinessActivities[] = [new BusinessActivities()];
    addresses: Address = new Address();
}

export class PreInitEherkVestigingen extends PreInit {

    constructor(protected injector: Injector,) {
        super(injector);
    }

    activate(obj: DefaultFormFunctions): Promise<boolean> {

        let field: FormFieldWithOptions = obj as FormFieldWithOptions;

        return new Promise<any>(resolve => {
            this.injector.get(Eherk2Service).tokenData.subscribe(value => {
                if (value) {
                    let vestigingenOptions: FormFieldOption[] = [];
                    this.setVestigingenFromJson(value.identities[0].data.companies).forEach(vestiging => {
                        let newVestiging: FormFieldOption;

                        newVestiging = FormFieldOptionFactory.newFactory()
                            .setId(vestiging.tradeNames.businessName + '-id')
                            .setComponent(EherkVestigingComponent)
                            .setData({
                                title: vestiging.tradeNames.businessName,
                                subtitle: vestiging.tradeNames.shortBusinessName + '<br> KvK nummer: ' + vestiging.kvkNumber,
                                content: vestiging.addresses.street + ' ' + vestiging.addresses.houseNumber + '<br>' + vestiging.addresses.postalCode + ' ' + vestiging.addresses.city,
                                data: vestiging,
                                value: vestiging
                            })
                            .build();

                        vestigingenOptions.push(newVestiging)
                    });
                    field.setNewOptions(vestigingenOptions);
                    resolve(true);
                }
            });
        });
    }

    private setVestigingenFromJson(vestigingenJsonArray: any[]): VestigingObject[] {
        let resultArray: VestigingObject[] = [];

        if (vestigingenJsonArray.length > 1) {
            for (let vestigingJSON of vestigingenJsonArray) {
                if (vestigingJSON.branch || vestigingJSON.mainBranch) {
                    let newVestigingObject = new VestigingObject();
                    newVestigingObject.kvkNumber = vestigingJSON.kvkNumber;
                    newVestigingObject.branchNumber = vestigingJSON.branchNumber;

                    let newTradeNames: TradeNames = new TradeNames();
                    newTradeNames.businessName = vestigingJSON.tradeNames.businessName;
                    newTradeNames.shortBusinessName = vestigingJSON.tradeNames.shortBusinessName;
                    newTradeNames.currentTradeNames = vestigingJSON.tradeNames.currentTradeNames;
                    newVestigingObject.tradeNames = newTradeNames;

                    newVestigingObject.legalForm = vestigingJSON.legalForm;
                    newVestigingObject.businessActivities = vestigingJSON.businessActivities;

                    let newAdres: Address = new Address();

                    let set: any;
                    set = vestigingJSON.addresses.find(set1 => set1.type === 'vestigingsadres');

                    newAdres.type = set.type;
                    newAdres.bagId = set.bagId;
                    newAdres.street = set.street;
                    newAdres.houseNumber = set.houseNumber;
                    newAdres.houseNumberAddition = set.houseNumberAddition;
                    newAdres.postalCode = set.postalCode;
                    newAdres.city = set.city;
                    newAdres.country = set.country;

                    newVestigingObject.addresses = newAdres;

                    resultArray.push(newVestigingObject);
                }
            }
        } else {
            for (let vestigingJSON of vestigingenJsonArray) {
                let newVestigingObject = new VestigingObject();
                newVestigingObject.kvkNumber = vestigingJSON.kvkNumber;
                newVestigingObject.branchNumber = vestigingJSON.branchNumber;

                let newTradeNames: TradeNames = new TradeNames();
                newTradeNames.businessName = vestigingJSON.tradeNames.businessName;
                newTradeNames.shortBusinessName = vestigingJSON.tradeNames.shortBusinessName;
                newTradeNames.currentTradeNames = vestigingJSON.tradeNames.currentTradeNames;
                newVestigingObject.tradeNames = newTradeNames;

                newVestigingObject.legalForm = vestigingJSON.legalForm;
                newVestigingObject.businessActivities = vestigingJSON.businessActivities;

                let newAdres: Address = new Address();

                let set: any;
                set = vestigingJSON.addresses[0];

                newAdres.type = set.type;
                newAdres.bagId = set.bagId;
                newAdres.street = set.street;
                newAdres.houseNumber = set.houseNumber;
                newAdres.houseNumberAddition = set.houseNumberAddition;
                newAdres.postalCode = set.postalCode;
                newAdres.city = set.city;
                newAdres.country = set.country;

                newVestigingObject.addresses = newAdres;

                resultArray.push(newVestigingObject);
            }
        }

        return resultArray;
    }
}
