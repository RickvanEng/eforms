import {Injector} from '@angular/core';
import {Form2} from '../../1-form/Form';
import {PreInit} from './preInit';
import {HttpClient} from '@angular/common/http';
import {FormService} from '../../1-form/form.service';
import {environment} from '../../../../../src/environments/environment';
import {Auth2Service} from '../../services/auth2.service';

// Used to get data and save it FormService
export class PreInitSaveFormData extends PreInit {
	
	private api: string;
	private saveAs: string;
	
	constructor(protected injector: Injector, api: string, saveAs: string) {
		super(injector);
		this.api = api;
		this.saveAs = saveAs;
	}
	
	public updateVariableContent(target: string, replacement: string) {}
	
	activateForm(obj: Form2): Promise<boolean> {
		return new Promise<any>(resolve => {
			this.injector.get(HttpClient).get(environment.baseUrl + this.api + this.injector.get(Auth2Service).loginToken.value.value).subscribe((res: {data: any}) => {
				FormService.getInstance().setFormData(res.data, this.saveAs);
				resolve(true);
			});
		});
	}
}
