import {PreInit} from './preInit';
import {Injector} from '@angular/core';
import {Form2} from '../../1-form/Form';
import {Eherk2Service} from '../../_presets/eherk/eherk2.service';

export class PreInitEherkLogin extends PreInit {

    constructor(injector: Injector) {
        super(injector);
    }

    activateForm(obj: Form2): Promise<boolean> {
        return new Promise<any>(resolve => {
            this.injector.get(Eherk2Service).tokenData.subscribe(value => {
                if (value) {
                    resolve(true);
                }
            });
        })
    }
}
