import {PreInit} from './preInit';
import {Form2} from '../../1-form/Form';
import {Injector} from '@angular/core';
import {Digid2Service} from '../../_presets/digid/digid2.service';

export class PreInitDigiDLogin extends PreInit {

    constructor(injector: Injector) {
        super(injector);
    }

    activateForm(obj: Form2): Promise<boolean> {
        return new Promise<any>(resolve => {
            this.injector.get(Digid2Service).tokenData.subscribe(value => {
                if (value) {
                    resolve(true);
                }
            });
        });
    }
}
