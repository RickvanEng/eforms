import {PreInit} from './preInit';
import {Auth2Service} from '../../services/auth2.service';
import {HttpClient} from '@angular/common/http';
import {
	FormFieldOption,
	FormFieldOptionFactory
} from '../../5-fields/form-field/multipleChoiceField/options/FieldOption';
import {ObjectOptionComponent} from '../../5-fields/form-field/multipleChoiceField/options/object-option/object-option.component';
import {environment} from '../../../../../src/environments/environment';
import {Injector} from '@angular/core';
import * as moment from "moment";
import {DefaultFormFunctions} from '../defaultFormFunctions';
import {FormFieldWithOptions} from '../../5-fields/form-field/multipleChoiceField/MultipleChoiceField';

export class PreInitEerdereVergunningen extends PreInit {
	
	private endpoint: string;
	
	constructor(protected injector: Injector, endpoint: string) {
		super(injector);
		this.endpoint = endpoint;
	}
	
	activate(obj: DefaultFormFunctions): Promise<boolean> {
		return new Promise<any>(resolve => {
			this.injector.get(Auth2Service).loginToken.subscribe(token => {
				if (token) {
					this.injector.get(HttpClient).get(environment.baseUrl + this.endpoint + token.value).subscribe((res: any) => {
						if (res.result) {

							let vergunningenOptions: FormFieldOption[] = [];

							// Build options
							res.data.forEach(option => {
								let newOption: FormFieldOption;
								newOption = FormFieldOptionFactory.newFactory()
									.setId(option.zaaknummer + '-id')
									.setComponent(ObjectOptionComponent)
									.setData({
										title: 'Evenementenvergunning',
										subtitle: option.naam + '<br/>Zaaknummer: ' + option.zaaknummer + '<br/> Van ' +
											moment(option.startDatumEvenement, 'YYYYMMDD').format('DD-MM-YYYY') +
											' tot en met ' +
											moment(option.eindDatumEvenement, 'YYYYMMDD').format('DD-MM-YYYY'),
										content: option.locatieStraatnaam + ' ' + option.locatieNummer + '<br>' + option.locatiePlaats,
										data: option
									})
									.build();

								vergunningenOptions.push(newOption);
							});
							
							let multipleChoiceField: FormFieldWithOptions = obj as FormFieldWithOptions;
							multipleChoiceField.setNewOptions(vergunningenOptions);
							
							resolve(true);
						}
					});
				}
			});
		})
	}
}
