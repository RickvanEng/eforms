import { AbstractControl, FormArray, FormControl, FormGroup } from '@angular/forms';
import { IValidatorResponse } from '../validators/IValidator';
import { BehaviorSubject } from 'rxjs';

export interface IEFormsAbstractControl {
    notifications(): Record<string, IValidatorResponse>;
    addNotification(id: string, data: IValidatorResponse): void;
    deleteNotification(id: string, data: IValidatorResponse): void
}

export abstract class EFormsAbstractControl extends AbstractControl implements IEFormsAbstractControl {
    public $notifications(): BehaviorSubject<Record<string, IValidatorResponse>> {
        return undefined;
    }

    public notifications(): Record<string, IValidatorResponse> {
        return { }
    }

    public addNotification(id: string, data: IValidatorResponse): void {};
    public deleteNotification(id: string): void {};
}

export class EFormsFormGroup extends FormGroup {
    public notifications(): Record<string, IValidatorResponse> {
        return this._notifications.value;
    }

    public $notifications(): BehaviorSubject<Record<string, IValidatorResponse>> {
        return this._notifications;
    }

    private _notifications: BehaviorSubject<Record<string, IValidatorResponse>> = new BehaviorSubject<Record<string, IValidatorResponse>>({});

    public addNotification(id: string, data: IValidatorResponse): void {
        if (!this._notifications.value[id]) {
            this._notifications.value[id] = data
            this._notifications.next(this._notifications.value);
        }
    };

    public deleteNotification(id: string): void {
        if (this._notifications.value[id]) {
            delete this._notifications.value[id];
            this._notifications.next(this._notifications.value);
        }
    };
}

export class EFormsFormArray extends FormArray {
    public notifications(): Record<string, IValidatorResponse> {
        return this._notifications.value;
    }

    public $notifications(): BehaviorSubject<Record<string, IValidatorResponse>> {
        return this._notifications;
    }

    private _notifications: BehaviorSubject<Record<string, IValidatorResponse>> = new BehaviorSubject<Record<string, IValidatorResponse>>({});

    public addNotification(id: string, data: IValidatorResponse): void {
        if (!this._notifications.value[id]) {
            this._notifications.value[id] = data
            this._notifications.next(this._notifications.value);
        }
    };

    public deleteNotification(id: string): void {
        if (this._notifications.value[id]) {
            delete this._notifications.value[id];
            this._notifications.next(this._notifications.value);
        }
    };
}

export class EFormsFormControl extends FormControl {
    public notifications(): Record<string, IValidatorResponse> {
        return this._notifications.value;
    }

    public $notifications(): BehaviorSubject<Record<string, IValidatorResponse>> {
        return this._notifications;
    }

    private _notifications: BehaviorSubject<Record<string, IValidatorResponse>> = new BehaviorSubject<Record<string, IValidatorResponse>>({});

    public addNotification(id: string, data: IValidatorResponse): void {
        if (!this._notifications.value[id]) {
            this._notifications.value[id] = data
            this._notifications.next(this._notifications.value);
        }
    };

    public deleteNotification(id: string): void {
        if (this._notifications) {
            if (this._notifications.value[id]) {
                delete this._notifications.value[id];
                this._notifications.next(this._notifications.value);
            }
        }
    };
}
