import {DefaultFormFunctions} from './defaultFormFunctions';

export class ObjValidPair {
    get obj(): DefaultFormFunctions {
        return this._obj;
    }

    get valid(): boolean {
        return this._valid;
    }

    private readonly _obj: DefaultFormFunctions;
    private readonly _valid: boolean;

    constructor(obj: DefaultFormFunctions, valid: boolean) {
        this._obj = obj;
        this._valid = valid;
    }
}
