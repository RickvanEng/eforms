import {ShowCondition} from "../showconditions/showCondition";
import {PreInit} from "./preInit/preInit";
import {IValidator} from "../validators/IValidator";
import {ICmsComponent} from '../ICmsComponents';
import {ICmsAbstractControl} from '../ICmsAbstractControl';

export interface IBase extends ICmsAbstractControl {
    label: string;
    component: string;
    buildingClass: string;
    showInOverviewSet: boolean;
    sendToBE: boolean;

    saveToState: boolean;
    showCondition: {
        all: ShowCondition[];
        single: ShowCondition[];
    };
    children: string[];
    preInits: PreInit[];
}
