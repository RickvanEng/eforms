import {ComponentFactoryResolver, Directive, Input, ViewContainerRef} from '@angular/core';
import {DefaultFormFunctions} from './defaultFormFunctions';

@Directive({
	selector: '[appBase]',
})
export class BaseDirective {
	
	public component: any;
	
	@Input()
	public set baseComponent(comp: DefaultFormFunctions) {
		this.render(comp);
	};
	
	constructor(
		private resolver: ComponentFactoryResolver,
		private container: ViewContainerRef
	) {
	}
	
	public render(comp: DefaultFormFunctions) {
		const factory = this.resolver.resolveComponentFactory<any>(comp.component);
		this.component = this.container.createComponent(factory);
		this.component.instance.baseComponent = comp;
	}
}
