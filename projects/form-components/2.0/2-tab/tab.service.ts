import {Injectable} from '@angular/core';
import {FormTab2} from './FormTab2';
import {BehaviorSubject} from 'rxjs';

@Injectable({
	providedIn: 'root'
})
export class TabService {
	get tabs(): BehaviorSubject<FormTab2[]> {
		return this._tabs;
	}
	
	public setTabs(value: FormTab2[]) {
		this._tabs.next(value);
	}
	
	private _tabs: BehaviorSubject<FormTab2[]> = new BehaviorSubject([]);
}
