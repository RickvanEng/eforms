import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {TabComponent} from './tab.component';
import {AkkoordVersturenComponent} from '../4-set/akkoord-versturen/akkoord-versturen.component';
import {TabFieldSetModule} from '../3-tab-field-set/tab-field-set.module';
import {TabFieldSetComponent} from '../3-tab-field-set/tab-field-set.component';
import {BaseModule} from '../base/base.module';
import {FieldErrors2Module} from '../5-fields/field-errors2/field-errors2.module';
import {ReactiveFormsModule} from '@angular/forms';
import {FormFieldModule} from '../5-fields/form-field/form-field.module';

@NgModule({
	imports: [
		CommonModule,
		TabFieldSetModule,
		BaseModule,
		FieldErrors2Module,
		ReactiveFormsModule,
		FormFieldModule,
	],
	declarations: [
		TabComponent,
	],
	exports: [
		TabComponent,
	],
	entryComponents: [
		TabFieldSetComponent,
		AkkoordVersturenComponent
	]
})
export class TabModule {
}
