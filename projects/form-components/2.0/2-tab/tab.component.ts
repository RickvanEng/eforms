import {Component, Host, Injector, Input, OnInit} from '@angular/core';
import {FormTab2} from './FormTab2';
import {FormComponent} from '../1-form/form/form.component';
import {DefaultFormFunctions} from '../base/defaultFormFunctions';

@Component({
	selector: 'app-tab',
	templateUrl: './tab.component.html',
	styleUrls: ['./tab.component.scss']
})
export class TabComponent implements OnInit {
	
	@Input() tab: FormTab2;
	public children: DefaultFormFunctions[] = [];
	
	constructor(@Host() public parent: FormComponent) {
		
	}

	ngOnInit(): void {
		// let compSingle: ComponentSingleton = ComponentSingleton.getInstance();
		// this.tab.children.forEach(child => {
		// 	this.children.push(compSingle.getComponent(child) as DefaultFormFunctions);
		// });
	}

	onSubmit(): void {
		// this.parent.form.toNextTab(this.parent.form.activeTab.value)
	}

	public previous(): void {
		// this.parent.form.previousTab();
	}
}
