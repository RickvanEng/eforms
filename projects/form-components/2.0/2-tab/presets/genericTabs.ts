import {FormTabBuilder} from '../FormTab2';
import {Injector} from '@angular/core';
import {GenericTabFieldSets} from '../../3-tab-field-set/presets/genericTabFieldSets';
import {SetComponent} from '../../4-set/set.component';
import {FormSetBuilder} from '../../4-set/FormSet';
import {AkkoordVersturenComponent} from '../../4-set/akkoord-versturen/akkoord-versturen.component';
import {GenericFields} from '../../5-fields/presets/genericFields';

export class GenericTabs {
	
	private static instance: GenericTabs;
	
	public static getInstance(injector: Injector): GenericTabs {
		if (!GenericTabs.instance) {
			GenericTabs.instance = new GenericTabs(injector);
		}
		return GenericTabs.instance;
	}
	
	constructor(private injector: Injector) {
	
	}
	
	private genericFieldSets: GenericTabFieldSets = new GenericTabFieldSets(this.injector);
	private genericFields: GenericFields = new GenericFields(this.injector);
	
	public akkoordVersturen: FormTabBuilder = FormTabBuilder.newBuilder(this.injector)
		.setID('akkoordversturen')
		.setLabel('Akkoord en versturen')
		.setUrl('akkoord-versturen')
		.setNextBtnText('Versturen')
		.setChildren([
			this.genericFieldSets.getNewDefaultTabSet()
				.setID('AkkoordVersturenTabset')
				.setLabel('Akkoord en versturen')
				.setChildren([
					FormSetBuilder.newBuilder(this.injector)
						.setComponent(AkkoordVersturenComponent)
						.setID('overview') as FormSetBuilder
				]),
			this.genericFieldSets.getNewDefaultTabSet()
				.setID('PrivacyTabset')
				.setLabel('Privacy')
				.setChildren([
					FormSetBuilder.newBuilder(this.injector)
						.setID('avgSet')
						.setComponent(SetComponent)
						.setChildren([
							this.genericFields.privacyCheckbox
								.setID('avg')
						])
						.setID('akkoord-versturen') as FormSetBuilder
				])
		])
		.setSendToBE(false)
		.setShowInOverviewSet(false) as FormTabBuilder;
}
