import {DefaultFormFunctions, DefaultFormFunctionsBuilder} from '../base/defaultFormFunctions';
import {IBase} from "../base/IBase";

export interface IFormTab extends IBase {
    url: string;
}

export class FormTabBuilder extends DefaultFormFunctionsBuilder {
    _config: IFormTab = {
        className: undefined,
        asyncValidators: [],
        basicValidators: [],
        buildingClass: "",
        children: [],
        component: "",
        id: "",
        label: "",
        preInits: [],
        saveToState: false,
        sendToBE: false,
        showCondition: {all: [], single: []},
        showInOverviewSet: false,
        url: ''
    };

    public static newBuilder(): FormTabBuilder {
        return new FormTabBuilder();
    }

    public setUrl(url: string): FormTabBuilder {
        this._config.url = url;
        return this;
    }

    build(): IFormTab {
        return this._config;
    }
}

export class FormTab2 extends DefaultFormFunctions {
    get url(): string {
        return this._config.url;
    }

    _config: IFormTab;

    constructor(config: IFormTab) {
        super(config);
    }
}
