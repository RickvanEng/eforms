import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {FooterModule} from '../lib/footer/footer.module';
import {HeaderModule} from '../lib/header/header.module';
import {FontAwesomeModule} from "@fortawesome/angular-fontawesome";
import {LoaderModule} from "../lib/loader/loader.module";

@NgModule({
    declarations: [],
    imports: [
        BrowserModule,
        FormsModule,
        FontAwesomeModule,
        FooterModule,
        HeaderModule,
        LoaderModule,
    ],
    providers: [],
    bootstrap: [],
    exports: [
        BrowserModule,
        FormsModule,
        FooterModule,
        HeaderModule,
        LoaderModule,
    ]
})
export class ComponentsModule {
}
