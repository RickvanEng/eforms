import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ScreenMessageComponent} from './screen-message.component';

describe('ScreenMessageComponent', () => {
  let component: ScreenMessageComponent;
  let fixture: ComponentFixture<ScreenMessageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ScreenMessageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ScreenMessageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
