import {Component, OnInit} from '@angular/core';
import {ScreenMessage, ScreenMessageService} from "../../../../forms/services/screenMessage.service";

@Component({
  selector: 'app-screen-message',
  templateUrl: './screen-message.component.html',
  styleUrls: ['./screen-message.component.scss']
})
export class ScreenMessageComponent implements OnInit {

  public messages: ScreenMessage[] = [];

  constructor(public messageService: ScreenMessageService) { }

  ngOnInit() {
    this.messageService.get().subscribe(value => {
      this.messages = value;
    })
  }

  closeMessage(message: ScreenMessage) {
    this.messageService.remove(message)
  }
}
