import {NgModule} from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {CommonModule} from '@angular/common';
import {BrowserModule} from '@angular/platform-browser';
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';
import {LoaderComponent} from "./loader.component";
import {MatProgressSpinnerModule} from "@angular/material";

@NgModule({
  declarations: [
    LoaderComponent
  ],
  imports: [
    ReactiveFormsModule,
    FormsModule,
    CommonModule,
    BrowserModule,
    FontAwesomeModule,
    MatProgressSpinnerModule
  ],
  providers: [],
  bootstrap: [],
  exports: [LoaderComponent]
})
export class LoaderModule { }
