import {Component, OnInit} from '@angular/core';
import {faFacebookSquare, faTwitterSquare} from '@fortawesome/free-brands-svg-icons';

@Component({
  selector: 'lib-footer-component',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {
  faTwitterSquare = faTwitterSquare;
  faFacebookSquare = faFacebookSquare;

  constructor() { }

  ngOnInit() {
  }

}
