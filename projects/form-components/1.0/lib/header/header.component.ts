import {Component, OnInit} from '@angular/core';
import {TitleService} from 'src/title.service';
import {NavbarService} from 'src/navbar.service';

@Component({
	selector: 'lib-header-component',
	templateUrl: './header.component.html',
	styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
	title: string;
	
	constructor(private titleService: TitleService, public nav: NavbarService) {
	}
	
	ngOnInit() {
		this.titleService.getTitle().subscribe(formTitle => {
			this.title = formTitle
		});
	}
	
	show() {
		this.nav.show();
	}
	
}
