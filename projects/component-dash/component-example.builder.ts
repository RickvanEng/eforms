import {DefaultFormFunctions, DefaultFormFunctionsBuilder} from '../form-components/2.0/base/defaultFormFunctions';


export interface IComponentExample {
    builder: DefaultFormFunctionsBuilder;
    component: DefaultFormFunctions;
    state: any,
    description: string;
    validateButton: boolean;
}

export class ComponentExampleBuilder {

    public get data(): IComponentExample {

        if (!this._data.builder) {
            throw 'Base constructor error. Please set an builder for this example builder!';
        }

        return this._data;
    }

    private _data: IComponentExample;

    constructor(data: IComponentExample) {
        this._data = data;
    }

    public static newBuilder(): ComponentExampleBuilder {
        return new ComponentExampleBuilder(
            {
                builder: undefined,
                component: undefined,
                state: undefined,
                description: '',
                validateButton: false,
            }
        );
    }

    public setBuilder(value: DefaultFormFunctionsBuilder): ComponentExampleBuilder {
        this._data.builder = value;
        return this;
    }

    public setDescription(value: string): ComponentExampleBuilder {
        this._data.description = value;
        return this;
    }

    public setValidateButton(value: boolean): ComponentExampleBuilder {
        this._data.validateButton = value;
        return this;
    }

    public setState(value: any): ComponentExampleBuilder {
        this._data.state = value;
        return this;
    }
}
