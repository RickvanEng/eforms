import {ComponentExampleModule} from './component-example.module';

describe('ComponentExampleModule', () => {
  let componentExampleModule: ComponentExampleModule;

  beforeEach(() => {
    componentExampleModule = new ComponentExampleModule();
  });

  it('should create an instance', () => {
    expect(componentExampleModule).toBeTruthy();
  });
});
