import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ComponentValidateComponent } from './component-validate.component';

describe('ComponentValidateComponent', () => {
  let component: ComponentValidateComponent;
  let fixture: ComponentFixture<ComponentValidateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ComponentValidateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ComponentValidateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
