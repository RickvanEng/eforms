import {Component, Input, OnInit} from '@angular/core';
import {IComponentExample} from '../../component-example.builder';
import {ObjValidPair} from '../../../form-components/2.0/base/objValidPair';

@Component({
    selector: 'app-component-validate',
    templateUrl: './component-validate.component.html',
    styleUrls: ['./component-validate.component.scss']
})
export class ComponentValidateComponent implements OnInit {

    @Input() component: IComponentExample;

    constructor() {
    }

    ngOnInit() {
    }

}
