import {Component, Injector, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {ComponentsExampleService} from '../components-example.service';
import {IComponentExampleConfig} from '../component-example-config.builder';
import {FormGroup} from '@angular/forms';

@Component({
	selector: 'app-component-example',
	templateUrl: './component-example.component.html',
	styleUrls: ['./component-example.component.scss']
})
export class ComponentExampleComponent implements OnInit {
	public testForm: FormGroup;
	public selectedComponent: IComponentExampleConfig;
	
	public showInput: boolean = false;
	public showOutput: boolean = false;
	
	constructor(private route: ActivatedRoute,
				private inj: Injector,
				private componentsService: ComponentsExampleService) {
	}
	
	ngOnInit() {
		this.route.params.subscribe((params: { id: string }) => {
			if (params) {
				let foundObj: IComponentExampleConfig = this.componentsService.allComponentsExamples(this.inj).find(obj => obj.id === params.id);

				let promises: Promise<any>[] = [];
				this.testForm = new FormGroup({});
				
				foundObj.examples.forEach(example => {
					example.component = example.builder.build();
					promises.push(example.component.init());
					
					this.testForm.addControl(example.component.id, example.component.constructFormControl(this.inj));
					
					example.component.setupShowConditions();
				});
				
				Promise.all(promises).then(() => {
					this.selectedComponent = foundObj;
				});
			}
		});
	}
	
	public toggleInput(): void {
		this.showInput = !this.showInput;
	}
	
	public toggleOutput(): void {
		this.showOutput = !this.showOutput;
	}
	
}
