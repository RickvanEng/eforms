import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ComponentExampleComponent} from './component-example.component';
import {RouterModule} from '@angular/router';
import {BaseModule} from '../../form-components/2.0/base/base.module';
import {BasicInputField2Component} from '../../form-components/2.0/5-fields/form-field/basic-input-field2/basic-input-field2.component';
import {DatePicker2Component} from '../../form-components/2.0/5-fields/form-field/date-picker2/date-picker2.component';
import {SmallTextAreaComponent} from '../../form-components/2.0/5-fields/form-field/textArea/small-text-area/small-text-area.component';
import {LargeTextAreaComponent} from '../../form-components/2.0/5-fields/form-field/textArea/large-text-area/large-text-area.component';
import {LabelFieldComponent} from '../../form-components/2.0/5-fields/form-field/label-field/label-field.component';
import {SingleCheckboxComponent} from '../../form-components/2.0/5-fields/form-field/single-checkbox/single-checkbox.component';
import {MultipleChoiceCheckboxComponent} from '../../form-components/2.0/5-fields/form-field/multipleChoiceField/multiple-choice-checkbox/multiple-choice-checkbox.component';
import {MultipleChoiceObjectComponent} from '../../form-components/2.0/5-fields/form-field/multipleChoiceField/multiple-choice-object/multiple-choice-object.component';
import {MultipleChoiceRadioComponent} from '../../form-components/2.0/5-fields/form-field/multipleChoiceField/multiple-choice-radio/multiple-choice-radio.component';
import {FileUploadComponent} from '../../form-components/2.0/5-fields/form-field/file-upload/file-upload.component';
import {ButtonComponent} from '../../form-components/2.0/5-fields/form-field/button/button.component';
import {TimePickerComponent} from '../../form-components/2.0/5-fields/form-field/time-picker/time-picker.component';
import {DropdownComponent} from '../../form-components/2.0/5-fields/form-field/dropdown/dropdown.component';
import {MultipleChoiceImageComponent} from '../../form-components/2.0/5-fields/form-field/multipleChoiceField/multiple-choice-image/multiple-choice-image.component';
import {LeafletComponent} from '../../form-components/2.0/5-fields/form-field/leaflet/leaflet.component';
import {MobStatusComponent} from '../../form-components/2.0/5-fields/form-field/mob-status/mob-status.component';
import {BasicInputFieldActionComponent} from '../../form-components/2.0/5-fields/form-field/basic-input-field-action/basic-input-field-action.component';
import {BasicInputFieldUnitComponent} from '../../form-components/2.0/5-fields/form-field/basic-input-field-units/basic-input-field-units.component';
import {ReactiveGeneratorComponent} from '../../form-components/2.0/4-set/multple-field-generator/reactive-generator/reactive-generator.component';
import {SetComponent} from '../../form-components/2.0/4-set/set.component';
import {ComponentValidateComponent} from './component-validate/component-validate.component';
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';
import {ReactiveFormsModule} from '@angular/forms';
import {FormFieldModule} from '../../form-components/2.0/5-fields/form-field/form-field.module';
import {TabFieldSetComponent} from '../../form-components/2.0/3-tab-field-set/tab-field-set.component';
import {TabComponent} from '../../form-components/2.0/2-tab/tab.component';

@NgModule({
	imports: [
		CommonModule,
		RouterModule.forRoot([
			{path: 'components/:id', component: ComponentExampleComponent},
		]),
		BaseModule,
		FontAwesomeModule,
		ReactiveFormsModule,
		FormFieldModule,
	],
	declarations: [
		ComponentExampleComponent,
		ComponentValidateComponent,
	],
	exports: [
		ComponentExampleComponent
	],
	entryComponents: [
		BasicInputField2Component,
		DatePicker2Component,
		SmallTextAreaComponent,
		LargeTextAreaComponent,
		LabelFieldComponent,
		SingleCheckboxComponent,
		MultipleChoiceCheckboxComponent,
		MultipleChoiceObjectComponent,
		MultipleChoiceRadioComponent,
		FileUploadComponent,
		ButtonComponent,
		TimePickerComponent,
		DropdownComponent,
		
		MultipleChoiceImageComponent,
		LeafletComponent,
		MobStatusComponent,
		BasicInputFieldActionComponent,
		BasicInputFieldUnitComponent,
		
		ReactiveGeneratorComponent,

		SetComponent,
		TabFieldSetComponent,
		TabComponent,
	]
})
export class ComponentExampleModule {
}
