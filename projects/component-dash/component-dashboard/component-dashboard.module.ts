import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ComponentDashboardComponent} from './component-dashboard.component';
import {RouterModule} from '@angular/router';

@NgModule({
	imports: [
		CommonModule,
		RouterModule.forRoot([
			{path: 'components', component: ComponentDashboardComponent},
		]),
	],
	declarations: [
		ComponentDashboardComponent
	],
	exports: [
		ComponentDashboardComponent
	]
})
export class ComponentDashboardModule {
}
