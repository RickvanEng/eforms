import {Component, Injector, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {ComponentsExampleService} from '../components-example.service';
import {IComponentExampleConfig} from '../component-example-config.builder';

@Component({
	selector: 'app-component-dashboard',
	templateUrl: './component-dashboard.component.html',
	styleUrls: ['./component-dashboard.component.scss']
})
export class ComponentDashboardComponent implements OnInit {
	
	public formComponents: IComponentExampleConfig[] = [];
	
	constructor(private router: Router,
				private inj: Injector,
				private activeRoute: ActivatedRoute,
				private componentsService: ComponentsExampleService) {
	}
	
	ngOnInit() {
		this.formComponents = this.componentsService.allComponentsExamples(this.inj);
	}
	
	public selectComponent(obj: IComponentExampleConfig): void {
		this.router.navigate([obj.id], {relativeTo: this.activeRoute})
	}
}
