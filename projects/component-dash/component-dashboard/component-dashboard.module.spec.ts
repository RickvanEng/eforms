import {ComponentDashboardModule} from './component-dashboard.module';

describe('ComponentDashboardModule', () => {
  let componentDashboardModule: ComponentDashboardModule;

  beforeEach(() => {
    componentDashboardModule = new ComponentDashboardModule();
  });

  it('should create an instance', () => {
    expect(componentDashboardModule).toBeTruthy();
  });
});
