import {IComponentExample} from './component-example.builder';

export interface IComponentExampleConfig {
    id: string;
    examples: IComponentExample[];
}

export class ComponentExampleConfigBuilder {

    public get data(): IComponentExampleConfig {
        return this._data;
    }

    private _data: IComponentExampleConfig;

    constructor(data: IComponentExampleConfig) {
        this._data = data;
    }

    public static newBuilder(): ComponentExampleConfigBuilder {
        return new ComponentExampleConfigBuilder(
            {
                id: undefined,
                examples: [],
            }
        );
    }

    public setComponent(value: string): ComponentExampleConfigBuilder {
        this._data.id = value;
        return this;
    }

    public setExamples(value: IComponentExample[]): ComponentExampleConfigBuilder {
        this._data.examples = value;
        return this;
    }

}
