import {Injectable, Injector} from '@angular/core';
import {BasicInputField2Example} from '../form-components/2.0/5-fields/form-field/basic-input-field2/basic-input-field2.example';
import {DefaultFormFunctions, DefaultFormFunctionsBuilder} from '../form-components/2.0/base/defaultFormFunctions';
import {IComponentExampleConfig} from './component-example-config.builder';
import {DatePicker2Example} from '../form-components/2.0/5-fields/form-field/date-picker2/date-picker2.example';
import {TimePickerExample} from '../form-components/2.0/5-fields/form-field/time-picker/time-picker.example';
import {BasicInputFieldUnitsExample} from '../form-components/2.0/5-fields/form-field/basic-input-field-units/basic-input-field-units.example';
import {DropdownExample} from '../form-components/2.0/5-fields/form-field/dropdown/dropdown.example';
import {LeafletExample} from '../form-components/2.0/5-fields/form-field/leaflet/leaflet.example';
import {MultipleChoiceCheckboxExmaple} from '../form-components/2.0/5-fields/form-field/multipleChoiceField/multiple-choice-checkbox/multiple-choice-checkbox.exmaple';
import {SetExample} from '../form-components/2.0/4-set/set.example';
import {MultipleChoiceRadioExample} from '../form-components/2.0/5-fields/form-field/multipleChoiceField/multiple-choice-radio/multiple-choice-radio.example';
import {MultipleFieldGeneratorExample} from '../form-components/2.0/4-set/multple-field-generator/multiple-field-generator.example';
import {CombinedFieldActionExample} from '../form-components/2.0/4-set/combined-field-action/combined-field-action.example';
import {ReactiveGeneratorExample} from '../form-components/2.0/4-set/multple-field-generator/reactive-generator/reactive-generator.example';
import {SumFieldsExample} from '../form-components/2.0/4-set/sum-field-config/sum-fields/sum-fields.example';

export interface IComponentExampleObj {
	componentID: string;
	components: {
		builder: DefaultFormFunctionsBuilder;
		component?: DefaultFormFunctions;

		description: string;
	}[];
}

@Injectable({
	providedIn: 'root'
})
export class ComponentsExampleService {
	public allComponentsExamples(inj: Injector): IComponentExampleConfig[] {
		return [
			new BasicInputField2Example().getField(inj),
			new DatePicker2Example().getField(inj),
			new TimePickerExample().getField(inj),
			new BasicInputFieldUnitsExample().getField(inj),
			new DropdownExample().getField(inj),
			new LeafletExample().getField(inj),
			new MultipleChoiceCheckboxExmaple().getField(inj),
			new MultipleChoiceRadioExample().getField(inj),

			new SetExample().getField(inj),
			new MultipleFieldGeneratorExample().getField(inj),
			new CombinedFieldActionExample().getField(inj),
			
			new ReactiveGeneratorExample().getField(inj),
			new SumFieldsExample().getField(inj),
		]
	}
}
